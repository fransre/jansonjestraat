window.addEventListener("load",
  function () {
    const images_disabled = document.querySelector(".logo").offsetWidth == 0

    function goToArticle(artikel) {
      artikel.preventDefault()
      window.location.href = artikel.target.closest("div.teaser").querySelector("a.teaser-more").href
    }

    async function masonNextTeaser(setOfTeasers, columnHeights, columnWidth, gap, index) {
      const max = setOfTeasers.children.length
      if (index < max) {
        const teaser = setOfTeasers.children[index]
        if (teaser.classList.contains("hidden")) {
          masonNextTeaser(setOfTeasers, columnHeights, columnWidth, gap, index + 1)
        }
        else {
          teaser.addEventListener("click", goToArticle)
          teaser.style.cursor = "pointer"
          const minimumColumnHeight = Math.min(...columnHeights)
          const columnNumber = columnHeights.indexOf(minimumColumnHeight)
          teaser.style.position = "absolute"
          teaser.style.left = (columnNumber * (columnWidth + gap)) + "px"
          teaser.style.top = minimumColumnHeight + "px"
          teaser.style.width = columnWidth + "px"
          teaser.style.display = "block"
          const text = teaser.querySelector(".teaser-body")
          const maxHeight = document.defaultView.getComputedStyle(text, null).getPropertyValue("max-height")
          const fraction = 1 / (1 + Math.exp(text.textContent.length / -300)) // sigmoid
          text.style.maxHeight = (parseFloat(maxHeight) * fraction) + "px"
          const image = teaser.querySelector("img")
          image.onload = () => {
            // set the height of the set of teasers to the height of its tallest column
            columnHeights[columnNumber] += teaser.clientHeight + gap
            const maximumHeight = Math.max(...columnHeights)
            setOfTeasers.style.height = (maximumHeight - gap) + "px"
            masonNextTeaser(setOfTeasers, columnHeights, columnWidth, gap, index + 1)
          }
          image.onerror = () => { image.onload() }
          image.style.height = "auto"
          image.src = image.dataset.src
          if (images_disabled)
            image.onload()
        }
      }
    }

    function shuffleHomeTeasers() {
      const parent = document.querySelector(".teasers-shuffle")
      if (parent) {
        const children = Array.from(parent.querySelectorAll(".teaser"))
        for (var i = children.length; i > 0; i--) {
          const child = children.splice(Math.floor(Math.random() * i), 1)[0]
          parent.appendChild(child)
        }
      }
    }

    function masonAllSetsOfTeasers() {
      document.querySelectorAll(".teasers").forEach(parent => {
        parent.querySelectorAll(".teaser").forEach(teaser => {
          teaser.style.display = "none"
          const image = teaser.querySelector("img")
          if (!image.dataset.src)
            image.dataset.src = image.src
          image.removeAttribute("src")
          image.onload = () => { }
          image.onerror = () => { }
        })
        const attributes = getComputedStyle(parent)
        const minimumColumnWidth = parseFloat(attributes.columnWidth) || parent.clientWidth
        const gap = parseFloat(attributes.gap) || 0
        const totalWidth = parent.clientWidth
        const division = (totalWidth + gap) / (minimumColumnWidth + gap)
        const numberOfColumns = Math.max(1, Math.floor(division))
        const columnHeights = Array(numberOfColumns).fill(0)
        const columnWidth = (totalWidth - (numberOfColumns - 1) * gap) / numberOfColumns
        parent.style.position = "relative"
        masonNextTeaser(parent, columnHeights, columnWidth, gap, 0)
      })
    }

    function remasonOnResize() {
      if (document.querySelectorAll(".teasers").length > 0) {
        var resize_timeout = 0
        window.onresize = () => {
          if (resize_timeout)
            clearTimeout(resize_timeout)
          resize_timeout = setTimeout(masonAllSetsOfTeasers, 1000)
        }
      }
    }

    shuffleHomeTeasers()
    masonAllSetsOfTeasers()
    remasonOnResize()

    function levenshteinDistance(a, b, m) {
      if (Math.abs(a.length - b.length) > m)
        return undefined
      if (b.length < a.length)
        [a, b] = [b, a]
      const matrix = []
      for (let i = 0; i <= b.length; i++) {
        matrix[i] = [i]
        for (let j = 1; j <= a.length; j++)
          matrix[i][j] = i === 0 ? j : Math.min(matrix[i - 1][j] + 1, matrix[i][j - 1] + 1, matrix[i - 1][j - 1] + (a[j - 1] === b[i - 1] ? 0 : 1))
        if (matrix[i][Math.min(i, a.length)] > m)
          return undefined
      }
      return matrix[b.length][a.length]
    }

    function findBestMatchingPages() {
      const currentPageElement = document.querySelector(".current-page")
      if (!currentPageElement)
        return
      const domain = window.location.origin
      const currentPathName = window.location.pathname
      currentPageElement.innerHTML = domain + currentPathName
      const bestMatchesElement = document.querySelector(".best-matches")
      if (bestMatchesElement)
        fetch("/sitemap.txt")
          .then(response => response
            .text()
            .split("\n")
            .map(fullPath => fullPath.replace(/^https?:\/\/[^/]+/, ""))
            .filter(fullPath => fullPath.length > 1)
            .map(pathName => [pathName, levenshteinDistance(currentPathName, pathName, 3)])
            .filter(pair => pair[1] != undefined)
            .sort((a, b) => a[1] - b[1])
            .concat([['/', 0]])
            .reverse()
            .map(pair => `<p><a href="${domain}${pair[0]}">${domain}${pair[0]}</a></p>`)
            .forEach(path => bestMatchesElement.insertAdjacentHTML("afterend", path))
          )
    }

    findBestMatchingPages()

    function initFilters() {
      document.querySelectorAll(".filter-input").forEach(input => {
        input.addEventListener("input", applyFilters)
        input.focus()
      })
      document.querySelectorAll(".filter-select").forEach(select =>
        select.addEventListener("change", applyFilters)
      )
    }

    function hideTeasers(teasers, func) {
      return teasers.filter(teaser => !func.call(undefined, teaser)).forEach(teaser =>
        teaser.classList.add("hidden")
      )
    }

    function applyFilters() {
      const teasers = Array.from(document.querySelectorAll(".teaser"))
      teasers.forEach(teaser => teaser.classList.remove("hidden"))

      const tekst = document.querySelector("#filter-tekst").value
      if (tekst) {
        const delen = tekst.match(/^\/.*\/$/)
          ? [tekst.slice(1, -1)]
          : tekst.match(/[^\s"]+|"([^"]*)"/g).map(deel => deel.replace(/"/g, "").replace(/[.*+?^${}()|[\]\\]/g, "\\$&").replace(/ /g, "\\s+"))
        const regexps = delen.map(deel => new RegExp(deel, "si"))
        hideTeasers(teasers, teaser => {
          const content = teaser.querySelector(".teaser-body").textContent
          return regexps.every(regexp => content.match(regexp))
        })
      }

      const decennium = document.querySelector("#filter-decennium").value
      if (decennium)
        hideTeasers(teasers, teaser => teaser.dataset.decennium === decennium)

      const onderwerp = document.querySelector("#filter-onderwerp").value
      if (onderwerp)
        hideTeasers(teasers, teaser => {
          const onderwerpen = teaser.dataset.onderwerpen
          return onderwerpen !== "null" && JSON.parse(onderwerpen.replace(/'/g, '"')).includes(onderwerp)
        })

      const onderwerpPlus = document.querySelector("#filter-onderwerp-plus").value
      if (onderwerpPlus)
        hideTeasers(teasers, teaser => {
          const onderwerpen = teaser.dataset.onderwerpen
          return onderwerpen !== "null" && JSON.parse(onderwerpen.replace(/'/g, '"')).includes(onderwerpPlus)
        })

      const onderwerpMin = document.querySelector("#filter-onderwerp-min").value
      if (onderwerpMin)
        hideTeasers(teasers, teaser => {
          const onderwerpen = teaser.dataset.onderwerpen
          return onderwerpen !== "null" && !JSON.parse(onderwerpen.replace(/'/g, '"')).includes(onderwerpMin)
        })

      const huisnummer = document.querySelector("#filter-huisnummer").value
      if (huisnummer === "geen-huisnummer")
        hideTeasers(teasers, teaser => {
          const huisnummers = teaser.dataset.huisnummers
          return huisnummers === "null"
        })
      else if (huisnummer)
        hideTeasers(teasers, teaser => {
          const huisnummers = teaser.dataset.huisnummers
          return huisnummers !== "null" && JSON.parse(huisnummers).includes(parseInt(huisnummer))
        })

      const achternaam = document.querySelector("#filter-achternaam").value
      if (achternaam === "geen-achternaam")
        hideTeasers(teasers, teaser => {
          const achternamen = teaser.dataset.achternamen
          return achternamen === "null"
        })
      else if (achternaam)
        hideTeasers(teasers, teaser => {
          const achternamen = teaser.dataset.achternamen
          return achternamen !== "null" && JSON.parse(achternamen.replace(/'/g, '"')).includes(achternaam)
        })

      const aantal = teasers.filter(teaser => !teaser.classList.contains("hidden")).length
      document.querySelector(".filter-count").innerHTML = aantal
      document.querySelector(".filter-singular").style.display = aantal == 1 ? "inline" : "none"
      document.querySelector(".filter-plural").style.display = aantal != 1 ? "inline" : "none"

      masonAllSetsOfTeasers()
    }

    initFilters()
  }
)
