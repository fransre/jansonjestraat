---
toegevoegd: 2024-08-14
gepubliceerd: 1908-06-01
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994822:mpeg21:a0212"
onderwerpen: [bedrijfsruimte, fiets]
huisnummers: [40]
kop: "Werkplaats enzovoort"
---
# Werkplaats enz*ovoort*

Direct te huur aan de
**Jan Sonjéstraat** 40 een
ruime Werkplaats, geheel
compleet voor Herstelplaats
enz*ovoort* v*oor* Fietsen
ook geschikt voor
gewone Smederij of voor
alle andere doeleinden.
Het is te huur met Woning
of met Magazijn
en Kantoor. Adr*es* Heemraadssingel 126,
hoek Middellandstraat
