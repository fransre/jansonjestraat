---
toegevoegd: 2024-08-08
gepubliceerd: 1927-06-16
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010211461:mpeg21:a0054"
onderwerpen: [huisraad, tekoop]
huisnummers: [24]
achternamen: [valster]
kop: "Te koop"
---
# Te koop

electrische lamp met zijden
kap en 3 lichten.
Prijs 8.—. Valster, **Jan Sonjéstraat** 24a.
