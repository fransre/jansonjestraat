---
toegevoegd: 2024-07-29
gepubliceerd: 1940-08-24
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002565:mpeg21:a0049"
onderwerpen: [huisraad, kleding, tekoop]
huisnummers: [20]
kop: "Aangeboden"
---
# Aangeboden

bed, divan, d*ames*-, h*eren*- en
kinderkleding, tafel witte
kruideniersjassen,
ijskast, hangklok, winterjas.
Gouden dameshorloge.
**Jan Sonjéstraat** 20B.
