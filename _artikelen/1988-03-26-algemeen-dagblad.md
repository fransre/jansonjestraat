---
toegevoegd: 2024-07-21
gepubliceerd: 1988-03-26
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003283024:mpeg21:a00021"
onderwerpen: [bellebom]
kop: "„Bellebom”"
---
# „Bellebom”

„Als de demontering
van de ‘Bellebom’ niet direct
slaagt, injecteren de
manschappen van de EOD
de bom met stollingsvloeistof
waarna het risico
nihil is. Waarom kan
dat niet meteen gedaan
worden zonder al die poespas
van evacueren en binnenblijven?
Het lijkt mij
ook heel wat goedkoper.
Het is nu wel duidelijk
dat de gemeente Rotterdam,
al vanaf het begin
bezig met de evacuatie
van duizenden mensen, dit
wel een leuke oefening
vindt, die het wel leuk
doet in de rest van Nederland.
Alleen jammer dat
de bewoners van de **Jan Sonjéstraat**,
die nu al vanaf
oktober hun woongenot
ernstig bedorven zien, met
een bosje bloemen worden
af gescheept.”

C.F. van 't Hof
