---
toegevoegd: 2024-07-22
gepubliceerd: 1970-02-26
decennium: 1970-1979
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259071022:mpeg21:a00069"
onderwerpen: [misdrijf]
kop: "Toeschouwer bij vechtpartij neergestoken"
---
# Toeschouwer bij vechtpartij neergestoken

(Van een onzer verslaggevers)

Rotterdam, donderdag *26 februari 1970*. —
Een onbekende man heeft in de
afgelopen nacht op de 1ste Middellandstraat
de 45-jarige fabrieksarbeider
G. R. met een mes
in de buik gestoken.

De man stond om ongeveer één uur
ter hoogte van de **Jan Sonjéstraat** met
vrienden naar een ruzie te kijken
tussen een man en een vrouw.
Plotseling stoof de man op het groepje
af en stak de fabrieksarbeider in de
buik. Zijn twee vrienden waren weggerend.
Een taxichauffeur heeft de
gewonde man daarna naar het academische
ziekenhuis Dijkzigt gebracht,
waar het slachtoffer werd geopeerd.

Van de dader en de vrouw met wie
hij ruzie maakte, ontbreekt ieder
spoor. Hij was ongeveer 1,70 meter
lang, had donker haar en was een zuidelijk
type. Hij droeg een donkere
overjas. De vrouw was gekleed in een
zwart met wit gevlekte bontjas en had
lang donker haar.

De recherche van het politiebureau
Marconiplein verzoekt eventuele getuigen
contact met het bureau op te
nemen via telefoon 010-143144 (toestel
5154).
