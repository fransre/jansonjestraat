---
toegevoegd: 2021-04-30
gepubliceerd: 1959-04-20
decennium: 1950-1959
bron: Rotterdamsch nieuwsblad
externelink: "https://schiedam.courant.nu/issue/RD/1959-04-20/edition/0/page/2"
onderwerpen: [cafe]
huisnummers: [15]
achternamen: [rueter]
kop: "Café „Intiem”"
---
1949 21 april 1959

dinsdag 21 april 1959 viert

# Café „Intiem”

zijn 2e lustrum

Uw bezoek op deze dag zal door mij ten zeerste worden
gewaardeerd.

Mevrouw W.C. Rüter.

**Jan Sonjéstraat** 15, Rotterdam.
