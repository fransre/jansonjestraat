---
toegevoegd: 2024-03-07
gepubliceerd: 1928-04-04
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010324971:mpeg21:a0055"
onderwerpen: [huisraad, tekoop]
huisnummers: [11]
kop: "Te koop"
---
Tegen elk aannemel*ijk* bod

# te koop

'n vouwwagentje, zandbak
met deksel, gaslamp
en mahoniehouten ronde
tafel. Adres **Jan Sonjéstraat** 11B.
