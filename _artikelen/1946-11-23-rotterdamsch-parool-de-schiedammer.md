---
toegevoegd: 2023-01-21
gepubliceerd: 1946-11-23
decennium: 1940-1949
bron: Rotterdamsch Parool / De Schiedammer
externelink: "https://schiedam.courant.nu/issue/SP/1946-11-23/edition/0/page/4"
onderwerpen: [reclame, auto]
huisnummers: [4]
achternamen: [take]
kop: "Auto-Rijschool H.P. Take,"
---
# Auto-Rijschool H.P. Take,

gedipl*omeerd* KNAC instructeur,
**Jan Sonjéstraat** 4 (nabij Middellandstraat).
Tel*efoon* 36777.
