---
toegevoegd: 2021-07-21
gepubliceerd: 1982-07-15
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010961123:mpeg21:a0239"
onderwerpen: [familiebericht]
achternamen: [shahid]
kop: "Na 2 dagen weer terecht"
---
# Na 2 dagen weer terecht

Verdriet en angst hebben
in huize Shahid, aan
de Rotterdamse **Jan Sonjéstraat**,
weer plaats gemaakt
voor vreugde en
blijdschap. Na twee dagen
te zijn vermist keerde
de 10-jarige verloren
zoon Waseem gisteren *14 juli 1982*
terug van een onaangekondigd
logeerpartijtje
bij ouders van zijn
vriendje. De jongen had maandagochtend *12 juli 1982*
de ouderlijke
woning verlaten en
was sindsdien spoorloos.
