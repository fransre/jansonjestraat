---
toegevoegd: 2024-08-06
gepubliceerd: 1929-01-21
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164642023:mpeg21:a00151"
onderwerpen: [familiebericht]
huisnummers: [7]
achternamen: [van-loo]
kop: "Sophia van Loo, geboren Rijkhart,"
---
Heden overleed, zacht en kalm,
mijn geliefde Vrouw, onze zorgzame
Moeder, Behuwd-, Groot- en
Overgrootmoeder, Mevrouw

# Sophia van Loo, geb*oren* Rijkhart,

in den ouderdom van bijna 84 jaar.

Rotterdam,

J.B.A. van Loo.

C.H.F. Schortemeyer-van Loo.

J.H.C. Schortemeyer.

Voorburg,

M.G. van Loo.

P.R. van Loo-Rubinga.

Amsterdam,

B.T. van Loo.

L. van Loo-de Recht.

Rotterdam,

H.M. van Loo.

A. van Loo-de Graaff.

Hilversum,

J.B.A. van Loo.

M.J. van Loo-de Graaff.

Rotterdam,

J.S. Korevaar-van Loo.

W.M. Korevaar.

G. Visser-van Loo.

H. Visser.

Klein- en Achterkleinkinderen.

Rotterdam, 20 Januari 1929.

**Jan Sonjéstraat** 7b.

De teraardebestelling zal plaats
hebben Woensdag 23 Januari *1929* op
de Alg*emene* Begraafplaats te Crooswijk.
Vertrek van het sterfhuis te 1 u*ur*

42
