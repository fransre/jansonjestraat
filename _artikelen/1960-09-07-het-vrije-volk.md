---
toegevoegd: 2024-07-24
gepubliceerd: 1960-09-07
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951146:mpeg21:a0029"
onderwerpen: [kleding, tekoop]
huisnummers: [24]
achternamen: [van-der-graaf]
kop: "bruidsjapon,"
---
Te koop moderne korte witte

# bruidsjapon,

maat 40-42,
prijs ƒ25,—. Mevr*ouw* v*an* d*er* Graaf,
**Jan Sonjéstraat** 24, R*otter*dam.
