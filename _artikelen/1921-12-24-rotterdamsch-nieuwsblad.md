---
toegevoegd: 2024-08-12
gepubliceerd: 1921-12-24
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493931:mpeg21:a0035"
onderwerpen: [woonruimte, tekoop]
huisnummers: [17]
achternamen: [de-bruyn]
kop: "woonschip"
---
Te koop een groot

# woonschip

voor elk aannemelijk
bod. T. de Bruyn,
**Jan Sonjéstraat** 17b,
Pakhuis.
