---
toegevoegd: 2024-08-13
gepubliceerd: 1919-09-22
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494303:mpeg21:a0042"
onderwerpen: [bedrijfsinformatie]
huisnummers: [38]
achternamen: [oprel]
kop: "Paul J. Oprel"
---
Verhuisd:

# Paul J. Oprel

Bouwkundige en timmerman

Rotterdam

naar

Kantoor en Werkplaats 38 **Jan Sonjéstraat**

Woonhuis 25 Schermlaan

Telefoonnummer blijft 261

Ontwerpen — Verbouwen

Repareeren — Taxeeren — Restaureeren

62900 60
