---
toegevoegd: 2023-01-08
gepubliceerd: 2001-05-12
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2001-05-12-de-middellander-2-origineel.jpg"
onderwerpen: [geraniumdag]
kop: "Geraniumdag Jan Sonjé"
---
# Geraniumdag Jan Sonjé

Op zaterdag 12 mei *2001* organiseerde de
straatgroep Jan Sonjé weer hun jaarlijkse
geraniumdag.

Ieder jaar wordt de straat letterlijk in de
bloemetjes gezet. De straatgroep verzorgde
een stand met geraniums en bewoners
konden aan de slag met het ophangen en
planten. Ieder jaar weer is het ook een
gezellige dag in het teken van de lente,
van ontmoeting en met een overheersend
gevoel dat de **Jan Sonjéstraat** een bijzondere
straat is. En dat is-ie!
