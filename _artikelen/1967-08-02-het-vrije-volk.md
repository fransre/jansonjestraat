---
toegevoegd: 2021-04-29
gepubliceerd: 1967-08-02
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956378:mpeg21:a0184"
onderwerpen: [straat]
kop: "Straten worden weer op peil gebracht"
---
# Straten worden weer op peil gebracht

(Van een onzer verslaggevers)

De Robert Fruinstraat, de Van der
Poelstraat, de Hendrick Sorchstraat,
de Joost van Geelstraat, de Jan Porcellistraat,
de **Jan Sonjéstraat**,
de Bellevoysstraat, de Jan van Vuchtstraat
en de Schermlaan in Rotterdam
zullen weer op het voorgeschreven
peil worden gebracht. Ze zijn
thans ernstig verzakt.

Gemeentewerken wil nog dit jaar
met het werk beginnen. Het zal ongeveer
een half jaar in beslag nemen.
De dienst heeft de bewoners
schriftelijk van het komende ongerief
op de hoogte gesteld.

Begonnen wordt met het ophalen
of vernieuwen van leidingen en kabels.
Daar elke leiding apart moet
worden behandeld, zal dit werk veel
tijd vergen.

Nadat ook een nieuwe riolering is
aangelegd, worden de trottoirs weer
op hoogte gebracht. Ten slotte volgt
dan de rijstraat.

Indien een woning voor een belangrijke
gebeurtenis (bij voorbeeld
een huwelijk of een sterfgeval) goed
bereikbaar moet zijn, zal Gemeentewerken
een tijdelijke voorziening
treffen.

De dienst heeft de bewoners geschreven,
dat ze daarvoor contact
moeten opnemen met de heer J.J. van Veen
in de directiekeet ter plaatse.
