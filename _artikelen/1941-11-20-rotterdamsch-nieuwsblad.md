---
toegevoegd: 2024-03-07
gepubliceerd: 1941-11-20
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002825:mpeg21:a0027"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Aangeboden:"
---
# Aangeboden:

ongemeub*ileerde*
voorkamer, zolderbergplaats,
ƒ3 of zolderkamer, schoorsteen, str*omend*
water, kookgas, ƒ2. **Jan Sonjéstraat** 20B,
vrij
bovenhuis.
