---
toegevoegd: 2021-04-29
gepubliceerd: 1987-12-29
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962589:mpeg21:a0165"
onderwerpen: [bellebom]
huisnummers: [14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34]
achternamen: [van-brenkelen]
kop: "Maanden in 't donker voor Bellebom"
---
# Maanden in 't donker voor Bellebom

Rotterdam — Bewoners
in 21 panden in de Bellevoysstraat
en **Jan Sonjéstraat** in
Rotterdam willen een tegemoetkoming
van het GEB
voor het blinderen van de
achterkant van hun woningen.
De houten planken dienen
als veiligheidsmaatregel
maar nemen ook drie maanden
lang het daglicht weg.
Het GEB heeft volgens de bewoners
nog niet gereageerd
op hun verzoek om een bijdrage
in de extra energiekosten.

Vanaf 13 januari *1988* moeten alle
woningen in een straal van
25 meter rond de plek waar
de bom ligt zijn afgeschermd.
Op die dag wordt een damwand
geslagen rond het projectiel
dat nog diep in de
grond zit. Tijdens het heien
en het afwateren van de
grond is er een geringe kans
dat de vliegtuigbom uit 1944
per ongeluk explodeert.

De feitelijke ruiming staat
gepland op zondag 27 maart *1988*.
Dan zullen duizenden mensen
in de wijk Middelland hun
huizen uit moeten. Tot die tijd
blijven de planken zitten aan
de achterzijde van de woningen
**Jan Sonjéstraat** 14 tot en
met 34 en de Bellevoysstraat
33, het pand waar de bom
schuin achter ligt.

## Tweede bom

De EOD heeft onlangs een
tweede ‘hoop staal’ ontdekt,
ongeveer twintig meter naast
de eerste bom. Ook rond dit
vermoedelijk tweede projectiel
wordt een damwand geslagen
op 27 januari *1988*. Op 13 en
27 januari *1988* moeten de bewoners
in de 21 panden dicht bij
de bom hun huis uit. Er zijn
genoeg opvangplaatsen voor
deze mensen maar de meesten
zullen een dag doorbrengen
bij vrienden en familieleden.

De heer en mevrouw Van Brenkelen
in de **Jan Sonjéstraat**
gaan naar hun twee
kinderen in Blijdorp. „Bij de
een gaan we op de koffie, bij
de ander op de thee. Dan hebben
we ze meteen alletwee
weer gehad,” zegt mevrouw
Van Brenkelen. Zij maken
zich zorgen over hun huisraad
als zij weg zijn. „Er
komt bewaking maar ik ben
er niet echt gerust op,” zegt
de heer Van Brenkelen.

*Onderschrift bij de foto*
De heer Van Brenkelen: „Er komt bewaking maar ik ben er niet echt gerust
op.” (Foto Rob Cornelder)
