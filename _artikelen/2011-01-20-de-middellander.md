---
toegevoegd: 2023-01-08
gepubliceerd: 2011-01-20
decennium: 2010-2019
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2011-01-20-de-middellander-origineel.jpg"
onderwerpen: [bewoner]
achternamen: [lutgerink, van-der-ploeg]
kop: "Zilveren opsteker voor bewoner van Middelland"
---
# Zilveren opsteker voor bewoner van Middelland

Op donderdag 20 januari *2011* ontving wijkbewoner Aad Lutgerink uit
handen van Deelraadbestuurder Carlos Conçalves de zilveren opsteker
van de Deelgemeente Delfshaven. De opstekers worden ieder
jaar uitgereikt aan mensen die zich verdienstelijk hebben gemaakt
in de deelgemeente. D*e* h*ee*r Conçalves roemde Aads tomeloze
inzet voor zijn straat, de **Jan Sonjéstraat**, en de buurt.
Aad is al jaren actief en een van de drijvende krachten in de straat.

*Onderschrift bij de foto*
Carlos Conçalves (midden) geflankeerd door de ontvangers van de
opstekers met rechts Aad Lutgerink.

foto: jan van der ploeg
