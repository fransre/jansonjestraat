---
toegevoegd: 2024-08-04
gepubliceerd: 1933-02-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164663061:mpeg21:a00074"
onderwerpen: [verloren]
huisnummers: [26]
achternamen: [mager]
kop: "Verloren:"
---
# Verloren:

verleden week zilveren
Sigarettenkoker, monogram
H.M. Tegen fl*inke*
bel*oning* terug **Jan Sonjéstraat** 26a.
Mager.
