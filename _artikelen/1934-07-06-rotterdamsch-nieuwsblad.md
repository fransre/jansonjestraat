---
toegevoegd: 2024-08-04
gepubliceerd: 1934-07-06
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164672006:mpeg21:a00046"
onderwerpen: [bedrijfsruimte, dans]
huisnummers: [38]
kop: "Zaal te huur"
---
# Zaal te huur

met Parketvloer, **Jan Sonjéstraat** 38,
zeer geschikt voor Danszaal,
Vergaderlokaal, Showroom,
Bergplaats van
Meubelen etc*etera*. Billijke
condities. Bevr*agen* Telef*oon*
N*ummer* 56204.
