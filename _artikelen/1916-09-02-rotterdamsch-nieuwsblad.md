---
toegevoegd: 2024-08-13
gepubliceerd: 1916-09-02
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296479:mpeg21:a0052"
onderwerpen: [woonruimte, tehuur]
huisnummers: [29]
kop: "Aangeboden:"
---
# Aangeboden:

voor net Persoon Kost
en Inwoning met gebr*uik*
van ruime Slaapkamer
met twee Hangkasten, op
vrij Bovenhuis. Adres:
**Jan Sonjéstraat** 29a.
