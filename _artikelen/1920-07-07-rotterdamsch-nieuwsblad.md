---
toegevoegd: 2024-08-13
gepubliceerd: 1920-07-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493991:mpeg21:a0066"
onderwerpen: [huispersoneel]
huisnummers: [31]
achternamen: [moll]
kop: "Waschvrouw"
---
# Waschvrouw

gevraagd, van 7½ tot
3½ loon, 2.25 per dag.
A.J. Moll, **Jan Sonjéstraat** 31b.
