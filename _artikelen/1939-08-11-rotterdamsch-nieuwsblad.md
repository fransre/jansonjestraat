---
toegevoegd: 2024-07-30
gepubliceerd: 1939-08-11
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164702036:mpeg21:a00100"
onderwerpen: [bedrijfsruimte]
huisnummers: [20]
kop: "Te huur:"
---
# Te huur:

pracht Benedenhuis
met Kelder, Sousterrain
zeer geschikt voor
klein Grossier of
Werkbaas, 32.50 p*er* m*aand*
**Jan Sonjéstraat** N*ummer* 20.
Te bezichtigen Zaterdagmiddag *12 augustus 1939*
van 2-4
uur. Tel*efoon* 43442.
