---
toegevoegd: 2021-05-03
gepubliceerd: 1917-10-18
decennium: 1910-1919
bron: Middelburgsche Courant
externelink: "https://krantenbankzeeland.nl/issue/mco/1917-10-18/edition/0/page/1"
onderwerpen: [diefstal]
kop: "Rijke buit."
---
# Rijke buit.

Te Rotterdam is een sleeperswagen
aangehouden, waarop vervoerd werden
15 balen, vaten en kisten stijfsel, 7 balen
tabak, 22 kisten vermicelli, 2 kisten
en 5 pakken lucifers. Deze waren werden
vervoerd naar Made en Drimmelen
en Zevenbergen in Noord-Brabant.

Deze aanhouding gaf aanleiding tot
een onderzoek in een pakhuis aan de
**Jan Sonjéstraat**, waar nog werden aangetroffen
3 kisten slaolie, 25 pak condorzeep,
20 dozijn stukken reukzeep,
10.000 stukken zeep 20 doozen kwatta,
alles toebehoorende (met den op den
sleeperswagen vervoerden voorraad) aan
een winkelier te Zevenbergen. Verder
vond men in hetzelfde pakhuis nog
150 pakken condorzeep, 4½ baal en
310 pakken karnemelkzeep, 2 kisten
zeep, 5 kisten thee, 21 pakken en 3
kisten vet, 50 kisten gecondenseerde
melk, 60 pakken zeeppoeder, alles eigendom
van een bewoner van de Zomerhofstraat.

Naar men mededeelde wordt door
twee schatters de waarde van de in bezit
genomen goederen getaxeerd, waarna
den houders der goederen een bon wordt
afgegeven ter waarde van het gemiddelde
dezer geschatte prijzen. Zonder
verwijl worden dan de goederen ter beschikking
van de bevolking of bedrijven
gesteld. Met een deel der in bezit genomen
voorraden heeft dit reeds plaats
gehad. Een en ander is geregeld in de
onteigeningswet. De schatters, van wie
hier sprake is, worden benoemd door
den Minister of den burgemeester.

(Hand.)
