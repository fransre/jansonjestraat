---
toegevoegd: 2024-08-08
gepubliceerd: 1927-01-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010513883:mpeg21:a0029"
onderwerpen: [woonruimte, tehuur]
huisnummers: [30]
kop: "zolderkamer"
---
Mooie gem*eubileerde*

# zolderkamer

op vrij Bovenhuis,
straatzijde, 2 Personen
of alleen. El*ectrisch* l*icht*, Kasten,
Waterleiding, b*eneden*huis,* b*oven*h*uis*
**Jan Sonjéstraat** N*ummer* 30a.
