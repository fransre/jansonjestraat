---
toegevoegd: 2024-08-14
gepubliceerd: 1910-01-25
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197795:mpeg21:a0017"
onderwerpen: [huispersoneel]
huisnummers: [37]
kop: "net Meisje, Protestants Gereformeerd,"
---
Gevraagd in kl*ein* gezin een

# net meisie, P*rotestantse* G*ezindte*,

voor halve dagen, Vrijdag den
geheelen dag.

Adres: **Jan Sonjéstraat** N*ummer* 37b,
tusschen 8-9 uur
's avonds.

10787 8
