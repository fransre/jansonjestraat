---
toegevoegd: 2021-04-29
gepubliceerd: 1904-02-22
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202178:mpeg21:a0043"
onderwerpen: [bedrijfsruimte]
huisnummers: [40]
kop: "Heet water enzovoort"
---
# Heet water enz*ovoort*

**Jan Sonjéstraat** 40, aan
de Schermlaan, te huur
een groote Waterstokerij
met Winkel apart,
groote ruimte, voor standplaats
van Waschvrouwen,
voor berging van
kolen enz*ovoort*, bijzondere gelegenheid
voor verhuring
van Wagens en groote
Woning.
