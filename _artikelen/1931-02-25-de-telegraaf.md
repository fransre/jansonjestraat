---
toegevoegd: 2024-08-06
gepubliceerd: 1931-02-25
decennium: 1930-1939
bron: De Telegraaf
externelink: "https://resolver.kb.nl/resolve?urn=ddd:110570568:mpeg21:a0219"
onderwerpen: [reclame, tekoop]
huisnummers: [20]
kop: "Uw eigen Vulpenhouder bijna voor niets."
---
# Uw eigen Vulpenhouder bijna voor niets.

Direct van fabriek aan verbruiker.

Leest deze annonce aandachtig
door, het
is in uw belang.

Voor den modernen mensch is de Vulpenhouder
van lieverlede een onmisbaar bezit
geworden, zoowel in zaken als in het particuliere
leven. Slechts stuitte men meeatal
op de hooge aanschaffingskosten. The „Universal”
Fabrieken brengt nu een luxe zelfvuller
in den handel, die gegarandeerd niet
te onderscheiden is vun de duurste Vulpenhouders
welke verkrijgbaar zijn. Ze wordt
gemaakt in 2 modellen voor Dames en
Heeren.

De pen zelf is van bijzonder hard en soepel
metaal (11 karaat Goud plated) en schrijft
licht en gemakkelijk. Tevens is ze voorzien
van een uitnemende inktgeleider, waardoor
lekken is uitgesloten en de pen altijd gereed
is voor het gebruik. Bovendien bij
iederen Vulpenhouder 2 jaar schriftelijke
garantie.

Wij willen dezen nieuwen zelfvuller snel en
doeltreffend invoeren en hebben daarom
besloten een groot aantal dezer fraaie Vulpenhouders
beschikbaar te stellen tegen een
geringe vergoeding van slechts

fl. 0.95 per exemplaar

(met 2 jaar schriftelijke garantie franco thuis)

Wij zijn van oordeel, dat deze practische
reclame de beste manier is, om dit nieuwe
artikel te introduceeren. Vul nog heden onderstaande
Coupon in, en met 1½ cents
postzegel frankeering in open enveloppe
zenden aan den alleenvertegenvvoordiger
voor Nederland:

Importhuis „De Atlas”

**Jan Sonjéstraat** 20B, R*otter*dam.

Coupon

Verzoeke toezending van: … „Universal”
Vulpenhouder(s) met 2 jaar schriftelijke
garantie. Het bedrag groot ƒ0.95 per exemplaar,
wordt door mij bij ontvangst van den
Vulpenhouder voldaan.


Naam: Dame / Heer …

Straat: …

Stad: …

Doorhalen wat niet verlangd wordt.

Voor Amsterdam zelf wordt(en) de Vulpenhouder(s)
thuisbezorgd.

Buiten Amsterdam gelieve postwissel te zenden.
U ontvangt dan de Vulpenhouder(s)
per keerende post.
