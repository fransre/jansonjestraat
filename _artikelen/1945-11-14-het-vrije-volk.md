---
toegevoegd: 2024-07-26
gepubliceerd: 1945-11-14
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954794:mpeg21:a0060"
onderwerpen: [bewoner]
achternamen: [hendrikse]
kop: "Ter nagedachtenis"
---
# Ter nagedachtenis

Maandagmiddag *12 november 1945* is op de begraafplaats
Crooswijk door bewoners van
de **Jan Sonjéstraat** een grafsteen
overgedragen aan de familie van den
oud-K*nok*P*loeg*-er J.P. Hendrikse (Louis),
die in een gevecht tegen den bezetter
is gesneuveld.

*(Zie <https://www.lo-lkp.nl/component/edocman/hggi-h2-547-563/download.html>: Johannes P. Hendrikse (Louis) Voorheen: OrdeDienst. Gearresteerd 28-1-1945 bij overval op schuit met wapens in Rotterdam, ten gevolge van doorslaan gearresteerde rijksduitser uit de KnokPloeg-Rotterdam.)*
