---
toegevoegd: 2021-04-29
gepubliceerd: 1906-09-29
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213908:mpeg21:a0127"
onderwerpen: [bedrijfsinformatie, fiets]
huisnummers: [42, 44]
achternamen: [pieterse]
kop: "Stadsnieuws."
---
# Stadsnieuws.

1o. aan de firma J. Pieterse & Co*mpagnon* voor
het plaatsen van een gasmotor van 4 paardekracht,
een dynamo en een moffel in de
smederij voor het herstellen van rijwielen
enz*ovoort* aan de **Jan Sonjéstraat** n*ummer* 42,44;
