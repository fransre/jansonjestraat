---
toegevoegd: 2024-08-05
gepubliceerd: 1931-10-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164655056:mpeg21:a00130"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [41]
achternamen: [van-veen]
kop: "Te koop:"
---
# Te koop:

een Kinderledikantje
zoo goed als nieuw, prima
lak, groote maat,
135×65 c*enti*M*eter*. Van Veen,
**Jan Sonjéstraat** 41,
R*otter*dam.
