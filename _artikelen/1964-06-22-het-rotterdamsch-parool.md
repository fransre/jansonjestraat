---
toegevoegd: 2024-07-23
gepubliceerd: 1964-06-22
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259030043:mpeg21:a00073"
onderwerpen: [misdrijf, tram]
achternamen: [herbert]
kop: "Trampersoneel slaags met passagier"
---
Wandelwagentje zonder hoes

# Trampersoneel slaags met passagier

(Van een onzer verslaggevers)

Rotterdam,  *22 juni 1964*. — Gisteravond *21 juni 1964*
ontstond in een tram op de Mathenesserdijk
een vechtpartij, toen de
23-jarige conducteur N. Herbert uit
de **Jan Sonjéstraat** aan een passagier
vroeg een hoes over zijn wandelwagentje
te doen.

De vechtpartij zette zich buiten voort,
toen de conducteur politie-assistentie
wilde vragen door in een telefooncel
op te bellen. De bestuurder van de
tram, de 38-jarige C.L. van Velzen uit
de Lemkensstraat, kwam zijn collega
te hulp, die daarop van de steeds bozer
wordende passagier enkele klappen
en schoppen kreeg, waardoor hij een
pijnlijke rug en linkerkaak opliep.

Daar nòch de bestuurder, nòch de
conducteur hun dienst konden vervolgen,
is deze overgenomen door een andere
bestuurder, die toevallig in de
buurt was.

De passagier, de 30-jarige kantoorbediende
S.W. A. uit Rotterdam, is ingesloten
in het bureau Marconiplein.
