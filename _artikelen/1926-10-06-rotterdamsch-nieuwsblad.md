---
toegevoegd: 2024-08-08
gepubliceerd: 1926-10-06
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010513806:mpeg21:a0168"
onderwerpen: [ongeval, fiets]
kop: "Gecompliceerde aanrijding."
---
# Gecompliceerde aanrijding.

Op 's Gravendijkwal zijn gisteren *5 oktober 1926* de 29-jarige
groentenhandelaar D.W. Z., uit de
**Jan Sonjéstraat**, die de Schermlaan kwam
uitrijden op zijn rijwiel en de 14-jarige J. v*an* Dalen,
uit de Rosier Faassenstraat, die
uit de richting Schietbaanlaan per fiets
aankwam, met elkander in botsing gekomen.
De aanrijding ontstond, doordat v*an* Dalen
zenuwachtig werd van den van de
1e Middellandstraat naderenden motorwagen 161
van lijn 10 der R*otterdamsche* E*lectrische* T*ramweg* M*aatschappij*, en
daardoor zijn bocht te kort nam. Beiden
vielen en kwamen op de rails terecht,
vlak vóór den motorwagen. Hoewel de
bestuurder daarvan, de 30-jarige J.W. A.,
uit de Ommoordschestraat onmiddellijk
electrisch remde en zand strooide, kon
hij niet voorkomen, dat beider rijwielen
ernstig werden beschadigd. Van Dalen bekwam
bovendien kneuzingen aan beide
knieën en moest per auto naar huis worden
vervoerd.
