---
toegevoegd: 2024-07-24
gepubliceerd: 1956-10-23
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164895020:mpeg21:a00082"
onderwerpen: [werknemer]
huisnummers: [23]
achternamen: [hendrikse]
kop: "leerling- en aankomend monteur"
---
Elektro Technisch Bureau Hendrikse
vraagt

# leerling- en aankomend monteur

Aanmelden
**Jan Sonjéstraat** 23 R*otter*dam.
