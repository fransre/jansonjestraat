---
toegevoegd: 2021-04-29
gepubliceerd: 1972-11-28
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958015:mpeg21:a0069"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Club-Angelique."
---
# Club-Angelique.

Wat is
nieuw in Rotterdam!! Geen
woorden, maar daden!! Gaan
wij u een onvergetelijke middag
of avond bezorgen, met
div*erse* show's verzorgd door onze
jonge, charm*ante* gastvrouwen
Angelique, 21 j*aa*r, Christine 24
j*aa*r, Muriël 21 j*aa*r, Monique 24
j*aa*r en Jaqueline 22 j*aa*r Bij ons
is elke avond een partyavond.
En kunt u aan onze bar genieten
van drink, die u gratis
worden aangeboden. Entree
prijs: 's middags van 13.00-17.00
ƒ25, 's avonds van 20.30-01.00
ƒ40. Tot ziens in de **Jan Sonjéstraat** 33,
zijstr*aat* Middellandstr*aat*
Tel*efoon* 010-256188, Rotterdam.

VZ16
