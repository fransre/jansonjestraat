---
toegevoegd: 2024-08-03
gepubliceerd: 1936-08-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164684051:mpeg21:a00196"
onderwerpen: [huisraad, tekoop]
huisnummers: [29]
kop: "Te koop:"
---
# Te koop:

wegens Verh*uizing* gr*ote* eiken
Tafel 5, ijzer Ledikant
met 2 Stroomatrassen
1.25, Waschkuip m*et*
Stamper 1.25 enz*ovoort*. **Jan Sonjéstraat** 29b.
