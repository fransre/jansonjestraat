---
toegevoegd: 2024-07-30
gepubliceerd: 1939-12-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164704036:mpeg21:a00102"
onderwerpen: [woonruimte, tehuur]
huisnummers: [38]
kop: "zit-slaapkamer"
---
Een gem*eubileerde* of ongem*eubileerde*

# zit-slaapkamer

te huur, prijs billijk,
voorzien van licht en
water. Adres **Jan Sonjéstraat** 38c.
