---
toegevoegd: 2024-08-08
gepubliceerd: 1927-01-28
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000197933:mpeg21:a0029"
onderwerpen: [huispersoneel]
huisnummers: [21]
achternamen: [reijnders]
kop: "een net dienstmeisje"
---
Voor direct gevraagd:

# een net dienstmeisje

voor dag en nacht. Mevr*ouw* Reijnders,
**Jan Sonjéstraat** 21a.

72042 4
