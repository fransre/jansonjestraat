---
toegevoegd: 2024-08-14
gepubliceerd: 1908-02-01
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000188850:mpeg21:a0023"
onderwerpen: [woonruimte, tehuur]
kop: "Bovenhuis."
---
# Bovenhuis.

Te huur a*an* d*e* 1ste Middellandstraat,
hoek **Jan Sonjéstraat**, een

flink ruim bovenhuis,

ƒ425.— Dagelijks te bezichtigen.

Adres: J.A. Peeters, Mathenesserlaan N*ummer* 202.

0885 12
