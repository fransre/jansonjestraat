---
toegevoegd: 2024-08-05
gepubliceerd: 1932-02-17
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164657052:mpeg21:a00123"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [23]
kop: "Aanbeveling."
---
# Aanbeveling.

Steller dezer, circa
drie jaar en Pension,
kan een ieder dat ten
zeerste aanbevelen.
Inl*ichtingen* **Jan Sonjéstraat** N*ummer* 23a, b.
