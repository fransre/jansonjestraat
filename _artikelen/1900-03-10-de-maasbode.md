---
toegevoegd: 2021-04-29
gepubliceerd: 1900-03-10
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000166177:mpeg21:a0012"
onderwerpen: [straat, schilder]
kop: "Stadsnieuws."
---
# Stadsnieuws.

Door B*urgemeester* en W*ethouders* zijn aan 89 nieuwe straten
namen gegeven, waaronder voorkomen de
Pretorialaan, Paul Krugerstraat, Bloemfonteinstraat,
Martinus Steynstraat, Afrikaanderplein of straat,
Bothastraat, Cronjéstraat, Joubertstraat,
Schalk Burgerstraat en Herman Costerstraat.
Deze namen zijn gegeven ter herinnering
aan het heldhaftig gedrag onzer stamgenooten
in den Oranje-Vrijstaat en de Zuid-Afrikaansche Republiek.

Ter herinnering aan de kunstschilders, die
alhier gewerkt hebben, zijn de namen gegeven
van Jan van Vuchtstraat, **Jan Sonjéstraat**, Jan-Porcellisstraat,
Joost-van-Geelstraat, Rochussenstraat,
De-Vliegerstraat, Lieve-Verschuierstraat
en Hondiusstraat, terwijl de Robert-Fruinstraat
de herinnering zal levendig houden aan den
bekenden geschiedschrijver.
