---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-15
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297061:mpeg21:a0146"
onderwerpen: [misdrijf]
kop: "Distributiewetzaken."
---
# Distributiewetzaken.

In de Tweede Middellandstraat is een
bewoner van de **Jan Sonjéstraat** aangehouden,
die een kaas vervoerde, welke bovendien
boven den maximumprijs was gekocht
van een bewoner van de Delftschestraat.
In de Delftschestraat zijn daarop
nog 62 kazen in beslag genomen.
