---
toegevoegd: 2024-05-20
gepubliceerd: 1924-05-20
decennium: 1920-1929
bron: Middelburgsche Courant
externelink: "https://krantenbankzeeland.nl/issue/mco/1924-05-20/edition/0/page/2"
onderwerpen: [bewoner]
huisnummers: [32]
achternamen: [van-doorn]
kop: "Opkomst onder de wapenen reserve officieren."
---
# Opkomst onder de wapenen reserve officieren.

Ook dit jaar bestaat er gelegenheid
voor de reserve officieren, die onder de
wapenen moeten komen, inlichtingen te
bekomen omtrent logies in de diverse
garnizoenen.

Ze kunnen zich hiertoe wenden tot
het bestuur der afdeeling Rotterdam der
Algemeene Vereeniging van Verlofsofficieren
der Land- en Zeemacht, adres de
heer G.C. van Doorn, **Jan Sonjéstraat** 32a, Rotterdam.
