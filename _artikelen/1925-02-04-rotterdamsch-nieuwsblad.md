---
toegevoegd: 2024-08-10
gepubliceerd: 1925-02-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495014:mpeg21:a0087"
onderwerpen: [bedrijfsinformatie]
huisnummers: [28]
achternamen: [eppinghaus]
kop: "Verkoop bij Executie"
---
# Verkoop bij Executie

van Kantoormeubelen en Electrische
Onderdeelen, in het pand
Nieuwehaven 152, Rotterdam, ten
laste van N.A. Eppinghaus, **Jan Sonjéstraat** 28.

Th. Korporaal, Deurwaarder.
Wijnstraat 121, R*otter*dam. Tel*efoon* 8521.

15750 10
