---
toegevoegd: 2024-08-14
gepubliceerd: 1910-09-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197087:mpeg21:a0171"
onderwerpen: [reclame]
huisnummers: [30]
achternamen: [verhagen]
kop: "Bekendmaking."
---
# Bekendmaking.

Mej*uffrouw* Verhagen. Somnambule,
werkzaam met
de geheime machten der
physionomie, slaapt voor
alle ziekten en ongesteldhedon
en geeft raad
in alle duistere zaken.
Is verhuisd van Tuindersstraat 37
naar **Jan Sonjéstraat** 30B, bij 1ste Middellandstraat.
Spreekuren
van 10-10.
