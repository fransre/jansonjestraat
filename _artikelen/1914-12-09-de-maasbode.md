---
toegevoegd: 2024-08-13
gepubliceerd: 1914-12-09
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000188595:mpeg21:a0013"
onderwerpen: [diefstal, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "diefstal van een damesmantel."
---
De schilder E.J. W*eier*, wonende **Jan Sonjéstraat** 38,
heeft aangifte gedaan van

# diefstal van een damesmantel.
