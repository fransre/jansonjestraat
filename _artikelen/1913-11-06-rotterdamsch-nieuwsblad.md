---
toegevoegd: 2021-05-18
gepubliceerd: 1913-11-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295853:mpeg21:a0083"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [20]
kop: "Te Huur aangeboden:"
---
# Te Huur aangeb*oden*:

een gemeubileerde Kamer
en Slaapkamer, voorzien
van gas- en waterleiding
met Pension, ƒ30
à ƒ35, zonder Pension
ƒ15 per maand. **Jan Sonjéstraat** 20
bij de
Schermlaan.
