---
toegevoegd: 2024-03-07
gepubliceerd: 1916-03-31
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296277:mpeg21:a0079"
onderwerpen: [gevraagd]
huisnummers: [36]
achternamen: [sas]
kop: "Fototoestel"
---
# Fototoestel

gevraagd 13 bij 18, liefst
Klapcamera, eventueel
met toebehooren. Adres:
Gebr*oeder*s Sas. **Jan Sonjéstraat** 36.
