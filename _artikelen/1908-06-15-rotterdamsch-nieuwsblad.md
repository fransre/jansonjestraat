---
toegevoegd: 2024-08-14
gepubliceerd: 1908-06-15
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994833:mpeg21:a0196"
onderwerpen: [bedrijfsruimte]
huisnummers: [15]
achternamen: [wessels]
kop: "pakhuis te huur"
---
Een enkel of dubbel

# pakhuis te huur.

Wessels, **Jan Sonjéstraat** 15.

24796 4
