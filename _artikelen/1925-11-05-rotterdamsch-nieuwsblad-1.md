---
toegevoegd: 2024-08-10
gepubliceerd: 1925-11-05
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495618:mpeg21:a0191"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [23]
kop: "zit-slaapkamer"
---
Aangeb*oden* vrije gem*eubileerde*

# zit-slaapkamer

met degelijk Pension
op vrij Bovenhuis.
Adres: **Jan Sonjéstraat** 23,
bij 's Gravendijkwal
