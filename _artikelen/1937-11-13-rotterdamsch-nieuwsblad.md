---
toegevoegd: 2024-08-02
gepubliceerd: 1937-11-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692012:mpeg21:a00066"
onderwerpen: [woonruimte, tehuur]
huisnummers: [34]
kop: "Benedenhuis"
---
# Benedenhuis

**Jan Sonjéstraat** 34, bevat
5 Kamers, Keuken,
Warande, Tuin, ruime
Kelder, ƒ35 p*er* m*aand* Sl*eutel* aldaar.
Bevr*agen* P. Rijshouwer,
Honingerdijk 81a.
