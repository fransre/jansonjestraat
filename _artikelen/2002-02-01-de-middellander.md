---
toegevoegd: 2023-01-08
gepubliceerd: 2002-02-01
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2002-02-01-de-middellander-origineel.jpg"
onderwerpen: [actie]
achternamen: [poot]
kop: "Middelland woedend!"
---
# Middelland woedend!

Als een donderslag bij heldere hemel maakte
de Postbank on 21 januari *2002* j*ongst*l*eden* bekend dat het
Postkantoor aan de Witte van Haemstedestraat
per 4 februari *2002* zou worden gesloten. Direkt na
de bekendmaking (met een armetierig foldertje
op de deur van het kantoor) meldden zich enige
tientallen woedende mensen bij de Bewonersorganisatie Middelland:
„Zijn ze daar
nou helemaal van de pot getrokken?”, „Zijn ze
krankzinning geworden?”, „Schandalig, er
blijft zo geen postkantoor meer over in heel
Delfshaven, na twee bankfilialen verdwijnt nu
ook nog ons postkantoor. Dit pikken we echt
niet. Actie, harde actie!!!”

Het is slechts een kleine greep uit de stortvloed
van de uiterst verontwaardigde reacties.

Er werd nog dezelfde dag een handtekeningenactie
gestart en er werd afgesproken
om bij elkaar te komen om te bespreken
welke andere acties er op een zo korte
termijn gevoerd konden worden. Een van
de eerste ideeën die opkwam was een bezetting
van het Postkantoor. Om dit goed
en zorgvuldig voor te bereiden werd een
afspraak voor een actievergadering gemaakt;
tijdens deze vergadering werd het
idee om het kantoor te bezetten met unanieme
stemmen aangenomen. Er werden
spandoeken met protestleuzen gemaakt,
de pers werd op de hoogte gesteld van de
voorgenomen actie en het in korte tijd
gemaakte draaiboek lag klaar.

## Nerveus

De bezetting begon op vrijdag 1 februari *2002*
om 16.30 uur. Gewapend met de spandoeken,
matrasjes, slaapzakken en eten en
drinken voor een eventueel onvoorzien
lang verblijf drongen de bewoners het
Postkantoor binnen; de toegesnelde manager
van het kantoor werd te woord gestaan
door bewoner Jan Poot uit de **Jan Sonjéstraat**.
Deze legde het doel van de actie
uit, vroeg de manager of hij bereid was de
1300(!) opgehaalde handtekeningen in
ontvangst te nemen en of hij zo vriendelijk
wilde zijn de hoofddirectie van de Postbank
uit te nodigen voor een gesprek ter plekke.
De manager reageerde hierop buitengewoon
nerveus. De handtekeningen wilde hij
niet aannemen, de directie was niet te
spreken en hij verbood de toegesnelde
persfotografen en de cameraman van het
Rotterdamse televisieprogramma ‘Cineac Delfshaven’
om hun werk te doen.

## Wachten

Hierop deelde Jan Poot de manager mee
dat de bewoners niet zouden vertrekken
voordat de eisen werden ingewilligd en dat
het dus uitsluitend aan hem was hoe lang
de bezetting zou duren; als het aan de
bewoners lag bleven zij desnoods de hele
nacht. Het zweet brak de arme man aan
alle kanten uit en hij verschanste zich
ijlings in zijn kantoor. Wat hij daar precies
heeft gedaan werd duidelijk toen een paar
minuten later de politie arriveerde.
Na ampel overleg met de manager verlieten
de agenten zonder mededelingen te
doen het Postkantoor. Inmiddels was het
5 uur geworden, de verlichting in het kantoor
werd gedoofd maar meegebrachte
zaklantarens brachten uitkomst. Al snel
werd duidelijk wat er binnen met de politie
besproken was: de chef meldde zich bij Jan Poot
en verklaarde alsnog bereid te zijn de
handtekeningen in ontvangst te nemen met
de belofte deze door te sturen naar de
hoofddirectie van de Postbank in Utrecht.
Hierop werd in goed overleg besloten de
bezetting te beëindigen

## Naar het hoofdkantoor

Bij de bewonersorganisatie werd nog even
nagepraat en er werden plannen gesmeed
voor een vervolg. Het plan is nu om de actie
te verbreden naar andere wijken van
Rotterdam en dan gezamenlijk naar het
hoofdkantoor van de Postbank in Utrecht
te gaan om daar ons ongenoegen te uiten
en de bezetting nog eens dunnetjes over
te doen.

De foto's zijn van Peter Kervezee.

*Onderschrift bij de foto*
Dat spreekt

*Onderschrift bij de foto*
Daar komt meneer de Knor…

*Onderschrift bij de foto*
Ik word niet goed!

*Onderschrift bij de foto*
Alle tijd…

*Onderschrift bij de foto*
Zo is dat.
