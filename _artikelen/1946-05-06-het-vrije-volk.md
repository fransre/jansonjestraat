---
toegevoegd: 2024-07-26
gepubliceerd: 1946-05-06
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955127:mpeg21:a0055"
onderwerpen: [woonruimte, tehuur]
huisnummers: [32]
achternamen: [hulsker]
kop: "Woningruil Rotterdam-Amsterdam"
---
# Woningruil Rotterdam-Amsterdam

Aangeboden: groot vrij bovenhuis, bev*attende* 5 kamers, keuken,
zolder, 2 veranda's, in het Westen van Rotterdam, huur
ƒ32.50 per maand.

Gevraagd: woning te Amsterdam, liefst Zuid, huur tot ƒ45
per maand.

Hulsker, **Jan Sonjéstraat** 32, Rotterdam. Hulsker, Rijnstraat 38-II,
Amsterdam.
