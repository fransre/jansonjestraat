---
toegevoegd: 2024-08-05
gepubliceerd: 1932-12-06
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164662041:mpeg21:p00014"
onderwerpen: [bewoner, fiets, auto]
huisnummers: [34]
achternamen: [valster]
kop: "Zonderlinge behandeling aan de grens."
---
Door Rijkskommiezen aangeschoten.

# Zonderlinge behandeling aan de grens.

Zondagavond *5 december 1932* is de familie A. Valster, wonende
**Jan Sonjéstraat** 34b, alhier, komende
met een luxe-auto van Baarle-Nassau, waar
men een bezoek had gebracht aan een familielid
in de Kweekschool voor Broeders, om omstreeks
7 uur op den weg tusschen Baarle-Nassau
en Chaams een eigenaardige behandeling
ten deel gevallen van de Rijkskommiezen.
Ongeveer één à twee kilometer van
Baarle-Nassau af vielen plotseling, terwijl de
auto met een vaart van ten hoogste 25 K*ilo*M*eter*
reed, twee schoten.

Het tweede trof de 42-jarige mevrouw A. Valster-Kolenberg,
die links van haar man zat,
die de auto bestuurde, ongevaarlijk onder de
borst. De auto stopte, waarna twee kommiezen
per fiets verschenen, die beweerden, dat zij
tevoren den bestuurder hadden gesommeerd te
stoppen, maar dat deze daaraan geen gevolg
had gegeven. De heer Valster nam met deze
mededeeling geen genoegen en verzocht de
ambtenaren voor hem uit te rijden naar het
raadhuis in Baarle-Nassau, waaraan slechts
schoorvoetend gevolg werd gegeven.

De burgemeester van Baarie-Nassau heeft
den kogel, die in den mantel van mevr*ouw* V. was
blijven steken, in beslag genomen.

De houding van de Rijkskommiezen was
voor den heer Valster aanleiding om een aanklacht
over het gebeurde ter plaatse in te dienen,
en hij zal, naar mevr*ouw* V. ons heden mededeelde,
ook op andere wijze trachten recht te
verkrijgen, waar hij uit de geheele behandeling
en wat zich daarna bij het onderzoek afspeelde,
sterk den indruk heeft verkregen, dat men
hem van de zijde der autoriteiten eer tegen dan
meewerkte.

Door het oponthoud miste de familie tenslotte
de laatste pont te Moerdijk maar zag dit
leed door de goede zorgen van den caféhouder
van café Het Veer tenslotte tot een minimum
beperkt.
