---
toegevoegd: 2021-04-30
gepubliceerd: 1972-06-16
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://schiedam.courant.nu/issue/VV/1972-06-16/edition/0/page/2"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Sexfilmclub"
---
# Sexfilmclub

Angelique
prolongeert wegens groot
success het exclusieve sexprogramma
m*et* m*edewerking* v*an* Angelique,
Monique, Chiquita en Helene.
Gezellige bar met charmante
bediening aanwezig waar U
kunt relaxen. Nieuwe geluidsfilms
in kleur. Het bekende
adres **Jan Sonjéstraat** 33a
(zijstraat Middellandstr).
Tel*efoon* 010-256188. Geopend van
's middags 2 tot 5 uur 's
avonds van 8.30 tot 12 uur.
Ook 's zaterdags. VZ16
