---
toegevoegd: 2022-07-23
gepubliceerd: 1980-08-04
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960605:mpeg21:a0127"
onderwerpen: [misdrijf]
kop: "Twee vrouwen ernstig gewond na steekpartij"
---
# Twee vrouwen ernstig gewond na steekpartij

(Van een onzer verslaggevers)

Rotterdam — In een huis
in de **Jan Sonjéstraat** in Rotterdam
zijn dit weekeind twee
vrouwen ernstig gewond bij een
steekpartij. Zij zijn overgebracht
naar een ziekenhuis. De
Rotterdammer J.P. B. stak hen
na een fikse ruzie en nam daarna
de benen.
