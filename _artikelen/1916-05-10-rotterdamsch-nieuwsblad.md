---
toegevoegd: 2024-08-13
gepubliceerd: 1916-05-10
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296308:mpeg21:a0103"
onderwerpen: [werknemer]
huisnummers: [34]
achternamen: [bosselaar]
kop: "Gevraagd:"
---
# Gevraagd:

een Leerling, tevens een
net Meisje om tegen vergoeding
van eenige
boodschappen het Naaivak
te leeren. Persoonlijk
aan te melden Jeanne Bosselaar,
**Jan Sonjéstraat** 34b.
