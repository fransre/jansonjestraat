---
toegevoegd: 2024-08-02
gepubliceerd: 1937-12-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692035:mpeg21:a00078"
onderwerpen: [woonruimte, tehuur]
huisnummers: [41]
achternamen: [van-hoof]
kop: "Zit-slaapkamer"
---
Aangeboden!

# Zit-slaapkamer

en Zolderkamer, stroomend
water, geschikt
voor 2 Broers of Vrienden.
Adres C. v*an* Hoof,
**Jan Sonjéstraat** 41a.
