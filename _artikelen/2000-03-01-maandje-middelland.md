---
toegevoegd: 2024-02-22
gepubliceerd: 2000-03-01
decennium: 2000-2009
bron: Maandje Middelland
externelink: "https://www.jansonjestraat.nl/originelen/2000-03-01-maandje-middelland-origineel.jpg"
onderwerpen: [eeuwfeest]
kop: "Terugblik op de receptie ‘100 jaar Jan Sonjéstraat’"
---
# Terugblik op de receptie ‘100 jaar **Jan Sonjéstraat**’

De receptie rond het 100-jarig bestaan van
de **Jan Sonjéstraat** was uitbundig en werd
door zo'n 200 personen bezocht. Tijdens
de receptie werd het boekje ‘Even terug in
de tijd’ gepresenteerd. Het is een werkelijk
schitterend boekje geworden met vele
prachtige sfeervolle en nostalgische foto's
en verhalen.

Wie nu denkt dat de bewoners de volgende
100 jaar lekker achterover gaan leunen
vergist zich. Integendeel!

Een aantal nieuwe activiteiten staan al
weer op de rol: zo is er op zaterdag 13 mei *2000* a*an*s*taande*
de jaarlijkse geraniumdag en op
zaterdag 3 juni *2000* volgt dan het jaarlijkse
straatdiner.

Kortom, de **Jan Sonjéstraat** is en blijft een
bijzonder actieve straat.
