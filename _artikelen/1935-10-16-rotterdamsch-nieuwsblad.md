---
toegevoegd: 2024-08-03
gepubliceerd: 1935-10-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164679050:mpeg21:a00044"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [34]
achternamen: [katoen]
kop: "Te huur:"
---
# Te huur:

wegens huwelijk twee
gem*eubileerde* frische Zit-Slaapkamers
m*et*/z*onder* Pension.
Wed*uwe* Katoen,
**Jan Sonjéstraat** 34a.
