---
toegevoegd: 2024-08-03
gepubliceerd: 1935-03-15
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164676017:mpeg21:a00076"
onderwerpen: [familiebericht]
huisnummers: [15]
achternamen: [van-oosten]
kop: "Cornelis van Oosten,"
---
Heden overleed, zacht en kalm.
na langdurig, geduldig gedragen
lijden, onze geliefde Vader, Behuwd- en
Grootvader,

# Cornelis van Oosten,

op den leeftijd van ruim 58 jaar.

C. van Oosten J*unio*r.

E. van Oosten en Verloofde.

L. van Oosten.

N. de Lange-van Oosten.

J. de Lange en Kind.

Rotterdam, 14 Maart 1935.

Plantageweg 79a,
(voorheen **Jan Sonjéstraat** 15).

Verzoeke geen rouwbeklag.

De teraardebestelling zal plaats
hebben Zaterdag *16 maart 1935* a*an*s*taande* op de Algem*ene* Begraafplaats Crooswijk.
Vertrek
van het Ziekenhuis Coolsingel
12½ uur.

28
