---
toegevoegd: 2024-03-07
gepubliceerd: 1917-12-20
decennium: 1910-1919
bron: De Telegraaf
externelink: "https://resolver.kb.nl/resolve?urn=ddd:110549965:mpeg21:a0124"
onderwerpen: [diefstal]
kop: "De diefstallen van zijde en manufacturen."
---
# De diefstallen van zijde en manufacturen.

Rotterdam. 19 Dec*ember* *1917*. De goederen, ontvreemd
uit de magazijnen van de manufacturier
Louis de Sterke, in de Oppert, alhier, zijn
heden door rechercheurs teruggevonden, gedeeltelijk
op de markt op den Goudschen Singel,
gedeeltelijk bij een anderen manufacturenhandelaar,
die het tricot-ondergoed enz*ovoort*
reeds uit de derde hand gekocht had.

Inmiddels zijn twee der aangehouden personen,
wier onschuld gebleken is, weder op vrije
voeten gesteld. Het zijn de vrouw van den
bankwerker W. S., wonende Diergaardekade,
en de winkelier J. de M., uit de **Jan Sonjéstraat**.

De andere verdachten zijn overgebracht naar
het Huis van Bewaring.
