---
toegevoegd: 2024-08-13
gepubliceerd: 1919-07-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010333589:mpeg21:a0084"
onderwerpen: [woonruimte, tehuur]
huisnummers: [42]
kop: "Aangeboden:"
---
# Aangeboden:

bij Wed*uwe* voor Juffrouw
op leeftijd, gem*eubileerde* Voorkamer
met Alkoof in
bezit van eigen bed.
**Jan Sonjéstraat** 42b, 2-m*aal*
bellen.
