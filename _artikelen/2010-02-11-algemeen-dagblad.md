---
toegevoegd: 2024-02-21
gepubliceerd: 2010-02-11
decennium: 2010-2019
bron: Algemeen Dagblad
externelink: "https://www.jansonjestraat.nl/originelen/2010-02-11-algemeen-dagblad-origineel.jpg"
onderwerpen: [kunst]
kop: "Onthulling kunstwerk"
---
# Onthulling kunstwerk

De **Jan Sonjéstraat** is vanaf zaterdag *13 februari 2010*
16.00 uur weer een stukje
mooier met de feestelijke onthulling
van een nieuw kunstwerk. Het
kunstwerk geeft de straatnaamgever
een gezicht. Alle bewoners van
omliggende straten zijn welkom.
