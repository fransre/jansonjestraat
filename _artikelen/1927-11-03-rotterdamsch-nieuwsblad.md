---
toegevoegd: 2024-08-07
gepubliceerd: 1927-11-03
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514134:mpeg21:a0177"
onderwerpen: [familiebericht]
huisnummers: [12]
kop: "Johanna Helena,"
---
Dankbetuiging.

Voor de vele bewijzen van deelneming,
ondervonden bij de ziekte en
het overlijden van onze lieve Dochter
en Verloofde,

# Johanna Helena,

betuigen wij onzen oprechten dank.

Uit aller naam:

L.J. Haarlemmer.

B. Dekkers.

**Jan Sonjéstraat** 12a.

13
