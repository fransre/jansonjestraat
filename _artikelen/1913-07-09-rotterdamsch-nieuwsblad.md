---
toegevoegd: 2024-08-13
gepubliceerd: 1913-07-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295681:mpeg21:a0099"
onderwerpen: [huisraad, tekoop]
huisnummers: [15, 17]
kop: "Waterketel."
---
# Waterketel.

Te koop een Ketel,
1½ meter inhoud, zoo
goed als nieuw, behoeft
niet ingemetseld te worden.
Adres **Jan Sonjéstraat** 15-17.
