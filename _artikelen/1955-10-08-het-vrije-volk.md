---
toegevoegd: 2024-07-24
gepubliceerd: 1955-10-08
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952331:mpeg21:a0050"
onderwerpen: [dieren]
huisnummers: [33]
achternamen: [van-hulst]
kop: "boxer,"
---
Pracht gele

# boxer,

10 mnd oud
lief voor kinderen, v*an* Hulst, **Jan Sonjéstraat** 33b.
