---
toegevoegd: 2024-08-14
gepubliceerd: 1909-04-17
decennium: 1900-1909
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000155346:mpeg21:a0013"
onderwerpen: [faillissement]
huisnummers: [8]
achternamen: [wouters]
kop: "Gerechtelijke aankondigingen."
---
# Gerechtelijke aankondigingen.

Bij vonnis der arrondissements-rechtbank te Rotterdam, van
14 April 1909, is Petrus Theodorus Wouters, bakker, wonende
aan de **Jan Sonjéstraat** 8a, te Rotterdam, verklaard in staat
van faillissement, met benoeming van den edelachtbaren heer
mr. P. IJssel de Schepper tot rechter-commissaris, en van den
ondergeteekende, advocaat en procureur, Zuidblaak 64, te Rotterdam,
tot curator.

Mr. H. van Blommestein.

[Kosteloos.]

(2271)
