---
toegevoegd: 2024-02-21
gepubliceerd: 2002-06-26
decennium: 2000-2009
bron: Postiljon
externelink: "https://www.jansonjestraat.nl/originelen/2002-06-26-postiljon-origineel.jpg"
onderwerpen: [bewoner]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Jan van Hooijdonk viert eeuwfeest"
---
# Jan van Hooijdonk viert eeuwfeest

Hillegersberg — „Mag ik
even kijken? Ja, keurig; u hebt
mijn naam in één keer goed
geschreven.”

De toon is gezet; Jan van Hooijdonk
gaat op 29 juni *2002* honderd
jaar met deze naam door
het leven en dat wil hij graag
correct op papier.

Zaterdag *29 juni 2002* viert de dan honderdjarige zijn
verjaardag in de straat waar hij 72 jaar
woont; de **Jan Sonjéstraat**. Zijn huis
staat er nog steeds, maar sinds enige tijd
verblijft Van Hooijdonk in het Reuma verpleeghuis.

Jan van Hooijdonk zag het levenslicht in
de Bloemstraat in het oude westen. „Ik
was nummer vier van de elf in het gezin
en nu ben ik nog de enige. Mijn laatste
jongere broer (92) is enkele maanden
geleden gestorven.”

Na hun huwelijk woonde het echtpaar
Van Hooijdonk anderhalf jaar in de
Bloemkwekersstraat, waarna het eerst
naar de Jan van Vucht- en later naar de
**Jan Sonjéstraat** verhuisde.

„Daar was net een mevrouw aan het
verhuizen toen ik langs liep. Is die
woning al verhuurd?, vroeg ik
haar. Ze dacht van niet en toen
hadden wij geluk. Een benedenwoning
met een kelder. Ik was
stoffeerder en kon klusjes doen in
mijn eigen kelder.”

Later startte Jan daar met een
eigen stoffeerderij. „Ik werkte veel
voor de Holland Amerikalijn. We
kregen zoveel opdrachten, dat ik
op een gegeven moment met
zeven knechts werkte.”

## Winkel

Jan en zijn echtgenote openden
een meubelzaak in de
Engelsestraat. „Dat liep goed, totdat
ik een ongeval kreeg. Ik werd
aan mijn knie geopereerd en dat
betekende ook min of meer het
einde van de zaak.”

Nadat zijn echtgenote op 85-jarige leeftijd
was overleden reisde Jan elk weekend
naar een van zijn drie dochters en
schoonzoons. „Daar was ik altijd welkom.
Inmiddels hebben we ook zes
kleinkinderen en veertien achterkleinkinderen
in de familie.”

Jan ging wat sukkelen en na een ziekenhuisopname
werd besloten dat hij
zou revalideren in het Reuma verpleeghuis.
„Mijn huis heb ik nog aangehouden.
Daarom vind ik het ook zo leuk
om zaterdag *29 juni 2002* in mijn eigen straat tussen
mijn mensen m'n verjaardag te vieren.”

*Onderschrift bij de foto*
Jan van Hooijdonk is een rasechte Rotterdamse eeuweling.

(Foto: Seert)
