---
toegevoegd: 2024-07-25
gepubliceerd: 1953-04-28
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164883098:mpeg21:a00145"
onderwerpen: [dieren]
huisnummers: [31]
achternamen: [mol]
kop: "goed tehuis"
---
Wegens omstandigh*eden* wordt gezocht

# goed tehuis

voor lief
hondje, geen ras. Mej*uffrouw* Mol,
**Jan Sonjéstraat** 31b. Liefst na
6 uur.
