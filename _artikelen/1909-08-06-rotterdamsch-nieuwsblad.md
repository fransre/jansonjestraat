---
toegevoegd: 2024-08-14
gepubliceerd: 1909-08-06
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197956:mpeg21:a0093"
onderwerpen: [fiets, tekoop]
huisnummers: [10]
kop: "Rijwiel 17.50."
---
# Rijwiel 17.50.

Te koop een goed onderhouden
en in uitstekenden
toestand verkeerende
Fiets voor ƒ17.50.
**Jan Sonjéstraat** 10b.
