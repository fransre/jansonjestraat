---
toegevoegd: 2024-05-18
gepubliceerd: 1940-12-06
decennium: 1940-1949
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1940-12-06/edition/0/page/4"
onderwerpen: [reclame, tram]
achternamen: [huisman]
kop: "Let op"
---
# Let op

Wij zijn te bereiken
met de tramlijnen 1, 4, 5,
9, 15, halte Henegouwerlaan;
8, 11, 16, halte Nieuwe Binnenw*eg*-'s Gravendijkwal

Besteedt uw

textielpunten

Op de meest voordeelige wijze!

Door onze nog zeer
uitgebreide sorteering

vóór-oorlogsche
kwaliteiten
Winterjassen
en
Costuums

zoowel voor Uzelf als voor Uw
kinderen, slaagt U bij ons
zeer gemakkelijk!

Kinder
ratinéjekkers
voor Meisjes en Jongens
worden
zonder vergunning
op punten verstrekt

Mooie sorteering
Stoffen voor
Kleeding naar Maat

Stalen zenden wij op aanvraag.

H. Huisman

Vanaf 1895 gevestigd:
Hoogstraat 322-326

thans

1e Middellandstraat 100
hoek **Jan Sonjéstraat**
Rotterdam — Tel*efoon* 33943
