---
toegevoegd: 2024-08-14
gepubliceerd: 1909-09-03
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197980:mpeg21:a0126"
onderwerpen: [gevraagd]
huisnummers: [25]
kop: "Teekenbord."
---
# Teekenbord.

Te koop gevraagd een
in goeden staat zijnd
groot Teekenbord met
Zak, voor 4e en 5e klasse
der Academie. Adres
**Jan Sonjéstraat** 25, boven.
