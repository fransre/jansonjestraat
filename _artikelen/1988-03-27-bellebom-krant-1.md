---
toegevoegd: 2023-01-12
gepubliceerd: 1988-03-27
decennium: 1980-1989
bron: Bellebom Krant
externelink: "https://beeldbank.40-45nu.nl/ovmfoto//10008/10008(1).jpg"
onderwerpen: [bellebom]
kop: "Wat gebeurde er op 29 november 1944?"
---
# Wat gebeurde er op 29 november 1944?

Een klein stukje geschiedenis.
Vooral
bestemd voor de lezers
die pas na de Tweede Wereldoorlog
geboren
zijn of toen nog niet in
Nederland woonden.
Het is 29 november 1944, tussen
11:20 en 11:31 uur. De Engelsen vallen
met bommenwerpers onder meer
het gebouw aan van de ‘Sicherheitsdienst’
aan de Heemraadsingel. Het
gebouw is in gebruik bij de Duitse
bezetter. De luchtaanval kost 61 mensen
het leven, 39 gewonden worden
in het ziekenhuis opgenomen.
Uit 't rapport dat luchtbeschermingsofficier
J.J. Verwey op 14 december 1944
schrijft over de luchtaanval:
‘Bij zeer helder weer werd
ondanks hevig afweervuur door een
onbekend aantal zeer snelle vliegtuigen
van geringe hoogte en aanval
gedaan op bepaalde punten in het
stadsgebied van Rotterdam…’ Zijn
rapport maakt melding van een
blindganger van 500 k*ilo*g*ram*, oftewel een
duizendponder, ‘in den tuin achter
pand n*ummer* 31’ in de Bellevoysstraat.
Een blindganger is een bom die nog
niet is ontploft. Archiefspeurwerk in
Engeland levert in 1987 op dat de
luchtaanval is uitgevoerd door 32
Typhoons, Britse vliegtuigen, die enkele
tientallen raketten en brisantbommen
boven Rotterdam hebben
afgeworpen.

## 43 jaar later

Een bewoner uit Middelland
schrijft na overleg met de politie op 1
mei 1987 een brief aan burgemeester
en wethouders van Rotterdam:
‘Binnenkort zal met de renovatiewerkzaamheden
in de Bellevoysstraat
worden begonnen. In verband
hiermede lijkt het mij van groot
belang U mede te delen dat in de tuinen
een bom is gevallen, circa 2 tot 3
meter gemeten uit de achtergevel.
Wegens ontploffingsgevaar mochten
wij toen de panden voor een paar dagen
niet betreden. Daarna is ons
meegedeeld dat de bom in de grond
weggezakt was. Wij hebben toen de
woningen weer betrokken. Ik kan de
inslagplaats nog aanwijzen. Met de
te verwachten graafwerkzaamheden
leek het mij nodig U van het toen gebeurde
in kennis te stellen.’

Die brief vormde het begin van de
grootste ruimingsoperatie die de Explosieven Opruimings Dienst
ooit verrichtte.

## Voorbereidingen vergden half jaar

Ruim 'n half jaar lang zijn de bewoners
van de Bellevoysstraat en de
**Jan Sonjéstraat** bepaald niet met rust
gelaten. Eén huis moest worden
gesloopt om de zware vrachtwagens,
hijskranen en een heimachine in de
achtertuinen van de Bellevoysstraat
te krijgen. Dat zorgde voor een hoop
kabaal. ‘Ze hebben ons niet
zo'n klein beetje overlast bezorgd’,
zegt mevrouw H.J. Ottenberg-van
den Bosch. Zij woont al ruim vijftig
jaar in de Bellevoysstraat, zeg maar
naast de bom. ‘Wel moet ik zeggen,
de mensen die er werken zijn ontzettend
vriendelijk voor ons geweest.
't Moet nu eenmaal gebeuren, het is
niet anders’, zegt zij. ‘We zouden
tijdens het heien allemaal houten
platen voor onze ramen krijgen voor
de veiligheid.
En toen we ze vroegen of ze daar luiken
in konden zetten is dat ook
meteen gedaan.
Daar ben ik wel blij mee, want dan
blijf je niet weken lang in het donker
zitten.’

*Onderschrift bij de foto*
Januari 1988, 'n heimachine slaat zware damwandplaten in de grond, nodig om een tien meter diepe
kuil te kunnen graven. Rechts de geblindeerde woningen aan de **Jan Sonjéstraat**, links de renovatiepanden
aan de Bellevoysstraat.

*Onderschrift bij de foto*
Februari 1988, adjudant P. Schoots van de
Explosieven Opruimings Dienst met het
staartstuk van de Bellebom.
