---
toegevoegd: 2021-05-18
gepubliceerd: 1912-07-01
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010222197:mpeg21:a0080"
onderwerpen: [werknemer]
huisnummers: [22]
kop: "Costuumnaaister"
---
# Costuumnaaister

jaren als eerste coupeuse in een
van de eerste firma's werkzaam
geweest, heeft zich gevestigd

**Jan Sonjéstraat** 22.

Beveelt zich aan. Voor prima
coupe en nette afwerking wordt
ingestaan. Aan hetzelfde adres
eenige Leerlingen en aankomende
Juffr*ouw* gevraagd.

32751 13
