---
toegevoegd: 2024-08-06
gepubliceerd: 1931-02-24
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164651061:mpeg21:a00086"
onderwerpen: [werknemer]
huisnummers: [38]
kop: "flinke jongen,"
---
Een

# flinke jongen,

17 j*aar*, uit eigen Zaken,
b*iedt* z*ich* a*an*, goede getuigen
voorzien ƒ12 p*er* week.
**Jan Sonjéstraat** 38.
