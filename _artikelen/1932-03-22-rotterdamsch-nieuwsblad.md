---
toegevoegd: 2024-08-05
gepubliceerd: 1932-03-22
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164658025:mpeg21:a00084"
onderwerpen: [bewoner]
achternamen: [t-hart]
kop: "Jubileum A. J. 't Hart."
---
# Jubileum A. J. 't Hart.

Heden was het 25 jaar geleden, dat de
heer A.J. 't Hart, thans hoofdagent-rechercheur
bij den Justitieele Dienst van
bureau Oppert, in Rotterdamschen politiedienst
trad.

De algemeen geachte jubilaris werd hedenmorgen *22 maart 1932*
gecomplimenteerd door den
commissaris der 2e afdeeling, den heer
C.G. van Gulden, terwijl ook de inspecteurs
van den Justitieelen Dienst hun opwachting
kwamen maken.

Ook van andere zijden mocht de heer
nog vele blijken van belangstelling
ondervinden; de receptie te zijnen
huize aan de **Jan Sonjéstraat** was druk
bezocht.
