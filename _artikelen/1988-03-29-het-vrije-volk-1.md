---
toegevoegd: 2021-04-29
gepubliceerd: 1988-03-29
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962738:mpeg21:a0207"
onderwerpen: [bellebom]
kop: "Tipgeefster Bellebom vindt het welletjes"
---
# Tipgeefster Bellebom vindt het welletjes

Een zuur
lachje kon er
nog net
af. Mevrouw H.J. Ottenberg
had vorige
week al
voorgenomen niemand meer
te woord te staan over de
Bellebom, maar voor burgemeester
Peper wilde ze een
uitzondering maken. Uit
zijn handen ontvingen de
veelgeplaagde ‘tipgeefster’
en haar man gisteren *28 maart 1988* een
gigantisch levensmiddelenpakket;
een geste van de
gemeente Rotterdam om al
het ongerief van de laatste
maanden te doen vergeten.

Voordat Peper in zijn
blinkende limousine arriveerde
had ambtenaar Max
de Ruiter, hoofd van het bureau
Rampenbestrijding, de
familie Ottenberg al voorzichting
op de hoogte gebracht
van het aanstaande
bezoek. „Okay, maar dit is
echt de laatste keer hoor,”
was haar weinig enthousiaste
reactie. Aan het verzoek
van een der fotografen
om aan de rand van de kuil,
waar de Bellebom werd uitgevist,
te poseren werd
door mevrouw Ottenberg
geen gehoor gegeven: „Alstublieft
niet, ik heb al zoveel
gedaan.”

Naast de heer en mevrouw
Ottenberg kregen
nog 24 ‘getroffen’ gezinnen
in de Bellevoysstraat en de
**Jan Sonjéstraat** een dergelijk
pakket uitgereikt. Deze
mensen zijn volgens de gemeente
‘drie-dubbel’-gedupeerd,
omdat ze niet alleen
zondag *27 maart 1988* maar ook in december *1987*
al twee maal hun woning
moesten verlaten.
„Een soort attentie,” legt
gemeentevoorlichter Ton Michielse
uit. „Kijk, je kunt
die mensen ook een paar
knaken geven, maar dit is
veel leuker. Ook de naweeën
van zo'n gebeurtenis
moeten worden verzorgd.”

*Onderschrift bij de foto*
Nog één keer dan poseren voor de fotografen, met burgemeester Peper (links)
