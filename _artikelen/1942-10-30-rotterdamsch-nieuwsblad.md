---
toegevoegd: 2024-07-26
gepubliceerd: 1942-10-30
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002988:mpeg21:a0010"
onderwerpen: [kleding, tekoop]
huisnummers: [46]
achternamen: [dukers]
kop: "1 Wintermantel,"
---
# 1 Wintermantel,

nieuw, l*ee*ft*ijd*
14-15 jaar ƒ50, dames regenjas
licht ƒ17.50, heeren regenjas
gabardine, licht, maat 50. J. Dukers.
**Jan Sonjéstraat** 46.
