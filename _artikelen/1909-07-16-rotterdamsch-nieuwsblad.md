---
toegevoegd: 2024-03-07
gepubliceerd: 1909-07-16
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197938:mpeg21:a0115"
onderwerpen: [huisraad, bedrijfsinventaris, tekoop]
huisnummers: [4]
kop: "Te Koop."
---
# Te Koop.

Een mooie partij Zoollederafval
te koop, zeer
goedkoop, alsmede een
Tafel, laatste model,
zeer geschikt voor jongelui,
die gaan trouwen.
Adres: 1e Middellandstraat 78,
Schoenmakerij en **Jan Sonjéstraat** 4.
