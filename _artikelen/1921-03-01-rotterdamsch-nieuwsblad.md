---
toegevoegd: 2024-08-12
gepubliceerd: 1921-03-01
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493683:mpeg21:a0043"
onderwerpen: [woonruimte, tekoop]
kop: "Te koop:"
---
# Te koop:

een solied pand, **Jan Sonjéstraat**, vrij Boven- en Benedenhuis,
zeer geschikt voor Zelfbewoning. Prijs ƒ11.000. Br*ieven* *onder* lett*er* B.474,
Adv*ies*-Bur*eau* Betcke, Slagveld 3, R*otter*dam.

19324 20
