---
toegevoegd: 2024-08-06
gepubliceerd: 1929-12-06
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010212649:mpeg21:a0080"
onderwerpen: [faillissement]
huisnummers: [17]
achternamen: [bosman]
kop: "Faillissementen."
---
# Faillissementen.

(Opgegeven door v*an* d*er* Graaf en Co*mpagnon*, N.V.,
afd*eling* Handelsinformaties).

Uitgesproken:

29 Nov*ember* *1929* Weduwe L. Bosman-Ossedrijver, winkelierster
Rotterdam, **Jan Sonjéstraat** 17. R*echter-*c*ommissaris*
mr. J.G. Huyser; cur*ator* mr. Koderitsch, Rotterdam.
