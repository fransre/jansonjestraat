---
toegevoegd: 2024-07-24
gepubliceerd: 1958-12-31
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000356078:mpeg21:a00243"
onderwerpen: [reclame]
huisnummers: [23]
achternamen: [hendrikse]
kop: "C.C.H. Hendrikse"
---
# C.C.H. Hendrikse

Electrotechnisch Bur*eau*
**Jan Sonjéstraat** 23 — R*otter*dam

G.N.
