---
toegevoegd: 2024-08-10
gepubliceerd: 1925-02-03
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495013:mpeg21:a0037"
onderwerpen: [woonruimte, tehuur]
huisnummers: [21]
kop: "achterkamer"
---
Ongem*eubileerde* Voor- of

# achterkamer

met Sl*aap*gelegenheid,
voor één of twee Dames,
Echtpaar. **Jan Sonjéstraat** 21b.
