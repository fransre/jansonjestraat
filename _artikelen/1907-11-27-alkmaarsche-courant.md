---
toegevoegd: 2021-05-03
gepubliceerd: 1907-11-27
decennium: 1900-1909
bron: Alkmaarsche Courant
externelink: "https://kranten.archiefalkmaar.nl/issue/ACO/1907-11-27/edition/0/page/2"
onderwerpen: [brand]
huisnummers: [27]
achternamen: [hofman]
kop: "Brand"
---
# Brand

~~Gisternacht *26 november 1907* is een uitslaande brand uitgebroken in
de Bagijnestraat te Rotterdam, in gebruik bij de loodgietersfirma
L. Sinjorgo. De bovenverdieping is geheel
uitgebrand. De schade is aanzienlijk, doch wordt door
verzekering gedekt.~~

De hofmeester L. Hofman, te wiens huize in de
**Jan Sonjéstraat** te Rotterdam Maandagavond *25 november 1907* tijdens de
afwezigheid der bewoners brand woedde, heeft bij de
politie aangifte gedaan dat bij dien brand een bedrag van
ƒ240 aan bankpapier is gestolen. Door het springen
van een petroleumlamp, die brandend achtergelaten
was, zou de brand ontstaan zijn.
