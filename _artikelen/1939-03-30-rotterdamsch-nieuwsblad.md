---
toegevoegd: 2024-07-30
gepubliceerd: 1939-03-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164700026:mpeg21:a00051"
onderwerpen: [woonruimte, tehuur]
huisnummers: [29]
kop: "Te huur:"
---
# Te huur:

Benedenhuis, 3 Kamers,
Warande. Tuin, grote
hoge Kelder, **Jan Sonjéstraat** 29b.
Bevragen
27 of 29a.
