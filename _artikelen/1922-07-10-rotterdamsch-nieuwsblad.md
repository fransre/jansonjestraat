---
toegevoegd: 2024-08-11
gepubliceerd: 1922-07-10
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494392:mpeg21:a0058"
onderwerpen: [bedrijfsruimte]
kop: "Te huur"
---
# Te huur

aangeboden voor berging
**Jan Sonjéstraat**,
betonnen parterreruimte
aan de straat droog.
Br*ieven* Bur*eau* N*ummer* 6057.
