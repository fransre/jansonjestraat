---
toegevoegd: 2024-08-14
gepubliceerd: 1907-06-18
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196702:mpeg21:a0066"
onderwerpen: [werknemer]
huisnummers: [18]
achternamen: [stooker]
kop: "Werkster"
---
Er biedt zich aan een

# Werkster

van buiten, genegen Winkel
of Kantoor schoon te
houden. Adres N. Stooker,
**Jan Sonjéstraat** 18
bij de Middellandstraat.
