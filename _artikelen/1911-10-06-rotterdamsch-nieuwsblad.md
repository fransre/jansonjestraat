---
toegevoegd: 2024-08-14
gepubliceerd: 1911-10-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010200113:mpeg21:a0058"
onderwerpen: [familiebericht]
huisnummers: [15]
achternamen: [kolloeffel]
kop: "Christian Jacob Kollöffel,"
---
Heden overleed tot onze diepe
droefheid, zacht en kalm, na
een kortstondige ziekte, onze
goede Echtgenoot, Vader, Behuwd- en
Grootvader, de Heer

# Christian Jacob Kollöffel,

in den ouderdom van ruim
70 jaren.

Rotterdam M. Kollöffel-Beyderwellen.

Amsterdam F.W.C. Kollöffel.

M.P. Kollöffel-van Eden.

Rotterdam C.J.W. Kollöffel.

H.W. Kollöffel.

D.P. Kollöffel.

A.W. Kollöffel

en Kleinkinderen

Rotterdam, 4 October 1911.

**Jan Sonjéstraat** 15c.

Eenige en algemeene kennisgeving.

26
