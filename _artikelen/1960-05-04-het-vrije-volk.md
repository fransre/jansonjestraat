---
toegevoegd: 2024-07-24
gepubliceerd: 1960-05-04
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951084:mpeg21:a0060"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [23]
achternamen: [mulders]
kop: "kinderwagen,"
---
Z*o* g*oed* a*ls* n*ieuwe* Mutsaerts

# kinderwagen,

compl*eet*. A. Mulders, **Jan Sonjéstraat** 23b.
