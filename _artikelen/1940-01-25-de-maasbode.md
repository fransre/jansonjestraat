---
toegevoegd: 2021-04-29
gepubliceerd: 1940-01-25
decennium: 1940-1949
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011070287:mpeg21:a0122"
onderwerpen: [familiebericht]
huisnummers: [8]
achternamen: [van-teeffelen]
kop: "Paulus Petrus Josephus van Teeffelen"
---
Algemeene kennisgeving.

Heden overleed tot onze diepe droefheid in het S*in*t
Franciscusgasthuis na een kortstondig lijden, voorzien van de
H*eilige* Sacramenten der Stervenden, onze innig geliefde zorgzame
Echtgenoot, Vader en Behuwdvader, Broeder en Behuwdbroeder
de Heer

# Paulus Petrus Josephus van Teeffelen

in den ouderdom van 53 jaren.

Rotterdam, 23 Januari 1940.

**Jan Sonjéstraat** 8b.

Mevr*ouw* Wed*uwe* J.A. van Teeffelen-Langendam.

M.G.L. van Teeffelen.

G.C. van Teeffelen-Conijn.

Riet

Tonnij

Joke

Massy

Toos

Ada en Paultje.

De Uitvaartdiensten zullen gehouden worden in de
parochiekerk van de H*eilige* Elisabeth, Mathenesserlaan, a*an*s*taande* Zaterdag,
27 Januari *1940*. De stille H*eilige* Missen te half acht en acht uur
de gezongen H*eilige* Mis van Requiem te 9½ uur, waarna de begrafenis
vanuit de Kerk op de R*ooms* K*atholieke* Begraafplaats aan den
Crooswijkscheweg te ongeveer elf uur.

Wij bevelen de ziel van den overledene in uwe godvruchtige
gebeden aan.

Bidden in de kapel van het Gasthuis, Vrijdagavond *26 januari 1940* te 6 uur.

Geen bezoek

Geen bloemen

Gaarne H*eilige* Missen.
