---
toegevoegd: 2024-08-04
gepubliceerd: 1934-04-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164670059:mpeg21:a00085"
onderwerpen: [brand]
huisnummers: [38]
achternamen: [chaarritter]
kop: "Brandjes."
---
Onder toezicht van den brandmeester, den
heer N.D. de Oude, hebben gasten van slangenwagen 56
gisteravond *22 april 1934* kwart vóór acht met de
gummislang een begin van brand gebluscht
bij J. Chaarritter, aan de **Jan Sonjéstraat** 38.
