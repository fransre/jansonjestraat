---
toegevoegd: 2024-08-14
gepubliceerd: 1907-07-18
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196872:mpeg21:a0076"
onderwerpen: [werknemer]
huisnummers: [18]
achternamen: [kool]
kop: "een Werkster,"
---
Er biedt zich aan:

# een Werkster,

van buiten, genegen Winkel
of Kantoor te doen.
Aan hetzelfde Adres kunnen
nog een paar netto
Wasschen geplaatst worden.
Kool, **Jan Sonjéstraat** 18.
