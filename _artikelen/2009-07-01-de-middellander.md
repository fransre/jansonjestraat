---
toegevoegd: 2023-01-08
gepubliceerd: 2009-07-01
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2009-07-01-de-middellander-origineel.jpg"
onderwerpen: [actie]
kop: "Bewoners Jan Sonjéstraat zitten niet bij de pakken neer"
---
# Bewoners **Jan Sonjéstraat** zitten niet bij de pakken neer

Bewoners uit de **Jan Sonjéstraat**
trokken begin dit jaar
aan de bel. In hun ogen gingen
de straat en de buurt hard
achteruit. Containers werden
in de avonduren en 's nachts
leeggehaald en alle rommel
werd op straat gedumpt.
Drugsdealers en -gebruikers
zorgden voor toenemende
overlast. Met regelmaat werden
eigendommen vernield of
gestolen. Parkeerdruk en
lawaai waren fors toegenomen.
Al met al zagen de bewoners
hun buurt verloederen.

In maart *2009* kwamen de burgemeester
en stadsmarinier poolshoogte
nemen in de straat. Het
bleef niet bij een luisterend oor alleen,
maar een aantal zaken werden
direct voortvarend aangepakt.
Zo werden de oude containers
snel vervangen door nieuwere en
betere
exemplaren.
Met
bewoners
werden
vervolgafspraken
gemaakt.
Bewoners
en diensten
schouwen
regelmatig
met elkaar. Samen lopen ze door
de buurt en brengen misstanden in
kaart en bespreken mogelijke oplossingen
voor problemen.
Ook met de winkeliersvereniging
van de Middellandstraat is contact
gezocht en is een eerste gezamenlijk
overleg gevoerd. Samen
probeert men nu het tij te keren en
de wens is om in de toekomst
nauw samen te gaan werken. Een
van de eerste stappen die ze gaan
zetten, is een gezamenlijke
schouw op de 1e Middellandstraat
op 7 juli *2009*.
Om de problemen te keren is in de
maanden april *2009* en mei *2009* Actie Zonneschijn
gehouden in de buurt.
Diensten als Roteb en
Gemeentewerken hebben in die
periode extra werkzaamheden
verricht en Stadstoezicht en politie
hebben extra controles verricht om
de overlast terug te dringen.
Bewoners zien de laatste maanden
verbeteringen, maar er is nog
een lange weg te gaan. Een weg
met vele hobbels, maar de betrokkenen
zijn bereid die met elkaar te
gaan nemen.

De inzet van de bewoners uit de
**Jan Sonjéstraat** kan een voorbeeld
zijn voor
andere
straten en
gebieden.
Zij zijn niet
bij de pakken
neer
gaan zitten,
maar
hebben
zich ingespannen
voor hun buurt. En natuurlijk werkt
dat alleen als die inspanningen
ook gehoor vinden. In dit geval is
dat zondermeer gelukt bij de
stadsmarinier, Marcel Dela Haije,
en zijn team. De tot nu toe behaalde
resultaten zijn voor bewoners
aanleiding om door te gaan.
