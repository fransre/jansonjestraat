---
toegevoegd: 2024-07-24
gepubliceerd: 1955-10-15
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952337:mpeg21:a0099"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Meubelmaker"
---
# Meubelmaker

en halfw*as*, kunnende
lakken en spuiten. Meubelfabriek
„Jehovu”, **Jan Sonjéstraat** 36, Tel*efoon* 38468.
