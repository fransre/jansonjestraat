---
toegevoegd: 2024-07-21
gepubliceerd: 1982-04-08
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002987007:mpeg21:a00223"
onderwerpen: [woonruimte, tekoop]
huisnummers: [33]
kop: "Pand"
---
R*otter*dam. **Jan Sonjéstraat** 33a+b.
Pand w*aar*v*an* schitt*erend*
verb*ouwde* ruime bov*en*won*ing* met c*entrale* v*erwarming*
en luxe badk*amer* leeg. Ben*eden*won*ing*
verhuurd voor ƒ3378,— p*er* j*aar*.
Vr*aag*pr*ijs* ƒ95.000,— k*osten* k*oper*. Almaco
makelaardij, Mathenesserlaan 220,
R*otter*dam. 010-770277
b*ij* g*een* g*ehoor* 010-253668.
