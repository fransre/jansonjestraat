---
toegevoegd: 2024-07-24
gepubliceerd: 1960-07-01
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951199:mpeg21:a0115"
onderwerpen: [werknemer]
huisnummers: [12]
achternamen: [dirven]
kop: "pijpenbuigers"
---
Reparatie en montagebedrijf Remont

Voor spoedige indiensttreding gevraagd:

# pijpenbuigers

geheel zelfstandig kunnende werken.

Aanmelden: zaterdagmiddag *2 juli 1960* tussen 16-18 uur, J. van Dirven,
**Jan Sonjéstraat** 12, Rotterdam.
