---
toegevoegd: 2024-08-03
gepubliceerd: 1935-09-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164679010:mpeg21:a00078"
onderwerpen: [huispersoneel]
huisnummers: [3]
achternamen: [sterk]
kop: "Huishoudster."
---
# Huishoudster.

Bied zich aan Juffrouw,
33 jaar, ook genegen
in Zaak behulpzaam
te zijn. Br*ieven*
Sterk, **Jan Sonjéstraat** N*ummer* 3b.
