---
toegevoegd: 2024-08-02
gepubliceerd: 1938-10-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164697038:mpeg21:a00067"
onderwerpen: [werknemer]
huisnummers: [15]
achternamen: [spring-in-t-veld]
kop: "Chauffeur."
---
# Chauffeur.

Monteur b*iedt* z*ich* a*an*, Binnen- en
Buitenland, Bus of
Vracht, 12-jarige practijk,
loon n*aar* overeenkomst.
Spring in 't veld, L.H.
**Jan Sonjéstraat** 15,
Tel*efoon* 30682.
