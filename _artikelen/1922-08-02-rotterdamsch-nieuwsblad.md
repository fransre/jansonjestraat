---
toegevoegd: 2024-08-11
gepubliceerd: 1922-08-02
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494412:mpeg21:a0150"
onderwerpen: [familiebericht]
huisnummers: [23, 29]
achternamen: [koomans, groenewold]
kop: "Lena Koomans en Wim Groenewold."
---
Verloofd:

# Lena Koomans en Wim Groenewold.

Rotterdam,

**Jan Sonjéstraat** 23b,

**Jan Sonjéstraat** 29a.
