---
toegevoegd: 2024-07-24
gepubliceerd: 1957-12-30
decennium: 1950-1959
bron: Maassluise courant
externelink: "https://resolver.kb.nl/resolve?urn=MMMAAS01:210154015:mpeg21:a00019"
onderwerpen: [familiebericht]
huisnummers: [35]
achternamen: [marcus]
kop: "M. Marcus"
---
Familie, vrienden en bekenden een gelukkig 1958.

# M. Marcus

**Jan Sonjéstraat** 35b Rotterdam
