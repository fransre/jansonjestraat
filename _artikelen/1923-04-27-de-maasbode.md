---
toegevoegd: 2024-08-11
gepubliceerd: 1923-04-27
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000191397:mpeg21:a0095"
onderwerpen: [bedrijfsinformatie]
huisnummers: [2]
kop: "„Groot Rotterdam”"
---
Rotterdam in woord en beeld!

Leest:

# „Groot Rotterdam”

Geïllustreerd Weekblad voor Rotterdam en omgeving!
Bureaux: Middellandstraat 100, ingang **Jan Sonjéstraat** 2.

Actueel! Boeiend! Veelzijdig!

Iedere week gratis roman-bijvoegsel.

Eerste 10.000 abonné's ontvangen prachtige premieplaat.

Prijs: 15 cent. Bij abonnement: ƒ1.62½ per kwartaal. 12½ cent per week.

Ondergeteekende abonneert zich tot wederopzegging
à ƒ1.62½ per 3 maanden of 12½
cent per week op „Groot Rotterdam”,
geïllustreerd Weekblad.

Duidelijk schrijven s*'il* v*ous* p*laît*.

Naam: …

Adres: …

27303 280
