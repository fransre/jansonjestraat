---
toegevoegd: 2024-05-15
gepubliceerd: 1916-09-27
decennium: 1910-1919
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005420092:00001"
onderwerpen: [reclame]
huisnummers: [24]
achternamen: [bosman]
kop: "V. Bosman,"
---
# V. Bosman,

Papierhandel. — Handelsdrukkerij.

**Jan Sonjéstraat** 24, Rotterdam.

pf.
