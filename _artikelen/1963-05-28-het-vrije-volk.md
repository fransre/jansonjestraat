---
toegevoegd: 2024-07-23
gepubliceerd: 1963-05-28
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954153:mpeg21:a0237"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [smeets]
kop: "Niet opvoeden"
---
# Niet opvoeden

Op de TV werd ons een programma
voorgeschoteld waarin
men liet zien hoe men stieren
fokt voor wellustigen in Spanje.
We mochten nog net zien hoe
zo'n jong beest werd gebrandmerkt,
maar toen het ‘grote’ moment
aanbrak waarop de stier in
de arena moest vechten, werd de
film afgebroken. Dat mochten
we niet zien.

Akkoord. Het is een wreed
schouwspel; maar laat ons, TV-kijkend
Nederland, verschoond
blijven van beelden ‘van mens
tegen mens’. Het is voor onze
kinderen en voor ons huiveringwekkend
en niet bepaald opvoedend.

N.J. Smeets,

**Jan Sonjéstraat** 30a,

Rotterdam.
