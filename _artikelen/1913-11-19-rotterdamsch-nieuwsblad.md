---
toegevoegd: 2024-08-13
gepubliceerd: 1913-11-19
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295863:mpeg21:a0049"
onderwerpen: [ongeval]
huisnummers: [19]
achternamen: [van-de-wetering]
kop: "Stadsnieuws"
---
# Stadsnieuws

Per brancard is naar zijn
woning aan de **Jan Sonjéstraat** N*ummer* 19a
gebracht de ijzerwerker H. v*an* d*e* Wetering,
die aan boord van het s*toom*s*chip* „Bessarabia”,
liggende in het Prins Hendrikdok aan de
Boven Heijplaat, met den linkervoet was
bekneld geraakt tusschen twee ijzeren platen.
