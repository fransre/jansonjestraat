---
toegevoegd: 2024-08-06
gepubliceerd: 1930-07-21
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164648024:mpeg21:a00100"
onderwerpen: [auto, ongeval]
kop: "Man door een auto doodgereden."
---
# Man door een auto doodgereden.

Zaterdagnacht *19 juli 1930* om 1 uur is de 56-jarige
R. Huisman, wonende Tuindersstraat, toen
hij plotseling op de Kruiskade de straat
overstak, aangereden door een auto, bestuurd
door A. D., uit de **Jan Sonjéstraat**.
Per auto van den Geneeskundige Dienst is
hij met een ernstige hoofdwond en een
schedelbreuk naar het Ziekenhuis aan den
Coolsingel vervoerd, waar hij eenige uren
later aan de bekomen verwondingen is
overleden.
