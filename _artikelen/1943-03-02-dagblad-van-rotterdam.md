---
toegevoegd: 2024-07-26
gepubliceerd: 1943-03-02
decennium: 1940-1949
bron: Dagblad van Rotterdam
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010312200:mpeg21:a0013"
onderwerpen: [werknemer]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Halfwas Stoffeerder"
---
# Halfwas Stoffeerder

gevraagd

bij Joh.L. van Hooijdonk,
**Jan Sonjéstraat** 13b, R*otter*dam.
