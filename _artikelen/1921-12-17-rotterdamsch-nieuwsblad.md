---
toegevoegd: 2024-08-12
gepubliceerd: 1921-12-17
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493925:mpeg21:a0129"
onderwerpen: [diefstal]
kop: "De instrumentmaker E.G. L."
---
# De instrumentmaker E.G. L.

wonende **Jan Sonjéstraat**, heeft aangifte
gedaan, dat te zijnen nadeele een jas en
ƒ11 zijn verduisterd; en ten nadeele van
A.H. v*an* T., wonende Aert van Nesstraat
is ƒ15 verduisterd. Van beide gevallen
verdenkt men een bewoner van de **Jan Sonjéstraat**.
