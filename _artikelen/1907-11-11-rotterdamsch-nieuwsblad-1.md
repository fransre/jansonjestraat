---
toegevoegd: 2021-05-18
gepubliceerd: 1907-11-11
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196971:mpeg21:a0125&1"
onderwerpen: [woonruimte, tehuur]
huisnummers: [18]
kop: "Te Huur"
---
**Jan Sonjéstraat** 18.

Met a*an*s*taande* December *1907*

# Te Huur

een lief Benedenhuis, bevat
Voor- en Achterkamer
met Warande, grooten
Kelder, Keuken, aparte
Slaapkamer, 2 Alkoven
en Tuin. Huur ƒ18
per maand. Adres Heemraadssingel
hoek Middellandstraat.
