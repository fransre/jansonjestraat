---
toegevoegd: 2024-08-04
gepubliceerd: 1933-05-31
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164665034:mpeg21:a00136"
onderwerpen: [diefstal]
kop: "Zakkenrollers op de markt."
---
# Zakkenrollers op de markt.

Op de markt op den Goudschesingel blijven
de zakkenrollers op de marktdagen nog steeds
in actie. Van mej*uffrouw* J. K., wonende Verschoorstraat,
werd een portemonnaie met ƒ6 gerold
van mej*uffrouw* G. K., wonende **Jan Sonjéstraat** één
met circa ƒ15 en een aantal gaspenningen.
