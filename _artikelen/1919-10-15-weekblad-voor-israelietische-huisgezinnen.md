---
toegevoegd: 2024-05-16
gepubliceerd: 1919-10-15
decennium: 1910-1919
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005421094:00001"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [kets-de-vries]
kop: "Branco van Dantzich en Jaap Kets de Vries."
---
Verloofd:

# Branco van Dantzich en Jaap Kets de Vries.

Rotterdam,

Roo valkstraat 10a.

**Jan Sonjéstraat** 24b.

15 October 1919.
