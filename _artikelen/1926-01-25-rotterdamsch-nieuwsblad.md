---
toegevoegd: 2024-08-10
gepubliceerd: 1926-01-25
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495258:mpeg21:a0110"
onderwerpen: [bewoner, muziek]
huisnummers: [38]
achternamen: [weier]
kop: "Liefdadigheidsavond"
---
Gebouw „De Eendracht”, Goudscheweg 130.

Excelsior-Bioscoop

Directeur: E.J. Weier, **Jan Sonjéstraat** 38, R*otter*dam.

# Liefdadigheidsavond

op 29 januari *1926* a*an*s*taande*, 8 uur.

De opbrengst wordt geheel ten bate van de noodlijdenden
door den watersnood afgedragen aan het Rotterd*amsch* Comité,

Programma:

1\. Openingswoord van den WelEerw*aarde* Heer Dr. A.F. Krull.

2\. Zang v*an* het Mannenkwart*et* „Con Amore”, leider de Heer J. van der Does.

3\. De Borculo-film.

4\. De Watersnood-film. — H*are* M*ajesteit* de Koningin in de geteisterde streken.

5\. Piano en Viool: Mej*uffouw* Suze Cahen en de Heer Jaap Callenbach.

6\. Sluiting door den WelEerw*aarde* Heer Ds. J.L. van der Wolf.

Kaartenverkoop en plaatsbespreking vanaf Dinsdag 28 Januari *1926*,
telkens van 3-8 uur, aan het Gebouw Goudscheweg 130.

Prijzen: 80, 60 en 40 cent.

De directies,

13611 60
