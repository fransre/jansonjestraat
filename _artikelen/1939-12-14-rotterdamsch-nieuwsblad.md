---
toegevoegd: 2024-07-30
gepubliceerd: 1939-12-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164704037:mpeg21:a00119"
onderwerpen: [huisraad, tekoop]
huisnummers: [46]
achternamen: [warendorp]
kop: "Te koop:"
---
# Te koop:

vaste Waschtafel, compleet,
z*o* g*oed* a*ls* n*ieuw*, maat 49-69.
Vaste prijs ƒ15.
P.F. v*an* Warendorp,
**Jan Sonjéstraat** 46.
