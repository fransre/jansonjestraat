---
toegevoegd: 2024-08-04
gepubliceerd: 1934-06-08
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164671043:mpeg21:a00153"
onderwerpen: [kampeerhuisje, tehuur]
huisnummers: [34]
achternamen: [glas]
kop: "Te huur:"
---
# Te huur:

Schitterend kampeerhuisje,
H*oek* v*an* Holland,
5e Zijstraat N*ummer* 13.
Huiskamer, Slaapkamers
m*et* 4 Bedden, Keuken,
Warande. D. Glas
**Jan Sonjéstraat** 34b.
