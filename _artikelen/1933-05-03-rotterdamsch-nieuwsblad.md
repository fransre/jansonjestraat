---
toegevoegd: 2024-08-04
gepubliceerd: 1933-05-03
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164665003:mpeg21:a00142"
onderwerpen: [bedrijfsinformatie]
huisnummers: [34]
achternamen: [valster]
kop: "Maison „Liesel” Amsterdam,"
---
# Maison „Liesel” Amsterdam,

bericht hiermede, dat

Mevrouw A. Valster,

**Jan Sonjéstraat** 34, Rotterdam, niet
meer voor bovengenoemde Firma
werkzaam is.

Mevr*ouw* L. Preusz.

21748 8
