---
toegevoegd: 2024-07-30
gepubliceerd: 1940-01-08
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002312:mpeg21:a0268"
onderwerpen: [familiebericht]
huisnummers: [9]
achternamen: [van-duin]
kop: "Roelof Cornelis van Duin,"
---
Eenige en algemeene
kennisgeving.

Heden overleed tot onze diepe
droefheid, door een noodlottig ongeval,
mijn lieve Man, der Kinderen
zorgzame Vader, Behuwdvader,
Broeder, Behuwdbroeder en
Oom, de Heer

# Roelof Cornelis van Duin,

in den ouderdom van 46 jaar.

Namens wederzijdsche Familie:

Wed*uwe* W.H.P.M. v*an* Duin-Reichardt.

Rotterdam, 6 Januari 1940.

**Jan Sonjéstraat** 9b.

De teraardebestelling zal plaats
hebben Donderdag 11 Januari *1940* a*an*s*taande*
op de Alg*emene* Begraafplaats Crooswijk.

Vertrek van huis te 2 uur.

10996 28
