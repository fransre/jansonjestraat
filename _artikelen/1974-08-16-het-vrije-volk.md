---
toegevoegd: 2024-03-17
gepubliceerd: 1974-08-16
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958539:mpeg21:a0055"
onderwerpen: [woonruimte, tekoop]
kop: "pand Jan Sonjéstraat"
---
R*otter*dam, pand **Jan Sonjéstraat**,
vrij bov*en*huis per
1-10 leeg, ben*eden*won*ing* verhuurd
voor ƒ147,80 p*er* m*aa*nd. Koopsom
ƒ23.000 k*osten* k*oper*. F*irm*a C. Berrevoets & Z*oo*n
Tel*efoon* 010-370311.

R14
