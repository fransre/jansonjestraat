---
toegevoegd: 2024-08-14
gepubliceerd: 1911-09-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010200215:mpeg21:a0079"
onderwerpen: [werknemer]
huisnummers: [8]
kop: "loopjongen"
---
Gevraagd: een nette

# loopjongen

en een Meisje, voor het maken
van haarwerken en verpakken van
flesschen. **Jan Sonjéstraat** 8a.

38531 5
