---
toegevoegd: 2024-08-14
gepubliceerd: 1907-02-13
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196600:mpeg21:a0089"
onderwerpen: [huispersoneel]
huisnummers: [9]
kop: "Gevraagd:"
---
# Gevraagd:

een net, handig Dagmeisje
van 's morgens 8 uur tot 2 uur,
circa 16
jaar oud. Adres **Jan Sonjéstraat** N*ummer* 9, beneden.
