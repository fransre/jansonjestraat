---
toegevoegd: 2021-04-29
gepubliceerd: 1914-05-27
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295520:mpeg21:a0023"
onderwerpen: [brand]
huisnummers: [20]
achternamen: [wolters]
kop: "Brand."
---
# Brand.

Door het achteloos wegwerpen van een
lucifer ontstond gisteravond *26 mei 1914* 10 uur brand
in de woning van den controleur D.J. Wolters
aan de **Jan Sonjéstraat** n*ummer* 20b,
waardoor de gordijnen op de eerste verdieping
vlam vatten. Personeel van spuit
n*ummer* 31 bluschte het vuur onder toezicht
van den brandspuitmeester, den heer J. Zuidervaart.
Aanwezig waren de spuiten
31, 32, brigade, 28 en 49.
