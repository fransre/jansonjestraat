---
toegevoegd: 2021-04-29
gepubliceerd: 1962-06-08
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010953858:mpeg21:a0269"
onderwerpen: [ongeval, auto]
kop: "Bromfietser eindigt in winkelruit"
---
# Bromfietser eindigt in winkelruit

(Van een onzer verslaggevers)

Met een diepe wonde aan de hals
moest donderdagavond *7 juni 1962* de achttienjarige
kantoorbediende G. de Vries
uit de Sourystraat naar het Dijkzigtziekenhuis,
nadat hij in de **Jan Sonjéstraat**
met zijn bromfiets tegen
de winkelruit van een kledingmagazijn
was opgegaan. Hij verloor
in een bocht de macht over het
stuur. De ruit brak.
