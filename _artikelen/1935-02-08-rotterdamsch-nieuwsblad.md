---
toegevoegd: 2024-08-04
gepubliceerd: 1935-02-08
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164675044:mpeg21:a00170"
onderwerpen: [woonruimte, tehuur]
huisnummers: [4]
kop: "Te huur:"
---
# Te huur:

mooie ongem*eubileerde* Voork*amer*
en Slaapk*amer* met gebr*uik*
van K*euken*, voor Juffr*ouw*.
**Jan Sonjéstraat** 4a.
