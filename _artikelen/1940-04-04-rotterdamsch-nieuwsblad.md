---
toegevoegd: 2024-07-29
gepubliceerd: 1940-04-04
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002382:mpeg21:a0096"
onderwerpen: [huispersoneel]
huisnummers: [18]
kop: "gevraagd,"
---
Meisje

# gevraagd,

's middags 2-8 uur, bij
2 schoolgaande Kinderen,
tevens licht huishoudelijk
werk, v*anaf* direct.
**Jan Sonjéstraat**
N*ummer* 18b.
