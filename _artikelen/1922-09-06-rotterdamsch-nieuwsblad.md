---
toegevoegd: 2024-08-11
gepubliceerd: 1922-09-06
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494440:mpeg21:a0100"
onderwerpen: [huispersoneel]
huisnummers: [46]
achternamen: [van-loon]
kop: "dienstbode"
---
Gevraagd in klein
Gezin een flinke

# dienstbode,

voor dag en nacht, goed
kunnende koken. Zonder
goede getuigen onnoodig
zich aan te melden.
Mevrouw Van Loon,
**Jan Sonjéstraat** 46,
hoek Schermlaan.
