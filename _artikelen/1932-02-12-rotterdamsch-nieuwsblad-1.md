---
toegevoegd: 2024-08-05
gepubliceerd: 1932-02-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164657047:mpeg21:a00118"
onderwerpen: [familiebericht]
huisnummers: [3]
achternamen: [budding]
kop: "Jan."
---
Heden overleed plotseling door
een droevig ongeval onze innig
geliefde Broer en Zwager

# Jan.

Zijn diepbedroefde Zuster
en Zwager,

E. Budding-Elshout.

H. Budding en Kind.

Rotterdam, 11 Febr*uari* 1932.

**Jan Sonjéstraat** 3.

16
