---
toegevoegd: 2024-08-14
gepubliceerd: 1912-10-12
decennium: 1910-1919
bron: Nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMMHW01:000989059:mpeg21:a00060"
onderwerpen: [reclame, schilder]
huisnummers: [23]
achternamen: [roomans]
kop: "Schilderschool J. Roomans,"
---
# Schilderschool J. Roomans,

1899 Opgericht 1899.

4 november *1912* a*an*s*taande* aanvang der driemaandelijksche dag- en avondcursus in het

Hout-, Marmer-, Decoratie- en Letterschilderen.

Vraagt inlichtingen **Jan Sonjéstraat** 23b, Rotterdam.

4950 24
