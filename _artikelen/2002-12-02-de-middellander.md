---
toegevoegd: 2023-01-09
gepubliceerd: 2002-12-02
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2002-12-02-de-middellander-origineel.jpg"
onderwerpen: [vegen]
kop: "Veegactie Jan Sonjéstraat"
---
# Veegactie **Jan Sonjéstraat**

Bewoners uit de **Jan Sonjéstraat** organiseerden in samenwerking
met de Roteb op 2 december *2002* een grote schoonmaakaktie
in de straat.
De straat werd deze dag autovrij gemaakt zodat de Roteb
nu eens echt de hele straat lekker kon schoonvegen en
-spuiten. En dat gebeurde dan ook. Met een hogedrukspuit,
veegwagen en ander materieel rukte de Roteb uit
waardoor de straat blinkend werd schoon gemaakt. Een
aantal bewoners wilden de handen uit de mouwen steken,
maar eigenlijk was dat niet echt nodig. Bewoners ontvingen
de medewerkers van de Roteb met koffie en gebak en
na afloop werden ze onthaald op soep en brood.

De aktie heeft ook tot doel de sociale contacten in de
straat verder te verbeteren. Overigens worden er al regelmatig
veegacties in de straat gehouden en staat de **Jan Sonjéstraat**
bekend om de vele activiteiten en de grote
betrokkenheid van de bewoners.
Ook de Snellinckstraat is gestart met regelmatige veegacties
door bewoners.
Goed voorbeeld doet volgen.
