---
toegevoegd: 2024-08-02
gepubliceerd: 1937-12-08
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692033:mpeg21:a00031"
onderwerpen: [woonruimte, tehuur]
huisnummers: [23]
kop: "Vrij bovenhuis"
---
# Vrij bovenhuis

**Jan Sonjéstraat** 23, 3 kamers, keuken,
zolder m*et* 2 kamers ƒ32.50 p*er* m*aand*.
Te bevr*agen* Hartmanstraat 11, Telefoon 55556
of 33477.

38467 7
