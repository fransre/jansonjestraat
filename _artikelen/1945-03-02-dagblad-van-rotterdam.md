---
toegevoegd: 2023-10-15
gepubliceerd: 1945-03-02
decennium: 1940-1949
bron: Dagblad van Rotterdam
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010312545:mpeg21:a0010"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Meubelmakers en jongens"
---
Gevraagd:

# meubelmakers en jongens

meubelfabriek „Jehovu”.

**Jan Sonjéstraat** 36 — R*otter*dam.
