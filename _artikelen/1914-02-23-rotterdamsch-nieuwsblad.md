---
toegevoegd: 2024-08-13
gepubliceerd: 1914-02-23
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295445:mpeg21:a0240"
onderwerpen: [werknemer]
huisnummers: [28]
kop: "Strijksters."
---
# Strijksters.

Terstond gevraagd
een Strijkster en een
leerlingstrijkster. Tevens
een Meisje voor alle
voorkomende werkzaamheden.
Loon naar
bekwaamheid. Wasch- en
Strijkinrichting **Jan Sonjéstraat** 28a.
