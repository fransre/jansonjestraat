---
toegevoegd: 2024-08-13
gepubliceerd: 1919-01-03
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010995611:mpeg21:a0111"
onderwerpen: [familiebericht]
huisnummers: [16]
achternamen: [sneepels]
kop: "Johannes Chr. Sneepels,"
---
In plaats van kaarten.

Heden overleed na een langdurig
lijden, zacht en kalm, onze
geliefde Echtgenoot, Vader,
Broeder, Behuwdbroeder
en Oom, de Heer

# Johannes Chr. Sneepels,

in den ouderdom van 51 jaar.

Mede namens verdere Familie,

Wed*uwe* E. Sneepels-Haas.

Rotterdam, 1 Januari 1919.

**Jan Sonjéstraat** 16B.

De teraardebestelling zal
plaats hebben Zaterdag 4 Jan*uari* *1919*
ten 1 ure vanaf het sterfhuis
op de Alg*emene* Begraafplaats Crooswijk.

23
