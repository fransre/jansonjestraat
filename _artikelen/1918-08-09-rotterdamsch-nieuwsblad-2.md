---
toegevoegd: 2024-08-13
gepubliceerd: 1918-08-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297083:mpeg21:a0121"
onderwerpen: [familiebericht]
huisnummers: [28]
achternamen: [de-mink]
kop: "Marie H. Hoogstraaten en J. de Mink."
---
Verloofd:

# Marie H. Hoogstraaten en J. de Mink.

Hendrik Sorchstr*aat* 17b.

**Jan Sonjéstraat** 28B

Rosier Faassenstr*aat* 9

R*otter*dam Aug*ustus* 1918

9
