---
toegevoegd: 2024-08-13
gepubliceerd: 1917-08-03
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296699:mpeg21:a0131"
onderwerpen: [reclame]
huisnummers: [38]
kop: "ijspudding — ijstaarten — ijswafels"
---
Na den Maaltijd,
op Verjaardagen, Bruiloften,
Kinderfeesten.

# ijspudding — ijstaarten — ijswafels

van de

R*otter*damsch IJsinrichting.

Opgericht door Vereenigde Banketbakkers.

**Jan Sonjéstraat** 38.

44329 81
