---
toegevoegd: 2024-08-13
gepubliceerd: 1916-03-10
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296259:mpeg21:a0084"
onderwerpen: [woonruimte, tekoop]
huisnummers: [27, 29]
kop: "Publieke Verkoopingen."
---
# Publieke Verkoopingen.

in het Notarishuis aan de Gelderschekade
te Rotterdam.

Woensdag 8 maart *1915*.

~~Definitieve Afslag:~~

~~Dubbel Pand met Open Plaats en Erve,
Feijenoordstraat n*ummer* 6, trekg*eld* ƒ10.400.
Daarop opgehouden.~~

Door de Notarissen Schrameier
Verbrugge & Maasgeesteranus,
Wijnhaven 13.

Pand. Open Plaatsje en Erve, **Jan Sonjéstraat** n*ummer* 27a en b,
trekg*eld* ƒ6200.

Idem, idem, idem n*ummer* 29a en b, trekg*eld*
ƒ6300.

Te zamen voor ƒ12.720 verkocht.
