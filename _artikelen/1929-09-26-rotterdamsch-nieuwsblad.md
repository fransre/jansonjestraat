---
toegevoegd: 2024-08-06
gepubliceerd: 1929-09-26
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164644101:mpeg21:a00042"
onderwerpen: [reclame, dans]
huisnummers: [38]
kop: "Academie de danse „Artistique”"
---
# Academie de danse „Artistique”

**Jan Sonjéstraat** 38, Tel*efoon* 30854.

Begin October *1929* aanvang der lessen op alle avonden.

Ook speciale R*ooms* K*atholieke* Clubs op Dinsdag 1 Oct*ober* *1929* van half 9 tot half 11 en
op Zondag 6 Oct*ober* *1929* van 4 uur.

Op Vrijdag 4 October *1929* uitsluitend Kantoorpersoneel.

Inl*ichtingen* Prosp*ectus* en Inschrijving dagelijks van 10-10 uur.

Privélessen dagelijks op elk gewenscht uur met Dames-assistentie.

Iederen zaterdag en zondag bal 8-12 u*ur*

3 tango accordions

50794 30
