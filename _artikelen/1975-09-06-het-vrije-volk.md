---
toegevoegd: 2023-01-21
gepubliceerd: 1975-09-06
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958864:mpeg21:a0017"
onderwerpen: [cafe, werknemer]
huisnummers: [15]
kop: "Buffetjuffr."
---
# Buffetjuffr.

gevr*aagd* 3 dagen
p*er* w*eek*. Café Intiem **Jan Sonjéstraat** 15,
tel*efoon* 010-773676.

R1
