---
toegevoegd: 2024-02-22
gepubliceerd: 2001-03-10
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2001-03-10-de-middellander-2-origineel.jpg"
onderwerpen: [actie]
achternamen: [poot]
kop: "Het begint met elkaar te groeten"
---
# Het begint met elkaar te groeten

Middelland weer in de ban van Opzoomeren

„Oh ja, dat lukt ons wel, geen probleem”. Mijnheer Poot
uit de **Jan Sonjéstraat** kijkt bijna triomfantelijk. Een
straatdîner voor minstens vijftig bewoners, daar draait
de **Jan Sonjéstraat** z'n hand niet voor om. „We hebben
dat al eens eerder gedaan”, zegt mijnheer Poot, „alle
bewoners hadden iets klaargemaakt. We zaten met z'n
allen aan lange tafels, het was heel erg gezellig.”

De ruimte van de bewonersorganisatie is
omgetoverd tot Opzoomercafé. Actieve
opzoomeraars zijn uitgenodigd om onder
het genot van een drankje en een hapje te
praten over de Opzoomercampagne 2001.
Ook dit jaar weer de gebruikelijke straatacties.
Elke deelnemende straat of groep
ontvangt een zogenaamde Eurobon die
123 euro's waard is om hun straat of
buurt mooier, leuker, veiliger en vooral
gezelliger te maken.

Daarnaast kunnen straten zich opgeven
om mee te doen aan een groot straatdîner.
Opzoomer Mee zorgt voor stoelen en
tafels en voor een budget om boodschappen
te doen. Alleen straten die echt denken
ruim vijftig bewoners uit verschillende
culturen aan tafel te krijgen maken een
kans om hieraan mee te mogen doen. Een
andere voorwaarde is dat ze ook een bijzondere
gast moeten uitnodigen.

## Coalities

Tijdens het Opzoomercafé worden ook
coalities gesloten. De bewoners van de
Vliegerstraat en de Lieve Verschuierstraat
komen in gesprek met de bewoners van
het 55+ complex ‘Lieve Vlieger’, dat net
om de hoek staat. „We hebben jullie al
eens vaker uitgenodigd om bij onze vergaderingen
te zijn,” aldus een bewoonster
van de Lieve Verschuierstraat, „ik gooi
altijd een aankondiging van onze vergadering
bij jullie in de bus.” „Kunnen we dan
niet mee doen met zo'n straatdîner?”,
vraagt een bewoner van het 55+ complex.
Even later zitten de groepen met elkaar
om tafel om plannen te smeden.

## Vele gezichten

Voor kindergroepen is de uitdaging dit jaar
om een ontdekkingsreis door *(de)* eigen wijk te
organiseren, als moderne wereldverkenners
op speurtocht in de buurt. De kindergroep
moet verschillende culturen en
generaties in kaart brengen en beter leren
kennen. Dat kan rond een bepaald thema,
zoals bijvoorbeeld muziek. Het team gaat,
met een begeleider, op bezoek bij instellingen
en organisaties die de vele gezichten
van de buurt vertegenwoordigen. Dat kan
bijvoorbeeld een ouderenflat zijn of het
Multi Cultureel Centrum Middelland, een
Marokkaans eethuisje, de Chineze ouderenlocatie
aan de Graaf Florisstraat of een
kinderdagverblijf.

Ook sportactiviteiten maken dit jaar weer
deel uit van de Opzoomercampagne en
natuurlijk het eindejaarsfeestje rond kerst
of rond het suikerfeest.

## Leefbare straat

Middelland gaat weer opzoomeren. Een
straat waar bewoners elkaar groeten en
kennen is een straat waar bewoners zich
vertrouwd voelen, zich veilig voelen en eerder
bereid zijn om samen te werken aan
een leefbare straat.

Straten of groepen die interesse hebben of
meer van de Opzoomercampagne 2001
willen weten, kunnen bellen naar
de bewonersorganisatie,
telefoonnummer: 4256898,
vragen naar Ireen van der Lem.
