---
toegevoegd: 2024-07-26
gepubliceerd: 1946-09-18
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955025:mpeg21:a0026"
onderwerpen: [kleding, tekoop]
huisnummers: [23]
kop: "2 paar schoenen maat 38,"
---
# 2 paar schoenen m*aat* 38,

zgan, 1 paar bruin,
1 paar zwart, en een
zwarte damesmantel.
**Jan Sonjéstraat** 23b.
