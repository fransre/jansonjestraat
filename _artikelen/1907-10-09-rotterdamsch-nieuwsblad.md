---
toegevoegd: 2024-08-14
gepubliceerd: 1907-10-09
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196943:mpeg21:a0130"
onderwerpen: [woonruimte, tehuur]
huisnummers: [10]
kop: "Terstond te huur"
---
# Terstond te huur

in de **Jan Sonjéstraat** N*ummer* 10,
een Benedenhuis
met Tuin, bevattende 3
Kamers, Keuken en Souterrain.
Te bevragen N*ummer* 16
beneden en bij den
Eigenaar Jacob Loisstraat 17.
