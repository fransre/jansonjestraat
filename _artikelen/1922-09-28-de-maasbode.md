---
toegevoegd: 2024-08-11
gepubliceerd: 1922-09-28
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000190657:mpeg21:a0021"
onderwerpen: [familiebericht]
huisnummers: [38]
achternamen: [vriens]
kop: "Joannes C.M. Vriens en Joanna H. Lap"
---
Getrouwd:

# Joannes C.M. Vriens en Joanna H. Lap

die, mede namens wederzijdsche
familie dank zeggen voor de vele
blijken van belangstelling bij hun
huwelijk ondervonden.

Rotterdam, **Jan Sonjéstraat** 38a,
28 September 1922.

51429 10
