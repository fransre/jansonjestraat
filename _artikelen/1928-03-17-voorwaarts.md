---
toegevoegd: 2024-08-07
gepubliceerd: 1928-03-17
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010211923:mpeg21:a0051"
onderwerpen: [huisraad, tekoop]
huisnummers: [41]
achternamen: [van-veen]
kop: "Te koop"
---
# TE koop

Vloerkleed 3×4 ƒ8.— en
een buffetkastje ƒ8.—,
Van Veen, **Jan Sonjéstraat** 41a.
