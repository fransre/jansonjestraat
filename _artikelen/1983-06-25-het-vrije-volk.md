---
toegevoegd: 2021-04-29
gepubliceerd: 1983-06-25
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010961411:mpeg21:a0175"
onderwerpen: [woonruimte, tekoop]
huisnummers: [22]
kop: "Rotterdam,"
---
T*e* k*oop*

# Rotterdam,

**Jan Sonjéstraat** 22,
pand best*aande* uit
verhuurde benedenwoning
(opbrengst ƒ3100,— per j*aa*r)
en leeg te aanvaarden bovenwoning
met vrije opgang. Ind*eling*:
Woonk*amer*, keuken, slaapk*amer*, balkon,
2 zolderslaapk*amers*, douche
en hobbyruimte. Vr*aag*pr*ijs*
ƒ37.500,— k*osten* k*oper*
Inl*ichtingen* Kats en Waalwijk Beheer onroerend goed b.v.,
tel*efoon* 010-117390.
