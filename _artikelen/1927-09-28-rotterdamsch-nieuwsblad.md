---
toegevoegd: 2024-08-07
gepubliceerd: 1927-09-28
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514103:mpeg21:a0120"
onderwerpen: [woonruimte, tehuur]
huisnummers: [19]
kop: "Te huur:"
---
# Te huur:

ongem*eubileerde* Kamer, met Alcoof
en gebruik van
Keuken. **Jan Sonjéstraat** 19b.
