---
toegevoegd: 2024-08-07
gepubliceerd: 1928-06-12
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514317:mpeg21:a0065"
onderwerpen: [werknemer]
huisnummers: [37]
achternamen: [halk]
kop: "leerlingen"
---
Handige

# leerlingen

gevraagd, voor 1ste
klas Damesmaatwerk,
bij gebleken geschiktheid
hoog salaris. L. Halk-Vreugdewater
**Jan Sonjéstraat** 37.
