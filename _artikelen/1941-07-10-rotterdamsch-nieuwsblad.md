---
toegevoegd: 2024-07-28
gepubliceerd: 1941-07-10
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002871:mpeg21:a0017"
onderwerpen: [verloren]
huisnummers: [26]
achternamen: [mager]
kop: "Verloren:"
---
# Verloren:

g*ouden* broche
Overschie-Rotterdam,
gedachtenis. Tegen bel*oning*
terug bezorgen Mager,
**Jan Sonjéstraat** 26.
