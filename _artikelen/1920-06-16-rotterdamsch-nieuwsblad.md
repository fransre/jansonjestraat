---
toegevoegd: 2024-08-13
gepubliceerd: 1920-06-16
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493974:mpeg21:a0135"
onderwerpen: [diefstal]
kop: "Hokie-pokie."
---
# Hokie-pokie.

Ten nadeele van den roomijsfabrikant
A.C. H., uit de **Jan Sonjéstraat**, zijn een
bedrag van ƒ11 en een koperen bel verduisterd.
