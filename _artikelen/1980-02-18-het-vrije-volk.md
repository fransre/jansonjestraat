---
toegevoegd: 2022-07-25
gepubliceerd: 1980-02-18
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960437:mpeg21:a0128"
onderwerpen: [bulldog]
kop: "Opvanghuis voor Bulldog"
---
# Opvanghuis voor Bulldog

(Van een onzer verslaggeefsters)

Rotterdam — De Stichting De Bulldog
krijgt waarschijnlijk
een eigen pand voor de opvang
van jonge verslaafde meisjes.
Tot nu toe moet De Bulldog
het doen met een gehuurde etage
in de **Jan Sonjéstraat**. De accommodatie
daar is zo slecht dat
al verschillende keren is overwogen
om achter de crisisopvang
een punt te zetten.

Het nieuwe onderkomen aan
de 's-Gravendijkwal is geschikt
voor de opvang van twaalf jongeren.
