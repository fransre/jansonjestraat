---
toegevoegd: 2024-08-13
gepubliceerd: 1917-07-18
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296662:mpeg21:a0144"
onderwerpen: [faillissement, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "Faillissementen in het Arrondissement Rotterdam."
---
# Faillissementen in het Arrondissement Rotterdam.

Verificatiën.

Zaterdag 21 Juli *1917*.

9½ Uur. Delib*eratie* en stemm*ing* acc*oord* faill*issement* E.J. Weier,
schilder, **Jan Sonjéstraat** 38, alhier. Cur*ator*
mr. H. van Wijk, rechtercomm*issaris* mr. C. Star Busmann.
