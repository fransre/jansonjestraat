---
toegevoegd: 2024-07-28
gepubliceerd: 1942-03-18
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003104:mpeg21:a0011"
onderwerpen: [huisraad, radio, tekoop]
huisnummers: [6]
kop: "Te koop"
---
# Te koop

Erres radio, in
g*oede* st*aat*, t*egen* e*lk* a*annemelijk* b*od*. lederen
citybag en bronzen el*ectrische*
lamp. Na 6 uur. **Jan Sonjéstraat** 6B.
