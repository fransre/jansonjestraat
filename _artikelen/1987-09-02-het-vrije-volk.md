---
toegevoegd: 2021-04-29
gepubliceerd: 1987-09-02
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962523:mpeg21:a0228"
onderwerpen: [bellebom]
kop: "‘Waar ligt die vliegtuigbom?’"
---
# ‘Waar ligt die vliegtuigbom?’

Rotterdam — „Bij ons
achter in de tuin van het huis
zag ik duidelijk het gat zitten,
waar die bom de grond in is
gegaan. Het was een krater
van drie bij vier meter,” vertelde
mevrouw Offenberg gisteravond *1 september 1987*.
Met de mededeling
dat er in haar tuin in de Bellevoysstraat
31 in de Rotterdamse
wijk Middelland een Engelse
vliegtuigbom ligt, veroorzaakte
zij veel opschudding.

Bewoners van de Bellevoysstraat
en **Jan Sonjéstraat**
waren gisteravond *1 september 1987* op uitgenodiging
van de gemeente bijeen
in het wijkcentrum om van de
experts van de Explosieven Opruimings Dienst (EOD)
te horen hoe deze het projectiel
gaan opsporen en bergen.

In september 1944 werd een
Engelse bommenwerper boven
Rotterdam getroffen door het
Duitse luchtafweergeschut.
Om gewicht te verliezen dropte
de piloot van het getroffen
vliegtuig twee bommen van
500 pond. De ene bom kwam terecht
in de wasserij van Borgh
en Zonen in de Bellevoysstraat
en de andere in de tuinen tussen
de Bellevoysstraat en de
**Jan Sonjéstraat**. De eerste bom
werd wél teruggevonden, de
tweede niet.

Kapitein L.P.G. de Made van
de EOD: „De grond in de tuinen
is erg zacht. Uit de praktijk
weten we, dat zo'n bom wel
twaalf meter door de grond
kan schieten. Uit onderzoek
blijkt dat het vliegtuig in noordelijke
richting vloog. Zo kan
de bom makkelijk doorgeschoven
zijn tot in de tuin van
nummer 19. Verder kan hij later
nog gezonken zijn, totdat
hij op een harde laag tot stilstand
is gekomen. Die harde
laag zit hier veertien meter
diep.”

Om de bom te vinden moet
360 vierkante meter tuin worden
afgegraven tot een diepte
van een halve meter. Tijdens
de afgraving en de zoekaktie,
die eind september *1987* begint,
kunnen de bewoners van de
omliggende huizen er gewoon
blijven wonen. Om de graafmachines
en vrachtauto's toegang
te verlenen tot het afgesloten
binnenterrein, wordt een
benedenverdieping in de Bellevoysstraat
gesloopt.

Afgraven van de tuinen is
noodzakelijk, omdat in de bovenste
laag veel ijzer afval zit,
dat de metaaldetectors van de
bommenzoekers stoort bij hun
speuraktie.
