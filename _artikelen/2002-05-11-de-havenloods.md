---
toegevoegd: 2023-01-10
gepubliceerd: 2002-05-11
decennium: 2000-2009
bron: De Havenloods
externelink: "https://www.jansonjestraat.nl/originelen/2002-05-11-de-havenloods-origineel.jpg"
onderwerpen: [geraniumdag]
achternamen: [poot, van-der-ploeg, lutgerink]
kop: "Geraniumdag"
---
# Geraniumdag

Op zaterdag 11 mei *2002* vond in de **Jan Sonjéstraat**
weer en gezellige ontmoeting plaats…: de traditionele
Geraniumdag. Een kleurrijk en bloemrijk
feest, dat nu al zo'n acht jaar plaatsvindt.
De straat wordt dan namelijk versierd met hangende
en staande geraniums.
Tegen negen uur komt tuinder Zijlstra (al
jaren verbonden aan de **Jan Sonjéstraat**)
met een kar vol geraniums en aarde zijn
vracht afleveren. Met man en macht wordt
dan keihard gewerkt om de 650 geraniums
en tientallen zakken aarde uit te laden.

## Oud en stekelig

Dan gaan Arjan en Jan Poot (al jááren de
vaste medewerkers in het vullen van de
plantenbakken) aan de slag. Verschillende
bewoners komen geleidelijk met hun bak
vol met oude, stekelige, verdorde, oude
geraniums naar de kraam waar de bakken
worden gevuld. Ondertussen open enkele
andere bewoners (Don, Aad, Eric, Ria,
Vahit, Jan, de fotograaf, Dinant) rond om
andere mensen uit bed te bellen, haken
aan de muren te hangen (alleen daar waar
ze kapot zijn), bakken te controleren op
bruikbaarneid en oudere mensen die nulp
nodig hebben te hulp te schieten.

## Genieten

De straat is op de Geraniumdag een groot
en levendig tafereel. We praten namelijk
over het vullen van 80 bakken met 600
geraniums voor 35 gezinnen, dat zijn ongeveer
100 bewoners. Van de 100 bewoners
hebben er ongeveer 11 een grote
hand in de begeleiding van de Geraniumdag.
Met ongeveer 50 mensen wordt er
koffie en thee gedronken, met zo'n 16 een
glaasje en uiteindelijk genieten er zo'n 200
bewoners van de geraniums, laat staan de
vele mensen die iedere dag door de **Jan Sonjéstraat**
lopen.

## Naborrelen

Tussen de bedrijvigheid door drinken we
koffie en eten we koek en tegen 13.00 uur
gebruiken we gezamenlijk de lunch (bereid
door de bewoners Tineke en Winnie).
Dit jaar was de straat rond 14.00 uur aan
de kant en werd er heerlijk nageborreld en
gepraat over het wel en wee in de straat
en natuurlijk over de politiek.

Via deze weg bedanken we alle bewoners,
de harde werkers, de tuinder, OpzoomerMee,
het wijkteam Middelland en de Bewonersorganisatie Middelland
voor hun medewerking
dit jaar.

Namens de bewoners, Aad Lutgerink

De foto bij dit artikel is genomen door Jan
van der Ploeg.

*Onderschrift bij de foto*
Geraniums, geraniums en nog eens geraniums…
