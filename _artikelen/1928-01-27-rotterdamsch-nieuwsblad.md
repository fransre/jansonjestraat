---
toegevoegd: 2024-08-07
gepubliceerd: 1928-01-27
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514205:mpeg21:a0191"
onderwerpen: [ongeval, auto]
kop: "Tegen een paal gereden"
---
# Tegen een paal gereden

Gisteravond *26 januari 1926* 8 uur is de heer A. B., wonende
**Jan Sonjéstraat**, alhier, met zijn
auto te Schiedam tegen een ijzeren paal
gereden, staande op den hoek Nieuwe Haven-Oranjebrug.
De auto werd ernstig beschadigd.
O*nder* a*ndere* was het rechterwiel vernield,
terwijl de motor defect geraakte.
