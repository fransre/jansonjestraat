---
toegevoegd: 2024-08-02
gepubliceerd: 1937-05-24
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164689018:mpeg21:a00064"
onderwerpen: [werknemer]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "Gevraagd:"
---
# Gevraagd:

een bekwame Strijkster
voor 2 à 3 dagen.
Vast werk. Wasscherij
L. van den Bos,
**Jan Sonjéstraat** 36.
