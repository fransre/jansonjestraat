---
toegevoegd: 2024-08-13
gepubliceerd: 1913-07-19
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295690:mpeg21:a0097"
onderwerpen: [gevraagd]
huisnummers: [38]
kop: "Een Tehuis"
---
# Een Tehuis

gezocht voor 2 Jongens
van 7 en 12 jaar. Protes*tantse*
Godsdienst. Brieven **Jan Sonjéstraat** 38a.
