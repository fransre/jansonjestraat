---
toegevoegd: 2021-04-29
gepubliceerd: 1973-04-13
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958163:mpeg21:a0113"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Club Angelique"
---
Nu terug van weggeweest

# Club Angelique

met daverende shows, films en
meisjes, drankjes vrij.
Van 's middags 2 t*ot* *en* m*et* 5 uur,
ƒ20.—, 's avonds van 8.30 t*ot* *en* m*et* 1
uur, ƒ30.—, ook zaterdags.

Tot ziens in
Club Angelique

**Jan Sonjéstraat** 33, zijstr*aat* 1e
Middellandstr*aat*, tel*efoon* 010-256188.

R16
