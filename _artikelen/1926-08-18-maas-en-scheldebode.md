---
toegevoegd: 2024-05-19
gepubliceerd: 1926-08-18
decennium: 1920-1929
bron: Maas- en Scheldebode
externelink: "https://krantenbankzeeland.nl/issue/mas/1926-08-18/edition/null/page/4"
onderwerpen: [reclame]
huisnummers: [20]
achternamen: [henke]
kop: "Kennisgeving"
---
# Kennisgeving

Door deze deelen wij onze geachte
clientèle mede, dat de firma:

D. Kouwenhoven & Co*mpagnon*

Rotterdam

vertegenw*oordiger* der Dr. Alimonda Electriseermachines,
vanaf heden is ontbonden,
doch op dezelfde voet zal worden voortgezet
door de

F*irm*a gebr*oeder*s Henke

(v*oor*h*een* D. Kouwenhoven & Co*mpagnon*)

Kantoor: **Jan Sonjéstraat** 20b. Postbox 450.
Vertegenw*oordier* der orgineele

Dr. Alimonda

electriseermachines.

Vraagt onze gratis uitgebreide
catalogi!
