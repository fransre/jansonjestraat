---
toegevoegd: 2024-08-09
gepubliceerd: 1926-01-13
decennium: 1920-1929
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000179809:mpeg21:p003"
onderwerpen: [faillissement]
huisnummers: [29]
achternamen: [hoogmolen]
kop: "Faillissementen"
---
# Faillissementen

Uitgesproken.

H.B.A. Hoogmolen, handelsreiziger, **Jan Sonjéstraat** 29,
Rotterdam; arr*ondissements*-rechtb*ank* Rotterdam, 8 Januari 1926;
rechter-comm*issaris* mr. J.E. Rosen Jacobson; cur*ator* mr. W.C.H. van Dillen,
Spaanschekade 3, Rotterdam.
