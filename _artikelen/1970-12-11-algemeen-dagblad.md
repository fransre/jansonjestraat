---
toegevoegd: 2024-07-22
gepubliceerd: 1970-12-11
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002860010:mpeg21:a00302"
onderwerpen: [woonruimte, tekoop]
kop: "Zelfbewoning"
---
# Zelfbewoning

**Jan Sonjéstraat** n*a*b*ij* Middellandstr*aat* Benedenhuis; 3 kamers,
grote kelder, tuin.

Direct leeg te aanvaarden

Bovenhuis verhuurt voor ƒ1350,60 p*er*jaar

Vaste prijs ƒ23.500 k*osten* k*oper*. Lasten ƒ320,37 p*er* j*aa*r

Zo nodig ƒ17.500 hypotheek 8½% aanwezig

Bevragen: Abr. van der Hoeven S*enio*r.

Beatrijsstraat 63, Rotterdam. Tel*efoon* 010-232860.

AD14
