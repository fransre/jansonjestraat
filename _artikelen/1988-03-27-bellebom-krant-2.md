---
toegevoegd: 2023-01-21
gepubliceerd: 1988-03-27
decennium: 1980-1989
bron: Bellebom Krant
externelink: "https://beeldbank.40-45nu.nl/ovmfoto//10008/10008(4).jpg"
onderwerpen: [bellebom]
kop: "Op zoek naar de ‘Bellebom’…"
---
# Op zoek naar de ‘Bellebom’…

*Onderschrift bij de foto*
Van de achtertuintjes in de Bellevoysstraat en de **Jan Sonjéstraat** is niets meer over.
