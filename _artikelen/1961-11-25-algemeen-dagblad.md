---
toegevoegd: 2024-07-23
gepubliceerd: 1961-11-25
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002795022:mpeg21:a00323"
onderwerpen: [reclame]
huisnummers: [34]
achternamen: [raap]
kop: "Foto Raap"
---
# Foto Raap

**Jan Sonjéstraat** 34b, tel*efoon* 37613

voor kinderfoto's aan
huis

6 verschillende met gratis vergroting
ƒ15.
