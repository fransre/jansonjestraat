---
toegevoegd: 2024-08-14
gepubliceerd: 1908-10-21
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197260:mpeg21:a0106"
onderwerpen: [werknemer]
huisnummers: [22]
achternamen: [de-bruin]
kop: "Pakhuis"
---
Een Timmermanshalfwas
vraagt beleefd werk,
ook niet ongenegen in

# Pakhuis

of Fabriek werkzaam te
zijn. Adres De Bruin,
**Jan Sonjéstraat** N*ummer* 22.
