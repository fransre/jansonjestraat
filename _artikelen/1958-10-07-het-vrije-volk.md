---
toegevoegd: 2024-07-24
gepubliceerd: 1958-10-07
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010953258:mpeg21:a0047"
onderwerpen: [huisraad, tekoop]
huisnummers: [8]
achternamen: [blackburn]
kop: "ameublement,"
---
Te koop mooi gotisch

# ameublement,

kloostertafel en
prachtig dressoir. Te bez*ichtigen* na 6 uur
mevr*ouw* G.C. Blackburn, **Jan Sonjéstraat** 8a.
