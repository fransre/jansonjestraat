---
toegevoegd: 2024-08-14
gepubliceerd: 1906-04-21
decennium: 1900-1909
bron: Nieuwe Tilburgsche Courant
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010158228:mpeg21:a0071"
onderwerpen: [diefstal]
kop: "Een ladelichter gesnapt."
---
# Een ladelichter gesnapt.

Woensdagavond *18 april 1906* omstreeks half elf betrapte
de vrouw van den sigarenwinkelier J.W. Wassink,
wiens sigarenmagazijh De Hoop is gevestigd
in de Henegouwerlaan n*ummer* 91, hoek
Middellandstraat te Rotterdam, een man in
haren winkel, die bezig was de toonbanklade te
lichten. Zij greep den dief beet en riep om
hulp. De betrapte dief echter greep een borstel
en sloeg de juffrouw op het hoofd, haar
bovendien stompende en trappende om los te
komen. Dit gelukte hem dan ook, waarop hij
de straat opvluchtte. Op dat oogenblik kwam
daar juist een schrijver bij de politie voorbij
en verwittigd van hetgeen er was voorgevallen
zette hij, bijgestaan door eenige burgers,
den vluchteling na. Nadat men verschillende
straten doorgerend was, kreeg men in de **Jan Sonjéstraat**
den dief te pakken. Hij verzette
zich geducht en kwam vrij gehavend in den politiepost
in de Adrianastraat aan, van waar hij
toen naar het bureau van politie in de Witte de Withstraat
gebracht werd. In zijn bezit
werden papieren gevonden ten name van F.L. van de Langeryt,
geboren 28 Mei 1877 te Hoboken;
voorts een adreskaart van een Bredaasch
logement, van waar hij vermoedelijk
het laatst gekomen is, na als ketelmaker te
Zwijndrecht bij Antwerpen werkzaam te zijn
geweest. De man weigert eenige inlichting te
geven omtrent zijn doen en laten hier en was in
het bezit van ƒ3.50, vermoedelijk uit de toonbanklade
van het sigarenmagazijn De Hoop
gestolen.
