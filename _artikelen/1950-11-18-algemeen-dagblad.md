---
toegevoegd: 2024-07-26
gepubliceerd: 1950-11-18
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259009121:mpeg21:a00118"
onderwerpen: [huisraad, reclame]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Reparatie aan alle soorten meubelen."
---
# Reparatie aan alle soorten meubelen.

De nieuwste stoffen
vanaf ƒ40.— p*er* stel. Joh.L. v*an* Hooijdonk,
**Jan Sonjéstraat** 13b, Tel*efoon* 36612, R*otter*dam.
