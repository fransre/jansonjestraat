---
toegevoegd: 2021-04-30
gepubliceerd: 1907-11-27
decennium: 1900-1909
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1907-11-27/edition/0/page/3"
onderwerpen: [brand]
huisnummers: [27]
achternamen: [hofman]
kop: "Gemengd Nieuws."
---
# Gemengd Nieuws.

Terwijl de bewoner van de bovenwoning
van het pand **Jan Sonjéstraat** 27 de hofmeester
L. Hofman zich gisteravond *26 november 1907* met
zijn vrouw in een Variété bevond, brak brand
uit in een tusschenkamer. Deze brand werd
door de benedenbewoonster J. Waardenburg
ontdekt doordat een gat in de vloer was
gebrand.

De brandweer wist den brand tot de binnenkamer
te beperken, welke echter geheel
uitbrandde.

Hofman was voor ƒ2000 verzekerd.

Volgens zijn aangifte vermiste hij ƒ240
aan bankpapier.
