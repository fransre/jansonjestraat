---
toegevoegd: 2024-07-28
gepubliceerd: 1941-10-31
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002808:mpeg21:a0055"
onderwerpen: [familiebericht]
huisnummers: [5]
achternamen: [oostenveld]
kop: "Jurjen Oostenveld"
---
Heden overleed plotseling, tot
onze groote droefheid, onze lieve
Man en Vader, Behuwdzoon,
Broeder, Behuwdbroeder en Oom
de Heer

# Jurjen Oostenveld

in den ouderdom van 53 jaar.

Uit aller naam:

Wed*uwe* J. Oostenveld-Harkink

en Kinderen.

Rotterdam, 30 October 1941

**Jan Sonjéstraat** 5A.

De begrafenis zal plaats hebben
Maandag *3 november 1941* a*an*s*taande* te 2¾ uur op
de Algemeene Begraafplaats Crooswijk.
Vertrek van het sterfhuis
te 2 uur.

31947 24
