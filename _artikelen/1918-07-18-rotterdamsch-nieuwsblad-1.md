---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-18
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297064:mpeg21:a0100"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [29]
kop: "Houten wieg."
---
# Houten wieg.

Te koop mahoniehouten
Schommelwieg in
Standaard, pracht meubelstuk,
voor ƒ20. **Jan Sonjéstraat** 29a.
