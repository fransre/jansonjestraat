---
toegevoegd: 2024-07-25
gepubliceerd: 1951-08-17
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164878092:mpeg21:a00032"
onderwerpen: [ongeval, fiets, auto]
kop: "Ernstig ongeval in Middellandstraat"
---
# Ernstig ongeval in Middellandstraat

De 41-jarige mevrouw J. Tegelaar-v*an* d*er* Horst
uit de Josephstraat
kwam gistermiddag *16 augustus 1951* omstreeks kwart
over vijf met haar fiets te vallen,
toen zij in de Middellandstraat nabij
de **Jan Sonjéstraat** een carrier
passeerde en daarbij de zijkant van
een naast haar rijdende zware
vrachtauto raakte. Zij sloeg met
haar hoofd eerst tegen de laadbak
van de auto en daarna tegen het
wegdek. Met een schedelbasisfractuur
en een gebroken schouder is
de vrouw in het S*in*t Franciscusgasthuis
opgenomen.
