---
toegevoegd: 2024-08-03
gepubliceerd: 1935-08-26
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164678065:mpeg21:a00057"
onderwerpen: [woonruimte, tehuur]
huisnummers: [26]
kop: "Westen."
---
# Westen.

Benedenhuis te huur
**Jan Sonjéstraat** 26. Te
bevragen Bovenhuis.
