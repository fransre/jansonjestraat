---
toegevoegd: 2022-02-05
gepubliceerd: 1953-05-22
decennium: 1950-1959
bron: Nieuw Israelitisch weekblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010873888:mpeg21:a0022"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [wachsman]
kop: "Oproeping"
---
# Oproeping

Allen, die iets te vorderen
hebben van, verschuldigd
zijn aan of onder zich hebben
van of als erfgenaam
aanspraak menen te kunnen
doen gelden op de onbeheerde
nalatenschap van

Alter Hirsch Wachsman

geboren te Cieszanow (Rusland)
op 28 Augustus 1894,
laatstelijk gewoond hebbende
te Rotterdam, **Jan Sonjéstraat** 30b
en overleden te
Sobibor (Polen) op 30 April 1943,
wordt verzocht hiervan
kennis te geven aan of
afgifte te doen aan ondergetekende,
uiterlijk op 30 Juni 1953.

Rotterdam, 12 Mei 1953.

De curator:

R. Schiferli

v*an* d*er* Dussenstraat 6a.

*(Zie <https://wachsman.nl> voor meer informatie over de familie Wachsman)*
