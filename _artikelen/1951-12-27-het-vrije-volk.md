---
toegevoegd: 2024-07-25
gepubliceerd: 1951-12-27
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010949270:mpeg21:a0199"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [wassenaar]
kop: "Abraham Wassenaar Senior"
---
Heden overleed zacht en
kalm onze beste Vader, Behuwd- en
Grootvader, Broeder,
Zwager en Oom, de heer

# Abraham Wassenaar S*enio*r

in de ouderdom van 71 jaar

Uit aller naam:

H.J. Wassenaar

Rotterdam, 2e Kerstdag 1951

**Jan Sonjéstraat** 33

Corr*espondentie*adres:

Grondheerendijk 61

Geen bloemen

Gelegenheid tot bezoek aan
de rouwkamer uitsluitend
Vrijdagmiddag *28 december 1951* 4.30 uur aan
het Gem*eentelijk* ziekenhuis Bergweg,
ing*ang* Bergselaan, (eindpunt
lijn 11)

De crematie zal plaatshebben
Zaterdag 29 Dec*ember* *1951* in het
Crematorium te Velsen na
aankomst van trein 12.01 u*ur*
halte Driehuis Westerveld.
