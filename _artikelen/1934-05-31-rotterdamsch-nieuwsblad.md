---
toegevoegd: 2024-08-04
gepubliceerd: 1934-05-31
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164671034:mpeg21:a00122"
onderwerpen: [muziek, tekoop]
huisnummers: [10]
achternamen: [t-hart]
kop: "luit-onderricht"
---
Piano, Mandoline,
Mandola, Guitaar en

# luit-onderricht

door gediplomeerd Leerares.
Billijke Conditiën
Mej*uffrouw* M.E. 't Hart,
**Jan Sonjéstraat** 10,
Westen.
