---
toegevoegd: 2024-07-29
gepubliceerd: 1941-05-02
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002599:mpeg21:a0032"
onderwerpen: [bedrijfsinventaris, auto, tekoop]
huisnummers: [36]
achternamen: [wessels]
kop: "Gesloten Carrier,"
---
# Gesloten Carrier,

geschikt
voor bakker, ƒ50,
prima banden, Juncker,
met trommelrem. Te
zien: E. Wessels, **Jan Sonjéstraat** 36.
