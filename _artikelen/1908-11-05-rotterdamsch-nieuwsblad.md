---
toegevoegd: 2024-08-14
gepubliceerd: 1908-11-05
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197273:mpeg21:a0137"
onderwerpen: [huisraad, tekoop]
huisnummers: [19]
kop: "Ledikant."
---
# Ledikant.

Te koop een massief 2-persoons
mahoniehouten
Ledikant met springveeren
matras, gekost hebbende
150 gulden, thans
ƒ37.50, een notenhouten
Ledikant ƒ15.00. Duivenhok
met 4 deuren ƒ5.00,
wegens vertrek. Te bezichtigen
**Jan Sonjéstraat** 19.
