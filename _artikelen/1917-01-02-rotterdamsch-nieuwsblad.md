---
toegevoegd: 2024-08-13
gepubliceerd: 1917-01-02
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296350:mpeg21:a0108"
onderwerpen: [bedrijfsinformatie]
huisnummers: [6]
achternamen: [de-jong]
kop: "P. de Jong,"
---
Aan mijn geachte Cliënteele,
Vrienden en Familie, zoowel
binnen als buiten deze stad,
het Compliment van den dag.

# P. de Jong,

Opzichter,

**Jan Sonjéstraat** 6a.
