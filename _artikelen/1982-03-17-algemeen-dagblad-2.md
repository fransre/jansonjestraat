---
toegevoegd: 2024-07-21
gepubliceerd: 1982-03-17
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002989014:mpeg21:a00136"
onderwerpen: [bewoner, fiets, tekoop, auto, huisraad]
huisnummers: [17]
achternamen: [hoefakker]
kop: "Executieverkoop"
---
# Executieverkoop

Donderdag 18 maart 1982,
te 11.00 uur te Rotterdam,
**Jan Sonjéstraat** 17b
ten laste van W.G. Hoefakker
div*erse* houten-ladders,
dames en herenfiets,
wasautomaat, jukebox,
Mercuri type 40 buitenboordmotor,
Berini
bromfiets, en een automobiel
American motors
type Hornet, kenteken
50-PZ-31 en wat verder
te koop zal worden
aangeboden.

Bezichtiging een half uur
voor de verkoop.

Contante betaling. Geen
opgeld.

De deurwaarder,
