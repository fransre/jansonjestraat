---
toegevoegd: 2023-01-09
gepubliceerd: 2009-09-01
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2009-09-01-de-middellander-origineel.jpg"
onderwerpen: [kunst]
achternamen: [lutgerink]
kop: "Portret dringend aan restauratie toe"
---
# Portret dringend aan restauratie toe

Diegenen die geregeld het Branco
van Dantzigpark passeren zal het
wellicht opgevallen zijn: het schilderij
van haar portret op de hoek van
het park en de Schietbaanlaan is
dringend aan restauratie toe.
Als initiatiefnemer van en motor
achter het Branco van Dantzig project
heb ik al eens een gesprek gehad
met iemand van het Centrum Beeldende Kunst Rotterdam.
Daar
is echter nog niets concreets uit voortgekomen.

Centenkwestie.

Onlangs maakte de heer Lutgerink
uit de **Jan Sonjéstraat** mij erop attent
dat restauratie wellicht gefinancierd
zou kunnen worden uit de zogenaamde
‘Delfshaven Duiten’, een
subsidiepot van de Deelgemeente.
De aanvraag daartoe is begin oktober
*2009* j*ongst*l*eden* door mij ingediend.
Inmiddels is door een deskundige
van het atelier dat het schilderij
destijds gemaakt heeft vastgesteld
dat het restauratie van het schilderij
niet meer mogelijk is. Het zal door
een nieuw exemplaar vervangen
moeten worden. Daarmee is in de
subsidieaanvraag rekening gehouden.

Ik houd u op de hoogte.

mr J.H. Wilmink
