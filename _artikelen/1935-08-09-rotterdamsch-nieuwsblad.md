---
toegevoegd: 2024-08-03
gepubliceerd: 1935-08-09
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164678046:mpeg21:a00073"
onderwerpen: [kampeerhuisje, tehuur]
huisnummers: [34]
achternamen: [plas]
kop: "Hoek van Holland."
---
# Hoek v*an* Holland.

Te huur groot
Kampeerhuis met veel
gerief van 24 Augustus *1935*-eind September *1935*.
Bevr*agen* J.D. Plas,
**Jan Sonjéstraat** 34b.
