---
toegevoegd: 2024-08-05
gepubliceerd: 1932-12-27
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164662064:mpeg21:a00019"
onderwerpen: [werknemer]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "Wasscherij"
---
# Wasscherij

vraagt net Meisje,
eenigszins bekend met
sorteer- en plakwerk.
Wasscherij L. v*an* d*en* Bos,
**Jan Sonjéstraat** 36a.
Telefoon 36565.
