---
toegevoegd: 2024-08-12
gepubliceerd: 1920-08-23
decennium: 1920-1929
bron: Nieuwe Rotterdamsche Courant
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010025011:mpeg21:a0128"
onderwerpen: [familiebericht]
huisnummers: [11]
achternamen: [gits]
kop: "de Heer Lambertus Gits"
---
Hiermede vervullen wij den
treurigen plicht, U kennis te
geven dat na voorzien te zijn van
de H*eilige* Sacramenten der Stervende*n*,
na een langdurig, geduldig
lijden, is overleden, onze innig
geliefde Echtgenoot, Vader, Behuwd- en
Grootvader

# de Heer Lambertus Gits

in den leeftijd van ruim 63 jaar,
in leven Hoofd der Duitsche School

Uit aller naam,

Wed*uwe* L. Gits-van Schendel.

M.Fl.P. Burgerhout-Gits.

J.H. Burgerhout en Kleinkinderen,

Rotterdam, 21 Aug*ustus* 1920.

**Jan Sonjéstraat** 11a.

Eenige en algemeene kennisgeving.
De begrafenis heeft plaats
woensdag *25 augustus 1920* a*an*s*taande* ten 9 uur vanaf
het sterfhuis.
