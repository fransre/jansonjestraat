---
toegevoegd: 2024-08-05
gepubliceerd: 1932-08-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164660057:mpeg21:a00061"
onderwerpen: [reclame]
huisnummers: [34]
achternamen: [valster]
kop: "Aangeboden:"
---
# Chauffeeren

leeren aangeboden door
Particulier, dag- en
Avondlessen, billijk tarief.
Valster, **Jan Sonjéstraat** 34.
