---
toegevoegd: 2024-08-11
gepubliceerd: 1923-11-29
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494961:mpeg21:a0241"
onderwerpen: [fiets, ongeval]
kop: "Rotterdam in de sneeuw."
---
# Rotterdam in de sneeuw.

De 18-jarige C. Nooteboom, uit de Rosestraat,
slipte in de **Jan Sonjéstraat** met
zijn rijwiel. Daar hij over pijn in de beenen
klaagde, werd hij ter onderzoek naar
het Ziekenhuis aan den Coolsingel vervoerd,
waar men evenwel geen letsel kon
constateeren.
