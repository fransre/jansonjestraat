---
toegevoegd: 2024-07-25
gepubliceerd: 1955-04-12
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952265:mpeg21:a0264"
onderwerpen: [familiebericht]
huisnummers: [26]
achternamen: [mager]
kop: "Johanna Christina Knöps,"
---
Heden is na een kortstondig
lijden van ons heengegaan,
onze lieve zorgzame Moeder,
Behuwd- en Grootmoeder,
Zuster, Behuwdzuster en
Tante

# Johanna Christina Knöps,

Weduwe van H. Mager, in de
ouderdom van 71 jaar.

Uit aller naam:

H.P.J. Mager

Rotterdam, 11 April 1955.

**Jan Sonjéstraat** 26a.

De bijzetting in het familiegraf
zal plaatshebben Donderdag 14 April *1955* a*an*s*taande* op de
Algem*ene* Begraafplaats „Crooswijk”
na aankomst in de
Kapel te 2 uur. Vertrek van
het sterfhuis te 1.30 uur.
