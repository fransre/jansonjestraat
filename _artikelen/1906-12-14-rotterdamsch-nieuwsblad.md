---
toegevoegd: 2021-04-29
gepubliceerd: 1906-12-14
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213971:mpeg21:a0109"
onderwerpen: [bedrijfsinformatie]
huisnummers: [15, 17]
achternamen: [van-uitert]
kop: "Stadsnieuws."
---
# Stadsnieuws.

4o. een verzoek van M. van Uitert om
vergunning tot het oprichten van een wasch- en
strijkinrichting, met een gasmotor van
10 paardekracht, in het pand aan de **Jan Sonjéstraat**
n*ummer* 15, 17;
