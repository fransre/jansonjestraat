---
toegevoegd: 2024-08-04
gepubliceerd: 1933-08-24
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164666062:mpeg21:a00093"
onderwerpen: [woonruimte, tehuur]
huisnummers: [39]
kop: "ongemeubileerde voorkamer"
---
Te huur

# ong*emeubileerde* voorkamer

met gebruik Keuken in
het Westen. Br*ieven* **Jan Sonjéstraat** 39
Bov*en*huis.
