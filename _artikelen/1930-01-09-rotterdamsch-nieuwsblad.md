---
toegevoegd: 2024-08-06
gepubliceerd: 1930-01-09
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164645009:mpeg21:a00073"
onderwerpen: [huisraad, tekoop]
huisnummers: [7]
kop: "ameublement."
---
Wegens plaatsgebrek
te koop pracht Chesterfield

# ameublement.

Te bezichtigen **Jan Sonjéstraat** 7b,
Benedenhuis.
