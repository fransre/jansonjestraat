---
toegevoegd: 2024-08-14
gepubliceerd: 1910-05-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197895:mpeg21:a0062"
onderwerpen: [werknemer, gevraagd]
huisnummers: [39]
kop: "Plaatsing"
---
# Plaatsing

gezocht voor een net
Meisje van 15 jaar, uit
netten stand, in Winkel.
Adres: **Jan Sonjéstraat** 39a.
