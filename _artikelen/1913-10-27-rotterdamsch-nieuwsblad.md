---
toegevoegd: 2024-08-13
gepubliceerd: 1913-10-27
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295844:mpeg21:a0134"
onderwerpen: [kunst]
huisnummers: [10]
kop: "Messchaert-Röntgenavond,"
---
Rotterd*amsche* Vereeniging

„Voor de Kunst”.

# Messchaert-Röntgenavond,

in de Groote Doelezaal
op 29 November e*erst*k*omend* *1913*

Uitsluitend toegankelijk voor
de Leden.

Opgaven voor Lidmaatschap te
zenden aan het Bureau der Vereeniging:
**Jan Sonjéstraat** 10b.

51546 14
