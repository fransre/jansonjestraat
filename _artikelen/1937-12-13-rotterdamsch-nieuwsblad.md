---
toegevoegd: 2024-08-02
gepubliceerd: 1937-12-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692037:mpeg21:a00208"
onderwerpen: [werknemer]
huisnummers: [9]
achternamen: [van-duin]
kop: "Gevraagd:"
---
# Gevraagd:

voor direct bekwaam
Costuumnaaister, uitsluitend
maatwerk. Loon
naar bekwaamheid. R.C. v*an* Duin,
**Jan Sonjéstraat** 9.
