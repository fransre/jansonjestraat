---
toegevoegd: 2024-08-14
gepubliceerd: 1906-10-03
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213911:mpeg21:a0072"
onderwerpen: [huispersoneel]
huisnummers: [27]
kop: "Dienstbode,"
---
Terstond gevraagd een
nette

# Dienstbode,

voor dag en nacht. Onnoodig
zich aan te melden
zonder goede getuigschriften.
Adres **Jan Sonjéstraat** 27, tusschen
7 en 8 uur.
