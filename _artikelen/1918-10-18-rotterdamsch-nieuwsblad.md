---
toegevoegd: 2024-08-13
gepubliceerd: 1918-10-18
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297142:mpeg21:a0066"
onderwerpen: [fiets, tekoop]
huisnummers: [42]
kop: "fietsjas 16 gulden."
---
Ter overname nog
nieuwe

# fietsjas 16 g*u*ld*en*.

**Jan Sonjéstraat** 42,
beneden.
