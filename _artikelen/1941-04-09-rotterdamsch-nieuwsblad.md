---
toegevoegd: 2024-07-29
gepubliceerd: 1941-04-09
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002707:mpeg21:a0014"
onderwerpen: [cafe, bedrijfsinventaris, tekoop]
huisnummers: [15]
kop: "Zaalstoelen"
---
# Zaalstoelen

30 stuks ƒ30. Toonbank
2.70 m*eter*, 2 spoelkuipen
partij flesschen, eenige
schilderijen Adres: **Jan Sonjéstraat** 15
