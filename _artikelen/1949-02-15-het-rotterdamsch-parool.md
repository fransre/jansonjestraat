---
toegevoegd: 2024-07-26
gepubliceerd: 1949-02-15
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164872038:mpeg21:a00076"
onderwerpen: [cafe, tehuur]
huisnummers: [15]
achternamen: [ringeling]
kop: "Te huur!"
---
# Te huur!

Vrije zaal voor bruiloften,
vergaderingen en partijen.
Vraagt inlichtingen **Jan Sonjéstraat** 15.
Tel*efoon* 30682. J. Ringeling.
