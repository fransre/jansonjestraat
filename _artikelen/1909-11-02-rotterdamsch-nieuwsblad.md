---
toegevoegd: 2024-08-14
gepubliceerd: 1909-11-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010198031:mpeg21:a0145"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [10]
kop: "Kostgangers."
---
# Kostgangers.

Er kunnen direct twee
nette Kostgangers gepl*aatst*
worden in net gezin, op
netten stand, met gebr*uik*
van vrije Slaapkamer,
goede Burgerpot met gezellig
huiselijk verkeer,
à ƒ5.50 per week. Adres:
**Jan Sonjéstraat** 10a, beneden,
bij de 1ste Middellandstraat.
