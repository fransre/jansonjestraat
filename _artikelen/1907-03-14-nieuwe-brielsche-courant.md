---
toegevoegd: 2024-03-02
gepubliceerd: 1907-03-14
decennium: 1900-1909
bron: Nieuwe Brielsche Courant
externelink: "https://proxy.archieven.nl/126/967F0F11886742C08FA4BB7CFC768552"
onderwerpen: [familiebericht]
huisnummers: [8]
achternamen: [bronder]
kop: "innigen dank"
---
Ondergeteekenden brengen bij
dezen hunnen innigen dank aan
den WelEd*ele* Heer L. van der Sloot,
Commissaris van het Loodswezen te Hellevoetsluis
en de Loodsen, benevens Burgemeester
en Wethouders van Rockanje, en verder
aan allen voor hunne groote belangstelling,
in het zoo treurig verlies, en voor de eer,
mijn onvergetelijken Echtgenoot en Vader,
bewezen.

Tevens dank voor de troostvolle woorden
die door Ds. Datema en Ds. Mol
tot ons gesproken zijn. Mogen die in aller
harten ingang hebben gevonden, is onze
wensch.

Wed*uwe* Jh. Bronder-J. Brobbel,

Kinderen en familie.

Rotterdam, **Jan Sonjéstraat** N*ummer* 8.
