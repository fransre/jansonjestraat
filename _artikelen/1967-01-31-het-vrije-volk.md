---
toegevoegd: 2021-05-04
gepubliceerd: 1967-01-31
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956179:mpeg21:a0150"
onderwerpen: [cafe, bewoner]
huisnummers: [15]
achternamen: [ringeling]
kop: "Plotseling overleden"
---
# Plotseling overleden

(Van een onzer verslaggevers)

De 73-jarige heer J.J. Ringeling
uit de **Jan Sonjéstraat** in Rotterdam
is maandagavond *30 januari 1967* in de Drievriendenstraat
in elkaar gezakt. In ziekenhuis
Dijkzigt bleek dat hij was overleden.
