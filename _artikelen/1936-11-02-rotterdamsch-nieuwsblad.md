---
toegevoegd: 2024-08-03
gepubliceerd: 1936-11-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164686001:mpeg21:a00140"
onderwerpen: [brand]
achternamen: [take]
kop: "Brandjes"
---
# Brandjes.

~~Gasten van slangenwagen 15 bluschten hedenmorgen *2 november 1936*
kwart over tien, onder toezicht
van den onderbrandmeester, den heer K. van der Wolf,
van den brandmeester, den heer A. van der Dussen
en van den hoofdman, den
heer L.A. van Glinsteren, met zand en emmers
water een brand in een pot met mastiek
bij T. van der Hurk aan de Goudschestraat
58b.~~

~~Onder toezicht van den onderbrandmeester,
den heer G. van der Wolf, hebben gasten van
slangenwagen 48 hedenmorgen *2 november 1936* kwart over elf
met emmers water een begin van brand gebluscht
bij Vrijenhoek aan de Oudaenstraat N*ummer* 15.~~

van slangenwagen 27, onder toezicht van den
brandmeester, den heer J.F.W. Holzapfel,
met zand een brandje in een in de Mauritsstraat
staande auto van H.J. Take, uit de
**Jan Sonjéstraat**.

~~Om kwart voor één vanmiddag *2 november 1936* bluschten
gasten van slangenwagen 41 een schoorsteen
brand ten huize van de wed*uwe* Smit aan de
Rechthuislaan 5.~~
