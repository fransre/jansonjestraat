---
toegevoegd: 2024-07-26
gepubliceerd: 1948-05-07
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010949527:mpeg21:a0045"
onderwerpen: [huisraad, tekoop]
huisnummers: [39]
kop: "eiken theemeubel"
---
Te koop aangeb*oden*

# eiken theemeubel

met glasplaat. **Jan Sonjéstraat** 39a.
