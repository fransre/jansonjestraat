---
toegevoegd: 2024-08-06
gepubliceerd: 1930-06-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164647055:mpeg21:a00104"
onderwerpen: [werknemer, fiets]
huisnummers: [36]
kop: "fietsjongen,"
---
Gevraagd nette

# fietsjongen,

leeftijd circa 16 jaar.
Loon ƒ6 à ƒ7 met verval.
Adres **Jan Sonjéstraat** 36.
