---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-17
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297063:mpeg21:a0072"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [29]
kop: "Vouwwagen."
---
# Vouwwagen.

Te koop voor ƒ10,
goed onderhouden Kindervouwwagen,
leeren
Zitting en rug, vernikkelde
Duwer en gummi
Bandjes. **Jan Sonjéstraat** 29a.
