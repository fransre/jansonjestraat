---
toegevoegd: 2024-08-03
gepubliceerd: 1936-04-22
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164682055:mpeg21:a00059"
onderwerpen: [radio, woonruimte, tehuur, pension]
huisnummers: [32]
kop: "Aangeboden:"
---
# Aangeboden:

zonnig gemeubileerde
Zit-Slaapkamer, m*et* of
z*onder* Pension, zeer billijk.
Kasten, Telefoon, Radio,
ook voor bejaard
Persoon. **Jan Sonjéstraat** 32.
