---
toegevoegd: 2024-07-21
gepubliceerd: 1982-03-25
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960290:mpeg21:a0262"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [smeets]
kop: "Nicolaas Jacobus Smeets"
---
Heden is toch nog onverwacht van ons
heengegaan mijn lieve man, onze zorgzame
vader, schoonvader, grootvader en
overgrootvader, broer, zwager en oom

# Nicolaas Jacobus Smeets

op de leeftijd van 68 jaar

M. Smeets-van Bennekum

Kinderen

Kleinkinderen

Achterkleinkinderen

en verdere familie

23 maart 1982

**Jan Sonjéstraat** 24a

3021 TX Rotterdam

Bezoek en gelegenheid tot condoleren in
de rouwkamer aan de Mathenesseriaan
282 (hoek Claes de Vrieselaan) donderdag *25 maart 1982*
van 18.30 tot 19.00 uur.

De crematie zal plaatsvinden vrijdag a*an*s*taande* *26 maart 1982* in
het crematorium „Hofwijk”; Delftweg 230
te Rotterdam-Overschie.

Vertrek vanaf het woonhuis om 11.00 uur.
Aankomst in het crematorium circa 11.20

Het crematorium is te bereiken met bus 56
van Westnederland, welke vertrekt vanaf
het busstation Delftseplein, perron NN, om
10.00 uur.
