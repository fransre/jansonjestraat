---
toegevoegd: 2021-05-18
gepubliceerd: 1965-02-25
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955396:mpeg21:a0198"
onderwerpen: [brand]
huisnummers: [26]
kop: "Brandweerrapport"
---
# Brandweerrapport

Rotterdam, woensdag 24 februari *1965*

20.03-20.23 uur: kleine binnenbrand
**Jan Sonjéstraat** 26, geblust met emmers
water, aanwezig 321-G4, leiding brandmeester
P.M. Daal.
