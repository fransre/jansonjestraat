---
toegevoegd: 2024-08-13
gepubliceerd: 1920-06-01
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493961:mpeg21:a0172"
onderwerpen: [familiebericht]
huisnummers: [11]
achternamen: [lamm]
kop: "Johan,"
---
Geboren:

# Johan,

Zoon van

K.W. Lamm en
A.L. Lamm-Holewijn.

R*otter*dam, 28 Mei 1920.

**Jan Sonjéstraat** 11B.

9
