---
toegevoegd: 2024-03-05
gepubliceerd: 1949-02-09
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010950186:mpeg21:a0050"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [3]
achternamen: [uilenbroek]
kop: "Te huur aangeboden"
---
# Te huur aangeboden

Gevraagd een nette kostganger,
huiselijk verkeer, degelijk
pension. Uilenbroek, **Jan Sonjéstraat** 3b.
