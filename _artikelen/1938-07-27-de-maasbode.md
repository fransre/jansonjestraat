---
toegevoegd: 2024-08-01
gepubliceerd: 1938-07-27
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000193435:mpeg21:a0165"
onderwerpen: [ongeval, motor, auto]
kop: "Rare capriolen van een motorcarrier."
---
# Rare capriolen van een motorcarrier.

Gisteren *26 juli 1938* reed op den Nesserdijk de 26-jarige
A. W. uit den **Jan Sonjéstraat** met
zijn motorcarriër in de richting van de stad.
Bij het passeeren van een auto reed hij den
36-jarigen J. Rademaker, wonende op dien
dijk aan. Deze voetganger werd van den dijk
geslingerd, doch kwam er vrij goed af. De
kwetsuren, welke hij opliep, waren van zoo
lichten aard, dat hij naar zijn woning kon
wandelen. De motorcarriër botste echter tegen
een lantaarnpaal en werd flink gehavend,
evenals de paal. Wonder boven wonder
bleef W. ongedeerd.
