---
toegevoegd: 2024-08-14
gepubliceerd: 1906-01-23
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213700:mpeg21:a0075"
onderwerpen: [werknemer]
huisnummers: [18]
achternamen: [kool]
kop: "Waschvrouw."
---
Gevraagd voor Dinsdag
en Woensdag een

# Waschvrouw.

Adres M. Kool, **Jan Sonjéstraat** 18,
bij de
Middellandstraat.
