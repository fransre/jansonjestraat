---
toegevoegd: 2024-07-23
gepubliceerd: 1962-11-07
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010953987:mpeg21:a0266"
onderwerpen: [familiebericht]
huisnummers: [30]
achternamen: [smeets]
kop: "Yvonne en Johnny"
---
Morgen donderdag 8 november *1962*
worden

# Yvonne en Johnny

6 jaar.

Fam*ilie* Smeets

**Jan Sonjéstraat** 30a
