---
toegevoegd: 2024-08-10
gepubliceerd: 1925-04-09
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495069:mpeg21:a0149"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [23]
achternamen: [vervoort]
kop: "Ter overname:"
---
# Ter overname:

beige Lakbak Kinderwagen,
in prima staat.
ƒ14. Vervoort. **Jan Sonjéstraat** 23.
