---
toegevoegd: 2024-07-26
gepubliceerd: 1944-04-25
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003693:mpeg21:a0012"
onderwerpen: [bedrijfsruimte]
huisnummers: [38]
kop: "Te koop: huizen winkelhuizen enzovoort"
---
# Te koop: huizen winkelhuizen enz*ovoort*

Rotterdam.
~~Eendrachtsweg n*ummer* 29, ƒ23.000.—. Huur ƒ2.000.—~~
~~G.J. Mulderstraat n*ummer* 16 ƒ16.000 Huur ƒ1.219.—~~
~~en n*ummer* 18 ƒ16.000.— Huur ƒ1.102.—~~
~~Graaf Florisstr*aat* n*ummer* 9 ƒ24.000.—. Huur ƒ1.650.—~~
~~Jan Porcellisstraat n*ummer* 5 en 7 ƒ22.000.—~~
**Jan Sonjéstraat** n*ummer* 38 ƒ23.000.— Huur ƒ1.937.—
~~Lieve Verschuierstraat n*ummer* 52 ƒ8.000.— Huur ƒ598.—~~
~~Proveniersplein n*ummer* 8 ƒ16.000.—. Huur ƒ1.164.—~~
~~Provenierssingel n*ummer* 20 ƒ14.250.— Huur ƒ1.000.—.~~
Nadere gegevens
adr*es*: J. Bokhorst, Overschieschestraat 106B.
Schiedam.

T 68154
