---
toegevoegd: 2024-08-12
gepubliceerd: 1921-11-24
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493905:mpeg21:a0195"
onderwerpen: [familiebericht]
huisnummers: [28]
achternamen: [de-mink]
kop: "Henri Leendert de Mink,"
---
In plaats van Kaarten.

Heden overleed plotseling tot
onze diepe droefheid mijn geliefde
Echtgenoot, onze Vader,
Behuwd- en Grootvader, de Heer

# Henri Leendert de Mink,

in den ouderdom van 61 jaren.

Uit aller naam,

C.A. de Mink-Maaten.

Rotterdam, 23 Nov*ember* 1921.

**Jan Sonjéstraat** 28B.

De teraardebestelling zal
plaats hebben Zaterdag *26 november 1921* a*an*s*taande* op
Algem*ene* Begraafpl*aats* te Crooswijk.

Vertrek van het sterfhuis te
1 uur.

21
