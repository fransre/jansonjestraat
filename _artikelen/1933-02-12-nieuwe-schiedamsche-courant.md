---
toegevoegd: 2024-05-20
gepubliceerd: 1933-02-12
decennium: 1930-1939
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1933-01-12/edition/0/page/7"
onderwerpen: [woonruimte, tekoop]
huisnummers: [3, 17]
kop: "Voorloopige afslag."
---
# Voorloopige afslag.

Pand en erf, **Jan Sonjéstraat** 3a, b. Trekgeld
ƒ7.300.

Pand en erf, **Jan Sonjéstraat** 17a, b. Trekgeld
ƒ7100.
