---
toegevoegd: 2024-07-30
gepubliceerd: 1939-12-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164704041:mpeg21:a00018"
onderwerpen: [bedrijfsinventaris, kleding, tekoop]
huisnummers: [18]
kop: "automaat"
---
Te koop: Kwartjes-

# automaat

4-laads, prima conditie,
spotkoopje; Wintermantel,
maat 50-52, ƒ9.50,
nieuw, v*an* Particulier.
**Jan Sonjéstraat** 18b.
