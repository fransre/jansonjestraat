---
toegevoegd: 2024-07-26
gepubliceerd: 1950-09-19
decennium: 1950-1959
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB15:000562068:mpeg21:a00053"
onderwerpen: [familiebericht]
huisnummers: [12]
achternamen: [de-ridder]
kop: "Hendrikus Petrus de Ridder"
---
Heden overleed tot onze
diepe droefheid, na een
langdurige ongesteldheid,
nog geheel onverwacht, de
Heer

# Hendrikus Petrus de Ridder

in de ouderdom van 54 j*aar*

Uit aller naam:

T. Leuverman

Thea

Rotterdam, 17 Sept*ember* 1950

**Jan Sonjéstraat** 12a

De teraardebestelling zal
plaats hebben Woensdag *20 september 1950*
a*an*s*taande* op de Algemene Zuider Begraafplaats.

Vertrek van het sterfhuis
2 uur. Aankomst begraafplaats
circa 2.30 uur.
