---
toegevoegd: 2024-08-10
gepubliceerd: 1925-10-23
decennium: 1920-1929
bron: Bataviaasch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011071690:mpeg21:a0123"
onderwerpen: [bewoner, fiets]
kop: "Weer een Schoolkind vergeten"
---
# Weer een Schoolkind vergeten

Rotterdam heeft thans ook zijn vergeten
schoolkind! Tot nog toe waren wij er hier
voor bespaard gebleven, schreef de Tel*efoon*-correspondent
aldaar. Gisteren 24 Sept*ember* *1925*
heeft n*ame*l*ijk* het langzamerhand chronisch wordend
geval zich ook te Rotterdam voorgedaan.

Te vijf uur 's middags *24 september 1925* waren de leerlingen
der 5e klasse van de Oranje-Nassauschool,
een school voor bijzonder onderwijs aan de
Velzenluststraat, naar huis gegaan. Eenige
jongens moesten nablijven. Na een kwartiertje
was de onderwijzer komen zeggen,
dat ook zij konden opruimen. Blijkbaar is
dit verkeerd begrepen door den 9-jarigen
D. M., wonende op den Beukelsdijk. De
jongen had zijn werk nog niet af en verkeerde
in de meening, dat dit eerst gereed
moest, vóór hij naar huis mocht gaan. De
andere jongens gingen weg en M. bleef alleen
in het lokaal achter. Te halfzes verliet het
hoofd der school, de heer C. de Bruyn, die
meende, dat alle kinderen weg waren het
gebouw en deed de buitendeur op slot.

Toen de jongen met zijn werk gereed was,
liep hij de gang in, om te zien, of de onderwijzer
er nog was. Tot zijn schrik bemerkte
hij, dat diens fiets, die altijd in de gang staat,
weg was. Hij liep naar de buitendeur, doch
vond deze op slot. Natuurlijk kwamen den
knaap allerlei gedachten aan vergeten kinderen,
die den geheelen nacht in de school
hadden gezeten, in het hoofd, en hij vergat
daardoor, dat de achteruitgang naar de
gymnastiekschool altijd open staat. Hij begon
luidkeels te schreeuwen en aan de deur
te rammelen. Toevallig passeerden juist
twee rechercheurs van politie, die den jongen
geruststelden en bij den conciërge, die betrekkelijk
dichtbij in de **Jan Sonjéstraat** woont,
den sleutel van 't gebouw gingen halen. Te
kwart voor zes was de jongen alweer bevrijd.
Thuis had men nog niets van zijn
achterblijven bemerkt.

Het hoofd der school wees er nog op, al
was de jongen niet door de rechercheurs
opgemerkt, hij toch niet lang meer opgesloten
had behoeven te blijven, daar om
zes uur de vrouw van den conciërge altijd
weer in het gebouw komt voor het schoonmaken
der lokalen. Maar, zoo voegde de
heer De Bruyn hieraan toe, ik kan u de
verzekering geven, dat het niet meer gebeuren
zal. Het is een goede les geweest en
wij zullen voortaan 's avonds voor het sluiten
der school nagaan, of er iemand achterblijft.
