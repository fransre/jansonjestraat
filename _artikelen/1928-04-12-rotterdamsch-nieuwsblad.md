---
toegevoegd: 2024-08-07
gepubliceerd: 1928-04-12
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514267:mpeg21:a0076"
onderwerpen: [woonruimte, tekoop]
huisnummers: [37]
kop: "Te koop:"
---
# Te koop:

**Jan Sonjéstraat** 37, tegen
8 procent. Beneden
huis met Kelder, 1 Mei *1928*
leeg. Te b*e*vr*agen* bij Nadort.
Heemraadsstraat 7.
