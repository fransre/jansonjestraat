---
toegevoegd: 2024-08-05
gepubliceerd: 1932-02-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164657047:mpeg21:a00161"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [23]
kop: "Westen."
---
# Westen.

Heer biedt wegens
overplaatsing zijn Zit- en
Slaapkamer aan,
met zeer goed Pension.
**Jan Sonjéstraat** 23a.
