---
toegevoegd: 2021-04-29
gepubliceerd: 1972-09-22
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957958:mpeg21:a0321"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Klacht tegen politie-optreden in sexclub"
---
# Klacht tegen politie-optreden in sexclub

Van een onzer verslaggevers

Rotterdam — Naar aanleiding
van een inval van de
politie in de Sexclub ‘Angelique’
in de **Jan Sonjéstraat** afgelopen
donderdag *21 september 1972*, heeft de
advocate van de gezamenlijke
sexclubexploitanten, mevr*ouw* De Kat-Presser
een aanklacht tegen
het politieoptreden ingediend.

Een aanklacht die gebaseerd
is op drie punten: Ten eerste is
er een filmprojektie-apparaat
meegenomen zonder bewijs van
afgifte. Ten tweede zijn de klanten
zonder opgaaf van reden
ondervraagd en ten derde is in
het huis (de club is op een gewone
bovenverdieping gevestigd)
een inval gedaan, zonder
bevel tot huiszoeking.
Gisteren *21 september 1972* heeft de afdeling bijzondere
wetten weer invallen
gedaan in twee sexclubs: één in
de Boomgaardstraat en één in
de Katendrechtsestraat. Projektie-apparaten
werden meegenomen.
