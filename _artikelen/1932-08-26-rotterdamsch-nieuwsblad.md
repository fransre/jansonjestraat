---
toegevoegd: 2024-08-05
gepubliceerd: 1932-08-26
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164660065:mpeg21:a00027"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [23]
kop: "Aangeboden:"
---
# Aangeboden:

voor net Meisje frissche
Slaapkamer desgewenscht
Pension, **Jan Sonjéstraat** 23a.
