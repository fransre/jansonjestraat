---
toegevoegd: 2024-07-29
gepubliceerd: 1940-06-18
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002416:mpeg21:a0006"
onderwerpen: [woonruimte, tehuur]
huisnummers: [12]
achternamen: [westerhout]
kop: "Te huur gevraagd"
---
# Te huur gevraagd

woning voor klein gezin,
ƒ6 per week, ook
samenwonen liefst W*est*.
D Westerhout. **Jan Sonjéstraat** 12a.
