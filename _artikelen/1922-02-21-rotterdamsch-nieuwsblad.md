---
toegevoegd: 2024-08-11
gepubliceerd: 1922-02-21
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494079:mpeg21:a0043"
onderwerpen: [motor, tekoop]
huisnummers: [3]
kop: "Stalling"
---
# Stalling

aangeboden voor Motorfiets
m*et* zijsp*an*, cond*ities* billijk.
Bevr*agen* **Jan Sonjéstraat** 3a.
