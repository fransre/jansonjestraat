---
toegevoegd: 2024-07-25
gepubliceerd: 1953-03-11
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164883059:mpeg21:a00088"
onderwerpen: [ongeval]
kop: "Dokwerker maakte lelijke val"
---
# Dokwerker maakte lelijke val

In de dokloods van de R.D.M. is
Maandagmiddag *9 maart 1953* de 54-jarige dokwerker
O.J. H. wonende **Jan Sonjéstraat**,
van een 2½ meter hoge
vlonder gevallen. Hij is met twee
gebroken hielen in het Havenziekenhuis
opgenomen.
