---
toegevoegd: 2021-04-29
gepubliceerd: 1977-11-30
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959705:mpeg21:a0267"
onderwerpen: [cafe, misdrijf]
huisnummers: [15]
kop: "‘Leuke bijverdienste’ in illegale gokclubs"
---
# ‘Leuke bijverdienste’ in illegale gokclubs

(Van een onzer verslaggevers)

Rotterdam — „Het was
een leuke bijverdienste,” zei
de spontane Marga van I. (20)
uit Rotterdam over haar
werkzaamheden in illegale
gokclubs. Daar was echter een
einde aan gekomen toen de
politie een inval deed in een
pand aan de **Jan Sonjéstraat**,
waar Willem B. (49) het rouletteballetje
lustig liet rollen.

Marga van I., al eerder veroordeeld
voor haar activiteiten
in de Sporting Club aan de
Eendrachtsweg, werkte in
haar nieuwe club onder meer
als barjuffrouw, stapelaarster
en croupière. Zij verdiende er,
volgens haar zeggen althans,
25 tot 50 gulden tip per avond
mee.

„Het balletje heeft voor deze
jongedame al eerder gerold,”
aldus officier van Justitie
mr. H.R.G. Feber gisteren *29 november 1977*
bij de rechtbank. Kwam er
vorig maal 500 gulden boete
uitrollen, ditmaal eiste de officier
750 gulden. Marga van
I., die haar vader (marktkoopman)
helpt, zei alleen zakgeld
te krijgen. De boete zou derhalve
betalingsmoeilijkheden
kunnen opleveren.

Haar raadsman mr. D. Hoedemaker
concludeerde echter
tot vrijspraak, omdat de Rotterdamse
niet bedrijfsmatig
zou hebben gehandeld. De exploitant
van het gokhol, Willem B.,
liet verstek gaan. De
officier eiste tegen hem, mede
gelet op eerdere veroordelingen,
twee weken gevangenisstraf.
Hij vorderde voorts verbeurdverklaring
van het in-beslag genomen speelmateriaal
en het (vele) geld. Vonnis
over twee weken.
