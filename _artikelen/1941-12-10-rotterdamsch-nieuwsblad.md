---
toegevoegd: 2024-03-07
gepubliceerd: 1941-12-10
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002842:mpeg21:a0022"
onderwerpen: [woonruimte, tehuur]
huisnummers: [35]
kop: "Te huur aangeboden:"
---
# Te huur aangeboden:

gem*eubileerd* vrij bovenhuis.
Bevragen **Jan Sonjéstraat** 35A,
tel*efoon* 36116.
