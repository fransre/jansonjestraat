---
toegevoegd: 2024-07-29
gepubliceerd: 1941-02-05
decennium: 1940-1949
bron: Algemeen Handelsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBNRC01:000056512:mpeg21:a0098"
onderwerpen: [diefstal, fiets]
kop: "Prestaties van het gilde der langvingers"
---
# Prestaties van het gilde der langvingers

Van een bakfiets, staande in de **Jan Sonjéstraat**
heeft men een doos met margarine en
een kilo roomboter gestolen ten nadeele van
een melkslijter uit Vlaardingen.
