---
toegevoegd: 2022-01-01
gepubliceerd: 2016-10-29
decennium: 2010-2019
bron: YouTube
externelink: "https://www.youtube.com/watch?v=au1UfV56GMw"
onderwerpen: [bellebom]
kop: "De ruiming van de Bellebom in 1988"
---
# De ruiming van de Bellebom in 1988

Een compleet videoverslag van de ruiming van ‘de Bellebom’ in 1988

Tijdens een geallieerd bombardement boven de Rotterdamse wijk Middelland, is minstens één bom niet tot ontploffing gekomen.
Dit projectiel lag diep onder een achtertuin van een rij woningen in de Bellevoysstraat.
Dit verslag is een weergave vanaf het begin (het zoeken naar de bom) tot het einde (het afvoeren van de bom).

*(op 43:40:
Door bijzondere omstandigheden was de afloop van de actie bijzonder chaotisch.
„Dat, en dat is daar al gezegd, sector B onmiddelijk naar de rand van sector A gaat want die willen even komen kijken.
De mensen van sector A die niet in de Bellevoysstraat wonen onmiddelijk hun huis in proberen te komen en dus ook in de Bellevoysstraat.
En met name de **Jan Sonjéstraat** dat natuurlijk in een positie verkeert waarbij iedereen die daar rondloopt via de achterramen bovenop de bom zit.”)*
