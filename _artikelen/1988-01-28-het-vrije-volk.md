---
toegevoegd: 2022-07-23
gepubliceerd: 1988-01-28
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962764:mpeg21:a0238"
onderwerpen: [bellebom]
kop: "Ruikertje voor last Bellebom"
---
# Ruikertje voor last Bellebom

Rotterdam — Vijfentwintig
bewoners van de
Bellevoysstraat en **Jan Sonjéstraat**
in de Rotterdamse
wijk Middelland hebben
vanmorgen *28 januari 1988* van de gemeente
Rotterdam een bloemetje
gekregen. De bewoners zijn
de afgelopen weken nogal
gedupeerd door overlast
rond de voorbereidingen
voor de demontage van
twee vliegtuigbommen in
hun achtertuinen.

Gisteren *27 januari 1988* moesten de vijfentwintig
gezinnen uit de
Bellevoysstraat en **Jan Sonjéstraat**
bovendien een dag
hun huizen uit. Dat was hen
op 13 januari *1988* ook al eens
overkomen, vandaar dat
juist zij in bloemen werden
gezet. Als op 27 maart *1988* de
feitelijke ontmantelijking
van de twee bommen zal
plaatsvinden, moeten in totaal
zevenduizend inwoners
van Middelland een dag elders
doorbrengen. Nog eens
14.000 buurtgenoten mogen
die dag de straat niet op in
verband met explosiegevaar.
