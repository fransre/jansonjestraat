---
toegevoegd: 2023-03-07
gepubliceerd: 1980-03-27
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960497:mpeg21:a0334"
onderwerpen: [bulldog]
kop: "Bulldog-werker gaat weer eten"
---
Bestuur wordt vervangen

# Bulldog-werker gaat weer eten

(Van een onzer verslaggevers)

Rotterdam — Stafmedewerker Rinus van Klaveren
van het jongerenopvangcentrum, de Bulldog, die vorige
week dinsdag *18 maart 1980* in hongerstaking ging, eet weer. Het bestuur
van Bulldog, dat op 1 april *1980* zal opstappen, zal op korte termijn
worden vervangen door nieuwe leden. Alleen voorzitter
Albert Ekerhof en Penningmeester Leo Ramasre blijven
aan. Hiermee is aan de belangrijkste eis van Rinus voldaan.

Het bestuur zal vrijwillig vertrekken.
Zij is het niet eens met
de werkwijze van Rinus die
vooral de opvang van jonge
drugverslaafden en jeugdprostituées
op zich wil nemen. Het
bestuur wil voor deze vorm van
hulpverlening zonder medische
begeleiding geen verantwoording
dragen.

Drie collega's van Rinus hebben
al de benen genomen. Zij
zouden zich alleen willen bezighouden
met jongeren die van
huis zijn weggelopen. „Dat zijn
de eenvoudigste cliënten voor
een hulpverlener,” stelde Rinus
onlangs.

Zijn drie collega's zullen binnenkort
worden vervangen. Dit
werd tot voor kort bemoeilijkt
door het bestuur die het pand
van de Bulldog in de **Jan Sonjéstraat**
een tijdje wilde sluiten
zodat Rinus tot bezinning kon
komen. Maar hij weigerde daar
op in te gaan omdat de drugverslaafden
die in het pand verbleven
de dupe zouden worden. Na
een gesprek met de gemeente
heeft voorzitter Albert Ekerhof
toegezegd alles in het werk te
stellen om zo snel mogelijk voor
nieuwe krachten te zorgen.
Nieuwe krachten die belangstelling
hebben voor mensen
met drugsproblemen. Reden
voor Rinus om weer wat voedsel
tot zich te nemen.

„Ik voel me nu een stuk beter,”
zegt hij na afloop van de
hongerstaking. „Ik eet nog niet
te veel want dat is slecht voor
mijn maag. Mijn hoofdmenu bestaat
uit bouillon en pap. Ik ben
een paar kilo afgevallen.”

*Onderschrift bij de foto*
Rinus van Klaveren weer
aan de bouillon.
