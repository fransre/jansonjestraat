---
toegevoegd: 2024-05-19
gepubliceerd: 1925-09-25
decennium: 1920-1929
bron: Zierikzeesche Nieuwsbode
externelink: "https://krantenbankzeeland.nl/issue/zni/1925-09-25/edition/0/page/2"
onderwerpen: [bewoner]
kop: "Het chronische geval"
---
Verschillende berichten

# Het chronische geval.

Door geschreeuw
en geklop bemerkten surveilleerende politie-agenten
Donderdagmiddag *24 september 1925* omstreeks
6 uur, dat in de Oranje Nassauschool aan
den Beukelsdijk te Rotterdam een kind
was achtergebleven. Bij den concierge
van G. in de **Jan Sonjéstraat** zijn ze den
sleutel gaan halen en daarna hebben zij
den jongen, die alleen in het gebouw was,
bevrijd.
