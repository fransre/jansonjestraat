---
toegevoegd: 2024-07-29
gepubliceerd: 1940-02-28
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002281:mpeg21:a0028"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Achterkamer,"
---
# Achterkamer,

gemeubileerd, Warande,
Keukengebruik ƒ2.50.
**Jan Sonjéstraat** 20b.
~~Achterkamer ongemeubileerd,
v*aste* Waschtafel
2.75. 's Gravendijkwal 136a,
ongemeubileerde
Voor-Zijkamer v*aste* Wastafel
ƒ2. Claes de Vrieselaan 139.~~
