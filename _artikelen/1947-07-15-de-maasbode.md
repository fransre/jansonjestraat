---
toegevoegd: 2024-07-26
gepubliceerd: 1947-07-15
decennium: 1940-1949
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB15:000549013:mpeg21:a00047"
onderwerpen: [reclame, tekoop]
huisnummers: [30]
kop: "10,000 Stuks electrische soldeerbouten"
---
# 10,000 Stuks electrische soldeerbouten

350 Watt 220 en 127 Volt ƒ12.80
voor export te koop aangeboden.

Gelieve schriftelijk te reflecteren aan Handelsonderneming E.T.A.,
**Jan Sonjéstraat** 30a, Rotterdam.
