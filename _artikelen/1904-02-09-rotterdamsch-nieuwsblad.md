---
toegevoegd: 2021-04-29
gepubliceerd: 1904-02-09
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202166:mpeg21:a0088"
onderwerpen: [woonruimte, tehuur]
kop: "'t Is zeldzaam"
---
# 't Is zeldzaam

zulke mooie Woningen
voor Heeren Onderwijzers
en Ambtenaren. Vrije
Boven- en Benedenhuizen
voor ƒ19, ƒ17, ƒ16, verder
een 1e en 2e Etage voor
ƒ14 en ƒ13. Overal Waschtafels
met Waterleiding
in de Alkoven. Dagelijks
te zien **Jan Sonjéstraat**
aan de Schermlaan. Bevragen
bij Kroon, Middellandstraat 51.
