---
toegevoegd: 2024-07-29
gepubliceerd: 1940-08-07
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002490:mpeg21:a0032"
onderwerpen: [werknemer]
kop: "Leerling-Kapster"
---
# Leerling-Kapster

gevr*aagd* om verder in het
vak te worden opgeleid.
Maison „Richard”.
Schermlaan, hoek **Jan Sonjéstraat**.
