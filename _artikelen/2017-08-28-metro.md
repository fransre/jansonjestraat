---
toegevoegd: 2021-12-27
gepubliceerd: 2017-08-28
decennium: 2010-2019
bron: Metro
externelink: "https://www.metronieuws.nl/in-het-nieuws/binnenland/2017/08/rotterdamse-wijk-is-helemaal-klaar-met-de-obike/"
onderwerpen: [obike, fiets]
achternamen: [jager]
kop: "Rotterdammers zijn oBike beu"
---
# Rotterdammers zijn oBike beu

Een groep bewoners in de **Jan Sonjéstraat** in de Rotterdamse wijk
Middelland is helemaal klaar met de oBike. Vooral omdat de straat zo
smal is, veroorzaken de gele fietsen veel overlast en kunnen invaliden en
kinderwagens niet fatsoenlijk over de stoep lopen.

Met het initiatief oBike heeft bewoner Robert Jager geen probleem. „Ik ben voor
delen, maar nu is het een vuilnisbelt van fietsen. Zeker in onze smalle straat tast
het de leefbaarheid aan.” Hij vindt dat de gemeente en oBike een oplossing
moeten bedenken. „In Milaan en Barcelona moeten mensen de fiets
terugplaatsen in een speciaal nietje, anders loopt de teller door. In Rotterdam
moet zoiets toch kunnen ten koste van een aantal parkeerplekken?” Hij vreest dat
het alleen nog maar erger gaat worden. „Er komen nog twee aanbieders bij.”

Bij de gebiedscommissie Centrum regent het al langer klachten over de oBike
en ook de gemeenteraad heeft vragen.
