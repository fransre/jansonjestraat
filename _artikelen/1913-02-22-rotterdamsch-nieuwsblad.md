---
toegevoegd: 2024-08-14
gepubliceerd: 1913-02-22
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010222392:mpeg21:a0105"
onderwerpen: [fiets, tekoop]
huisnummers: [21]
kop: "Te Koop:"
---
# Te Koop:

een Heerenrijwiel in z*eer*
goeden staat voor ƒ12.50,
een Kinder Promenadewagen
en een Tuinschommel
alles in goeden staat
voor spotprijs. Adres **Jan Sonjéstraat** 21b.
