---
toegevoegd: 2024-08-06
gepubliceerd: 1931-02-24
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164651061:mpeg21:a00039"
onderwerpen: [huisraad, tekoop]
huisnummers: [38]
achternamen: [knulst]
kop: "Theekast:"
---
# Theekast:

Te koop mooie glasdichte
Theetafel, in prima
conditie. Prijs billijk.
Adr*es*: H. Knulst.
**Jan Sonjéstraat** 38c.
