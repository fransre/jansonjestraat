---
toegevoegd: 2023-01-10
gepubliceerd: 2000-03-13
decennium: 2000-2009
bron: Rotterdams Dagblad
externelink: "https://www.jansonjestraat.nl/originelen/2000-03-13-rotterdams-dagblad-origineel.jpg"
onderwerpen: [eeuwfeest]
achternamen: [poot, lassooij, lutgerink, ladage, huisman, gelderblom, van-der-ploeg]
kop: "Jan Sonjéstraat viert honderdjarig bestaan met boek"
---
# **Jan Sonjéstraat** viert honderdjarig bestaan met boek

Door Rein Wolters

Rotterdam — Aan het eind van de
19de eeuw ging het snel met de
groei van Rotterdam. In tal van
wijken regen de straten zich aaneen
tot buurten. Dat gebeurde onder
meer in Feijenoord, Charlois,
Afrikaanderwijk, het Oude Westen
en Middelland. Veel straten in
die buurten bereiken dezer dagen
en maanden de leeftijd van honderd
jaar.
De bewoners van de Heemraadssingel,
de meest ‘deftige’ straat
van de wijk Middelland in de deelgemeente
Delfshaven timmerde
begin vorige week al aan de weg
met een eeuwviering. In september *2000*
komt er een boek uit over de
historie van de straat.
De bewoners van de minder ‘deftige’
**Jan Sonjéstraat**, een zijstraat
van de 1e Middellandstraat, waren
hun wijkgenoten van de
Heemraadssingel voor met een
boek. Zij presenteerden zaterdag *11 maart 2000*
in wijkcentrum Middelland hun
‘Even terug in de tijd — 100 jaar **Jan Sonjéstraat**’.
Dat ging gepaard
met een feest, waar de nostalgische
verhalen vanaf dropen. De
vloer bezweek bijna onder het gewicht
van de ongeveer tweehonderd
bewoners en ex-bewoners.
Al tijdens de bijeenkomst bleek
dat initiatiefnemers Jan Poot,
Don Lassooij en Aad Lutgerink
van het organiserende comité
veel energie in hun idee hadden
gestoken. „De **Jan Sonjéstraat** is
niet zo maar een straat,” vertolkte
73-jarige Jan Poot. „Wij zijn er
trots op dat de bewoners van vroeger
en nu, graag in deze straat wonen.”

## Hecht

Deelraadsvoorzitter Tom Harreman
van Delfshaven benadrukte
dat, handenschuddend in de menigte,
nog eens. „Het zijn mensen
die op een fantastische wijze met
hun woonstraat bezig zijn. In Middelland
is het een woonstraat met
hechte banden. De **Jan Sonjéstraat**
mag gelden als een voorbeeld
voor heel de deelgemeente.”

Tegen de achtergrondmuziek van
John Verkroost, die uitbundig
werd toegezongen vanwege zijn
zestigste verlaardag, kwamen de
verhalen los. Zoals over de gezusters
Ladage die op de hoek met de
Schermlaan tot aan het begin van
de jaren zestig een winkel in knopen,
garen, band en fournituren
hadden. „Mijn moeder heeft daar
in 1929 de inhoud van haar
naaidoos gekocht en ik deed in
1958 hetzelfde.
En ook van de firma Huisman van
de slogan ‘Kleeren maken de man
en Huisman maakt de kleeren’.
Vanaf 1885 bevond de zaak zich op
de Hoogstraat. In 1940, na de verwoesting
van het centrum, werd
een noodwinkel ingericht op de
hoek **Jan Sonjéstraat**/1e Middellandstraat.
Dat was op de plek
waar anno 2000 het chinees-restaurant
Augi is gevestigd. Nog tijdens
de Tweede Wereldoorlog
werd op nummer 4 een opslagplaats
in gebruik genomen. Na de
oorlog bleef de noodvestiging
voortbestaan als filiaal en werd in
1950 zelfs uitgebreid met en aangrenzend
pand aan de 1e Middellandstraat.
Dat duurde tot de sluiting
van het bedrijf aan het eind
van de jaren zeventig.”

## Immigratie

De bewoners van nu van de **Jan Sonjéstraat**
vormen en gemengd
gezelschap van harde werkers. De
immigratie is ook zichtbaar in de
straat: nog nooit is deze zo kleurrijk
geweest. De bewoners komen
uit 21 verschillende landen en de
straat heeft dus een ander gezicht
gekregen. Maar de verschillen
met vroeger zijn minder groot dan
ze misschien lijken, constateert
historicus Oscar Gelderblom (28)
in een van de hoofdstukken van
het door hem geschreven boek.
Daarin is ook een fotogalerij in
kleur opgenomen van huidige bewoners,
gemaakt door fotograaf
Jan van der Ploeg. Het boekje is
gedrukt in een oplage van 750
stuks en is in beginsel alleen bestemd
voor huidige en ex-bewoners.
Met de receptie van zaterdag *11 maart 2000* is
het niet gedaan met de feestelijkheden.
Deze worden in mei *2000* voortgezet
met net vullen van de
bloembakken in de straat en in juni *2000*
in het kader van opzoomeren
met een straatdiner. „Zo blijven
we optimaal en positief bezig,”
verwoordt bewoner Aad Lutgerink.

*Onderschrift bij de foto*
Terwijl de bewoners aan het feesten waren in het wijkcentrum, wapperen vrolijke vlaggen in de
honderdjarige straat. Foto Jaap Rozema/Rotterdams Dagblad
