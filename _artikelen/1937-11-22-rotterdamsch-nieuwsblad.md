---
toegevoegd: 2024-08-02
gepubliceerd: 1937-11-22
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692019:mpeg21:a00108"
onderwerpen: [werknemer]
huisnummers: [38]
achternamen: [janknegt]
kop: "Biedt zich aan:"
---
# B*iedt* z*ich* a*an*:

net Meisje voor Leerling
Kapster of voor
Winkel. Adres: A. Janknegt,
**Jan Sonjéstraat** N*ummer* 38a.
