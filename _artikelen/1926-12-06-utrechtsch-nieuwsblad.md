---
toegevoegd: 2023-07-31
gepubliceerd: 1926-12-06
decennium: 1920-1929
bron: Utrechtsch Nieuwsblad
externelink: "https://proxy.archieven.nl/0/BAF8123BF4E7445381F2E35839E66EB8"
onderwerpen: [faillissement]
huisnummers: [20]
achternamen: [henke]
kop: "Faillissementen."
---
# Faillissementen.

(Opgegeven door Van der Graaf en Co*mpagnon*, afd*eling* handelsinformaties).

Uitgesproken: F.A.L. Henke, koopman
te Rotterdam, **Jan Sonjéstraat** 20B.
