---
toegevoegd: 2023-10-07
gepubliceerd: 1963-02-21
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954074:mpeg21:a0169"
onderwerpen: [bewoner]
huisnummers: [14]
kop: "Gouden huwelijksfeest"
---
Maandag 25 februari *1963* vieren de
heer C.J. Nelemans en mevrouw
G.J. Nelemans-Kamp, **Jan Sonjéstraat** 14a,
te Rotterdam hun
gouden huwelijksfeest.
