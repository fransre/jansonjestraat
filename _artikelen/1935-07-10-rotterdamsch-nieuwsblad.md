---
toegevoegd: 2024-08-03
gepubliceerd: 1935-07-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164678011:mpeg21:a00093"
onderwerpen: [woonruimte, tehuur]
huisnummers: [36]
kop: "vrij bovenhuis"
---
Te huur voor direct

# vrij bovenhuis

**Jan Sonjéstraat** 36b.
Huurprijs ƒ32.50 p*er* m*aand*.
Tel*efoon* 56093, na 6 uur
N*ummer* 31594.
