---
toegevoegd: 2024-08-13
gepubliceerd: 1914-03-05
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295454:mpeg21:a0094"
onderwerpen: [huispersoneel]
huisnummers: [18]
kop: "zindelijk Meisje,"
---
Wordt gevraagd: een net,

# zindelijk Meisje,

bij twee kinderen, 1 en 4 jaar oud,
goede behandeling verzekerd.
Adres **Jan Sonjéstraat** 18a.

17903 6
