---
toegevoegd: 2024-07-25
gepubliceerd: 1951-05-16
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164878012:mpeg21:a00083"
onderwerpen: [huisraad, tekoop]
huisnummers: [18]
kop: "electrische wasmachine,"
---
Te koop wegens verhuizing
prima

# electr*ische* wasmachine,

overgordijnen,
rok-lampekap, schoorsteenloper
en 1 pers*oons* bed.
Koopje! **Jan Sonjéstraat** 18b.
