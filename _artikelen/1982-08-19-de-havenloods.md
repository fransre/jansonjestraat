---
toegevoegd: 2023-01-21
gepubliceerd: 1982-08-19
decennium: 1980-1989
bron: De Havenloods
externelink: "https://schiedam.courant.nu/issue/HAV/1982-08-19/edition/0/page/15"
onderwerpen: [bedrijfsruimte]
huisnummers: [17]
kop: "pakhuis"
---
T*e* h*uur* aangeb*oden*

# pakhuis

42 m2
WC, kantoortje 4 M2, en achterplaats
**Jan Sonjéstraat** 17,
Rotterdam tel*efoon* 772507
