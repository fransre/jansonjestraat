---
toegevoegd: 2024-08-03
gepubliceerd: 1936-07-29
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164684033:mpeg21:a00152"
onderwerpen: [woonruimte, tehuur]
huisnummers: [29]
kop: "benedenhuis,"
---
Te huur 1 Sept*ember* *1936* flink

# benedenhuis,

ƒ28 per maand, met
Tuin en grooten drogen
Kelder. **Jan Sonjéstraat** 29b.
