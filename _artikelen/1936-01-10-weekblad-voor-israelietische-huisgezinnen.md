---
toegevoegd: 2024-05-16
gepubliceerd: 1936-01-10
decennium: 1930-1939
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005428054:00001"
onderwerpen: [reclame]
huisnummers: [7]
achternamen: [mak]
kop: "Timmerman en Meubelmaker"
---
# Timmerman en Meubelmaker

Administratie en onderhoud van huizen

M.A. Mak, **Jan Sonjéstraat** 7. Indien gesloten G.J. Mulderstr*aat* 87
