---
toegevoegd: 2024-08-03
gepubliceerd: 1936-05-04
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164683004:mpeg21:a00200"
onderwerpen: [huispersoneel]
huisnummers: [30]
kop: "Gevraagd:"
---
# Gevraagd:

beslist net Meisje in de
Huishouding. Zich melden
na 6 u*ur* **Jan Sonjéstraat** 30b.
