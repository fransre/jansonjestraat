---
toegevoegd: 2021-05-18
gepubliceerd: 1981-01-24
decennium: 1980-1989
bron: NRC Handelsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBNRC01:000027393:mpeg21:a0094"
onderwerpen: [woonruimte, tekoop]
huisnummers: [9]
kop: "Aankondigingen openbare verkoping"
---
# Aankondigingen openbare verkoping

op woensdagen 4 en 11 februari 1981, resp*ectievelijk* bij voorlopige en
definitieve afslag, t*en* o*verstaan* v*an* de navolgende notarissen:

(ex art*ikel* 1223 B.W.)

notaris Mr. J.A. Mulder te Rotterdam in collegiale
samenwerking met notaris J.P. Turion te 's-Gravenhage van
het navolgend onroerend goed:

het pand bestaande uit een beneden- en een afzonderlijke
bovenwoning met onder- en bijbehorende grond te Rotterdam
aan de

**Jan Sonjéstraat** 9A-B

kadastraal bekend gemeente Delfshaven, sectie C, nummer
3718, groot 1,05 are; bovenwoning verhuurd voor ƒ158,90 per
maand, de benedenwoning is zonder toestemming van de
executerende hypotheekbank verhuurd.

Onroerend goedbel. ƒ248,—; rioolrecht ƒ240,—; waterschapslasten
ƒ21,20, alles geschat en per jaar; D.W.L. ƒ118,44
per kwartaal, incl*usief* BTW.

Aanvaarding en betaling der kooppenningen uiterlijk 11 maart 1981.

Nadere inlichtingen ten kantore van voornoemde notaris
Mulder, Heemraadssingel 122, Rotterdam, tel: 010-774266.
