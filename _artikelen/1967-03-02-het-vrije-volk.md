---
toegevoegd: 2021-05-04
gepubliceerd: 1967-03-02
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956182:mpeg21:a0280"
onderwerpen: [cafe, familiebericht]
huisnummers: [15]
achternamen: [ringeling]
kop: "J.J. Ringeling"
---
Met deze delen wij u
mede, dat door het plotseling
overlijden van de heer

# J.J. Ringeling

**Jan Sonjéstraat** 15,

de lopende zaken afgewikkeld
zullen worden
door zijn erfgenamen,
hopende op medewerking
en clementie.

Ten deze danken wij voor
het 40-jarig vertrouwen
in hem gesteld.

De kinderen
