---
toegevoegd: 2024-08-03
gepubliceerd: 1935-09-18
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164679018:mpeg21:a00164"
onderwerpen: [huisraad, tekoop]
huisnummers: [19]
kop: "Schrijfbureau."
---
# Schrijfbureau.

Te koop aangeboden
Cylindorbureau tegen
aannemelijk Bod en
een Schrijfmach*ine*. Adres
**Jan Sonjéstraat** 19 ben*eden*
