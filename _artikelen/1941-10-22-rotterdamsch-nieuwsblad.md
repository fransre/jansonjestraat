---
toegevoegd: 2024-07-28
gepubliceerd: 1941-10-22
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002794:mpeg21:a0012"
onderwerpen: [kinderartikel, kleding, tekoop]
huisnummers: [6]
achternamen: [veldboer]
kop: "Te koop"
---
# Te koop

Kinderstoel
ƒ6. Zw*arte* Bontjasje ƒ5,
vaste prijs. P. Veldboer
**Jan Sonjéstraat** 6.
