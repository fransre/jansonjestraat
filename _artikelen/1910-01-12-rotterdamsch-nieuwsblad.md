---
toegevoegd: 2024-08-14
gepubliceerd: 1910-01-12
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197784:mpeg21:a0064"
onderwerpen: [werknemer]
huisnummers: [18]
kop: "Een Jongen"
---
# Een Jongen

gevraagd. Adres: **Jan Sonjéstraat** 18,
Benedenhuis.
Aanmeldingen
's avonds tusschen 6-7
uur.
