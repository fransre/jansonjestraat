---
toegevoegd: 2024-07-24
gepubliceerd: 1956-07-06
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952558:mpeg21:a0025"
onderwerpen: [fiets, tekoop]
huisnummers: [8]
achternamen: [jansen]
kop: "herenfiets"
---
Te koop

# herenfiets

m*et* trommelremmen
in pracht staat ƒ65,
ook  *8 juli 1956*. Jansen, **Jan Sonjéstraat** 8.
