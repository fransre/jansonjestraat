---
toegevoegd: 2024-03-07
gepubliceerd: 1920-05-20
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493952:mpeg21:a0139"
onderwerpen: [verloren]
huisnummers: [19]
kop: "Verloren:"
---
# Verloren:

gouden Broche met roode
Steentjes en Haar.
Gaande van **Jan Sonjéstraat**
naar Mathenesserlaan.
Tegen belooning
terug te bezorgen:
**Jan Sonjéstraat** 19a.
