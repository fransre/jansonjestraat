---
toegevoegd: 2024-07-26
gepubliceerd: 1948-01-09
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164870007:mpeg21:a00069"
onderwerpen: [dieren]
huisnummers: [31]
kop: "jong hondje,"
---
Eigenaar of liefhebber gezocht
voor ingenomen

# jong hondje,

wit-zwart gevlekt. **Jan Sonjéstraat** 31b.
