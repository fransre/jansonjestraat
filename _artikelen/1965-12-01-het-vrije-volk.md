---
toegevoegd: 2024-07-23
gepubliceerd: 1965-12-01
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955861:mpeg21:a0112"
onderwerpen: [kleding, tekoop]
huisnummers: [32]
achternamen: [van-tol]
kop: "Bruidsjapon"
---
# Bruidsjapon

te koop. v*an* Tol,
**Jan Sonjéstraat** 32, R*otter*dam, na
6 uur.
