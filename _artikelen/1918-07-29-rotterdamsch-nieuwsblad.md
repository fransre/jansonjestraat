---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-29
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297073:mpeg21:a0153"
onderwerpen: [diefstal, fiets]
kop: "Jacht op 'n rijwieldief."
---
# Jacht op 'n rijwieldief.

Gisteravond *28 juli 1918* 6 uur deed mevr*ouw* B. v*an* d*er* S.,
uit de **Jan Sonjéstraat** eenige inkoopen op
den Schiedamscheweg en had toen haar
rijwiel voor den winkel, waarin zij iets
koopen wilde, geplaatst. Plotseling zag zij,
dat de fiets door een man met een stroohoed
op werd weggenomen. Zij snelde naar
buiten en zag den man er mee wegrijden.
Zij *(rende?)* hem achterna onder het roepen van
„Houdt den dief”. Verschillende voorbijgangers
namen aan de klopjacht deel. De
dief reed de Spanjaardstraat in, daarna de
Rosier Faassen- en de Bingleystraat. Toen
hij geen uitweg meer zag, daar het aantal
achtervolgers steeds toenam, wierp hij in
de Bingleystraat het rijwiel weg en liep
de Spanjaardstraat in… regelrecht in de
armen van een agent van politie. Overgebracht
naar het politiebureau aan de Voorstraat
bleek hij te zijn db 20-jarige Van O.
