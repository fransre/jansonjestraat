---
toegevoegd: 2024-07-22
gepubliceerd: 1972-02-10
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002873009:mpeg21:a00127"
onderwerpen: [bewoner]
huisnummers: [8]
achternamen: [hijnen]
kop: "Executieverkoop"
---
# Executieverkoop

vrijdag 11 februari 1972, te
11 uur, **Jan Sonjéstraat** 8b
te Rotterdam, ten laste
van J.W.J. Hijnen, van
meubilaire goederen en
wat verder te koop zal
worden aangeboden.

Bezichtiging is mogelijk
een half uur voor de verkoop.
Contante betaling.
Geen opgeld.

De deurwaarder,

Joh. v*an* d*er* Zwaal.
