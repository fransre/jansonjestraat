---
toegevoegd: 2024-07-23
gepubliceerd: 1965-02-26
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259034048:mpeg21:a00208"
onderwerpen: [reclame, tekoop]
huisnummers: [41]
kop: "vloeistof-duplicators"
---
Verzorg zelf uw:
reclamefolders, voorraadstaten
prijslijsten menu's
tijdbriefjes
contactbladen enz*ovoort*
d*oor* m*iddel* v*an*

# vloeistof-duplicators

Reeds v*an*a*f* ƒ75.—.

„BICO”, **Jan Sonjéstraat** 41B
Rotterdam, 010-250226.

AD17
