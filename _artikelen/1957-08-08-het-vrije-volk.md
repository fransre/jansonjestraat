---
toegevoegd: 2024-07-24
gepubliceerd: 1957-08-08
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952909:mpeg21:a0035"
onderwerpen: [fiets, tekoop]
huisnummers: [16]
achternamen: [guillot]
kop: "tandem,"
---
Te koop goed onderhouden

# tandem,

z*o* g*oed* a*ls* n*ieuwe* banden, n*ieu*we
Torpedonaaf. Vaste prijs ƒ85,
J. Guillot, **Jan Sonjéstraat** 16b,
R*otter*dam-Centrum.
