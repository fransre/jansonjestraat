---
toegevoegd: 2024-08-13
gepubliceerd: 1918-08-24
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297096:mpeg21:a0071"
onderwerpen: [ongeval]
huisnummers: [26]
achternamen: [smit]
kop: "De 19-jarige kantoorbediende P.J. Smit,"
---
# De 19-jarige kantoorbediende P.J. Smit,

wonende **Jan Sonjéstraat** 26,
heeft uit de Delfshavensche Schie gered
den 10-jarigen A. B., wonende Van der Palmstraat,
die aan het zwemmen was en in levensgevaar verkeerde.
