---
toegevoegd: 2024-07-23
gepubliceerd: 1966-03-18
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002808016:mpeg21:a00249"
onderwerpen: [familiebericht]
huisnummers: [8]
achternamen: [wight]
kop: "Thomas Godfriedus Ignatius Wight"
---
Enige kennisgeving

Tot onze droefheid overleed na een smartelijk
lijden onze Vader, Behuwd- en Grootvader,
Broer, Zwager en Oom

# Thomas Godfriedus Ignatius Wight

op de leeftijd van 65 jaar.

Mede namens verdere familie:

Th.G.I. Wight

L.E. Wight-Visser

A.C. Clasquin-Wight

G.V. Clasquin

en kleinkinderen

Rotterdam, 17 maart 1966.

Correspondentieadres: **Jan Sonjéstraat** 8a.

De Overledene ligt opgebaard in de rouwkamer
aan de Mathenesserlaan 470.

Bezoek zaterdagmiddag *19 maart 1966* van 4 tot 4.30 uur en
zondagavond *20 maart 1966* van 7 tot 7.30 uur.

De begrafenis zal plaatsvinden maandag *21 maart 1966* a*an*s*taande*
op de algemene begraafplaats „Crooswijk”.

Vertrek van de rouwkamer te 1 uur. Aankomst
aan de kapel te 1.30 uur.
