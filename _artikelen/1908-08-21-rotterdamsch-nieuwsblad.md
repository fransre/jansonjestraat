---
toegevoegd: 2024-03-07
gepubliceerd: 1908-08-21
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197209:mpeg21:a0077"
onderwerpen: [werknemer]
huisnummers: [35]
kop: "Gevraagd:"
---
Terstond

# Gevraagd:

een nette Schoenmakersjongen,
om het vak te
leeren of eenigszins bekend.
Adres **Jan Sonjéstraat** 35,
bij de Schermlaan.
