---
toegevoegd: 2024-08-04
gepubliceerd: 1933-02-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164663057:mpeg21:a00115"
onderwerpen: [radio, tekoop]
huisnummers: [3]
achternamen: [budding]
kop: "Radio,"
---
# Radio,

gelijkstroomtoestel ter
overname, elk aannemelijk
bod. Te bevragen
en te hooren H. Budding,
**Jan Sonjéstraat** 3a.
