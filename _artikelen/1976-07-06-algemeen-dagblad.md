---
toegevoegd: 2024-07-21
gepubliceerd: 1976-07-06
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002925005:mpeg21:a00169"
onderwerpen: [familiebericht]
huisnummers: [9]
achternamen: [touw]
kop: "Cornelia Mijsbergh"
---
Na een langdurig en moedig gedragen lijden
overleed op 4 juli 1976 onze lieve moeder,
groot-en overgrootmoeder, behuwdzuster
en tante

# Cornelia Mijsbergh

weduwe van C.M. Touw

in de ouderdom van 77 jaar

Uit aller naam:

M. van Straaten-Hulst

**Jan Sonjéstraat** 9b

Corr*espondentie*adres: Rozenlaan 20, Schiedam

De begrafenis zal plaatshebben donderdag
8 juli 1976 op de begraafplaats Hofwijk om
11.30 uur.

Vertrek van rouwkamer Maasdamstraat 4,
Rotterdam-Zuid om 11.00 uur.

Gelegenheid tot condoleren in de rouwkamer
op woensdag 7 juli *1976* van 19.30 tot 20.00
uur.
