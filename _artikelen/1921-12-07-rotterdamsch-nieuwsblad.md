---
toegevoegd: 2024-08-12
gepubliceerd: 1921-12-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493916:mpeg21:a0015"
onderwerpen: [huispersoneel]
huisnummers: [41]
achternamen: [baatsen]
kop: "iemand voor de huishouding,"
---
Gevraagd bij bejaarde Menschen

# iemand v*oor* d*e* huishouding,

c*irc*a 40 jaar, voorzien van g*oede* g*etuigen*. Van
's morgens halfnegen tot 's av*onds*
zeven uur. Zondags vrij. Te bespreken
's avonds 7 tot 9 uur.
Baatsen, **Jan Sonjéstraat** 41a.

70113 8
