---
toegevoegd: 2024-08-07
gepubliceerd: 1928-04-27
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514280:mpeg21:a0114"
onderwerpen: [auto, ongeval]
kop: "Roekeloos autobestuurder."
---
# Roekeloos autobestuurder.

Gisteravond *26 april 1928* halfzeven kwam de 16-jarige
A. D., uit de **Jan Sonjéstraat**, met groote
snelheid en zonder signalen te geven den
hoek van den Binnenweg en de Mauritsstraat
omrijden. Hij nam zijn draai te kort,
met als gevolg, dat de 32-jange C. v*an* d*er* Wolf
en de 46-jarige mej*uffrouw* N. Visser, beiden
wonende in de Van der Duynstraat, door
de auto werden gegrepen, v*an* d*er* Wolf bekwam
een kneuzing in de rechterheup, ter
wijl mej*uffrouw* Visser een wond aan den linkerarm
kreeg. Beiden werden ter plaatse door
een verpleger van den Geneeskundige
Dienst verbonden. Tegen den roekeloozen
chauffeur is door de politie van het bureau
Witte de Withstraat proces-verbaal opgemaakt.
