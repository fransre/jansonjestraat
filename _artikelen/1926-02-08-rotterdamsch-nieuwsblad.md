---
toegevoegd: 2024-08-08
gepubliceerd: 1926-02-08
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495274:mpeg21:a0188"
onderwerpen: [familiebericht]
huisnummers: [39]
achternamen: [van-wulven]
kop: "Fransiscus Gerardus van Wulven,"
---
Heden overleed na een langdurig
geduldig lijden in Eudokia
a*an* d*e* Bergweg, mijn innig geliefde
Echtgenoot, onze Vader, Behuwd-,
Grootvader en Broeder de Heer

# Fransiscus Gerardus v*an* Wulven,

In den ouderdom van 69 jaren.

Mevr*ouw* E. v*an* Wulven-van de Broek

Kinderen en Kleinkind.

Rotterdam, 7 Febr*uari* 1926.

**Jan Sonjéstraat** 39a.

v*oor*h*een* Firma A. Héron
Weste Wagenstraat 97.

De teraardebestelling zal plaats
hebben op Woensdag 10 Febr*uari 1926* a*an*s*taande*
te 3 uur op de Algem*ene* Begraafplaats
Crooswijk.

25
