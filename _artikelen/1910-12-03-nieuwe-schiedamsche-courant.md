---
toegevoegd: 2021-05-03
gepubliceerd: 1910-12-03
decennium: 1910-1919
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1910-12-03/edition/0/page/3"
onderwerpen: [reclame, muziek]
huisnummers: [36]
kop: "Orgels."
---
# Orgels.

Heerlijke harp-Pijptoon

Degelijk afgewerkten kasten in verlangde houtsoort
en stijl bewerkt.

Men verzuime niet bij aankoop van een Orgel
een bezoek aan onze fabriek te brengen.

Door inruiling 2de hands instrumenten te
koop waaronder Cata Organ gebeeldh*ouwde* kast,
spiegelkop.

Speciale reparatie-inrichting

(255)

**Jan Sonjéstraat** 36, Rotterdam,

Overgauw en Westerhof.
