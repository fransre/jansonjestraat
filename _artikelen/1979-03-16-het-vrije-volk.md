---
toegevoegd: 2022-07-23
gepubliceerd: 1979-03-16
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960101:mpeg21:a0204"
onderwerpen: [bulldog]
kop: "Start crisiscentrum in gevaar door uitblijven subsidie"
---
# Start crisiscentrum in gevaar door uitblijven subsidie

Van een onzer verslaggevers

Rotterdam — De zes reeds aangetrokken medewerkers voor het drug-crisiscentrum
aan de **Jan Sonjéstraat**, vrezen ernstige vertraging in de openstelling van
het gebouw. Per 1 april *1979* zou het de deuren moeten openen, maar blijkens een noodkreet
van de hulpverleners is deze datum ernstig in gevaar.

Volgens hen komt dit door een laks reageren van het ministerie van C*ultuur,* R*ecreatie en* M*aatschappelijk Werk*, de belangrijkste
geldschieter voor het centrum.

Het ministerie zegt eerst een
landelijke studie te willen afwachten
voordat het geldmiddellen
verstrekt voor het project
aan de **Jan Sonjéstraat**. Maandag *19 maart 1979*
willen de crisiswerkers
hierover opheldering van de
ambtelijke werkgroep die is ingesteld
voor het opzetten van
het centrum.

Rotterdam heeft toegezegd en
al grotendeels waargemaakt
ƒ35.000 in het project te stoppen.
Het pand aan de **Jan Sonjéstraat**
is voor dit doel aangekocht en
reeds voor een groot deel door
vrijwilligers opgeknapt.

Over de medewerking van
Rotterdam zeggen de vrijwilligers
geen klagen te hebben.
Maar over de medewerking van
C*ultuur,* R*ecreatie en* M*aatschappelijk Werk* uiten zij zich minder gunstig.
Doordat C*ultuur,* R*ecreatie en* M*aatschappelijk Werk* het (nog) laat
afweten, hebben de vrijwilligers
uit eigen zak reeds een
flink bedrag geput voor de opknap
van het crisiscentrum.

„Eén van ons heeft er al ruim
drieduizend gulden ingestoken”,
aldus één der aangenomen medewerksters.
„Dit kan zo niet
doorgaan, zonder financiële
steun van C*ultuur,* R*ecreatie en* M*aatschappelijk Werk* is ons project bij
voorbaat al tot mislukken gedoemd,
en dat terwijl er verschrikkelijk
veel behoefte aan
is. Door die houding van C*ultuur,* R*ecreatie en* M*aatschappelijk Werk*
dreigen wij verschrikkelijk gedemotiveerd
te raken. De onzekerheid
is slopend. Niemand
kan ons iets zinnigs vertellen.”
