---
toegevoegd: 2021-04-30
gepubliceerd: 1940-04-16
decennium: 1940-1949
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1940-04-16/edition/0/page/2"
onderwerpen: [ongeval, auto]
achternamen: [overman]
kop: "Dagelijksche ongevallen"
---
# Dagelijksche ongevallen

Bij het oversteken van de **Jan Sonjéstraat**
is de 10-jarige W. Overman uit de Eerste Middellandstraat,
aangereden door een personenauto,
bestuurd door den 58-jarigen F.B. P.,
uit de Hennewierstraat. Het meisje
werd door den voorbumper gegrepen en tegen
de straat geworpen. Zij brak het linkerdijbeen
en moest in het Ziekenhuis Eudokia
ter verpleging worden opgenomen.
