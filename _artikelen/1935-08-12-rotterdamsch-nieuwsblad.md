---
toegevoegd: 2024-08-03
gepubliceerd: 1935-08-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164678049:mpeg21:a00061"
onderwerpen: [huisraad, kleding, tekoop]
huisnummers: [32]
kop: "Te koop:"
---
# Te koop:

Vulkachel, lak met
nikkel 5 g., elec*trische* Hanglamp
met Toplicht, 3
g*u*ld*en*., Plusfourpak,
leeftijd 14 à 15 jaar, 3
g. en eenige Schilderijen,
alles zoo goed
als nieuw. **Jan Sonjéstraat** 32b.
