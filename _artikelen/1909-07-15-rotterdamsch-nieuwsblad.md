---
toegevoegd: 2024-08-14
gepubliceerd: 1909-07-15
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197937:mpeg21:a0034"
onderwerpen: [familiebericht]
huisnummers: [30]
achternamen: [muys]
kop: "Josiena Maria Tuynenburg Muys."
---
Geboren:

# Josiena Maria Tuynenburg Muys.

**Jan Sonjéstraat** 30a.

Rotterdam, 13 Juli 1909.

5
