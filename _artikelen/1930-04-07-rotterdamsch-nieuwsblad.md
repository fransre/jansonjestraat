---
toegevoegd: 2024-08-06
gepubliceerd: 1930-04-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164646042:mpeg21:a00077"
onderwerpen: [bedrijfsruimte, tehuur]
huisnummers: [38]
kop: "Feestgebouw,"
---
# Feestgebouw,

**Jan Sonjéstraat** 38a,
Telef*oon* 20654. Zalen
voor Bruiloften en Partijen,
ook Zaterdags.
Vraagt condities.
