---
toegevoegd: 2024-05-16
gepubliceerd: 1929-06-13
decennium: 1920-1929
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005425128:00001"
onderwerpen: [familiebericht]
huisnummers: [17]
achternamen: [bosman]
kop: "Victor Bosman"
---
Heden overleed, na langdurig lijden,
onze geliefde Man, Vader, Behuwd- en
Grootvader, de Heer

# Victor Bosman

in den ouderdom van 64 jaar.

Uit aller naam

Wed*uwe* L. Bosman-Ossedrijver

R*otter*dam,

8 Juni 1929

29 Ijar 5689

**Jan Sonjéstraat** 17
