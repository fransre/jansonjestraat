---
toegevoegd: 2024-08-14
gepubliceerd: 1908-10-24
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197263:mpeg21:a0017"
onderwerpen: [fiets, tekoop]
huisnummers: [10]
achternamen: [van-wingerden]
kop: "Ter overname"
---
# Ter Overname

aangeboden een goed onderhouden
Damesfiets.
Adres v*an* Wingerden,
**Jan Sonjéstraat** 10.
