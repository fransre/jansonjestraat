---
toegevoegd: 2024-08-14
gepubliceerd: 1905-06-09
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213664:mpeg21:a0132"
onderwerpen: [woonruimte, tehuur]
huisnummers: [35, 39]
achternamen: [polman]
kop: "Te Huur:"
---
# Te Huur:

't Bovenhuis **Jan Sonjéstraat** 35,
bevatt*ende* Voorkamer,
Slaapkamer, Achterkamer,
Serre, alles en
suite, Keuken, Zolder,
Logeerkamer, vele Kasten
enz*ovoort*. Te bevragen bij
Th.W.H. Polman, **Jan Sonjéstraat** 39.
