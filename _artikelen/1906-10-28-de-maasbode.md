---
toegevoegd: 2024-08-14
gepubliceerd: 1906-10-28
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000189388:mpeg21:a0047"
onderwerpen: [werknemer]
huisnummers: [29]
achternamen: [van-beurden]
kop: "naaister,"
---
Gevraagd, een flinke

# naaister,

zoowel voor boven als ondergoed. Voor
Vrijdag middag een zindelijke werkster.
Adres: A.J. v*an* Beukden, **Jan Sonjéstraat** 29.

77A
