---
toegevoegd: 2024-08-06
gepubliceerd: 1928-09-05
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514390:mpeg21:a0192"
onderwerpen: [reclame, dans]
huisnummers: [38]
achternamen: [beyleveldt]
kop: "Dansinstituut A. Beyleveldt"
---
# Dansinstituut A. Beyleveldt

**Jan Sonjéstraat** 38. Telefoon 30654.

Danslessen

Op de Zondaglessen van 4-5¾ en 6-7¾, kunnen zich nog eenige Dames en
Heeren aansluiten. Inschr*ijving* dag*elijks* 8-10 en tijdens de lessen a*an* h*et* Instituut.

45745 12
