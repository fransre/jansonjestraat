---
toegevoegd: 2024-07-26
gepubliceerd: 1944-09-15
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003593:mpeg21:a0014"
onderwerpen: [ongeval, fiets]
achternamen: [vis]
kop: "Door autobus doodgereden."
---
# Door autobus doodgereden.

Op den
hoek van den Goudschesingel en de Meent is
Donderdagmiddag *14 september 1944* een doodelijk ongeval gebeurd.
De 45-jarige fietser A. Vis uit de **Jan Sonjéstraat**
is daar door een autobus overreden.
Met zware inwendige kneuzingen is
hij naar het ziekenhuis aan den Coolsingel
vervoerd. Tijdens het transport is hij overleden.
