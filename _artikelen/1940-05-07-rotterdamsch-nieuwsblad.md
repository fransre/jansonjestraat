---
toegevoegd: 2024-07-29
gepubliceerd: 1940-05-07
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002377:mpeg21:a0063"
onderwerpen: [kinderartikel, muziek, tekoop]
huisnummers: [24]
kop: "Te koop:"
---
# Te koop:

compl*eet* Raceframe, Studiepiano,
Kinderledikant
Jasje en cape (3 jaar).
**Jan Sonjéstraat** 24a.
