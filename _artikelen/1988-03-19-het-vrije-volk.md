---
toegevoegd: 2021-04-29
gepubliceerd: 1988-03-19
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962730:mpeg21:a0422"
onderwerpen: [bellebom]
kop: "‘Je moet een beetje stressbestendig zijn’"
---
Adjudant Schoots zal de
Bellebom
demonteren:

# ‘Je moet een beetje stressbestendig zijn’

Door
Marcel Potters

Rotterdam — Zevenduizend
mensen
moeten voor één dag *27 maart 1988*
huis en haard verlaten.
Veertienduizend anderen
krijgen de opdracht
elders onderdak te vinden
óf het grootste deel
van de dag binnen te
blijven. Duizend politie-agenten
zullen op de
been zijn. Geen auto is
op straat te vinden. Een
compleet stadsdeel
wordt hermetisch afgegrendeld.
Een speciale
noodtelefooncentrale wordt ingericht; GGD,
Rode Kruis en huisartsen
staan paraat. Duizenden
honden, katten,
mogelijke wurg- of gifslangen
en die ene ‘bruine
beer’ blijven netjes
thuis of worden ondergebracht
bij een dierenvriend.
De voorbereidingen namen
een half jaar in beslag, het
draaiboek voor de bewuste 27e
maart *1988* beslaat enkele honderden
pagina's en de actie zelf
zet heel Rotterdam op z'n kop.
Toch komt het die zondag *27 maart 1988*, als
de Operatie — Bellebom haar
hoogtepunt bereikt, uiteindelijk
allemaal aan op de ‘vaste
hand’ van adjudant P. Schoots
(53) van de Explosieven Opruimingsdienst
(EOD). Het
draait bij de ontmanteling van
de vijfhonderd kilo zware Britse
vliegtuigbom in de Bellevoysstraat
immers allemaal
om een klein langwerpig slagpijpje,
dat met een punttang
uit de ‘kont van de bom’ moet
worden getrokken.

„Je moet natuurlijk wel een
beetje stressbestendig zijn,”
zegt adjudant Schoots over deze
voor buitenstaanders zo linke
klus, „want het blijft als het
erop aankomt puur handwerk.
Kijk, de ontsteking van de bom
kunnen we er nog op afstand
uithalen. Daarna zal ik echter
zelf in de put moeten afdalen
om de rest van de klus te klaren.
Of het de eerste duizendponder
is die ik onschadelijk
maak? Nee hoor, ik demonteer
jaarlijks een stuk of tien van
dergelijke vliegtuigbommen.”

## ‘Voor opgeleid’

De ervaren ‘explosieven-opruimer’,
die samen met zijn
collega sergeant 1e klas J.C.
Linschoten (23) de Bellebom
onder handen zal nemen, zegt
geen enkele reden te hebben
aan de goede afloop van de
operatie te twijfelen. „Je bent
er tenslotte voor opgeleid,”
luidt zijn nuchtere uitleg, „dus
je weet wat je te doen staat.
Bovendien kan het onschadelijk
maken van een simpele,
kleine handgranaat veel gevaarlijker
zijn. Pas achteraf
zullen we echter weten hoe
link het allemaal was.”

De EOD is sinds het prille
begin bij de operatie
Bellebom betrokken. In juni 1987
werd de dienst door de gemeente
Rotterdam gevraagd
een onderzoek in te stellen
naar de mogelijke aanwezigheid
van een vliegtuigbom in
de tuinen tussen de **Jan Sonjéstraat**
en de Bellevoysstraat.
Een bewoonster van de wijk
Middelland had een maand
eerder een brief aan het Rotterdamse
college van burgemeester
en wethouders geschreven
waarin zij wees op de
aanwezigheid van de ondergrondse
‘duizendponder’.

Uit historisch materiaal
blijkt dat het één meter tachtig
lange projectiel op 29 november 1944,
tussen 11.20 en
11.31 uur, tijdens een aanval op
het gebouw van de Duitse Sicherheitsdienst
aan de Heemraadssingel
is afgeworpen.
Een officier van de luchtbescherming
maakte melding
van een blindganger (een niet
ontploft projectiel), die door
een volgens ooggetuigen in
moeilijkheden verkerende Engelse
Typhoon-bommenwerper
aan de achterzijde Bellevoysstraat
werd gedropt. De
bom boorde zich daarbij in de
zachte grond, stuitte op een
grote boomstam waardoor hij
kantelde.

## Niet op scherp

Schoots: „Normaliter was
dat ding bij zo'n inslag zeker
geëxplodeerd. Misschien is die
bom in dit geval niet ontploft
omdat-ie van geringe hoogte
werd afgeworpen. De laadvleugel
aan de achterkant van de
bom, die in de lucht ronddraait
en zodoende zorgt dat de ontsteking
wordt ontgrendeld,
heeft nét niet genoeg omwentelingen
kunnen maken. De Bellebom
stond op het moment
van de inslag dus nog niet op
scherp.”

De EOD is eveneens bekend
dat, gezien de haast waarmee
in de oorlog wapens werden
gefrabriceerd, tien percent van
de bommen dóór fabricagefouten
weigerde. Blindgangers
waren tijdens de oorlog schering
en inslag getuige de tien
duizendponders die onlangs op
de plaats van een voormalig
Duits vliegveld bij Venlo werden
geborgen.

Om de precieze plaats van
de Bellebom te kunnen vaststellen
werd het terrein tussen
de **Jan Sonjéstraat** en de Bellevoysstraat
tot een diepte van
vijftig centimeter afgegraven.
Vervolgens gingen Schoots en
de zijnen aan de slag met bom
locators die het ijzeren gevaarte
moesten opsporen. In
eerste instantie leverde dit onderzoek
geen resultaat op
waarna werd besloten tot het
toepassen van ‘diepte-detectie’.
Met water onder druk werden
260 diepe gaten in de bodem
gespoten die ruimte maakten
voor veertien meter lange
plastic pijpjes.

„Deze methode leverde uiteindelijk
succes op,” zegt
Schoots, „op zeven en elf meter
stuitten we op ‘iets’. Doordat er
twee losse dingen waren gesignaleerd
kwam het verhaal in
de wereld dat er mogelijk
sprake was van twee bommen.
Later bleek het dus te gaan om
de staart en het lichaam van
dezelfde bom. Overigens wil ik
benadrukken dat ik de Bellebom
in feite niet heb ontdekt.
Ik heb slechts ijzer gevonden.”

Nadat duidelijk was geworden
dat het inderdaad een
vliegtuigbom betrof werden er
damwanden geslagen, werden
twee gigantische putten gegraven
en kon het grondwater
worden weggepompt. Het
staartstuk werd als eerste geborgen
en begin deze maand
werd uiteindelijk de Bellebom
zelf blootgelegd. Schoots: „Het
projectiel bevindt zich in perfecte
staat. De bom lijkt gloednieuw,
de verf zit er zelfs nog
op.”

„Nu je die bom eenmaal hebt
gezien,” gaat hij verder, „moet
je 'm bergen ook. Die duizendponder
ligt ten slotte middenin
de stad. Het is bekend dat de
springstof in de bom kan gaan
uitkristalliseren waardoor het
projectiel alsnog detoneert.
Dat kan natuurlijk pas over
een jaar of vijftig gebeuren,
maar de kans bestaat. De huizen
eromheen zullen na zo'n
ondergrondse explosie gegarandeerd
instorten. Kijk, ligt
zo'n bom op een ongevaarlijke
plaats, dan neem je uiteraard
niet het risico dat gekke ding
te demonteren.”

## Grootste evacuatie

Volgens de EOD verschilt de
technische kant van deze klus
niet veel van alle andere ‘demontages’
van soortgelijke explosieven.
Wél nieuw voor de
specialisten uit Culemborg is
het grote aantal voorzorgsmaatregelen
dat bij de operatie — Bellebom wordt genomen.
In *19*81 kwam de EOD in actie
bij het onschadelijk maken
van een andere duizendponder
in de binnenstad van Leiden
waarbij eveneens een — zij het
veel kleinere — evacuatie
noodzakelijk was. De tijdelijke
verhuizing van de zevenduizend
inwoners uit de Rotterdamse
wijk Middelland is de
grootste evacuatie sinds de
watersnoodramp van 1953.

Verder wijst Schoots op het
feit dat de Bellebom zich op
een ‘uitzonderlijke diepte’ bevindt
en het werkterrein beperkt
is. Ook de omstandigheid
dat de blindganger op een zondag
wordt ontmanteld is voor
de EOD ‘nieuw’. „We hebben
het verzoek om op zondag *27 maart 1988* aan
de slag te gaan zo lang mogelijk
afgewezen,” zegt coördinator
kapitein L.P.G. van Maren
hierover, „maar in dit geval
kón het gewoon niet anders.
Het blijft echter een uitzondering,
anders zijn we 52 zondagen
per jaar in de weer.”

Ofschoon Schoots en Linschoten
— de Bellebom is z'n
vierde duizendponder — het
klappen van de zweep kennen
waken zij ervoor van het demonteren
van deze blindganger
een te routinematige handeling
te maken.

## Met z'n tweeën

De regels van de EOD
schrijven ook de aanwezigheid
van twee specialisten bij dit
soort werkzaamheden voor, zodat
men elkaar kan controleren.
Als de Bellebom onschadelijk
is zal hij uit de put worden
gelicht en naar Culemborg
worden vervoerd. Mocht de demontage
op 27 maart *1988* niet slagen,
dan wordt de ontsteking
van de bom met een stollend
mengsel geïnjecteerd en elders
tot ontploffing gebracht.
Schoots tenslotte: „Of we
trots zullen zijn als de klus is
geklaard? Tja, wat moet ik
daarop zeggen? Een beetje,
misschien, maar ik zal me
heus niet zo hard op de borst
slaan dat ik er aan de achterkant
van m'n lichaam weer
uitkom.”

## De mislukte aanval op het hoofdkwartier van de SD

Een deel van de wijk Middelland met de plaats waar de Bellebom
op 29 november 1944, even voor het middaguur, insloeg.
De vijfhonderd kilo zware vliegtuigbom werd door een
Britse Typhoon-bommenwerper boven de schoorsteen van
wasserij Borgh losgelaten en beschreef vervolgens de aangegeven
baan. Diezelfde wasserij aan de Bellevoysstraat kreeg
overigens ook een blindganger te verwerken, die echter al
korte tijd later onschadelijk kon worden gemaakt.
Het bombardement had als doel de vernietiging van de Dienstenstelle
van de Rotterdamse SD aan de Heemraadssingel,
op de plaats waar nu het gebouw van het ziekenfonds staat.
De (precisie-)aanval werd uitgevoerd door vier squadrons
Typhoons van de 146e Wing, gestationeerd op de basis Deurne
bij Antwerpen en onder bevel van de 28-jarige kolonel Denys
‘kill'em’ Gillam.
Gezien de reputatie van het Duitse afweergeschut (flak) in
en rond Rotterdam werd besloten ‘afleidingsaanvallen’ boven
de havens uit te voeren terwijl acht Typhoons de klus in
Middelland zouden klaren. De twee eerste vliegtuigen markeerden
het SD-gebouw met fosforrakettten, waarna de overige
met een snelheid van vierhonderd mijl per uur hun duizendponders
loslieten.
De aanval was geen succes. Niet één bom raakte het doel;
het SD-gebouw liep slechts luchtdrukschade op. Wél konden
enkele gevangen verzetsmensen in de commotie aan de Duitsers
ontkomen. Op diverse plaatsen in de omtrek werden
panden vernield. Pas later kon de trieste balans worden opgemaakt:
61 mensen hadden bij het bombardement het leven
verloren, 39 anderen raakten gewond.

*Onderschrift bij de foto*
Het deel van Rotterdam dat op 27 maart *1988* direct met de demontage van de Bellebom te maken heeft. Het donkere gebied
binnen de stippellijn zal gedurende de werkzaamheden volledig zijn verlaten. In de ‘schil’ daarbuiten, op een afstand
van driehonderd tot zeshonderd meter van de bom, moet iedereen op die zondag *27 maart 1988* binnenshuis blijven.
Rechts onderaan: De Bellebom, of ‘duizendponder’ zoals een dergelijk projectiel in het EOD-jargon wordt genoemd.

*Onderschrift bij de foto*
De drie specialisten van de Explosieven Opruimingsdienst (EOD)
die de Bellebom op zondag 27 maart *1988* onder
handen zullen nemen. Adjudant P. Schoots (links)
en J.C. Linschoten (rechts)
demonteren de ‘duizendponder’
ter plekke, kapitein L.P.G. van Maren (midden)
coördineert
de werkzaamheden. (Foto
Rob Cornelder)

## De bom van maand tot maand

Mei 1987: Het college van
burgemeester en wethouders
van Rotterdam krijgt
de eerste melding van de
mogelijke aanwezigheid
van een vliegtuigbom in de
Bellevoysstraat. Op basis
daarvan worden andere getuigen
gehoord. De gegevens
die hieruit voortkomen
geven het gemeentebestuur
aanleiding de tip verder
te onderzoeken.

Juni 1987: De politie-archieven
worden nagezocht
op meldingen van bombardementen
die tijdens de
oorlog in dit stadsdeel hebben
plaatsgevonden. Het resultaat
wordt gemeld aan
het college, dat besluit de
Explosieven Opruimingdienst
(EOD) in te schakelen.

Juli/augustus 1987: Eerste
(voor-) onderzoek van de
EOD vindt plaats.

September 1987: De bewoners
van de Bellevoysstraat
en de **Jan Sonjéstraat** worden
uitgebreid voorgelicht
over de gang van zaken. In
Groot-Brittannië bekijkt
men het oorlogsarchief en
worden uiteindelijk de gegevens
over het bombardement
van 29 november 1944
boven water gehaald.
Het bureau Rampenbestrijding
stelt vervolgens
een onruimingsplan op. Dit
plan wordt goedgekeurd
door B. en W., het ministerie
van Binnenlandse Zaken
en de EOD. De projectgroep
die de operatie in goede banen
moet leiden, wordt opgericht.
De werkgroepen
Ruiming, Voorlichting en
Opvang & Logistiek beginnen
hun werkzaamheden.

Oktober 1987: Een pand in
de Bellevoysstraat wordt
gesloopt om een graafmachine
doorgang naar het
binnenterrein te geven. De
tuinen worden een stukje
afgegraven; de eerste ‘oppervlakte-detectie’
van de
EOD levert geen resultaat
op. B. en W. geven toestemming
voor de zogenoemde
diepte-detectie.

November 1987: 260 plastic
pijpjes, van ieder veertien
meter lang, verdwijnen
in met water ‘gespoten’ gaten
in de bodem. Met behulp
van sonar-apparatuur worden
verstoringen gesignaleerd
waarna de EOD officieel
het verzoek krijgt het
eventuele projectiel bloot te
leggen.

December 1987: De damwanden
worden aangevoerd,
met een kraan over
de huizen getild en in de
grond geheid. Een tweede
voorlichtingsbijeenkomst
voor de bewoners vindt
plaats. Een steekproef-enquête
levert de eerste gegevens
op over onder andere
het aantal inwoners en het
aantal huisdieren in dit gebied.
Er worden ter plekke
enkele maatregelen getroffen,
zoals het met houten
platen afschermen van de
achterzijde van een aantal
woningen.

Januari 1988: Het graven
naar de Bellebom start. Op
drie meter wordt een lange
‘prikstok’ aangetroffen
waarmee de Duitsers eind
*19*44 naar de bom hebben gezocht.
Op zes meter diepte
legt men het staartstuk van
de duizendponder bloot.

Februari 1988: Het zoeken
naar het ‘bomlichaam’
duurt voort. Gespeurd
wordt nu op een diepte van
tien meter.

Maart 1988: De Bellebom
wordt op 4 maart *1988* ‘in perfecte
staat’ aangetroffen. Zondag
27 maart *1988* is gereserveerd
voor de demontage
van de bom, waarvoor onder
meer zevenduizend inwoners
van de wijk Middelland
hun huis moeten verlaten.
