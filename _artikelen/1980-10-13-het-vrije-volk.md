---
toegevoegd: 2024-07-21
gepubliceerd: 1980-10-13
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960681:mpeg21:a0080"
onderwerpen: [bedrijfsruimte]
huisnummers: [41]
kop: "Heel pand leeg"
---
Rotterdam. **Jan Sonjéstraat** 41.
Heel pand
leeg, bestaande uit beg*ane* grond
bedrijfsruimte. 1e et*age* woonk*amer*
\+ slaapet*age* (3 slaapk*amers*) +
badk*amer*. V*oorzien* v*an* c*entrale* v*erwarming*, geheel gerenouveerd.
Vr*aag*pr*ijs* ƒ85.000,—
k*osten* k*oper*. Edro Assurantiën.
010-765009.
