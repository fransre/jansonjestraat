---
toegevoegd: 2024-08-03
gepubliceerd: 1936-04-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164682065:mpeg21:a00110"
onderwerpen: [woonruimte, tehuur]
huisnummers: [32]
kop: "Vrij bovenhuis,"
---
# Vrij bovenhuis,

**Jan Sonjéstraat** 32 te
huur à ƒ35 p*er* m*aand* 3 K*amers*,
Keuken, Warande, Zolder,
2 Kamers. Bevr*agen*:
P. Rijshouwer. Honingerdijk 81.
