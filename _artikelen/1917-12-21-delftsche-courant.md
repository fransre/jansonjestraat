---
toegevoegd: 2024-08-13
gepubliceerd: 1917-12-21
decennium: 1910-1919
bron: Delftsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000141033:mpeg21:a0007"
onderwerpen: [diefstal]
kop: "Dievenbende."
---
# Dievenbende.

In verband met de inbraak bij de
gebr*oeder*s Groosjohan te Rotterdam, waar
voor een waarde van ƒ19.000 aan zijden
stoffen is gestolen en waarvoor reeds 3
personen zijn gearresteerd, zijn door de
recherche nog aangehouden de loswerkman
F. v*an* W., zonder vaste woonplaats,
de los-werkman W. M., wonende Nadorststraat,
en zekere J. de M., wonende
**Jan Sonjéstraat** te Rotterdam.

Zooals wij gemeld hebben, is op 8 Dec*ember*
een belangrijke inbraak gepleegd
in de magazijnen van den manufacturier
Louis de Sterke in den Oppert te Rotterdam,
waar voor een waarde van
ƒ4500 aan tricot-goederen enz*ovoort* gestolen
werd.

Door de aanhoudingen, die de politie
thans gedaan heeft, schijnt ook in deze
geschiedenis eenig licht gebracht te zijn.
De inbraken waren op dezelfde manier
uitgevoerd. Ook in dit geval waren de
gestolen goederen per wagen weggebracht.
Deze goederen zijn door de
recherche teruggevonden, gedeeltelijk
op de markt op den Goudschen Singel,
gedeeltelijk bij een anderen manufacturenhandelaar,
die het tricot-ondergoed
enz*ovoort* reeds uit de derde hand gekocht
had.

In het geheel werden zes personen in
bewaring gesteld, van wie er twee — de
vrouw van den bankwerker W. S. en de
winkelier J. de M. — weer op vrije voeten
zijn gesteld.
