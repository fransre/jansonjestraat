---
toegevoegd: 2024-08-14
gepubliceerd: 1906-06-05
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213808:mpeg21:a0078"
onderwerpen: [woonruimte, tehuur]
huisnummers: [25]
kop: "Te Huur,"
---
# Te Huur,

In het Westen aan de
**Jan Sonjéstraat** N*ummer* 25
een prachtig riant Bovenhuis,
bevattende
Voorkamer, Slaapkamer,
Achterkamer en Suite,
Keuken, Logeerkamer,
Zolder enz*ovoort*. Huurprijs
ƒ21 per maand. Te bevragen N*ummer* *28*.
