---
toegevoegd: 2023-01-08
gepubliceerd: 2005-09-03
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2005-09-03-de-middellander-origineel.jpg"
onderwerpen: [actie, dans, muziek]
achternamen: [lassooij, lutgerink, van-der-ploeg]
kop: "Verslag van ons straatdiner in de Jan Sonjéstraat"
---
# Verslag van ons straatdiner in de **Jan Sonjéstraat**

Enkele maanden geleden zijn we met de
voorbereidingen begonnen, Winnie heeft
toen de trekkersfunctie op zich genomen.
Zij heeft ons bij het Opzoomeren aangemeld,
ruim van te voren een keet besteld,
reclame gemaakt in de straat (een bord
met foto's van een eerder gehouden diner),
uitnodigingen verstuurd en afspraken
gemaakt met de politie en de gemeente
voor afzetting van de straat.
Een week van tevoren zijn we met een
groepje uit de straatgroep bij elkaar gekomen
om afspraken te maken over het programma
en opzet en wat te doen met een
toeloop aan kinderen vanuit andere straten,
hoe activeren we bewoners om eten te
maken, hoe zetten we een leuke opstelling
van tafels, buffet en stoelen neer, wat doen
we aan versiering, wie doet wat, enz*ovoort*
Winnie had een draaiboek gemaakt, zodat
we gemakkelijk afspraken met elkaar konden
maken.

Op zaterdag 3 september *2005* zijn we om ongeveer
11 uur gestart met de opbouw.
De jeugd heeft goed meegeholpen met
sjouwen van tafels en stoelen en in elkaar
zetten van kramen. De partytenten zijn een
aanwinst, maar het was even puzzelen om
ze goed in elkaar te zetten. De volgende
keer is het een fluitje van een cent.
Tegen 3 uur was alles klaar en was het
wachten op de ‘gasten’.
Geleidelijk kwamen heel wat mensen met
exotische schotels, fruit, salades, vleesgerechten,
kip en vis uit alle windstreken.
Het buffet zag er smakelijk, verzorgd en
lekker uit.
We hebben met ongeveer 40 volwassenen
en 30 kinderen heerlijk gegeten van al het
lekkers en gezellig met elkaar gepraat.
Intussen was het belangrijk kinderen te
vermaken met spelletjes (watertrap, handdoekvolley,
lepelloop, ballen en touwtje
springen).

Vanuit de Opzoomerkeet verzorgde twee
DJ's de muziek uit diverse landen, twee
jongens hielden een trommelshow en er
waren dansoptredens door een meidengroep
uit de straat.
Na het eten hebben we gezellig na zitten
praten en borrelen tot ongeveer 10 uur en
onderwijl werd de boel opgeruimd.
Al met al een geslaagd straatdiner onder
de bezielende leiding van Winnie Lassooij.
De moeite waard om het nog eens te herhalen.

**Jan Sonjéstraat**bewoner Aad Lutgerink

*Onderschrift bij de foto*
jan van der ploeg
