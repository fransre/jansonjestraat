---
toegevoegd: 2023-10-07
gepubliceerd: 1963-03-27
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954103:mpeg21:a0239"
onderwerpen: [verloren]
huisnummers: [18]
achternamen: [kramer]
kop: "Verloren"
---
# Verloren

~~Gaande van het station Noord naar de
Schiekade heeft mej*uffrouw* Koopmans, Prins Hendrikstraat 23,
maandagmorgen *25 maart 1963*
haar lichtroze paarlen ketting verloren.
Tijdens een wandeling in het park
aan de Westzeedijk miste Beppie Folmer,
Schans 59, haar horloge. Het was
een gedachtenis aan haar oma.~~ Wie heeft
twee speelgoed pistolen, die op de hoek
**Jan Sonjéstraat**-Middellandstraat op
straat lagen, meegenomen? Twee kleine
jongetjes van 6 en 8 jaar, zoontjes van
mevr*ouw* J.J. Kramer, **Jan Sonjéstraat** 18b,
zijn hier zeer verdrietig over.
