---
toegevoegd: 2024-08-03
gepubliceerd: 1935-07-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164678018:mpeg21:a00096"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [19]
kop: "tafelboormachine"
---
Te koop aangeb*oden* 1

# tafelboormach*ine*

tot Boor 13 m*illi*M*eter*, 1
Platenschaar, Bankschroef,
Slijpmach*ine* en
ander Gereedschap, tegen
aann*nemelijk* bod. **Jan Sonjéstraat** 19 ben*eden*
