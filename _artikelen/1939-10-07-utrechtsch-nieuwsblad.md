---
toegevoegd: 2023-07-31
gepubliceerd: 1939-10-07
decennium: 1930-1939
bron: Utrechtsch Nieuwsblad
externelink: "https://proxy.archieven.nl/0/2594856A38EF4ABCB98C7A489CFBDD78"
onderwerpen: [brand, motor]
achternamen: [dekker]
kop: "Benzinetank ontploft"
---
# Benzinetank ontploft

Rotterdam, 7 Oct*ober* *1939*. — Vanmorgen *7 oktober 1939* omstreeks
negen uur is brand uitgebroken in den motorhandel
van den heer P. Dekker, op den hoek Middellandstraat-**Jan Sonjéstraat**
te Rotterdam.

Toen de eigenaar in de werkplaats achter den
winkel bezig was met het soldeeren van een benzinetank
sloeg plotseling een steekvlam uit, die de
werkbank vlam deed vatten. De kleeding van den
motorhersteller vatte vlam, doch de man wist deze
zelf te dooven en naar buiten te snellen.

In de werkplaats met de zeer brandbare omgeving
verspreidden de vlammen zich met groote
snelheid en spoedig sloeg het vuur over naar den
winkel, waar de vlammen voedsel vonden in de
triplexbekleeding van muren en étalage. Met een
harden knal sprongen de ruiten, waardoor het
vuur lucht kreeg en de vlammen nog verder oplaaiden.
Direct na het uitbreken van den brand
was de brandweer gealarmeerd, die spoedig sein
„middelalarm” gaf.

Toen de brandweer arriveerde, sloegen de vlammen
uit het benedenhuis langs den gevel omhoog
en hadden reeds de daklijst aangetast. Omdat de
bovenverdiepingen groot gevaar liepen, werd het
vuur direct krachtig aangepakt. Met drie stralen
werd het vuur bestreden, zoodat het vuur zienderoogen
minderde. Na twintig minuten had men den
brand onder de knie. Het benedenhuis met werkplaats
was geheel uitgebrand, terwijl de daklijst
gedeeltelijk was vernield. De bovenverdiepingen
liepen slechts weinig schade op. Een vijftal in den
winkel en werkplaats staande motorrijwielen werd
een prooi der vlammen.
