---
toegevoegd: 2024-07-26
gepubliceerd: 1948-06-05
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164870130:mpeg21:a00078"
onderwerpen: [werknemer]
huisnummers: [12]
kop: "koperslagers en fitters"
---
Voor direct gevraagd

# koperslagers en fitters

Aanmelden: **Jan Sonjéstraat** 12,
Rotterdam.
