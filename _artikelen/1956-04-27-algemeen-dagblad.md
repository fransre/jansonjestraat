---
toegevoegd: 2021-05-04
gepubliceerd: 1956-04-27
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000334022:mpeg21:a00155"
onderwerpen: [cafe]
huisnummers: [15]
achternamen: [rueter]
kop: "Heropend"
---
# Heropend

vrijdag 27 april *1956* te 4 uur n*a*m*iddags*

café Intiem

Mevr*ouw* W.C. Rüter-v*an* d*en* Berge — **Jan Sonjéstraat** 15 — Rotterdam
