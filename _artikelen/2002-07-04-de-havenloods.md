---
toegevoegd: 2024-02-21
gepubliceerd: 2002-07-04
decennium: 2000-2009
bron: De Havenloods
externelink: "https://www.jansonjestraat.nl/originelen/2002-07-04-de-havenloods-origineel.jpg"
onderwerpen: [bewoner]
huisnummers: [13]
achternamen: [van-hooijdonk, van-der-ploeg]
kop: "Groot straatfeest"
---
Zaterdag 29 juni *2002* werd in de **Jan Sonjéstraat** een groot straatfeest gehouden ter ere van de 100-ste
verjaardag van d*e* h*ee*r J.L. van Hooijdonk. D*e* h*ee*r van Hooijdonk woont maar liefst al 72 jaar in deze straat. Namens de deelgemeente Delfshaven bood voorzitter Tom Harreman een bloemetje aan.

*Onderschrift bij de foto*
Foto: Jan van der Ploeg
