---
toegevoegd: 2024-08-14
gepubliceerd: 1909-01-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197319:mpeg21:a0272"
onderwerpen: [bedrijfsinformatie]
huisnummers: [34]
achternamen: [bosselaar]
kop: "J.C. Bosselaar,"
---
De Complimenten van den dag
aan mijne geachte Begunstigers.

# J.C. Bosselaar,

**Jan Sonjéstraat** 34.
