---
toegevoegd: 2021-04-29
gepubliceerd: 1904-05-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202644:mpeg21:a0132"
onderwerpen: [woonruimte, tehuur]
huisnummers: [40]
kop: "Jan Sonjéstraat."
---
# **Jan Sonjéstraat**.

Te huur het vrije Bovenhuis
N*ummer* 40, bevat 5
Kamers, Keuken, Warande
enz*ovoort*, ƒ19 p*er* maand en
het ruime Benedenhuis
N*ummer* 18 met hoogen doorloopenden
Kelder ƒ16 p*er*
maand. Sleutels op N*ummer*
18 boven en bij Kroon,
Middellandstraat 51.
