---
toegevoegd: 2023-01-11
gepubliceerd: 2000-03-20
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2000-03-20-de-middellander-origineel.jpg"
onderwerpen: [eeuwfeest]
kop: "100 jaar Jan Sonjéstraat"
---
# 100 jaar **Jan Sonjéstraat**

Op 7 maart *2000* a*an*s*taande* bestaat
de **Jan Sonjéstraat**, gelegen
in het hartje van
Middelland, 100 jaar.

De **Jan Sonjéstraat** is niet zo maar
een straat. We zijn er trots op, dat
over het algemeen de bewoners,
vroeger en nu, graag in deze straat
wonen.

Om het 100-jarig bestaan niet
onopgemerkt voorbij te laten gaan
nodigen wij u uit op onze feestelijke
receptie op zaterdag 11 maart 2000
tussen 14.30 en 16.30 uur in het
Buurthuis Trefcentrum Middelland,
Middellandstraat 103, 1e etage.

Op deze receptie wordt u in de
gelegenheid gesteld met bewoners,
oud-bewoners, sponsoren en andere
genodigden te toosten, herinneringen
op te halen en met elkaar
van gedachten te wisselen over de
straat. Bovendien wordt u het
boekje ‘Even terug in de tijd’ over
de **Jan Sonjéstraat** uitgereikt.
Wij stellen het zeer op prijs u allen
deze middag te ontmoeten.
