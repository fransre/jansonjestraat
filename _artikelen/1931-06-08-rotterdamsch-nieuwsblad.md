---
toegevoegd: 2024-08-05
gepubliceerd: 1931-06-08
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164653042:mpeg21:a00069"
onderwerpen: [woonruimte, tehuur]
huisnummers: [19]
kop: "Benedenhuis"
---
# Benedenhuis

te huur, 40 p*er* m*aand*. **Jan Sonjéstraat** 19.
Bevr*agen*
Bovenhuis.
