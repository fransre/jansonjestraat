---
toegevoegd: 2024-08-14
gepubliceerd: 1909-10-11
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010198012:mpeg21:a0226"
onderwerpen: [werknemer]
huisnummers: [15]
achternamen: [molter]
kop: "nette Leerling"
---
Terstond gevraagd een

# nette Leerling

en een aankomende Platgoedstrijkster
en een
nette Waschvrouw bij J. Molter,
Wasch-, Glans- en
Strijkinrichting, **Jan Sonjéstraat** N*ummer* 15a en b.
