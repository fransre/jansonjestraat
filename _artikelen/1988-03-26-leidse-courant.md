---
toegevoegd: 2021-04-29
gepubliceerd: 1988-03-26
decennium: 1980-1989
bron: Leidse Courant
externelink: "https://leiden.courant.nu/issue/LLC/1988-03-26/edition/0/page/5"
onderwerpen: [bellebom]
kop: "„Niet iedereen is me dankbaar”"
---
Tipgeefster Rotterdamse bom:

# „Niet iedereen is me dankbaar”

Rotterdam — Operatie Bellebom
die morgen *27 maart 1988*
in Rotterdam haar hoogtepunt
bereikt, begon vorig
jaar mei *1987* met een brief aan
B*urgemeester* en W*ethouders* van Rotterdam.

Die brief, waarin de ligplaats
van de bom al vrij
exact werd aangegeven,
werd geschreven door
mevrouw Henny Ottenberg
(64) uit de Bellevoysstraat
en dat wordt haar
niet door iedereen in
dank afgenomen.

Nog steeds krijgt het echtpaar
Ottenberg regelmatig
veelal anonieme telefoontjes
waarin vaak krachttermen
vallen. Het echtpaar wordt
dan het verwijt gemaakt dat
het er voor heeft gezorgd dat
de geplande renovatie van de
woningen in de dezer dage
„beroemdste straat” van Nederland
ten minste met een
jaar is vertraagd.

Een absurd verwijt, vindt het
echtpaar. Welk voordeel zouden
zij daar dan bij hebben?
Haar man Ernst (63) vraagt
zich af wat men had gezegd
wanneer de bom gewoon was
blijven liggen en met renovatie-werkzaamheden
was
begonnen. „Stel dat zo'n man
in zo'n graafmachine de bom
ophaalt en het ding gaat af?”
Mevrouw Ottenberg wil
graag wat rechtzetten. Die
brief, zegt zij nadrukkelijk, is
opgesteld in overleg met de
politie. „Vorig jaar kwamen
hier twee agenten aan de
deur. Ze kwamen van een
opgraving aan de Binnenweg
want daar zou een bom
moeten hebben liggen. Maar
daar had een meneer gezegd:
„Jullie moeten niet hier zijn
maar in de Bellevoysstraat.
Daar moet ergens in de tuinen
een blindganger liggen”.
Ik kon ze de plaats zo aanwijzen.
Zij vroegen of ik dat
aan de gemeente wilde melden.
Toen heb ik die brief
geschreven aan burgemeester
en wethouders. Een aangetekende
brief, want ik heb
er een hekel aan als zoiets in
de onderste la terecht komt”.

## 1944

Henny Ottenberg was in het
oorlogsjaar 1944 twintig jaar.
Ze werkte op een atelier aan
de Essenburgsingel. Tussen
de middag ging ze altijd thuis
bij haar ouders een boterham
eten, in hetzelfde huis in de
Bellevoysstraat 33a waar ze
nu vier en veertig jaar later
met haar man nog steeds
woont.

Dat was op die 29e november 1944,
de dag dat de Britse
luchtmacht op verzoek van
het verzet een aanval op het
gebouw van de gehate Sicherheitsdienst
aan de
Heemraadssingel uitvoerde,
niet anders. Eenmaal thuis
trof zij haar ouders, nog
enigszins beduusd van wat
zich kort daarvoor had afgespeeld.
„Mijn vader zei dat
het toestel zo laag had gevlogen
dat je de piloot praktisch
kon zien zitten. Vandaar dat
we dachten dat het vliegtuig
was aangeschoten en z'n
bommen kwijt wilde om
weer op hoogte te kunnen
komen”.

Officieel staat de bom, die
vermoedelijk vanuit dit
vliegtuig werd gedropt, nog
steeds te boek als een blindganger,
een bom die haar
doel miste. Staande op het
balkon van het echtpaar Ottenberg
kan in ieder geval
geconstateerd worden dat de
blindganger zich met een
wonderlijke precisie in de
zachte grond van een smalle
strook tussen twee huizenrijen
heeft geboord.

Mevrouw Ottenberg: „Ik zie
dat gat van drie tot vier meter
in het rond nog voor me.
Er was nóg een blindganger
gevallen in de Bellevoysstraat,
hier verderop bij wasserij
Borgh. Die bom was
blijven hangen in het dak en
kon zo worden weggehaald.
Maar deze konden ze niet
meer vinden. Van die bom
heb ik nooit een nacht minder
geslapen omdat ik altijd
dacht dat er niets mee kon
gebeuren”. Totdat vorig jaar
die agenten aan de deur
kwamen. „Die zeiden:
‘Straks komen al die zware
vrachtwagens…’. Dat wilde
ik niet op mijn geweten hebben,
stel je voor dat er iets
was gebeurd, dat had ik dat
mijzelf nooit vergeven”.

*Onderschrift bij de foto*
Vanaf het Rotterdamse balkon van tipgeefster Henny Ottenberg (64) is de tien meter diepe put te
zien waarin de Engelse duizendponder onschadelijk moet worden gemaakt.

*Onderschrift bij de foto*
De gemeente Rotterdam verwacht dat tussen de 1000 en 1200
bewoners, onder wie 25 baby's, gebruik zullen maken van de opvang
in de Energiehal.
