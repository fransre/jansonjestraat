---
toegevoegd: 2023-01-11
gepubliceerd: 2000-03-16
decennium: 2000-2009
bron: De Havenloods
externelink: "https://www.jansonjestraat.nl/originelen/2000-03-16-de-havenloods-origineel.jpg"
onderwerpen: [eeuwfeest]
achternamen: [van-wingerden, van-hooijdonk, gelderblom, van-der-ploeg, poot, lutgerink, lassooij]
kop: "Presentatie boekje hoogtepunt feest honderd jaar Jan Sonjé"
---
# Presentatie boekje hoogtepunt feest honderd jaar **Jan Sonjé**

Met onder andere een drukbezochte receptie en de presentatie van een
fotoboek is zaterdag *11 maart 2000* het honderdjarig bestaan van de **Jan Sonjéstraat**
gevierd. De meeste huidige bewoners waren op de receptie in het
Trefcentrum aanwezig. Daarnaast waren er veel voormalige bewoners
van het straatje tussen Eerste Middellandstraat en Schermlaan.
Een van hen, Koos van Wingerden, vertelde dat zijn vader, wanneer een van de kinderen
weer eens een deur open had laten staan, standaard riep: ‘We stoken niet voor
de **Jan Sonjéstraat**!’
Twee generates later is die kreet binnen de familie nog steeds
een gevleugelde uitdrukking, ook al wonen de leden ervan al vijftig jaar elders.
Hoogtepunt van het feest was de presentatie van het boekje ‘Even terug in de tijd’
dat in een oplage van 750 stuks is verschenen. Het eerste exemplaar werd overhandigd
aan de 97-jarige bewoner Jan van Hooijdonk. Aan hem is een pagina in het
boekje gewijd. In het boekje wordt aandacht besteed aan de kunstschilder Jan Sonjé,
een klein beetje aan de geschiedenis van de straat, de verschillende bouwstijlen,
woonvormen, het sociale leven, de winkels en bedrijven, de straat tijdens de Tweede Wereldoorlog,
de Bellebom, de stadsvernieuwing, het opzoomeren en overlast van
de drugshandel. De tekst werd geschreven door straatbewoner Oscar Gelderblom, en
voorzien van links en rechts geritselde foto's. Het hart van het boekje, en dat is
uniek, is een overzicht met portreten van op twee na, alle bewoners van de **Jan Sonjéstraat**.
Fotograaf Jan van der Ploeg maakte alle foto's, in kleur, bij de bewoners
voor de deur, op een foto na, waar hij zelf als straatbewoner op staat. Boekje en
feest zijn er gekomen doordat een drietal bewoners voortdurend bezig bleef en anderen
wisten te stimuleren. Jan Poot, Aad Lutgerink en Don Lassooij werden dan
ook in de bloemen gezet. Voor de bewoners van de **Jan Sonjéstraat** is het feest nog niet afgelopen.
In het voorjaar kleden zij de straat aan met bloembakken, en er staat nog
en straatdiner op het programma, waarbij iedereen, er wonen ruim twintig nationaliteiten,
een maaltijd uit eigen land bereidt.

*Onderschrift bij de foto*
De 97-jarige Jan van Hooijdonk krijgt het eerste exemplaar van het Jan Sonjéboekje uitgereikt. Foto: Leunis Verlinde
