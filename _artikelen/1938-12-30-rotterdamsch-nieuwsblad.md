---
toegevoegd: 2024-08-01
gepubliceerd: 1938-12-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164698051:mpeg21:a00161"
onderwerpen: [woonruimte, tehuur]
huisnummers: [7]
kop: "Benedenhuis,"
---
# Benedenhuis,

**Jan Sonjéstraat** 7 te
huur, ƒ30 per maand,
3 Kamers, Keuken,
ruime Kelder en Tuin.
Bevragen J. de Groot
Oostmaaslaan 181.
