---
toegevoegd: 2024-03-07
gepubliceerd: 1910-05-17
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197887:mpeg21:a0143"
onderwerpen: [woonruimte, tehuur]
huisnummers: [25]
kop: "Te huur:"
---
# Te Huur:

~~het Benedenhuis Jan Porcellisstraat 30
2 Kamers, 2 Alkoven, Keuken
enz*ovoort*, ƒ18 per m*aa*nd,~~
Benedenhuis **Jan Sonjéstraat** 25,
2 Kamers, 2
Alkoven, Keuken, grooten
Kelder enz*ovoort*, ƒ19 per
maand. Dagelijks te bezichtigen
aldaar. Adres
Th.W.H. Polman, Insulindestraat 39.
