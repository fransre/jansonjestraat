---
toegevoegd: 2024-08-14
gepubliceerd: 1906-01-20
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213698:mpeg21:a0090"
onderwerpen: [huispersoneel]
huisnummers: [20]
achternamen: [kool]
kop: "Dag-Dienstbode,"
---
Er biedt zich aan een
nette

# Dag-Dienstbode,

20 j*aar*, liefst Zondags vrij.
Adres: Kool, **Jan Sonjéstraat** 20.
