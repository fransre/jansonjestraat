---
toegevoegd: 2024-07-20
gepubliceerd: 1994-03-14
decennium: 1990-1999
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003122012:mpeg21:a00091"
onderwerpen: [woonruimte, tehuur]
kop: "2 studentes,"
---
**Jan Sonjéstraat** (Middelland) 2
k*a*m*e*r ben*eden*won*ing* met tuin en gr*ote*
kelder, gesch*ikt* voor 2 studentes,
ƒ675 p*er* m*aa*nd Inl*ichtingen* Caesar Onr*oerend* Goed,
010-4255630.
