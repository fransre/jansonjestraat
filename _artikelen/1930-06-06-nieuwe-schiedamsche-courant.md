---
toegevoegd: 2024-05-19
gepubliceerd: 1930-06-06
decennium: 1930-1939
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1930-06-06/edition/0/page/8"
onderwerpen: [familiebericht]
achternamen: [stoop]
kop: "Jos. Beudeker en Rie Stoop"
---
Ondertrouwd:

# Jos. Beudeker en Rie Stoop

Rotterdam 5 Juni 1930.

N*ieuwe* Binnenweg 195a.

**Jan Sonjéstraat** 34a.

Huwelijksvoltrekking Dinsdag 24 juni *1930*
ten 10 ure, in de kerk van de
H*eilige* Elisabeth, Mathenesserlaan.

81204 10
