---
toegevoegd: 2023-10-07
gepubliceerd: 1930-09-18
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002222:mpeg21:a0062"
onderwerpen: [reclame]
huisnummers: [20]
kop: "Rheumatiek en Ischias"
---
# Rheumatiek en Ischias

kunnen genezen. Vraagt gratis inlichting en brochure
aan Handelsbureau E/M.A.V., Postbox 498, kantoor
**Jan Sonjéstraat** 20b.

46953 6
