---
toegevoegd: 2023-10-15
gepubliceerd: 1945-07-18
decennium: 1940-1949
bron: De Rotterdammer
externelink: "https://resolver.kb.nl/resolve?urn=MMNIOD05:000154200:mpeg21:a0047"
onderwerpen: [reclame]
huisnummers: [36]
kop: "Reparatie"
---
# Reparatie

van waardevolle en
antieke meubelen.

Meubelfabriek „Jehovu”

**Jan Sonjéstraat** 36,
Rotterdam.
