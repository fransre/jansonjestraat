---
toegevoegd: 2024-08-11
gepubliceerd: 1924-06-02
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494809:mpeg21:a0124"
onderwerpen: [werknemer, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "leerjongen"
---
Een Schilders-

# leerjongen

gevraagd, E.J. Weier,
Werpl. Josephstr*aat* 63.
Woonpl. **Jan Sonjéstraat** 38,
Tel*efoon* 33084.
