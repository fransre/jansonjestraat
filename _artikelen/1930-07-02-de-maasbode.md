---
toegevoegd: 2024-08-06
gepubliceerd: 1930-07-02
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000198346:mpeg21:a0121"
onderwerpen: [bedrijfsruimte, dans]
huisnummers: [38]
kop: "parterreruimte"
---
Te huur:

# parterreruimte

Geschikt voor Feest- of Danszaal en
andere doeleinden, gelegen

**Jan Sonjéstraat** 38

Huur ƒ40 per week. Te bevragen
bij A. Kooij, Lusthofstraat 125, Tel*efoon* 57226.

83285
