---
toegevoegd: 2024-07-20
gepubliceerd: 1957-11-30
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952990:mpeg21:a0168"
onderwerpen: [kleding, tekoop]
huisnummers: [16]
achternamen: [guillot]
kop: "herenjas"
---
Te koop z*o* g*oed* a*ls* n*ieuwe*

# herenjas

(diagonaal tweed), groengrijs
m*aat* 48 ƒ40,—. J. Guillot, **Jan Sonjéstraat** 16b.
