---
toegevoegd: 2023-01-08
gepubliceerd: 2001-03-10
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2001-03-10-de-middellander-1-origineel.jpg"
onderwerpen: [eeuwfeest]
achternamen: [van-hooijdonk]
kop: "Voor het nageslacht"
---
# Voor het nageslacht

Op zaterdag 10 maart *2001* j*ongst*l*eden*
vond er in de **Jan Sonjéstraat**
een opmerkelijke
gebeurtenis plaats.
Iedereen was het er over eens dat het
vorig jaar ter gelegenheid van het honderdjarig
bestaan van de **Jan Sonjéstraat**
verschenen boekje ‘Even terug in de tijd’
een groot succes genoemd mag worden.
Tijdens een onlangs gehouden straatvergadering
werd het idee geopperd om het
boekje voor het nageslacht te behouden.
De vraag was alleen: hoe? Al snel kwam
men op het idee om het boekje ergens een
woning in de straat in te metselen en te
voorzien van een gedenkplaat. Zo gezegd,
zo gedaan.

Tijdens een gezellige bijeenkomst met een
hapje en een drankje in de **Jan Sonjéstraat**
op 10 maart *2001* j*ongst*l*eden* werd de gedenkplaat aan
de muur van het pand nummer 36 onthuld
door de 99 jarige heer Hooijdonk (in juni *2001*
wordt de heer Hooijdonk 100!), die al
sinds 1930 in de **Jan Sonjéstraat** woont.
Wij zijn heel erg benieuwd naar de gezichten
van de toekomstige slopers die het
boekje over 100 (?), 200 (?) jaar zullen
tegenkomen.

‘De Middellander’ zal hier dan ook zeker
weer verslag van doen.

*Onderschrift bij de foto*
De onthulling (inzet: de — spiegelende — gedenkplaat)

Peter Kervezee
