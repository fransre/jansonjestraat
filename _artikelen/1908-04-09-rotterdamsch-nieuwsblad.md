---
toegevoegd: 2024-08-14
gepubliceerd: 1908-04-09
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994784:mpeg21:a0155"
onderwerpen: [woonruimte, tehuur]
huisnummers: [10]
kop: "Terstond te huur"
---
# Terstond te huur

**Jan Sonjéstraat** N*ummer* 10,
een Benedenhuis, 3 Kamers,
Keuken, Warande
en Kelder onder het geheele
Huis, met Tuin; en
tegen 1 Mei *1908* een Bovenhuis N*ummer* 10,
bestaande
uit, 3 Kamers, Keuken,
Warande, Zolderkamer,
met Gas- en Waterleiding.
Sleutel boven op
N*ummer* 10 en bij den eigenaar
N. v*an* Leeuwen
Van der Poelstraat N*ummer* 37,
1-m*aal* b*ellen*.
