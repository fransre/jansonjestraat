---
toegevoegd: 2022-07-27
gepubliceerd: 2011-11-21
decennium: 2010-2019
bron: Algemeen Dagblad
externelink: "https://www.ad.nl/andere-sporten/honkballer-gregory-halman-24-vermoord~aacbc679/"
onderwerpen: [gregory, radio]
huisnummers: [17]
achternamen: [halman]
kop: "Honkballer Gregory Halman (24) vermoord"
---
# Honkballer Gregory Halman (24) vermoord

De Nederlandse honkbalinternational Gregory Halman (24) is
vanochtend *21 november 2011* bij een steekpartij in Rotterdam om het leven gekomen.
Dat heeft de honkbalbond KNBSB bevestigd na contact met de familie
van de geboren Haarlemmer. De politie heeft de 22-jarige broer van
Halman aangehouden voor verhoor.

Het incident gebeurde om iets na
half zes in een woning in de **Jan Sonjéstraat**.
Het slachtoffer en zijn
broer hadden 's ochtends ruzie
gekregen over de radio die te hard
zou staan. De politie verdenkt de
broer van betrokkenheid bij de
steekpartij. De vriendin van de
honkbalinternational heeft de politie
gealarmeerd.

Halman tekende in 2004 een
langjarig contract bij Seattle Mariners,
dat in de Amerikaanse
profcompetitie MLB speelt. Hij
maakte vorig jaar, na enkele
seizoenen in de lagere divisies, zijn
debuut in de belangrijkste
honkbalcompetitie ter wereld.

Halman genoot zijn opleiding bij Kinheim en werd daar in 2004 gekozen tot
meest waardevolle speler van de Nederlandse competitie.

Met Oranje veroverde de buitenvelder in 2007 de Europese titel. In datzelfde
jaar bereikte Halman met het Nederlands team de halve finale van het WK.
De Haarlemmer maakte geen deel uit van het Nederlands team dat dit jaar
in Panama de wereldtitel veroverde.

Belangrijkste prestaties

Halman werd Europees kampioen met Oranje in 2007, vierde met Oranje op
WK 2007, meest waardevolle speler van Nederlandse competitie in 2004.

*Onderschrift bij de foto*
© Henk Seppen

*Onderschrift bij de foto*
© Proshots
