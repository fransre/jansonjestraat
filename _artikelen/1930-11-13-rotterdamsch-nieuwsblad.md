---
toegevoegd: 2024-08-06
gepubliceerd: 1930-11-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164650013:mpeg21:a00150"
onderwerpen: [kinderartikel, huisraad, tekoop]
huisnummers: [32]
kop: "Kinderwagen"
---
# Kinderwagen

ter overname, lakbak,
zonder gebreken. Spotprijs
ƒ3, groene Vulkachel
ƒ2, **Jan Sonjéstraat** 32a.
