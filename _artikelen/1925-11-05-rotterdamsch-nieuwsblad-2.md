---
toegevoegd: 2024-08-10
gepubliceerd: 1925-11-05
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495618:mpeg21:a0198"
onderwerpen: [huispersoneel]
huisnummers: [21]
achternamen: [reijnders]
kop: "dagmeisje,"
---
Gevraagd degelijk

# dagmeisje,

boven 20 jaar, half
acht tot 3 uur. Donderdag
geheelen dag, loon
ƒ5. Reijnders. **Jan Sonjéstraat** 21a.
