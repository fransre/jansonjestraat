---
toegevoegd: 2024-07-22
gepubliceerd: 1972-03-11
decennium: 1970-1979
bron: NRC Handelsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBNRC01:000031839:mpeg21:a0019"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Geen rendez-vous"
---
# Geen rendez-vous

wordt
meer gegeven in de filmclub
Angelique aan de **Jan Sonjéstraat**,
omdat deze op last van B*urgemeester*
en W*ethouders* van Rotterdam ingevolge
art*ikel* 49 van de A*lgemene* P*laatselijke* V*erordening* is gesloten.
