---
toegevoegd: 2024-08-03
gepubliceerd: 1935-10-26
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164679063:mpeg21:a00093"
onderwerpen: [woonruimte, tehuur]
huisnummers: [25]
kop: "Te huur:"
---
# Te huur:

Benedenhuis met Tuin,
**Jan Sonjéstraat** 25.
Willebrand, Hartmansstraat 11,
Telef*oon* N*ummer* 55556.
