---
toegevoegd: 2023-01-21
gepubliceerd: 1945-06-25
decennium: 1940-1949
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011063703:mpeg21:a0011"
onderwerpen: [actie]
huisnummers: [30]
achternamen: [kofflard]
kop: "Rooms Katholieke metaalbewerkers organiseert u"
---
# R*ooms* K*atholieke* metaalbewerkers organiseert u

Adressen van aanmelding: J.M. Kofflard, **Jan Sonjéstraat** 30:
Th. v*an* Son, Raephorststraat 98: J. Snethorst,
Spanjaardstraat 137d: P. Kegel, Rosestraat 70.
