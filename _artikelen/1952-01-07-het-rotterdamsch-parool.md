---
toegevoegd: 2024-07-25
gepubliceerd: 1951-01-07
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164880005:mpeg21:a00031"
onderwerpen: [brand]
kop: "Hele kerstboom in de kachel"
---
# Hele kerstboom in de kachel

Een hele kerstboom tegelijk stopten
bewoners van de **Jan Sonjéstraat**
Zaterdagmiddag *6 januari 1951* in de haard.
Daardoor kon de klep echter niet
meer dicht en ontstond er een
enorme rookwolk. De brandweer
rukte ijlings uit, doch gelukkig
beperkte de ramp zich tot een
„brand in de kachel”. Men kon het
vuurtje rustig laten uitbranden. Bij
volgende gelegenheden zullen de
bewoners zich echter wel op andere
wijze van hun kerstboom ontdoen.
Aanwezig waren de slangenwagen 40,
de G 3 en de slangenwagen 41.
Brandmeester J. Broekman
had de leiding.
