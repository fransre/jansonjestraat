---
toegevoegd: 2024-07-25
gepubliceerd: 1952-01-19
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000326016:mpeg21:a00090"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [34]
achternamen: [raap]
kop: "Fotografie."
---
# Fotografie.

Aangeb*oden* wegens
vertrek do*nkere* ka*mer* droogkast voor
kleinbeeldfilms en andere formaten
met ingebouwde thermometer
en droogtemeter ƒ300.—.
Narita atelierlamp verplaatsbaar
ƒ75.—. ± 400 reportage albumpjes
ƒ100.—. 200 6×9 films
iets over tijd ƒ200.—. Tenax
„2” lens 1:2 gekoppeld ƒ400.—.
Raap, **Jan Sonjéstraat** 34. Tel*efoon* 37613.
R*otter*dam.
