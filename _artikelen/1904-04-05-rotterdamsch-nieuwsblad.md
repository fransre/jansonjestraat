---
toegevoegd: 2021-04-29
gepubliceerd: 1904-04-05
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010178645:mpeg21:a0133"
onderwerpen: [woonruimte, tehuur]
huisnummers: [42, 44]
kop: "Te Huur met Mei"
---
# Te Huur met Mei *1904*

Blommerdijkschelaan 4, vrij
Bovenhuis ƒ18, Winkelhuis
goed voor barbier of schoonmaker
ƒ4, Pijnackerplein 1
2e Etage ƒ17, bevragen in
den winkel; **Jan Sonjéstraat** 42
en 44 Eerste Etages ƒ14,
Idem 40 vrij Bovenhuis ƒ19
en groote Waterstokerij
ƒ6.50, idem N*ummer* 18 mooi
Benedenhuis ƒ16.
