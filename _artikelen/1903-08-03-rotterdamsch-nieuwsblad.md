---
toegevoegd: 2021-04-29
gepubliceerd: 1903-08-03
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010179205:mpeg21:a0064"
onderwerpen: [bedrijfsinformatie]
huisnummers: [42, 44]
achternamen: [nuy]
kop: "Ter centrale-secretarie der gemeente (afdeling A) zijn ter visie gelegd:"
---
# Ter centrale-secretarie der gemeente (afd*eling* A) zijn ter visie gelegd:

3o. een verzoek van J.A.C. Nuy, om
vergunning tot het oprichten van een smederij
voor licht werk, met een smidse, in
het pand aan de **Jan Sonjéstraat** n*ummer* 42-44;
