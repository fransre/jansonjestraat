---
toegevoegd: 2021-04-29
gepubliceerd: 1978-03-17
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959768:mpeg21:a0190"
onderwerpen: [verkeer, auto]
kop: "Eenrichtingsverkeer"
---
# Eenrichtingsverkeer

Middelland — Een beperkt
éénrichtingsverkeer
geldt vanaf 22 maart *1978* in een
deel van Middelland. Motorvoertuigen
mogen dan alleen
nog in zuidelijke richting rijden
door de Van der Poelstraat,
Joost van Geelstraat,
Jan van Vuchtstraat en **Jan Sonjéstraat**;
alleen in noordelijke
richting door de Hendri*c*k Sorchstraat,
Jan Porcellisstraat
en Bellevoysstraat.
Ook in de Robert Fruinstraat
zal deze maatregel gelden.
Auto's mogen via deze straat
nog slechts in de richting van
de Heemraadssingel rijden.
