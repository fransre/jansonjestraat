---
toegevoegd: 2021-05-04
gepubliceerd: 1907-03-01
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196614:mpeg21:a0083"
onderwerpen: [werknemer]
huisnummers: [15, 17]
achternamen: [van-uitert]
kop: "Loopjongen."
---
# Loopjongen.

Terstond gevraagd een
flinke nette Loopjongen,
vertrouwd met Wagen en
Hond te rijden, boven de
16 jaar, van goede getuigen
voorzien. Adres:
Wasch- en Strijkinrichting
M. van Uitert,
**Jan Sonjéstraat** 15-17,
einde Westkruiskade.
