---
toegevoegd: 2024-07-29
gepubliceerd: 1941-05-06
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002602:mpeg21:a0011"
onderwerpen: [huisraad, tekoop, dieren]
huisnummers: [6]
achternamen: [veldboer]
kop: "Te koop"
---
# Te koop

twee lederen
Fauteuils en kleine
Huishond. Te zien na
7 uur: P. Veldboer, **Jan Sonjéstraat** 6.
