---
toegevoegd: 2024-07-22
gepubliceerd: 1969-12-11
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957094:mpeg21:a0149"
onderwerpen: [bewoner, radio]
huisnummers: [30]
achternamen: [smeets]
kop: "Tuberculose-onderzoek"
---
# T*u*b*er*c*ulose*-onderzoek

Ik was nogal verbaasd over het
bericht dat het jaarlijks onderzoek
op t*u*b*er*c*ulose* op de scholen in, het vervolg
eens per vier jaar zal plaatsvinden.
Alleen aan ouden van dagen
en buitenlandse arbeiders zal
nog speciale aandacht worden geschonken.
Het steeds kleiner worden
van het aantal t*u*b*er*c*ulose*-gevallen
wordt, tot mijn nog grótere verbazing,
door het ministerie van
Volksgezondheid een ontwikkeling
genoemd, die het thans nog toegepaste,
zeer kostbare onderzoek niet
langer noodzakelijk maakt. Met
andere woorden: het is te duur!
Dat is het kardinale punt.

In een radiopraatje op 10 november *1969* j*ongst*l*eden*
zei de directeur van het
Consultatiebureau in Rotterdam:
het zou een dwaasheid en een grote
domheid zijn wanneer wij ons consultatiebureauwezen
op dit moment
zouden liquideren. Dat men speciale
aandacht aan buitenlandse
arbeiders besteedt, spreekt vanzelf,
maar wat doet men met hen die
hier clandestien komen werken en
die juist een groter gevaar vormen?
T*u*b*er*c*ulose* is tegenwoordig niet meer zo
gevreesd als vroeger, maar daar ik
zelf enkele kinderen ermee heb
liggen vraag ik mij af: moet men
nu niet extra op zijn hoede zijn?

N.J. Smeets

**Jan Sonjéstraat** 30a, Rotterdam
