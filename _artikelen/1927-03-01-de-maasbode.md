---
toegevoegd: 2024-08-08
gepubliceerd: 1927-03-01
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000196417:mpeg21:a0058"
onderwerpen: [bedrijfsinformatie]
huisnummers: [2]
kop: "Verhuizing"
---
# Verhuizing

Het kantoor van de N.V. Stuiver's
Woning- en Bouwbureau is per
1 Maart *1927*, wegens uitbreiding verplaatst
naar

**Jan Sonjéstraat** 2

hoek 1e Middellandstraat 100, Tel*efoon*
blijft 34177.
