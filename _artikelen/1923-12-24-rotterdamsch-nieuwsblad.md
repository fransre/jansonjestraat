---
toegevoegd: 2024-08-11
gepubliceerd: 1923-12-24
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494981:mpeg21:a0250"
onderwerpen: [familiebericht]
huisnummers: [30]
achternamen: [wijntjes]
kop: "Jakob Pieter Wijntjes,"
---
Heden overleed tot onze diepe
droefheid, na een langdurig lijden,
mijn geliefde Echtgenoot,
onze lieve Vader, Behuwd- en
Grootvader, de Heer

# Jakob Pieter Wijntjes,

in den ouderdom van 67 jaren.

Uit aller naam,

Wed*uwe* H. Wijntje-Kappers.

Rotterdam, 22 Dec*ember* 1923.

**Jan Sonjéstraat** 30a.

Geen bloemen.

De teraardebestelling zal
plaats hebben Dinsdag *25 december 1923* a*an*s*taande* op
Alg*emene* Begraafplaats Crooswijk.
Vertrek van het Sterfhuis
ten 1 ure.

24
