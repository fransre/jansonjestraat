---
toegevoegd: 2024-08-05
gepubliceerd: 1933-01-30
decennium: 1930-1939
bron: Nieuwe Utrechtsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMUTRA04:253390103:mpeg21:a00060"
onderwerpen: [familiebericht]
huisnummers: [6]
achternamen: [bosma]
kop: "Willem Bosma,"
---
Heden overleed, tot onze
diepe droefheid, na een
langdurig, geduldig gedragen
lijden, onze lieve Man,
Vader, Behuwd- en Grootvader,
de Heer

# Willem Bosma,

op den leeftijd van ruim
61 jaar.

Uit aller naam,

S. Bosma-Littooij.

Rotterdam, 28 Januari 1933.

**Jan Sonjéstraat** 6.

Geen bloemen,

De teraardebestelling zal
plaats hebben Woensdag *1 februari 1933*
a*an*s*taande* op „Crooswijk”, vertrek
om 1½ uur van huis.
