---
toegevoegd: 2024-08-03
gepubliceerd: 1936-08-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164684047:mpeg21:a00111"
onderwerpen: [werknemer, fiets]
huisnummers: [15]
kop: "Bierbottelarij"
---
# Bierbottelarij

vraagt nette Jongen
voor Fiets- en Pakhuiswerk,
Loon ƒ3.50. Brieven
**Jan Sonjéstraat** 15.
