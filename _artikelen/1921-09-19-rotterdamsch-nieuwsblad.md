---
toegevoegd: 2024-08-12
gepubliceerd: 1921-09-19
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493848:mpeg21:a0074"
onderwerpen: [huispersoneel]
huisnummers: [5]
kop: "Gevraagd:"
---
# Gevraagd:

in klein gezin een
Dienstbode, voor heele
dagen. Zondags een
paar uren. Adres: **Jan Sonjéstraat** 5a.
