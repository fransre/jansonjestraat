---
toegevoegd: 2023-03-07
gepubliceerd: 1980-03-21
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960492:mpeg21:a0160"
onderwerpen: [bulldog]
kop: "Bulldog-werker in hongerstaking"
---
‘Bestuur ondermijnt doelstelling’

# Bulldog-werker in hongerstaking

(Van een onzer verslaggevers)

Rotterdam — „Ik zal sterven
als mijn eisen niet worden
ingewilligd. Het bestuur van
stichting de Bulldog probeert op
slinkse wijze van mij af te komen.
De doelstelling van de
Bulldog wordt door het bestuur
ondermijnd. Drugverslaafden
en jeugdprostituées mogen hier
niet komen. Dat pik ik niet.
Daarom eet ik niet meer sinds
afgelopen dinsdagavond *18 maart 1980*. Ik ben
in hongerstaking.”

Rinus van Klaveren, coördinator
van de Bulldog, meent het.
Hij vreest dat de Bulldog moet
worden opgeheven als het bestuur
hem nog langer dwarsboomt.
Zijn belangrijkste eis is
dat het bestuur alle papieren die
betrekking hebben op stichting
de Bulldog aan hem teruggeeft.

Op verzoek van Rinus zijn de
leden opgestapt. „Ik ben bezig
met de opvang van drugverslaafden
en jeugdprostituées.
Bijna al mijn collega's vinden
dat onzin. Zij willen liever jongeren
opvangen die van huis
zijn weggelopen. Maar dat doet
het Jongeren Advies Centrum
al. Ook het bestuur zag mijn
ideeën niet zitten. Dat nu bijna
iedereen weg is, we zijn nog met
ons drieën, vind ik prima.”

De Bulldog is gehuisvest in de
**Jan Sonjéstraat**. De brandweer
noemt het pand onveilig. Er is
sprake van een nieuw onderkomen
voor de stichting aan de
's-Gravendijkwal 161. Collega
Jolanda van de Linden: „Het is
allemaal erg onduidelijk. Van
het bestuur krijgen we niet te
horen of de 's-Gravendijkwal
doorgaat. De gemeente staat gelukkig
achter ons. Officieel zal
de leiding van de Bulldog op 1
april *1980* aftreden. Het is nog maar
de vraag of de leden dan de papieren
terug willen geven. Het
nieuwe onderkomen komt nu
wel op de tocht te staan omdat
het minstens nog drie maanden
duurt voordat we nieuwe
krachten kunnen aannemen”.

Bestuurslid Leo Ramasre is
Rinus trouw gebleven. Hij verzorgt
de boekhouding. Het bestuur
zou hem hebben beschuldigd
van financieel wanbeleid.
„Zo proberen ze me te ontslaan,”
zegt Leo. „Jolanda en Rinus waren
alleen achtergebleven als ze
dat voor elkaar hadden gekregen.
Een paar dagen geleden
hebben twéé ambtenaren de
boeken gecontroleerd. Ze hebben
niets gevonden.”

Momenteel verblijven vier
meisjes met drugsproblemen in
het Bulldogpand. Rinus: „De bestuursleden
hebben voorgesteld
de boel dicht te gooien totdat alles
is opgelost. Dat doe ik nooit
want dan worden die meisjes de
dupe.”

Albert Ekerhof, voorzitter van
de Bulldog: „De Bulldog moet de
Bulldog blijven. Rinus moet
daar in meedraaien. Op zijn
verzoek zijn we afgetreden. We
vinden dat er iemand bij moet
zijn die verstand heeft van
drugproblemen, maar Rinus is
tegen iedereen met een diploma.
Waarschijnlijk omdat hij zelf
zoiets niet heeft. De papieren
mag Ramasre allemaal hebben,
hij is ook bestuurslid.

Vandaag *21 maart 1980* praat het bestuur
met de gemeente om tot een oplossing
te komen.”

*Onderschrift bij de foto*
Rinus in hongerstaking. Boekhouder Ramasre en collega Jolanda aan het voeteneind.
