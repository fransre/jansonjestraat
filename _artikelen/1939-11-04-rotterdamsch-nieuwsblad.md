---
toegevoegd: 2024-07-30
gepubliceerd: 1939-11-04
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164704004:mpeg21:a00086"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Aangeboden:"
---
# Aangeboden:

gemeubileerde Achterkamer,
Warande ƒ3,
gratis gebruik v*an* Bad,
v*aste* Wastafel, Manspersoon,
b*ezigheden* b*uitens*h*uis* h*ebbende*. **Jan Sonjéstraat** 20b,
vrij Bovenhuis.
Ook Zondag.
