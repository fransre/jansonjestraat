---
toegevoegd: 2024-07-21
gepubliceerd: 1987-04-09
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003050008:mpeg21:a00196"
onderwerpen: [woonruimte, tehuur, tram]
kop: "Jan Sonjéstraat"
---
# **Jan Sonjéstraat**

Rotterdam Centrum.
Per direct 4
kamers te huur gelegen op 1e
en 2e étage van goed onderhouden
pand. Huurprijs gem*eubileerd*
ƒ375,— p*er* m*aand* incl*usief*. Per étage
eigen douche/toilet en keuken.
Etages zijn gestoffeerd,
Tram en bus naar Erasmus Universiteit
en HTS op 1 minuut
loopafstand. Inl*ichtingen* tijdens
kant*oor*uren: 070-634970 privé
070-548420.
