---
toegevoegd: 2024-07-25
gepubliceerd: 1954-04-14
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951887:mpeg21:a0114"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [46]
achternamen: [woudenberg]
kop: "kostganger"
---
Nette

# kostganger

met of
zonder pension, badcel en telefoon
aanwezig. Woudenberg,
**Jan Sonjéstraat** 46.
