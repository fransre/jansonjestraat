---
toegevoegd: 2024-08-04
gepubliceerd: 1934-03-08
decennium: 1930-1939
bron: Delftsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000076305:mpeg21:a0042"
onderwerpen: [faillissement]
huisnummers: [18]
achternamen: [knulst]
kop: "Faillissementen."
---
# Faillissementen.

Opgegeven door v*an* d*er* Graaf & Co*mpagnon* N.V.

Hubrecht Knulst, **Jan Sonjéstraat** 17 en
Ludovicus Franciscus Hendrikus Kosters,
Jan van Avennestraat 11a, tezamen handelende
onder den naam „Drukkerij Trio”, gevestigd
Gaffelstraat 123, alles Rotterdam.
R*echter-*c*ommissaris* mr. S.N.B. Halbertsma. Cur*ator* mr. K. Langemeijer,
Rotterdam.
