---
toegevoegd: 2024-07-23
gepubliceerd: 1963-05-09
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954138:mpeg21:a0074"
onderwerpen: [kleding, tekoop]
huisnummers: [21]
achternamen: [van-dilst]
kop: "kostuum,"
---
Te koop nieuw Horeca

# kostuum,

wegens omst*andigheden* m*aat* 52.
Prijs ƒ75,—. Tel*efoon* 38643. J.M. v*an* Dilst,
**Jan Sonjéstraat** 21a, Rotterdam-3.
