---
toegevoegd: 2024-07-21
gepubliceerd: 1980-08-29
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003077025:mpeg21:a00205"
onderwerpen: [woonruimte, tekoop]
huisnummers: [31]
kop: "leuke vooroorlogse beneden woning"
---
R*otter*dam, leuke vooroorlogse
ben*eden*won*ing* m*et* zonn*ige* tuin aan de
**Jan Sonjéstraat** 31. Bevat o*nder* a*ndere*
ruime gezell*ige* woonkam*er*, 2
sl*aap*kam*ers*, souterain onder gehele
huis. Te aanv*aarden* okt*ober* *19*80.
ƒ59.000,— k*osten* k*oper*. Bureau Dake,
tel*efoon* 010-180464.
