---
toegevoegd: 2024-08-14
gepubliceerd: 1908-03-16
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994763:mpeg21:a0190"
onderwerpen: [woonruimte, tehuur]
huisnummers: [18, 44]
kop: "Te Huur"
---
# Te Huur

een ruim Benedenhuis,
**Jan Sonjéstraat** 18 met
3 Kamers, 2 Alkoven,
Keuken, Tuin en hoogen
Kelder, huur ƒ18, en
aldaar N*ummer* 44 een 2de
Etage met 2 groote Kamers,
Keuken, 2 Alkoven
en grooten Zolder,
huur ƒ14. Adres: Heemraadssingel
hoek Middellandstraat,
van 2-5
uur.
