---
toegevoegd: 2021-04-29
gepubliceerd: 1904-02-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202159:mpeg21:a0074"
onderwerpen: [woonruimte, tehuur]
kop: "ƒ17, ƒ16, ƒ14, ƒ13."
---
# ƒ17, ƒ16, ƒ14, ƒ13.

Geknipt voor Ambtenaren.
In de **Jan Sonjéstraat**,
aan de Schermlaan, ruime
Benedenhuizen, vrije Bovenhuizen
en Eerste en
Tweede Etages te huur,
alle voorzien van vaste
Waschtafels met Waterleiding
in de Alkoven. Dagelijks
te zien en sleutels bij
Kroon, Middellandstr*aat* 51.
