---
toegevoegd: 2024-07-23
gepubliceerd: 1965-07-28
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955514:mpeg21:a0039"
onderwerpen: [huisraad, tekoop]
huisnummers: [38]
achternamen: [matthijssen]
kop: "eethoek,"
---
Te koop z*o* g*oed* a*ls* n*ieuwe*

# eethoek,

tafel, 4 stoelen ƒ125. C.A. Mathijssen,
**Jan Sonjéstraat** 38c.
