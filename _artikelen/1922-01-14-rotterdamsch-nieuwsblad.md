---
toegevoegd: 2024-08-12
gepubliceerd: 1922-01-14
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494048:mpeg21:a0347"
onderwerpen: [diefstal]
kop: "Diefstalkroniek"
---
# Diefstalkroniek

Ten nadeele van P. J., wonende **Jan Sonjéstraat**,
zijn van zijn in de Rotte liggend
schip, de luiken en een zinken emmer
ontvreemd.
