---
toegevoegd: 2023-07-31
gepubliceerd: 1926-07-17
decennium: 1920-1929
bron: Utrechtsch Nieuwsblad
externelink: "https://proxy.archieven.nl/0/AAE55112C7A446F0BD7039AA50D1D249"
onderwerpen: [ongeval, motor]
kop: "Motor-ongeluk."
---
# Motor-ongeluk.

Gisteravond *16 juli 1926*
kwart voor tien is de 18-jarige A.L.N. H.
uit de **Jan Sonjéstraat** te Rotterdam
met zijn motorfiets op den hoek van de
Kruiskade en den Mauritsweg, in volle
vaart tegen een motorwagen van lijn 4
opgereden. Hij en de duo-rijder, de 16-jarige
J. v*an* M. van den Hooidrift vielen.
H. kreeg wonden aan het linkerbeen en
aan het hoofd; v*an* M. brak het rechterbeen
en kreeg een hoofdwonde. Beiden
zijn per auto van den geneeskundigen
dienst naar het ziekenhuis aan den
Coolsingel overgebracht, waar zij ter
verpleging zijn opgenomen. De motorfiets
is geheel vernield. Het was de
eerste keer, dat H. op een motorfiets
reed.
