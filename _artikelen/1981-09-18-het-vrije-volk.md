---
toegevoegd: 2024-07-21
gepubliceerd: 1981-09-18
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960951:mpeg21:a0035"
onderwerpen: [woonruimte, tekoop]
huisnummers: [28]
kop: "grote beletage,"
---
T*e* k*oop* grote beletage, souterain
bestaande uit: Woonk*amer* 12×5
m*eter*, toilet 1×2.5 m*eter*, keuken
en bijkeuken, ev*en*t*ueel* ben*eden* 3
slaapk*amers* te maken, geh*eel* c*entrale* v*erwarming*,
hardhouten ramen, dubbelglas.
**Jan Sonjéstraat** 28A,
R*otter*dam. Vr*aag*pr*ijs* ƒ69.500,—
k*osten* k*oper*. Tel*efoon* 010-774829.
