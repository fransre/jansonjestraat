---
toegevoegd: 2024-08-11
gepubliceerd: 1922-06-02
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000191829:mpeg21:a0021"
onderwerpen: [familiebericht]
huisnummers: [34]
achternamen: [stoop]
kop: "Corry Stoop en Wenceslaus Welters."
---
Verloofd:

# Corry Stoop en Wenceslaus Welters.

**Jan Sonjéstraat** 34a.

Ontvangdag 2e Pinksterdag *1922* van
2-3½ uur.

30019 8
