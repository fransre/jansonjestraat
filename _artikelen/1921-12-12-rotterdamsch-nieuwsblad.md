---
toegevoegd: 2024-08-12
gepubliceerd: 1921-12-12
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493920:mpeg21:a0093"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [waaleman]
kop: "Dankbetuiging."
---
# Dankbetuiging.

Bij aankoop van een pakje Sodex, mocht
ik daarin aantreffen een bon welke recht geeft op
een paar prachtige Zilveren Bloemvaasjes,
waarvoor ik hierbij de Vereenigde Zeepfabrieken
te Zwijndrecht beleefd dank zeg.

Rotterdam, 3 December 1921,
M. Waaleman, **Jan Sonjéstraat** 30b, Rotterdam.

70637 40
