---
toegevoegd: 2024-08-14
gepubliceerd: 1910-03-14
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197836:mpeg21:a0117"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [3]
kop: "Te Koop:"
---
Wegens vertrek naar
het buitenland

# Te Koop:

twee mooie Roomijswagens,
een overdekte Wagen,
geschikt voor allen
handel en een mooi Stel,
geschikt voor een Winkel
om Roomijs te maken,
alles voor een kleinen
prijs. Te zien **Jan Sonjéstraat** N*ummer* 3a.
