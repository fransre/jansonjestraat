---
toegevoegd: 2024-07-24
gepubliceerd: 1956-07-03
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000336002:mpeg21:a00107"
onderwerpen: [motor, tekoop]
huisnummers: [39]
kop: "JLO motorrijw."
---
# JLO motorrijw.

in nieuwe st.,
± 10.000 km, m*et* verz*ekering* en bel*asting*.
Veel access*oires*. Op dokters voorschr.
Pr*ijs* nader overeen te
komen. **Jan Sonjéstraat** 39a.
