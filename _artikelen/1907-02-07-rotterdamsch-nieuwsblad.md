---
toegevoegd: 2024-08-14
gepubliceerd: 1907-02-07
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196595:mpeg21:a0076"
onderwerpen: [fiets, tekoop]
huisnummers: [4]
kop: "Jongensfiets,"
---
Te koop aangeboden
een sterke

# Jongenfiets,

zonder gebreken, met
prima Binnen- en Buitenbanden.
Prijs ƒ23.
Adres **Jan Sonjéstraat** 4, benedenhuis.
