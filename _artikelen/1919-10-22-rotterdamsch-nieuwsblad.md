---
toegevoegd: 2024-08-13
gepubliceerd: 1919-10-22
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494329:mpeg21:a0149"
onderwerpen: [familiebericht]
huisnummers: [28]
achternamen: [alester]
kop: "Grete Alester en Corns. J. Zeelenberg."
---
In plaats van kaarten.

Verloofd:

# Grete Alester en Corns. J. Zeelenberg.

R*otter*dam, 20 October 1919.

**Jan Sonjéstraat** 28a.

Hoogstraat 111B.

Geen ontvangdag.

10
