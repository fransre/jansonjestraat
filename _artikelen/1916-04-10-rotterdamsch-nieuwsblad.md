---
toegevoegd: 2024-08-13
gepubliceerd: 1916-04-10
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296285:mpeg21:a0141"
onderwerpen: [werknemer, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "Schilders"
---
Bekwame

# Schilders

en een Jongen gevraagd.
E.J. Weier,
Werkplaats Josefstraat 63.
Woonplaats **Jan Sonjéstraat** 38.
