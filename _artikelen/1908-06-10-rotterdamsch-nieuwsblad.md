---
toegevoegd: 2024-03-07
gepubliceerd: 1908-06-10
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994829:mpeg21:a0113"
onderwerpen: [verloren]
huisnummers: [3]
kop: "Verloren"
---
# Verloren

Vrijdagmiddag *5 juni 1908*, een granaten
Halsketting met
gouden tonnetjes (erfstukje).
omtrek **Jan Sonjéstraat**, Claes de Vrieselaan.
Tegen belooning terug
te bezorgen **Jan Sonjéstraat** 3,
Bovenhuis.
