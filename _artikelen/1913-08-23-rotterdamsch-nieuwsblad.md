---
toegevoegd: 2024-08-13
gepubliceerd: 1913-08-23
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295720:mpeg21:a0188"
onderwerpen: [familiebericht]
huisnummers: [42]
achternamen: [ladage]
kop: "Bastiaan Ladage en Trijntje G.M. van Zonneveld."
---
Getrouwd:

# Bastiaan Ladage en Trijntje G.M. van Zonneveld.

Die namens wederzijdsche familie
hun hartelijken dank betuigen
voor de vele bewijzen van belangstelling
bij hun huwelijk ondervonden.

Rotterdam, 21 Augustus 1913.

**Jan Sonjéstraat** 42a.

12
