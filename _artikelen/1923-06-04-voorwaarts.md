---
toegevoegd: 2024-08-11
gepubliceerd: 1923-06-04
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010210550:mpeg21:a0109"
onderwerpen: [diefstal]
kop: "Diefstallen."
---
# Diefstallen.

Mej*uffouw* S. in de **Jan Sonjéstraat**
deed aangifte bij de politie, dat een onbekend
persoon heeft getracht haar op te lichten voor
ƒ54.—.
