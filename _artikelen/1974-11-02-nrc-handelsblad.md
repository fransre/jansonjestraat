---
toegevoegd: 2021-04-29
gepubliceerd: 1974-11-02
decennium: 1970-1979
bron: NRC Handelsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBNRC01:000032875:mpeg21:a0252"
onderwerpen: [brand]
huisnummers: [33]
kop: "Openbare verkoping"
---
# Openbare verkoping

ex art. 1223 B.W.

t*en* o*verstaan* v*an* Notaris Mr. C.M. Esser en Mr. M.J.J.M. Boelen, c.n., waarn*emend* v*oor* h*et*
vac*ante* kantoor van Notaris Mr. A.W.A. van Eekelen, te Rotterdam, op 6
en 13 november 1974 van het door een binnenbrand lichtelijk beschadigde
pand en erf te Rotterdam,

**Jan Sonjéstraat** 33a-b

kad*astraal* Gemeente Delfshaven, Sectie C. nummer 3706, groot 1 are 2 centiaren.

Dit onroerend goed wordt verkocht zoals het er thans staat.

Door de executrice wordt beroep gedaan op de nietigheid van een
eventueel zonder haar toestemming aangegane huurovereenkomst;
het pand kan na betaling van de koopsom vrij van huur worden aanvaard.

Lasten per jaar: Grondbelasting ƒ57,62; straatbelasting ƒ166,25,
Schielands sluisgeld ƒ10,24 Drinkwaterleiding ƒ73,29 per kwartaal incl*usief* B.T.W.

Aanvaarding en betaling 13 december 1974; Bezichtiging 4, 5, 6 en 13
november 1974 van 10-13 uur. Inl*ichtingen* Mathenesserlaan 218, tel*efoon* 768833.
