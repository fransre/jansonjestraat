---
toegevoegd: 2024-03-17
gepubliceerd: 1978-01-07
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959737:mpeg21:a0088"
onderwerpen: [woonruimte, tekoop]
kop: "Almaco"
---
# Almaco

Rotterdam, **Jan Sonjéstraat**,
fraai verbouwde
eengezinswoning met
tuin. Koopsom
ƒ119.000,— k*osten* k*oper*.
