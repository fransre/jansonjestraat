---
toegevoegd: 2024-08-07
gepubliceerd: 1928-02-29
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514233:mpeg21:a0124"
onderwerpen: [woonruimte, tekoop]
huisnummers: [10]
kop: "Te koop:"
---
# Te koop:

Pand, **Jan Sonjéstraat** 10.
Spoed. Modern ingericht.
Vrij Bovenhuis
leeg Te zien, bevragen
**Jan Sonjéstraat** 10b.
