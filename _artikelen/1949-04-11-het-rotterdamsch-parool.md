---
toegevoegd: 2024-07-26
gepubliceerd: 1949-04-11
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164872085:mpeg21:a00102"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [28]
kop: "kinderwagen."
---
Te koop: I*n* g*oede* st*aat* z*ijnde* crème

# kinderwagen.

**Jan Sonjéstraat** 28a.
