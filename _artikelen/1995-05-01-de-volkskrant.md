---
toegevoegd: 2021-04-29
gepubliceerd: 1995-05-01
decennium: 1990-1999
bron: De Volkskrant
externelink: "https://resolver.kb.nl/resolve?urn=ABCDDD:010870827:mpeg21:a0144"
onderwerpen: [bewoner]
huisnummers: [35]
achternamen: [riedstra, ijkel]
kop: "Er is geen studentenleven zonder God"
---
# Er is geen studentenleven zonder God

In hartje centrum van Rotterdam
staat een studentenhuis dat er duidelijk
uitspringt. Terwijl niemand in
deze buurt vreemd opkijkt als er
weer eens een criminele afrekening
plaatsvindt, praten Jan Willem, Ale
en Maarten er dagelijks over God
en zijn grenzeloze liefde. Ze zijn lid
van de Rotterdamse Vereniging van Gereformeerde Studenten
en maken
deel uit van een van de weinige
zuilen die Nederland resten. Waar
veel studenten het geloof als een
opeenstapeling van restricties en
verplichtingen ervaren, gaan de bewoners
van **Jan Sonjéstraat** nummer
35 er helemaal in op. Zonder
God is er voor hen geen leven. En al
helemaal geen studentenleven.

‘Het geloof moet je niet laten versloffen.
Je moet niet in je eentje
gaan zitten wachten totdat Christus
terugkomt. Je hebt wel de opdracht
om anderen te vertellen over het
evangelie’, zegt Ale Riedstra (25)
als huisgenoot Jan Willem na het
eten de Bijbel resoluut dichtklapt.
De discussie is het logische vervolg
op de gelijkenis van de vijf wijze en
vijf dwaze maagden uit het Mattheüs-evangelie.

Jan Willem: ‘De boodschap is duidelijk.
God eren en God liefhebben,
en dat moet een ander ook kunnen
merken. Het doel is niet om met zo
weinig mogelijk inspanning toch in
de hemel zien te komen.’ Ale: ‘Je
bedoelt de short cut to heaven waar
dominee De Bruijne het altijd over
heeft?’ Ook Ale's aanstaande echtgenote
Wendelmoet mengt zich in
het gesprek. ‘Hoe moet het dan met
mensen in de bush bush? Wij hebben
het geluk dat we toevallig bij
christelijke ouders geboren zijn.
Maar wat doe je met die mensen?’

Terwijl het gemiddelde wensenpakket
van de student anno 1995
uit vergaande vrijheid en autonomie
bestaat, verkondigen deze vrijgemaakt
gereformeerden dagelijks
de boodschap van naastenliefde en
respect voor zowel ouders als overheid.
Maarten IJkel (20): ‘Medestudenten
denken vaak dat wij heel
veel moeten inleveren. Een vriend
van mij zei laatst nog: „Dan mag ik
zeker geen schietfilms meer kijken?”
Maar wij kijken heus wel tv.
We zijn alleen selectiever.’

Jan Willem: ‘Waar wij onze huisgenoten
op selecteren? Op man-zijn,
kookkunst en sociaalheid. Zo
iemand moet bij de rest passen en
deel willen nemen aan het huisgebeuren.
Wij eten dagelijks samen en
doen gezamenlijk inkopen. Een gezin
zijn we niet, maar wel een sociale
gemeenschap. Je betaalt ook
overal aan mee, ongeacht of je er
bent. En ja, we selecteren inderdaad
ook op geloof. Alle huisgenoten zijn
lid van de Vereniging van Gereformeerde Studenten
te Rotterdam.’

Hoe gezellig en harmonieus de
verhalen aan tafel ook mogen klinken,
hebben Jan Willem, Ale en
Maarten soms niet het gevoel dat zij
zich in een eng kringetje begeven?
Maarten: ‘Je komt overal dezelfde
mensen tegen omdat je je in hetzelfde
wereldje beweegt. En je hebt
daar al zoveel contacten dat je ze
ook niet elders hoeft te zoeken.’

Ale: ‘Wat buitenstaanders van
ons huis denken? Neem onze buurman.
Die heeft een volstrekt ander
wereldbeeld dan wij. Hij zegt: „Jullie
huis springt eruit in de straat”,
terwijl hij pas sinds een half jaar
weet dat we christen zijn. Wij proberen
gewoon open te staan voor
mensen uit de buurt. Je consumeert
niet alleen in menselijke relaties,
maar kijkt ook wat je voor een ander
kunt betekenen.’ Jan Willem: ‘Ik
noem dat: respect dat verder gaat
dan vrijblijvende tolerantie. Als ik
om me heen kijk, schrik ik wel eens.
Dan constateer ik zo'n onbeschaamd
egoïsme.’

Huize Jan Sonjé mag dan serieus
met intermenselijke relaties omspringen,
de relatie met eventuele
partners is eenzelfde lot beschoren.
Ale zal het huis binnen afzienbare
tijd verlaten voor een huwelijk met
Wendelmoet. Ze willen elkaar zekerheid
geven en dat ook duidelijk
aan anderen laten zien. Sex voor
het huwelijk komt daar niet aan te
pas. ‘Dat wij niet met elkaar naar
bed gaan’, nuanceert Maarten, ‘betekent
niet dat er helemaal geen
sprake mag zijn van seksualiteit.
Maar wij wachten tot na het trouwen
uit respect voor elkaar.’

Hoewel zé allemaal behoorlijk
strak en rechtlijnig denken, dwingen
de studenten toch respect af.
Ze zijn wie ze zijn en schamen zich
niet voor hun principes. Ze lezen,
naast de Bijbel, de meest uiteenlopende
literaire boeken, spellen ook
de niet-christelijke kranten en luisteren
net als ieder ander naar ‘wereldse’
popmuziek. Ook uitgaan is
niet uit den boze. Ale: ‘Maar als een
huisgenoot iedere dag naar de discotheek
zou gaan, of elke drie
maanden met een ander meisje in
bed zou belanden, dan zou ik wel
vragen wat hem daartoe beweegt.’

Als de anderen hard lachen om
zijn uitspraak, haast Ale zich te zeggen
dat ze wel begrijpen wat hij
bedoelt. ‘Nou was dit geval ook behoorlijk
hypothetisch. Laat ik het
zo uitdrukken: wij staan duidelijk
ergens voor en kunnen elkaar daar
ook op aanspreken.’

Ellen Danhof

*Onderschrift bij de foto*
De Bijbel staat centraal in Huize Jan Sonjé.

Foto Hans Heus — De Volkskrant
