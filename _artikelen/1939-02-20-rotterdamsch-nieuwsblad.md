---
toegevoegd: 2024-08-01
gepubliceerd: 1939-02-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164699042:mpeg21:a00202"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Zolderkamer."
---
# Zolderkamer.

gestoffeerd of gemeubileerd,
desverkiezend,
Ontbijt, hoog opschuifraam,
recht Plafond,
Kasten, Water, **Jan Sonjéstraat** 20b,
vrij
Bovenhuis.
