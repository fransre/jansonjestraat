---
toegevoegd: 2024-05-16
gepubliceerd: 1938-03-25
decennium: 1930-1939
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005429065:00001"
onderwerpen: [reclame]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Joh. L. van Hooijdonk"
---
# Joh. L. v*an* Hooijdonk

Stoffeerderij. Behangerij. Beddenmakerij

Complete Woning-inrichting

**Jan Sonjéstraat** 13
