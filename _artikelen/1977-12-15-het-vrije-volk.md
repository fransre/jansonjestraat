---
toegevoegd: 2021-04-29
gepubliceerd: 1977-12-15
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959718:mpeg21:a0269"
onderwerpen: [cafe, misdrijf]
huisnummers: [15]
kop: "Weedboer veroordeeld"
---
# Weedboer veroordeeld

Rotterdam — ~~Boekhandelaar Thaddeus van der L. (27)
uit Rotterdam, die tijdens het popfestival
in het Zuiderpark ‘experimentele
weed uit eigen tuin’
verkocht, is door de rechtbank
veroordeeld tot vier maanden
gevangenisstraf, waarvan drie
voorwaardelijk met een proeftijd
van twee jaar. Er was vijf
maanden, waarvan twee voorwaardelijk,
tegen hem geëist.~~

De rechtbank legde twee weken
gevangenisstraf op aan Willem B. (49)
uit Rotterdam, die in
de **Jan Sonjéstraat** een illegaal
gokhol had geëxploiteerd. De
man was al eerder voor een
soortgelijk feit veroordeeld.
Marga van I. (20) uit Rotterdam,
die in het speelhuis diverse
functies vervulde, kreeg — eveneens
wegens voortgezet misdrijf
— een boete van 750 gulden.
