---
toegevoegd: 2024-08-13
gepubliceerd: 1918-01-19
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296817:mpeg21:a0102"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [3]
achternamen: [ritmeijer]
kop: "Grondbascule"
---
# Grondbascule

te koop aangeboden
weinig gebruikt. Adres
J. Ritmeijer, **Jan Sonjéstraat** 3,
bij Middellandstraat.
