---
toegevoegd: 2024-08-02
gepubliceerd: 1937-12-29
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692050:mpeg21:a00229"
onderwerpen: [huisraad, muziek, tekoop]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "Te koop aangeboden:"
---
# Te koop aangeb*oden*:

zwarte studie-Piano,
groot Bureau 1.35 m*eter* bij
1.10 m*eter*, wegens vertrek
naar Indië. L. v*an* d*en* Bos,
**Jan Sonjéstraat** N*ummer* 36.
