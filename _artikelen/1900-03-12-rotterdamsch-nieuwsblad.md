---
toegevoegd: 2021-04-29
gepubliceerd: 1900-03-12
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000200560:mpeg21:a0025"
onderwerpen: [straat, schilder]
kop: "Nieuwe straten."
---
# Nieuwe straten.

Burgemeester en Wethouders hebben een
aantal nieuwe of in aanleg zijnde straten
namen gegeven en wel:

A. in Kralingen: Taborstraat, Commanderipstraat,
Nieuwe Rubensstraat, Rozenburgstraat,
Adamshofstraat en Evastraat.

B. op Jericho: Jericholaan, 1ste en 2de Jerichostraat,
Taxisstraat, Wilderveenstraat,
Kralingsche Plaslaan, Slachthuisstraat, Schuttersweg
en Ruhstraat.

C. In Oost Blommersdijk: Vijverhofstraat.

D. In de Polder Cool: Benthemdwarsstraat,
Van der Hilsdwarsstraat, Henegouwerlaan,
Kogelvangerstraat, Adrianadwarsstraat, Bajonetstraat,
Korte Bajonetstraat, Middellandplein,
Jan van Vuchtstraat, **Jan Sonjéstraat**,
Jan Porcellisstraat, Joost van Geelstraat, Robert Fruinstraat
(naar den hoogleeraar Robert Fruin),
Rochussenstraat (naar den schilder
Charles Rochussen), De Vliegerstraat, Lieve Verschuurstraat,
Hondiusstraat, Heemraadsingel.
