---
toegevoegd: 2024-08-04
gepubliceerd: 1934-11-29
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164674033:mpeg21:a00197"
onderwerpen: [kleding, tekoop]
huisnummers: [38]
kop: "Rokcostuum"
---
# Rokcostuum

Rokcostuum te koop,
8 g*u*ld*en*. **Jan Sonjéstraat** 38a,
2-m*aal* b*ellen*.
