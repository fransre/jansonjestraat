---
toegevoegd: 2024-08-03
gepubliceerd: 1936-12-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164686036:mpeg21:a00239"
onderwerpen: [familiebericht]
huisnummers: [26]
achternamen: [mager]
kop: "Hendrik Mager,"
---
Heden overleed tot onze diepe
droefheid, plotseling, na een
kortstondig lijden, mijn innig geliefde
Man en der Kinderen zorgzame
Vader, de Heer

# Hendrik Mager,

in den ouderdom van 60 jaren.

Mevr*ouw* J.C. Mager-Knops.

H.P.J. Mager.

J.G. Mager.

L.M. Mager.

P.J.H. Mager.

Rotterdam, 11 December 1936.

**Jan Sonjéstraat** 26a.

De teraardebestelling zal plaats
hebben 15 December a.s *1936*
op de Algemeene Begraafplaats „Crooswijk”.
Vertrek vanaf het sterfhuis te
2 uur n*a*m*iddag*.

37499 27
