---
toegevoegd: 2023-01-21
gepubliceerd: 1965-03-17
decennium: 1960-1969
bron: Nationaal Archief
externelink: "https://www.nationaalarchief.nl/en/research/photo-collection/aa99c098-d0b4-102d-bcf8-003048976d84"
onderwerpen: [brand]
kop: "Brand in magazijn zuivelhandel aan de Jan Sonjéstraat te Rotterdam"
---
# Brand in magazijn zuivelhandel aan de **Jan Sonjéstraat** te Rotterdam
