---
toegevoegd: 2024-08-13
gepubliceerd: 1913-05-31
decennium: 1910-1919
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000170591:mpeg21:a0013"
onderwerpen: [faillissement]
huisnummers: [20]
achternamen: [van-dulken]
kop: "Gerechtelijke aankondigingen."
---
# Gerechtelijke aankondigingen.

Bij vonnis der arrondissements-rechtbank te Rotterdam, van
28 Mei 1913, is J.M. van Dulken, reiziger, wonende te Rotterdam,
aan de **Jan Sonjéstraat** n*ummer* 20a, in staat van faillissement
verklaard, met benoeming van den edelachtbaren heer mr. A.A.F.W. van Romondt
tot rechter-commissaris, en van den ondergeteekende,
advocaat en procureur te Rotterdam, kantoor
houdende aan de Geldersche kade n*ummer* 23a aldaar, tot curator.

De Curator,

Mr. H.J.F. Heyman.

[Kosteloos.]

(3133)
