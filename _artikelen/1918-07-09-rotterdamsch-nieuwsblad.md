---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297056:mpeg21:a0058"
onderwerpen: [fiets, muziek, tekoop]
huisnummers: [5]
achternamen: [roodenburg]
kop: "Heerenrijwiel"
---
# Heerenrijwiel

te koop aangeboden.
Freewheel met Terugtraprem,
ƒ35. Hetzelfde
adres goede bespeelde
Viool voor ƒ20. Roodenburg,
**Jan Sonjéstraat** 5a.
