---
toegevoegd: 2024-08-05
gepubliceerd: 1931-12-28
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164656059:mpeg21:a00117"
onderwerpen: [ongeval, fiets]
achternamen: [de-koning]
kop: "Rijwielbotsing."
---
# Rijwielbotsing.

Zaterdagmorgen *26 december 1931* kwart voor tien
is de 13-jarige T. de Koning, wonende
**Jan Sonjéstraat**, in de Van Borselenstraat
aangereden door den 17-jarigen
wielrijder H. de V., uit de Jan van Avennesstraat.
De Koning viel en kwam met
het hoofd op een trottoirband terecht. Hij
kreeg een gat in het voorhoofd, welke
door een in de omgeving wonend geneesheer
is verbonden.
