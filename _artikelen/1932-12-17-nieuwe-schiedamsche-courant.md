---
toegevoegd: 2021-04-30
gepubliceerd: 1932-12-17
decennium: 1930-1939
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1932-12-17/edition/0/page/2"
onderwerpen: [misdrijf]
kop: "Fraaie praktijken!"
---
# Fraaie praktijken!

Opschudding in de **Jan Sonjéstraat**.

Gisterenochtend *16 december 1932* verscheen in de **Jan Sonjéstraat**
een man met een bord op zijn nek
waarop op beide zijden het volgende stond
te lezen:

„Schuldbekentenis te koop, groot ƒ500 ten
name van … (hier volgde de naam van
een te goeder naam en faam bekend staand
bewoner van de straat) … tegen elk aannemelijk
bod”.

Buren bemoeiden zich met het geval en
trachtten den man met het bord te overreden
om weg te gaan. Zelfs boden zij een bedrag
van ƒ100 aan om de schuldbekentenis in handen
te krijgen en zoodoende het schandaal
uit de wereld te helpen. Niets hielp echter.

De man verklaarde opdracht gekregen te
hebben van de Provinciale Commissiebank aan
de Ammanstraat om met het bord naar de
**Jan Sonjéstraat** te gaan.

Bij informatie is gebleken, dat de zaak als
volgt in elkaar zit:

Eenigen tijd geleden heeft de bewuste bewoner
van de **Jan Sonjéstraat** bij de bank een
bedrag van ƒ300 geleend en daarvoor een
schuldbekentenis van ƒ500 geteekend. Het
geld moest binnen 14 dagen betaald worden!
Door tegenslag in zaken was hem dit onmogelijk
en thans tracht de bank op deze manier
het geld binnen te krijgen. Een, om het maar
zacht uit te drukken, niet bepaald gebruikelijke
manier!

De Centrale Recherche is van een en ander
volkomen op de hoogte.
