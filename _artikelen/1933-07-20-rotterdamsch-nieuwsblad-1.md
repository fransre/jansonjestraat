---
toegevoegd: 2024-08-04
gepubliceerd: 1933-07-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164666022:mpeg21:a00094"
onderwerpen: [werknemer]
huisnummers: [38]
achternamen: [beyleveldt]
kop: "Bakkerij."
---
# Bakkerij.

B*iedt* z*ich* a*an* Jongen, 15 j*aar*,
reeds als zoodanig
werkzaam geweest.
Beyleveldt, **Jan Sonjéstraat** 38a.
