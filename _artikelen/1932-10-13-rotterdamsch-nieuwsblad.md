---
toegevoegd: 2024-08-05
gepubliceerd: 1932-10-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164661048:mpeg21:a00167"
onderwerpen: [reclame, dans]
huisnummers: [38]
achternamen: [beyleveldt]
kop: "Dansclubje."
---
# Dansclubje.

kunnen nog Dames geplaatst
worden, van 6-7¾ u*ur* en 8-11 uur.
Vlietstraat 27. A. Beyleveldt.
Dansleeraar.
**Jan Sonjéstraat** 38
