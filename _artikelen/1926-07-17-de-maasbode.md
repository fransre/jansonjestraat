---
toegevoegd: 2024-08-08
gepubliceerd: 1926-07-17
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000195539:mpeg21:a0018"
onderwerpen: [ongeval, motor, tram]
kop: "Ongevallen."
---
# Ongevallen.

Gisterenavond *16 juli 1926* zou de 18-jarige A.L.W. H.
uit de **Jan Sonjéstraat**, die nog nimmer
een motorfiets had bestuurd, een ritje
op zulk een vehikel door de stad gaan maken.
De 16-jarige J. v*an* M. uit de Hooidrift nam
plaats op de duozitting, de motor knalde dat
hooren en zien verging en het tweetal stoof
door de straten. H. had den motor echter niet
voldoende in zijn macht, zoodat toen hij op
den hoek Kruiskade-Diergardesingel was gekomen,
de motor veel te groote vaart had, met
gevolg, dat de onkundige rijder tegen een
tramwagen van lijn 4 opbotste. H. liep been- en
hoofdwonden op, v*an* M. brak zijn rechterbeen
en kreeg eveneens een hoofdwonde. Beiden
werden door den Geneeskundigen Dienst
naar het ziekenhuis aan den Coolsingel vervoerd,
waar zij werden opgenomen.
