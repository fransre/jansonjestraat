---
toegevoegd: 2024-07-25
gepubliceerd: 1952-09-11
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164882010:mpeg21:a00119"
onderwerpen: [faillissement]
huisnummers: [12]
achternamen: [dirven]
kop: "Faillissementen"
---
# Faillissementen

Uitgesproken 10 September:

N.V. Constructiewerkplaats J. Dirven J*unio*r,
**Jan Sonjéstraat** 12. Rotterdam. Curator:
mr. Q.J. Blaauw. Westplein 7,
R*otter*dam.
