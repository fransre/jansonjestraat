---
toegevoegd: 2024-08-02
gepubliceerd: 1937-09-14
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000193298:mpeg21:a0080"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [spitters]
kop: "Jacquelina"
---
Voor de overstelpende bewijzen
van deelneming ontvangen bij het
overlijden en begrafenis van ons
aller lieveling

# Jacquelina

betuigen wij onzen hartelijken
dank.

Familie Spitters.

Rotterdam, 14 September 1937.

**Jan Sonjéstraat** 33a.

40258 10
