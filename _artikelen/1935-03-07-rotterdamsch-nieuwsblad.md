---
toegevoegd: 2023-10-06
gepubliceerd: 1935-03-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164676008:mpeg21:a00137"
onderwerpen: [bewoner, radio]
kop: "U hebt toch 'n Radio?"
---
# U hebt toch 'n Radio?

Storing **Jan Sonjéstraat**.

Vrijwel den geheelen dag, slechts door kleine
tusschenpoozen onderbroken, treedt in de
radio-ontvangst in de **Jan Sonjéstraat** een hinderlijke
storing op, vermoedelijk veroorzaakt
door een niet afgeschermden motor. Het geluid
is zeer hinderlijk. De bewoners van deze
straat zouden gaarne zien, dat de eigenaar
van dezen motor maatregelen nam om het
euvel te verhelpen.
