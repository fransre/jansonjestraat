---
toegevoegd: 2024-08-06
gepubliceerd: 1930-08-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002147:mpeg21:a0028"
onderwerpen: [pension, woonruimte, tehuur]
huisnummers: [30]
kop: "Aangeboden:"
---
# Aangeboden:

mooi frisch gemeub*ileerd*
Kamertje, m*et* of z*onder* P*ension*,
alle gemakken, kl*ein* Gezin,
vrij Bovenhuis, net
iemand, b*eneden*h*uis,* b*oven*h*uis* **Jan Sonjéstraat** 30a.
