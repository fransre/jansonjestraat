---
toegevoegd: 2024-03-07
gepubliceerd: 1942-04-22
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003132:mpeg21:a0037"
onderwerpen: [huispersoneel]
huisnummers: [12]
achternamen: [de-ridder]
kop: "Meisje"
---
# Meisje

voor den geheelen dag.
De Ridder, **Jan Sonjéstraat** 12.
Aanmelden na half acht.

A2021
