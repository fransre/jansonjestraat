---
toegevoegd: 2024-08-04
gepubliceerd: 1933-05-04
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164665005:mpeg21:a00210"
onderwerpen: [woonruimte, tehuur]
huisnummers: [4]
kop: "Te huur:"
---
# Te huur:

ongem*eubileerde* Zitkamer op vrij
Bovenhuis, in stil Gezin,
voorzien van Licht
en Water en Kasten.
**Jan Sonjéstraat** 4a.
