---
toegevoegd: 2024-08-13
gepubliceerd: 1917-08-10
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296682:mpeg21:a0111"
onderwerpen: [faillissement, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "Faillissementen in het Arrondissement Rotterdam."
---
# Faillissementen in het Arrondissement Rotterdam.

Uitgesproken faillissementen:

Gehomologeerd is het accoord, ad. 10% door
E.J. Weier, schilder. **Jan Sonjéstraat** 38a, zijn
crediteuren aangeboden.
