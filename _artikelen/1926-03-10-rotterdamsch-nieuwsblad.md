---
toegevoegd: 2024-08-08
gepubliceerd: 1926-03-10
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495295:mpeg21:a0233"
onderwerpen: [familiebericht]
huisnummers: [39]
achternamen: [van-wulven]
kop: "F.G. van Wulven,"
---
Dankbetuiging.

Voor de vele bewijzen van deelneming,
ontvangen bij het overlijden
van mijn geliefden Echtgenoot, den
Heer

# F.G. van Wulven,

betuig ik, uitsluitend langs dezen
weg, mijn hartelijken dank.

E. van Wulven-van de Broek.

Rotterdam, 10 Maart 1926.

**Jan Sonjéstraat** 39a.

(v*oor*h*een* F*irm*a A. Héron.
Weste Wagenstraat 97.)

16
