---
toegevoegd: 2024-07-28
gepubliceerd: 1941-09-16
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002752:mpeg21:a0169"
onderwerpen: [ongeval]
achternamen: [leen]
kop: "Ongevallen."
---
# Ongevallen.

~~Op den Crooswijkscheweg is gisteren *15 september 1941* de
6-jarige G.J. van Kampenhout, wonende
Pleretstraat, van een vrachtauto gevallen,
waarbij hij een schedelbreuk bekwam.
Hij werd in het Ziekenhuis, aan
den Bergweg ter verpleging opgenomen.~~

~~Bij de Tunnel aan den Beukelsdijk is
gisteren *15 september 1941* de 42-jarige mevr*ouw* L. van Dorp-Flipse,
wonende Coolschestraat, van haar
rijwiel gevallen. Met een hersenschudding
werd zij in een der hospitaalschepen ter
verpleging opgenomen.~~

Daarin werd ook de 27-jarige D. Leen,
wonende **Jan Sonjéstraat**, opgenomen,
die aan de Waalhaven op een bouwwerk
een vallenden balk op het hoofd had gekregen
en daardoor een hersenschudding
had bekomen.
