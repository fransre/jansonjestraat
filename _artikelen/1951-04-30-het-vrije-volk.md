---
toegevoegd: 2024-07-26
gepubliceerd: 1951-04-30
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010950760:mpeg21:a0046"
onderwerpen: [huisraad, tekoop]
huisnummers: [44]
achternamen: [boer]
kop: "Meubelen en Huisraad"
---
# Meubelen en Huisraad

Te koop 2 faut., 4 stoelen,
ƒ45.—. A. Boer, **Jan Sonjéstraat** 44, C*entrum*.
Na 18 uur.
