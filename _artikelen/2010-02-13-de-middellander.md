---
toegevoegd: 2023-01-09
gepubliceerd: 2010-02-13
decennium: 2010-2019
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2010-02-13-de-middellander-origineel.jpg"
onderwerpen: [kunst, schilder]
achternamen: [van-der-ploeg, baan, lutgerink, zweerts-de-jong]
kop: "Kunst in de Jan Sonjéstraat"
---
# Kunst in de **Jan Sonjéstraat**

Op zaterdag 13 februari *2010* werd in de **Jan Sonjéstraat**
een print van een schilderij van de
straat onthuld met als titel ‘Gezicht op de Rotte’.
Zo'n veertig mensen woonden de onthulling
bij. Bewoner van de straat Sander Zweerts de Jong
hield daarbij het volgende verhaal:

„Welkom bij de officiële onthulling
van een nieuw kunstwerk hier in
de **Jan Sonjéstraat**. Ik woon hier
nog maar een klein jaar, maar
omdat ik zelf ben afgestudeerd
als tekenaar/schilder vond men
het een goed idee als ik een
kleine inleiding naar de onthulling
zou verzorgen. En dat doe
ik graag.

Wat we straks gaan zien is een
print van het schilderij van Jan Gabrielsz. Sonjé
met als titel
‘Gezicht op de Rotte’.
Een jaar geleden hebben Jan van der Ploeg,
Barbara Baan en
Aad en Tineke Lutgerink bedacht
dat het een mooi idee zou
zijn als er in de **Jan Sonjéstraat**
ook een werk van deze 17e
eeuwse Rotterdamse schilder te
zien zou zijn. Na een flink traject
hangt hier tenslotte het resultaat:
een bijzonder fraaie print
van het Gezicht op de Rotte.

## Licht uit Italië

Jan Sonjé is al ver in de zestig als
hij dit schilderij maakt in 1692.
Hij schilderde het op een koperplaat,
iets wat in die tijd veel gebruikelijker
was dan tegenwoordig.
Koper was toen betrekkelijk
goedkoop; je kan er heel fijn op
schilderen en doordat de verf er
dun op geschilderd wordt er een
fantastische transparantie ontstaat.
Op koper schilderen kwam op in
Italië en veel zeventiende eeuwse
schilders trekken naar Italië
om daar de landschappen in het
felle zonlicht te schilderen.
Van Jan Sonjé is niet bekend of
hij ook daadwerkelijk naar Italië
is gegaan, maar hij is wel zeker
beïnvloed geweest door schilders
die de Italiaanse landschappen
vereeuwigden. Sonjé
deed dat ook, maar het kan
goed zijn dat hij daarin vooral
anderen nadeed. Op latere leeftijd
ging hij echter weer Rotterdamse
schilderijen maken.

‘Gezicht op de Rotte’ heeft echter
op geen enkele manier Italiaanse
invloeden. Er schijnt geen
zon op het schilderij, de lucht is
grijs en van veelkleurige tonen is
geen sprake. Het is een wat grijze
wereld die Jan Sonjé schildert
en daarin misschien wel
een van zijn meer interessante
schilderijen.

Want laten we eerlijk zijn. Jan Sonjé
is geen grote naam in de
wereld geworden. Zeker niet als
we zien wat er uit de 17e eeuw
allemaal is voortgekomen:
Vermeer, Rembrandt, Potter of
Jan Steen. Jan Sonjé is een kleine
naam… maar wel een zeer
vaardige schilder.

## Wat zien we

We zien een meanderende rivier,
de Rotte, in de buurt van de
Crooswijksebocht. Op de voorgrond
een schuit die werd gebruikt
voor het vervoer van personen
en een beladen roeiboot
voor het vervoer van goederen.
Een groot gedeelte van het
schilderij wordt ingenomen door
een wolkenlucht. Iets dat wel
aan de 17e eeuwse meesters
was toevertrouwd. Het geheel
straalt zowel bedrijvigheid als
rust uit. We zien wat zwanen,
een paar molens en een aantal
boten op weg naar Rotterdam.
Een alledaags zeventiende
eeuws Rotterdams tafereel.

Dat het ‘Gezicht op de Rotte’ nu
in de **Jan Sonjéstraat** hangt is
een mooi eerbetoon aan deze
schilder. Voor ons is het ook ontzettend
leuk om iets te hebben
hangen van een naam die wij zo
dagelijks gebruiken.

Ik ben heel blij dat ik sinds kort
in deze straat woon en er nog
lang te mogen wonen. Ik vind
het ontzettend leuk dat er dergelijke
activiteiten worden verzonnen
en vooral worden uitgevoerd.
Daarbij moet ik niet
vergeten dat het financieel mogelijk
is gemaakt door geld van
Delfshavense Duiten en met
medewerking van het Historisch Museum Rotterdam,
waar het
origineel van het schilderij aanwezig
is.”

De officiële opening werd verricht
door de oudste bewoner
van de straat Aad Lutgerink en
de jongste bewoner Sarah, bijgestaan
door haar moeder.

*Onderschrift bij de foto*
De oudste bewoner van de straat Aad Lutgerink verricht de officiële opening
