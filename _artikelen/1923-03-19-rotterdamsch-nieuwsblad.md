---
toegevoegd: 2024-08-11
gepubliceerd: 1923-03-19
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494675:mpeg21:a0075"
onderwerpen: [huispersoneel]
huisnummers: [41]
kop: "werkster,"
---
Gevraagd nette ongehuwde

# werkster,

twee halve dagen loon
ƒ1.50, 9-2 uur, v*an* g*oede* g*etuigen* v*oorzien*
**Jan Sonjéstraat** 41a
niet 's avonds.
