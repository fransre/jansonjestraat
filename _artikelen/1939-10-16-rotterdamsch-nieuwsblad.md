---
toegevoegd: 2024-07-30
gepubliceerd: 1939-10-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164703037:mpeg21:a00283"
onderwerpen: [huisraad, tekoop]
huisnummers: [18]
kop: "3 kamers zeil,"
---
Te koop

# 3 kamers zeil,

Ganglooper, 70 *cm* br*eed*,
nieuw, door omstandigheden,
moet weg, spotkoopje.
**Jan Sonjéstraat** 18b.
