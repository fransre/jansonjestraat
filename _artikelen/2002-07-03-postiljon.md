---
toegevoegd: 2024-02-21
gepubliceerd: 2002-07-03
decennium: 2000-2009
bron: Postiljon
externelink: "https://www.jansonjestraat.nl/originelen/2002-07-03-postiljon-origineel.jpg"
onderwerpen: [bewoner]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Straatfeest voor actieve 100 jarige"
---
# Straatfeest voor actieve 100 jarige

Hillegersberg — Kinderen,
kleinkinderen, achterkleinkinderen,
neven, nichten, oudburen,
kennissen, medehuisgenoten,
de zaterdag *29 juni 2002* 100 jaar
geworden Jan van Hooijdonk
had een feestje te vieren en
daar deed iedereen aan mee.

Ter ere van zijn verjaardag boden de
bewoners van **Jan Sonjéstraat**, samen
met (klein)kinderen van ‘eeuweling’ Jan
van Hooijdonk een straatfeest aan. Tot
zes jaar geleden maakte Van Hooijdonk
nog actief deel uit van de straatgroep
Jan Sonjé.

„Opa werd 's morgens *29 juni 2002* met een busje
gehaald vanaf het Reuma Verpleeghuis”,
vertelt een van de (achter)kleinkinderen.

„'s Morgens *29 juni 2002* is daar een receptie gehouden.
Tussen twee en vier uur is het
straatfeest gevierd. We hebben nog
even met elkaar de straat opgeruimd en
daarna is de familie tot half tien 's
avonds in het Van der Valk-restaurant
geweest voor een buffet.”

Jan van Hooijdonk deed de hele dag *29 juni 2002*
onvermoeibaar aan alle activiteiten
mee. „Hij heeft genoten”, vertelt zijn
nazaat. „Tijdens het straatfeest kwamen
veel oude bekenden langs om samen
met hem herinneringen op te halen en
hem het beste te wensen voor de toekomst.”

Ook zondag *30 juni 2002* stond in het teken van de
verjaardag van de eeuweling. „Het was
dat opa het wat te koud vond, anders
waren we ook nog een rondrit door het
Lage Bergse Bos gaan maken.”

Om straks in alle rust te kunnen nagenieten
krijgen Jan van Hooijdonk en de
bewoners van het Reuma verpleeghuis
een door een familielid gemaakt videoverslag
van het verjaardagsfestijn.

*Onderschrift bij de foto*
Jan van Hooijdonk vierde zaterdag *29 juni 2002* een straatfeest mee ter ere van zijn
honderdste verjaardag.

(Foto: Eduard Engel)
