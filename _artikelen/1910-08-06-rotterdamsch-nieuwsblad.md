---
toegevoegd: 2024-08-14
gepubliceerd: 1910-08-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197044:mpeg21:a0087"
onderwerpen: [werknemer]
huisnummers: [15, 17]
kop: "Een Jongen"
---
# Een Jongen

gevraagd aan de Electrische
Wasscherij **Jan Sonjéstraat** 15-17,
bij de
Middellandstraat.
