---
toegevoegd: 2024-08-07
gepubliceerd: 1928-04-13
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514268:mpeg21:a0136"
onderwerpen: [reclame, tehuur]
huisnummers: [38]
kop: "Feestzaal"
---
# Feestzaal

't Westen. **Jan Sonjéstraat** 38.
Tel*efoon* 30654. 1e
klas inrichting voor
Bruiloften Partijen en
Vergaderingen. Billijke
condities.
