---
toegevoegd: 2024-05-16
gepubliceerd: 1922-04-07
decennium: 1920-1929
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005422065:00001"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [kets-de-vries]
kop: "Jacob Kets de Vries en Branco van Dantzich"
---
Ondertrouwd:

# Jacob Kets de Vries en Branco van Dantzich

Rotterdam, 6 April 1922

**Jan Sonjéstraat** 24B.

Roovalkstraat 10A.
