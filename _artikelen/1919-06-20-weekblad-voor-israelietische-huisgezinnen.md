---
toegevoegd: 2024-05-15
gepubliceerd: 1919-06-20
decennium: 1910-1919
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005421077:00001"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [bosman]
kop: "Alexander Frank en Alida Bosman."
---
Ondertrouwd:

# Alexander Frank en Alida Bosman.

Nijmegen, Lange Hezelstraat 34,

Rotterdam, **Jan Sonjéstraat** 24,

19 juni *1919*

Huwelijksinzegening: 10 Juli *19*19
י״ב תמוז תרע״ט
*12 Tamuz 5679*
in de Synagoge Botersloot ten 1½ uur.

Receptie 10 Juli *1919* van 2-5 uur,
**Jan Sonjéstraat** 24a.
