---
toegevoegd: 2024-08-05
gepubliceerd: 1933-01-11
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164663011:mpeg21:a00066"
onderwerpen: [huispersoneel]
huisnummers: [34]
kop: "Publieke verkoopingen."
---
# Gevraagd:

net Meisje voor hulp
Huishouding, halve dagen,
circa 15 jaar. Goede
behandeling. **Jan Sonjéstraat** 34b,
tusschen 7 en 8.
