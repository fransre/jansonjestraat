---
toegevoegd: 2023-08-01
gepubliceerd: 1929-05-11
decennium: 1920-1929
bron: Onze courant
externelink: "https://resolver.kb.nl/resolve?urn=MMWFA01:000260031:mpeg21:a00141"
onderwerpen: [diefstal]
kop: "Twee beruchte inbrekers na achtervolging over daken gegrepen."
---
# Twee beruchte inbrekers na achtervolging over daken gegrepen.

Gister nacht *10 mei 1929* heeft de Rotterdamsche
politie, dank zij de activiteit van een
burger, een bijzonder goede vangst kunnen
doen, zoodat twee beruchte Rotterdamsche
inbrekers, de gebr*oeders* K.,
thans achter slot en grendel zitten.

Dit tweetal had zich n*ame*l*ijk* in den loop
van den nacht, vermoedelijk over de
daken van aangrenzende panden, toegang
weten te verschaffen tot de pettenfabriek
van v*oor*h*een* Wolf en Zonen aan
de 1e Middellandstraat 110.

Toen zij echter goed en wel aan het
werk zouden gaan had een hunner het
ongeluk, een ruit te breken. Dit werd
door een voorbijganger gehoord, die onmiddellijk
de politie waarschuwde. Het
gevolg was, dat binnen zeer korten
tijd het geheele huizenblok waarin de
fabriek is gelegen, werd afgezet, zodat
ontvluchten onmogelijk zou zijn.

De inbrekers, die bij de brandkast
aan het werk waren gegaan, bemerkten
spoedig het onraad en klommen
weer op de daken, achternagezeten door
eenige politiemannen.

De kerels gaven zich echter niet spoedig
gewonnen en sprongen door een
dakraam, dat met een hor was afgesloten.
Zoo kwamen zij in een pand,
dat uitgang heeft in de **Jan Sonjéstraat**.
Zij stormden de trappen af,
wisten de voordeur te openen en stonden
op straat. Reeds wilden beide de
vlucht nemen, toen zij een agent zagen.
Deze riep hun het „halt” toe en loste
als waarschuwing een schot in de
lucht.

De inbrekers bemerkten toen, dat er
niets meer aan te doen was en lieten
zich arresteeren. Zij zijn de 36-jarige
G. K. en diens broer de 32 jarige J.J. K.
Bij fouilleering werden electrische
zaklantaarns en loopers op hen gevonden.
Het bleek, dat zij in de fabriek het
kantoor hadden overhoop geld en
op energieke wijze begonnen waren,
de brandkast te forceeren.

Een inbrekerszak en een stel zeer gevaarlijke
uitstekende inbrekerswerktuigen,
waaronder een zware koevoet
met verlengstuk, tangen en scherpgemaakte
breekijzers hadden zij bij hun
vlucht moeten achterlaten. Hieruit en
uit het verleden der heeren bleek voldoende
dat men met vaklui te doen
had. Beiden zijn, zooals gezegd, goede
bekenden van de politie, kort geleden
uit de gevangenis ontslagen.

Het tweetal heeft, daar er niets anders
opzat, volledig bekend.
