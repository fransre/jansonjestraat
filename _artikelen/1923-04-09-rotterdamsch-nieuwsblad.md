---
toegevoegd: 2024-08-11
gepubliceerd: 1923-04-09
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494541:mpeg21:a0143"
onderwerpen: [familiebericht]
huisnummers: [12]
achternamen: [schenk]
kop: "W.C. Schenk en Mejuffouw N. Schenk-Prins,"
---
Voor de vele bewijzen van deelneming,
ondervonden bij het over
lijden van onze dierbare Ouders
Behuwd-, Groot- en Overgrootouders,
den Heer

# W.C. Schenk en Mej*uffouw* N. Schenk-Prins,

betuigen wij onzen welgemeenden
dank.

Uit aller naam,

W.C. Schenk S*enio*r.

**Jan Sonjéstraat** 12a.

16
