---
toegevoegd: 2024-07-25
gepubliceerd: 1953-10-26
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164885048:mpeg21:a00034"
onderwerpen: [ongeval, auto]
achternamen: [torenga]
kop: "Ongevallen"
---
# Ongevallen

~~In de Vroesenlaan is gistermiddag *25 oktober 1953*
de 13-jarige Lambertus Bedaf
uit de Abraham Kuyperlaan
tegen een auto gelopen, waardoor
hij op straat viel en zijn onderbenen
brak. Hij liep ook een hersenschudding
op en is in het Bergwegziekenhuis
opgenomen.~~

De 64-jarige opperman J.H. Torenga
uit de **Jan Sonjéstraat** is
gistermiddag *25 oktober 1953* door een auto op de
Henegouwerlaan aangereden waarbij
hij een gebroken been en een hersenschudding
opliep.

~~Voor haar woning m de Rosener Manzstraat
is Zaterdagmiddag *24 oktober 1953*
de 56-jarige J.T. Hart-Broere gevallen,
waarbij zij het rechter heupbeen
brak. Zij is naar het Sint Franciscus Gasthuis
overgebracht.~~
