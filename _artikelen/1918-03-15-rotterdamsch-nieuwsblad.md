---
toegevoegd: 2024-08-13
gepubliceerd: 1918-03-15
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296963:mpeg21:a0115"
onderwerpen: [tekoop]
huisnummers: [27]
kop: "ƒ2500.—"
---
# ƒ2500.—

à 4½% verkrijgbaar voor 1e
Hypotheek van particulier.
**Jan Sonjéstraat** 27b.
