---
toegevoegd: 2024-07-29
gepubliceerd: 1940-05-25
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002396:mpeg21:a0040"
onderwerpen: [huispersoneel]
huisnummers: [20]
achternamen: [veger]
kop: "Net dienstmeisje"
---
# Net dienstmeisje

14 jaar, biedt zich aan.
Halve dagen, ook intern
Co Veger, **Jan Sonjéstraat** 208.
