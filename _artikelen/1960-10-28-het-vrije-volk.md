---
toegevoegd: 2024-03-17
gepubliceerd: 1960-10-28
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951190:mpeg21:a0052"
onderwerpen: [werknemer, schilder]
huisnummers: [29]
achternamen: [pennings]
kop: "Schilders"
---
# Schilders

gevraagd

P. Levering, Spechtstraat 7b, Rotterdam, Telefoon 74798.
Na 6 uur P. Pennings, **Jan Sonjéstraat** 29b.
