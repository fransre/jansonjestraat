---
toegevoegd: 2024-07-26
gepubliceerd: 1947-08-27
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010949770:mpeg21:a0048"
onderwerpen: [werknemer]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Stoffeerdersleerling"
---
# Stoffeerdersleerling

Goed loon. J*.*L*.* v*an* Hooijdonk.
**Jan Sonjéstraat** 13b West.
