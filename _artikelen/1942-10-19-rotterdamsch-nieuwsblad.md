---
toegevoegd: 2024-07-26
gepubliceerd: 1942-10-19
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002978:mpeg21:a0008"
onderwerpen: [huisraad, tekoop]
huisnummers: [38]
achternamen: [stepanenco]
kop: "Inboedel,"
---
# Inboedel,

meubels, vazen, tafels,
vloerkleed, balatum, schilderstukken,
1-pers*oons* ledikant. 2-pers*oons*
ledikant met bedden, lampekappen,
vulkachel, dressoir,
antiek kastje Te bez*ichtigen* Maandag *19 oktober 1942*
en Dinsdag *20 oktober 1942* den heelen dag tot
5 uur, bel direct J. Stepanenco.
**Jan Sonjéstraat** 38, 2e et*age*, R*otter*dam.
