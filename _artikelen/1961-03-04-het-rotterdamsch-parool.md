---
toegevoegd: 2024-07-23
gepubliceerd: 1961-03-04
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259011004:mpeg21:a00265"
onderwerpen: [auto, tekoop]
huisnummers: [38]
achternamen: [gelton]
kop: "Opel bestelwagen,"
---
# Opel bestelwagen,

500 k*ilo*g*ram*,
staat, prima banden,
bouwjaar 1957, van 1e eigenaar.
Te bevragen F*irm*a P. Gelton & Z*oo*n,
**Jan Sonjéstraat** 38B,
Rotterdam, Tel*efoon* 34158,
5 uur (01893)-348.
