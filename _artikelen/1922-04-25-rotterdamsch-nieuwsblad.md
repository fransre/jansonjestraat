---
toegevoegd: 2024-08-11
gepubliceerd: 1922-04-25
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494230:mpeg21:a0083"
onderwerpen: [woonruimte, tehuur]
huisnummers: [11]
kop: "Te huur:"
---
# Te huur:

keurig gem*eubileerde* Zit- en
Slaapkamer, ook met
gebruik v*an* Keuken, elec*trisch*
licht. **Jan Sonjéstraat** N*ummer* 11a.
