---
toegevoegd: 2024-07-29
gepubliceerd: 1940-12-17
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002543:mpeg21:a0044"
onderwerpen: [dieren]
huisnummers: [29]
kop: "Dierenvrienden"
---
# Dierenvrienden

Te koop: gitzwart
hondje. 9 weken oud,
klein blijvend ras. **Jan Sonjéstraat** 29A.
