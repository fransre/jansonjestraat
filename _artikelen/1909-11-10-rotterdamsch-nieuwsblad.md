---
toegevoegd: 2021-07-24
gepubliceerd: 1909-11-10
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010198038:mpeg21:a0122"
onderwerpen: [woonruimte, tehuur]
huisnummers: [35]
kop: "Te Huur:"
---
# Te Huur:

een vrij Benedenhuis,
bevattende Voorkamer,
Achterkamer, Alkoven,
Keuken, droog Souterrain
met Kamer, Warande
en Tuin, p*er* m*aand*
ƒ19 aan de **Jan Sonjéstraat**
N*ummer* 35. Sleutel
aan het Bovenhuis.
