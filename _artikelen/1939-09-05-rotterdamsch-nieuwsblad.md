---
toegevoegd: 2024-07-30
gepubliceerd: 1939-09-05
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164703003:mpeg21:a00106"
onderwerpen: [huispersoneel]
huisnummers: [35]
kop: "Dagmeisje"
---
# Dagmeisje

gevraagd in Gezin zonder
Kinderen. Zondags
vrij. Aanmelden na 7
uur. **Jan Sonjéstraat** N*ummer* 35a.
