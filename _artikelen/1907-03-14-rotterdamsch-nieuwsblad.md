---
toegevoegd: 2024-08-14
gepubliceerd: 1907-03-14
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196625:mpeg21:a0047"
onderwerpen: [reclame]
huisnummers: [3]
achternamen: [noorthoek]
kop: "Mejuffrouw J.J. Noorthoek,"
---
Verhuisd:

# Mej*uffrouw* J.J. Noorthoek,

van Bergweg 307 naar

**Jan Sonjéstraat** 3.

Blijft zich beleefd aanbevelen tot
geven van Stenografielessen
systeem Stolze Wery.

15257 14
