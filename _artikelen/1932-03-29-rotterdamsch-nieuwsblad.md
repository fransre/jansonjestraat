---
toegevoegd: 2023-10-07
gepubliceerd: 1932-03-29
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164658031:mpeg21:a00238"
onderwerpen: [reclame]
huisnummers: [38]
achternamen: [beyleveldt]
kop: "Gewoon- en snelwitterij"
---
Gewoon- en

# snelwitterij

Beyleveldt & co*mpagnon*,
Tel*efoon* 32301. **Jan Sonjéstraat** 38a,
in één keer
klaar. Geen Kunstkalk.
Concurreerend witwerk.

Telefoon 32301. Gewoon- en

# snelwitterij

D. Zoetmulder en
co*mpagnon*, Havenstraat 194.
**Jan Sonjéstraat** 38. Speciaal
Adres voor beter,
concurreerend witwerk.
