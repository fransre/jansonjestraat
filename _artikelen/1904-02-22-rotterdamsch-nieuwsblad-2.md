---
toegevoegd: 2021-04-29
gepubliceerd: 1904-02-22
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202178"
onderwerpen: [woonruimte, tehuur]
kop: "Zoek niet langer"
---
# Zoek niet langer

en zoek maar uit. **Jan Sonjéstraat**,
lieve Benedenhuizen
met geheele
Kelders, buiten ingaande,
ƒ16; Etages van ƒ13
en 14 en een zeldzaam
Bovenhuis ƒ19. ~~In de
Blommerd*ijkse*laan 4 het Bovenhuis
ƒ17, idem Winkelhuis
ƒ4, geschikt voor
R.-C. Boekhandel. Hillegondastraat
169 Winkelhuis
ƒ3.25, geschikt voor Drogist.~~
