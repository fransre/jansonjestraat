---
toegevoegd: 2024-07-21
gepubliceerd: 1973-06-16
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002890013:mpeg21:a00447"
onderwerpen: [angelique, dans]
huisnummers: [33]
kop: "Club Angelique"
---
Nee, u leest het niet verkeerd, al is het dan helemaal te gek.

# Club Angelique

gaat nu 's middags van 2 tot 5 uur voor ƒ20 en 's avonds vanaf
8.30 voor ƒ30,— draaien, ook 's zaterdags.

Voor deze belachelijke prijs kunt u onze nieuwste films zien en
krijgt u natuurlijk een heerlijk drankje aangeboden door een van
onze charmante gastvrouwen. Als klap op de vuurpijl kunt u zich
helemaal ontspannen en naar onze 5 gogo-girls kijken, die een
exotisch dansnunimer weg zullen geven dat echt wel de moeite
waard is. Maar ja, komt u zelf kijken in onze vernieuwde club. Ons
adres is: **Jan Sonjéstraat** 33a (2e zijstraat 1e Middellandstr*aat*)
Telefoon 010-256188.

R16
