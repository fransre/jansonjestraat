---
toegevoegd: 2023-04-25
gepubliceerd: 1947-07-12
decennium: 1940-1949
bron: Rotterdamsch Parool / De Schiedammer
externelink: "https://schiedam.courant.nu/issue/SP/1947-07-12/edition/0/page/4"
onderwerpen: [werknemer]
huisnummers: [12]
achternamen: [dirven]
kop: "Voorman"
---
N.V. Constructiewerkplaats
v*oor*h*een* J. Dirven J*unio*r.
vraagt
voor terstond:
all-round vakman, die als

# voorman

de leiding krijgt over een
aantal koperslagers, fitters
en machine-bankwerkers.
Schr*iftelijke* soll*icitaties* aan ons kantoor:
**Jan Sonjéstraat** 12, R*otter*dam.
