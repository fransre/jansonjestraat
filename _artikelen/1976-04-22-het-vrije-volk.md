---
toegevoegd: 2024-07-21
gepubliceerd: 1976-04-22
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959205:mpeg21:a0256"
onderwerpen: [familiebericht]
huisnummers: [20]
achternamen: [de-jong]
kop: "Teunis Ruurds de Jong"
---
Een goed mens is van ons
heengegaan, mijn man,
onze vader en opa, broer,
zwager en oom

# Teunis Ruurds de Jong

op de leeftijd van 59 jaar

Mede namens verdere
familie:

J.M.M. de Jong-Steinbach

Gijs en Lenie

Henk en Mary

Ed en Gaby

en kleinkinderen

Rotterdam, 21 april 1976

**Jan Sonjéstraat** 20b

Rouwbezoek en gelegenheid
tot condoleren vrijdag *23 april 1976*
van 19.15 tot 19.45
uur en zaterdag *24 april 1976* van 12.30
tot 13.00 uur in de rouwkamer
van het Van Damziekenhuis,
Westersingel 115.

De crematie zal plaatsvinden
maandag *26 april 1976* a*an*s*taande* in het
crematorium Rotterdam-Zuid,
Maeterlinckweg 101

Vertrek vanaf de rouwkamer
om 14.00 uur. Aankomst
in het crematorium
circa 14.35 uur.
