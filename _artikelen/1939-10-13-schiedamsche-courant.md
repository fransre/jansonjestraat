---
toegevoegd: 2024-05-19
gepubliceerd: 1939-10-13
decennium: 1930-1939
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1939-10-13/edition/0/page/4"
onderwerpen: [familiebericht]
huisnummers: [23]
achternamen: [van-rooijen]
kop: "Joh.G. Nauman E.C. den Rooijen."
---
Ondertrouwd:

# Joh.G. Nauman E.C. den Rooijen.

Schiedam, Lange Haven 64.

Rotterdam, **Jan Sonjéstraat** 23.

13 October 1939.
