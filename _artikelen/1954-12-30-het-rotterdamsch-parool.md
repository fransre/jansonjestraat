---
toegevoegd: 2023-10-07
gepubliceerd: 1954-12-30
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164888102:mpeg21:a00134"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Meubelmakers en Halfwas gevraagd."
---
# Meubelmakers en Halfwas gevraagd.

Meubelfabriek „Jehovu”,
**Jan Sonjéstraat** 36

Tel*efoon*38468 — Rotterdam
