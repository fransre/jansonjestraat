---
toegevoegd: 2024-07-30
gepubliceerd: 1939-10-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164703035:mpeg21:a00179"
onderwerpen: [woonruimte, tehuur]
huisnummers: [31]
kop: "Te huur:"
---
# Te huur:

ongem*eubileerde* Voorkamer of
Zit-Sl*aap*kamer, tevens
mooi Zolder-Sl*aap*kamertje.
Te bez*ichtigen* **Jan Sonjéstraat** 31B.
