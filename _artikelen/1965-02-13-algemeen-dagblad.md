---
toegevoegd: 2024-07-23
gepubliceerd: 1965-02-13
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002826037:mpeg21:a00234"
onderwerpen: [reclame, tekoop]
huisnummers: [41]
kop: "Sorteermachine"
---
# Sorteermachine

voor ponskaarten, systeem Powers.
„BICO”, **Jan Sonjéstraat** 41b,
Rotterdam, 010-250226.

AD 17
