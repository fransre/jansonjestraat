---
toegevoegd: 2024-07-29
gepubliceerd: 1940-09-28
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002469:mpeg21:a0030"
onderwerpen: [cafe, bedrijfsinventaris, tekoop]
huisnummers: [15]
achternamen: [van-gameren]
kop: "Te koop"
---
# Te koop

400 Witte liter flesschen
300 crapefruitflesschen
en carrier. Spoed. v*an* Gameren,
**Jan Sonjéstraat** 15.
