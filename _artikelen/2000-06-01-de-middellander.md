---
toegevoegd: 2023-01-11
gepubliceerd: 2000-06-01
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2000-06-01-de-middellander-origineel.jpg"
onderwerpen: [eeuwfeest]
achternamen: [poot, lutgerink, lassooij]
kop: "Het 100-jarig bestaan van Jan Sonjéstraat"
---
# Het 100-jarig bestaan van **Jan Sonjéstraat**

ingezonden mededeling

Het 100-jarig bestaan van de straat is niet niks
geweest. We hebben er behoorlijk bij stil gestaan.
Het boekje ‘Even terug in de tijd’ is
goed gevallen en heeft een plekje in
Rotterdam gekregen.
De receptie bij Café Canim op de
officiële dag, 7 maart 2000, waar de
eerste exemplaren aan de bewoners
werden uitgereikt was prima.
De receptie in het Trefcentrum
voor bewoners, oud-bewoners,
genodigden en vele nieuwsgierigen
was een ware happening.
Tussen haakjes: er is een videoband
van de receptie gemaakt.
Als je in de straat loopt en om je
heen kijkt zie je allerlei bakken vol
met prachtige geraniums bloeien.
Kleurrijk en mooi om te zien. Een
echte verfraaiing.
De afsluiting van onze festiviteiten
rond het 100-jarig bestaan was het
gezellige en goede diner dat voor
en door elkaar was verzorgd op 3
juni 2000. Helaas werden we door
de weersomstandigheden gedwongen
het binnen te houden, terwijl
een diner op straat de bedoeling
was. Jammer!
Dit alles hebben we gedaan om
met elkaar de straat leefbaar te
maken, te proberen er voor elkaar
te zijn, elkaar te helpen om de
straat schoon, heel, veilig en minder
lawaaiig te maken.
Bedankt voor alle spontane medewerking.

Jan Poot, Aad Lutgerink en Don Lassooij
