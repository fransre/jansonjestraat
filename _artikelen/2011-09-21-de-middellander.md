---
toegevoegd: 2023-01-09
gepubliceerd: 2011-09-21
decennium: 2010-2019
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2011-09-21-de-middellander-origineel.jpg"
onderwerpen: [dagjevanplezier]
kop: "Dagje van plezier"
---
# Dagje van plezier

De bewonersgroep van de **Jan Sonjéstraat** organiseert ieder
jaar met andere Opzoomerstraten het dagje van plezier in
het Branco van Dantzigpark. Het Dagje van Plezier staat
elk jaar weer als een huis en leert dat samen spelen leuk kan
zijn als volwassenen en kinderen aardig met elkaar omgaan
en zich houden aan een aantal regels.

Foto: Laura Haan
