---
toegevoegd: 2024-08-03
gepubliceerd: 1936-06-27
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164683065:mpeg21:a00040"
onderwerpen: [auto, tekoop]
huisnummers: [2, 4]
achternamen: [smit]
kop: "Te koop:"
---
# Te koop:

Hupmobile *19*31 en Ford
de Luxe, beide prima
in lak en zonder gebreken.
Garage Smit, **Jan Sonjéstraat** 2-4,
Tel*efoon* N*ummer* 31600.
