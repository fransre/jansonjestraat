---
toegevoegd: 2024-08-03
gepubliceerd: 1935-05-27
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164677031:mpeg21:a00141"
onderwerpen: [diefstal, fiets]
kop: "Nummer twee."
---
# Nummer twee.

In verband met den diefstal van twee rijwielen
op 7 Mei *1935* j*ongst*l*eden* uit een stalling aan de
Jan van Avennesstraat, resp*ectievelijk* ten nadele van
G. E., uit die straat en van H.L. B., wonende
**Jan Sonjéstraat**, waarvoor reeds een verdachte
in het Huis van Bewaring werd ingesloten,
heeft de politie Zaterdag *25 mei 1935* nog den 25-jarigen
timmerman J.F. den B. aangehouden.
Hij wordt verdacht van medeplichtigheid aan
den diefstal der rijwielen.
