---
toegevoegd: 2024-08-14
gepubliceerd: 1907-02-11
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196598:mpeg21:a0168"
onderwerpen: [huispersoneel]
huisnummers: [29]
kop: "Dagmeisje."
---
# Dagmeisje.

Gevraagd een net Dagmeisje,
van 's morgens
7½ tot 's middag 2½ uur,
in een gezin zonder
kinderen. Zich aan te
melden **Jan Sonjéstraat** 29, boven.
