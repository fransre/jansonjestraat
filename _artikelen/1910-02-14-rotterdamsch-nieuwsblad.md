---
toegevoegd: 2024-08-14
gepubliceerd: 1910-02-14
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197812:mpeg21:a0126"
onderwerpen: [bedrijfsruimte]
huisnummers: [36]
kop: "Te Huur"
---
Wasch- en Strijkinr*ichting*

# Te Huur,

het daarvoor uitstekend
ingerichte Benedenhuis
met groote Woning en
open grond, **Jan Sonjéstraat** 36,
ook zeer geschikt
voor Bierbottelarij,
enz*ovoort*. Adres Heemraadssingel 128,
hoek
Middellandstraat.
