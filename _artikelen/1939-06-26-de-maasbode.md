---
toegevoegd: 2021-04-29
gepubliceerd: 1939-06-26
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000193600:mpeg21:a0044"
onderwerpen: [ongeval, fiets]
huisnummers: [32]
achternamen: [hulsker]
kop: "Verkeersongevallen."
---
# Verkeersongevallen.

Om half 11 vanmorgen *26 juni 1939* is de 15-jarige
B.J.L. Hulsker wonende in de **Jan Sonjéstraat**
op den Nieuwe Binnenweg met haar
fiets over de rails geslipt. Met een vermoedelijke
linkerschouderbreuk is het meisje
opgenomen in het ziekenhuis aan den Coolsingel.
