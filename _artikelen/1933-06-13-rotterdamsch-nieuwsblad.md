---
toegevoegd: 2024-08-04
gepubliceerd: 1933-06-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164665048:mpeg21:a00086"
onderwerpen: [woonruimte, tehuur]
huisnummers: [23]
achternamen: [vervoort]
kop: "zit- en slaapkamer"
---
Wegens overplaatsing
beveel ik mijn

# zit- en sl*aap*kamer

aan, op vrij Bovenhuis.
M. Vervoort, **Jan Sonjéstraat** 23a.
