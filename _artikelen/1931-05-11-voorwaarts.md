---
toegevoegd: 2024-08-05
gepubliceerd: 1931-05-11
decennium: 1930-1939
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213082:mpeg21:a0175"
onderwerpen: [bewoner, tram]
achternamen: [barends]
kop: "Plotselinge dood."
---
# Plotselinge dood.

In een tram op het Hofplein werd de 72-jarige
mej*uffrouw* M. Barends-*van der* Vegt uit de **Jan Sonjéstraat** plotseling
onwel. De Geneesk*undige* Dienst bracht de vrouw
naar haar woning, doch bij aankomst aldaar was
zij reeds overleden.
