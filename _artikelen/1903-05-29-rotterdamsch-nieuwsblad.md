---
toegevoegd: 2021-04-29
gepubliceerd: 1903-05-29
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010178841:mpeg21:a0118"
onderwerpen: [woonruimte, tehuur]
kop: "Jan Sonjéstraat,"
---
# **Jan Sonjéstraat**,

Schermlaan, Middellandstr*aat*,
prachtige Woningen te huur,
*sma*kelijk ingericht, de Alkoven
voorzien van Waschtoestel;
tevens aldaar te huur: groot
Pakhuis, voorzien van veel
licht. Dagelijks te bevragen
aldaar en bij J. Oprel,
Bergweg 24, hoek Hillegondastraat.
Aan hetzelfde adres
mooie en voordeelige perceeltjes
Bouwgrond te koop.
