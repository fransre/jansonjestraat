---
toegevoegd: 2024-03-07
gepubliceerd: 1935-02-01
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164675036:mpeg21:a00084"
onderwerpen: [werknemer]
huisnummers: [25]
achternamen: [voet]
kop: "Biedt zich aan:"
---
# B*iedt* z*ich* a*an*:

Timmerman, 22 jaar,
alle voorkomende
werkzaamheden. Hoog
Loon geen vereischte.
G. Voet, **Jan Sonjéstraat** 25,
R*otter*dam.
