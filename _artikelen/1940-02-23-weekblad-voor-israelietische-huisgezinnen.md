---
toegevoegd: 2024-05-14
gepubliceerd: 1940-02-23
decennium: 1940-1949
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005424060:00001"
onderwerpen: [dans]
huisnummers: [23]
achternamen: [pijpeman]
kop: "Joodsche Dansclub"
---
# Joodsche Dansclub

Als technisch adviseur en dansleeraar is tot
de J.D.C. toegetreden de heer H. Frenk, die
tot de oprichting van de J.D.C. den eersten
stoot heeft gegeven. Het ligt in de bedoeling van
het bestuur, om ter gelegenheid van Poerim a*an*s*taande*
een cabaret-voorstelling te organiseeren op 25 Maart *1940*,
waaraan in hoofdzaak leden der club zullen
medewerken. Alle inlichtingen zijn te verkrijgen
aan het Secretariaat: J. Pijpeman, **Jan Sonjéstraat** 23,
tel*efoon* 33577.
