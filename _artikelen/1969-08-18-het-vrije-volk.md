---
toegevoegd: 2024-07-22
gepubliceerd: 1969-08-18
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957063:mpeg21:a0142"
onderwerpen: [familiebericht]
huisnummers: [10]
achternamen: [blind]
kop: "Cornelis Blind"
---
Heel plotseling, zoals hij het zelf altijd heeft gewild,
overleed mijn onvergetelijke man, onze vader en
grootvader

# Cornelis Blind

oud 68 jaar.

A.I. Blind-Roos

G.I. Verhagen-Blind

J.C. Verhagen

Kees en Maaike

Carla Blind en verloofde.

Rotterdam, 15 augustus 1969

**Jan Sonjéstraat** 10a.

Gelegenheid voor rouwbezoek in het Van Dam's ziekenhuis,
Westersingel 115 te Rotterdam, maandag *18 augustus 1969* en
dinsdag *19 augustus 1969* van 7.15 tot 7.45 uur n*a*m*iddags*

De teraardebestelling zal plaatshebben woensdag 20
augustus *1969* a*an*s*taande* op de begraafplaats „Hofwijk” aan de
Delftweg 230 te Rotterdam-Overschie om 2.30 uur.

Vertrek van het ziekenhuis om 2 uur.

Gelegenheid tot condoleren na de begrafenis in de
kapel op de begraafplaats.
