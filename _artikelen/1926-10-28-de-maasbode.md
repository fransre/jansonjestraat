---
toegevoegd: 2024-08-08
gepubliceerd: 1926-10-28
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000196885:mpeg21:a0058"
onderwerpen: [woonruimte, tekoop]
huisnummers: [38]
kop: "Publieke verkoopingen"
---
# Publieke verkoopingen

te Rotterdam.

In het Notarishuis aan de Geldersche kade,
Op Donderdag 28 October *1926*, 's middags te 2 uur,

Eindafslag:

Dubbel pand en erf, **Jan Sonjéstraat** 38, staande
in bod op ƒ30.000, niet doorgegaan.
