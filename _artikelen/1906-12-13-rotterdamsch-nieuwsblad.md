---
toegevoegd: 2024-08-14
gepubliceerd: 1906-12-13
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213970:mpeg21:a0093"
onderwerpen: [werknemer]
huisnummers: [15, 17]
achternamen: [van-uitert]
kop: "Strijkster."
---
# Strijkster.

Terstond gevraagd een
Platgoedstrijkster, eenigzins
gevorderd en een
Meisje voor de droge
Wasch en in het vak te
worden opgeleid, ongev*eer*
14 jaar. Aan de Wasch- en
Strijkinrichting van
M. van Uitert, **Jan Sonjéstraat** 15-17,
waar
tevens nog eenige nette
Wasschen geplaatst kunnen
worden.
