---
toegevoegd: 2024-07-26
gepubliceerd: 1946-05-27
decennium: 1940-1949
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259000024:mpeg21:a00046"
onderwerpen: [huisraad, radio, tekoop]
huisnummers: [15]
kop: "Bijzondere veiling"
---
Veilingbedrijf „Stad Rotterdam”

Veilingmeester Jac. Perridon, Makelaar

Heemraadssingel 163 Rotterdam Tel*efoon* 36093

# Bijzondere veiling

op vrijdag 31 mei *1946* a*an*s*taande* van 10 tot 4 uur
in de Veilingzaal, **Jan Sonjéstraat** 15,

van goud en zilver, juweelen, sieraden, schilderijen,
antieke voorwerpen, antiek meubilair, bijzondere postzegelverzameling,
illegaal radiotoestel, diverse meubelen enz*ovoort*

Kijkdagen: Dinsdag 28 en Woensdag 29 Mei *19*46 v*an* 10-4 u*ur*

Inbrengen van goederen voor de volgende veiling
Heemraadssingel 163 en aan de zaal.

Op verzoek gratis afhalen
