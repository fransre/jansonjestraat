---
toegevoegd: 2024-07-29
gepubliceerd: 1941-06-17
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002803:mpeg21:a0040"
onderwerpen: [werknemer, schilder]
huisnummers: [17]
achternamen: [koole]
kop: "Gevraagd"
---
# Gevraagd

2 flinke aank*omende*
halfwas Schilders. Aanmelden
's morgens ± 8
uur: F*irm*a Koole, **Jan Sonjéstraat** 17.
