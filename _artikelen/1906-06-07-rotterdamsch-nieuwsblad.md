---
toegevoegd: 2024-08-14
gepubliceerd: 1906-06-07
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213810:mpeg21:a0087"
onderwerpen: [huispersoneel]
huisnummers: [11]
kop: "Terstond gevraagd:"
---
# Terst*ond* gevraagd:

een net Dagmeisje, liefst
R*ooms*-K*atholiek* en gewoon met
kinderen om te gaan.
Zij, die meer op goede
behandeling dan op hoog
salaris letten, genieten
de voorkeur. Adres **Jan Sonjéstraat** 11, beneden,
's avonds 7½-8½ uur.
