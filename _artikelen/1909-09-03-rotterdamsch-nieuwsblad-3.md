---
toegevoegd: 2024-08-14
gepubliceerd: 1909-09-03
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197980:mpeg21:a0129"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [42]
kop: "Te Koop"
---
# Te Koop

aangeboden een gebruikte,
doch in goeden staat
verkeerende Kinderwagen
met twee Duwers.
**Jan Sonjéstraat** 42a.
