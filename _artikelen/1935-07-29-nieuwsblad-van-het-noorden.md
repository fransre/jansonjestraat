---
toegevoegd: 2024-08-03
gepubliceerd: 1935-07-29
decennium: 1930-1939
bron: Nieuwsblad van het Noorden
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010673758:mpeg21:a0122"
onderwerpen: [familiebericht]
huisnummers: [16]
achternamen: [van-der-werf]
kop: "Tjipke van der Werf."
---
Heden overleed plotseling,
rustig en kalm, tot onze
diepe droefheid, onze lieve
Man, Vader, Behuwd- en
Grootvader

# Tjipke van der Werf.

in den ouderdom van 69 jaar

Rotterdam, 27 Juli 1935.

**Jan Sonjéstraat** 16.

Uit aller naam:

Wed*uwe* L. v*an* d*er* Werf-van Hoogen.
