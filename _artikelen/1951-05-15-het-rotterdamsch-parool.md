---
toegevoegd: 2024-07-26
gepubliceerd: 1951-05-15
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164878011:mpeg21:a00036"
onderwerpen: [verloren]
huisnummers: [38]
achternamen: [jongste]
kop: "Wie?"
---
# Wie?

~~Mevrouw Van Dalsem,
Groenezoom 331, verloor
Woensdagavond *9 mei 1951* Marokkaanse
portefeuille met
ƒ26.35.~~

En mevr*ouw* Jongste, **Jan Sonjéstraat** 38C,
tel*efoon* 37844
verloor Vrijdag *11 mei 1951* omgeving
Claes de Vrieselaan langwerpig
gouden damesarmbandhorloge
aan zwart
koord.

~~Mej*uffrouw* Palli, A. Engelmanstraat 27
verloor op 5
Mei *1951* haar broche met rode
steen. Gedachtenis van
kleinzoon uit Indonesië.~~
