---
toegevoegd: 2023-08-01
gepubliceerd: 1939-10-07
decennium: 1930-1939
bron: De Dordrechtsche Courant
externelink: "https://proxy.archieven.nl/0/245BA8B688DD43A6984F4DD728680C11"
onderwerpen: [brand, motor]
achternamen: [dekker]
kop: "Motorhandel uitgebrand"
---
# Motorhandel uitgebrand

De vlammen laaiden op
tot aan het dak; de
winkel en werkplaats
brandden uit

Men schrijft ons uit Rotterdam:

Vanmorgen *7 oktober 1939* is brand uitgebroken in
een motorhandel in de 1e Middellandstraat.
Bij het soldeeren van een benzinetank
zette een steekvlam de werkbank
in vlammen en binnen korten tijd
breidde het vuur zich uit naar den
winkel. Toen de brandweer arriveerde
sloegen de vlammen langs den gevel
omhoog en hadden den daklijst reeds
aangetast. De brand werd met drie
stralen gebluscht; een vijftal motoren
verbrandde.

Vanmorgen *7 oktober 1939* omstreeks negen uur is brand
ontstaan in den motorhandel van den heer
P. Dekker, in de Eerste Middellandstraat 100
op den hoek van de **Jan Sonjéstraat**. Toen de
eigenaar bezig was in de werkplaats, die
achter den winkel in de **Jan Sonjéstraat** is
gelegen, met het soldeeren van een benzinetank,
sloeg plotseling een steekvlam uit de
tank. De werkbank met zijn brandbare omgeving
geraakte in brand, terwijl ook de
kleeding van den motorhersteller vlam
vatte. Het gelukte den man naar buiten te
snellen en zijn brandende kleeren te dooven,
waarna omwonenden de brandweer alarmeerden.
Omdat de brand zich aanvankelijk
niet zoo ernstig liet aanzien, werd het sein
„klein alarm” gegeven. Binnen korten tijd
sloegen de vlammen echter over naar den
winkel, terwijl in de werkplaats, waar een
vat met zestig liter olie en een bus benzine
stonden, de vlammen hoog oplaaiden. Met de
hulp van enkele voorbijgangers kon men nog
enkele nieuwe motoren uit den winkel in
veiligheid brengen, maar de machines, die in
de werkplaats stonden, waren niet meer te
bereiken.

In den winkel, waarvan de wanden geheel
met triplex bekleed waren, vond het vuur
gretig voedsel, terwijl ook daar busjes met
olie de vlammen hoog deden oplaaien.

## Vlammen slaan uit

Het duurde een kwartier voor de brandweer
arriveerde en toen stonden winkel en
werkplaats reeds over de geheele lengte in
brand. De groote étalageruiten waren gesprongen
en de vlammen sloegen langs den
gevel omhoog. Zoo hoog laaiden zij, dat zij
via eerste en tweede étage de daklijst bereikten,
zoodat deze eveneens vlam vatten.
Een oogenblik liepen de bovenverdiepingen
dan ook groot gevaar. Wanneer de ruiten
gesprongen zouden zijn, had het vuur zich
ongetwijfeld medegedeeld aan de hooger
gelegen étages, waarvan kozijnen en vensterbanken
reeds blakerden!

Brandmeester J. van Onlangs, die met
slangenwagen 31 het eerst aanwezig was, zag
dat gevaar onmiddellijk in en liet het vuur
krachtig aanpakken. Twee slangen, werden
uitgelegd en van verschillende zijden werden
flinke hoeveelheden water in den winkel, die
het felst brandde, geworpen.

Inmiddels was door de centrale brandweer
melding het sein „middelalarm” gegeven,
waarop meer slangenwagens en de gereedschapswagen
onder leiding van opzichter
Mallan uitrukten.

Ook hoofdman A.J. Kruis was spoedig ter
plaatse en stelde slangenwagen 28 met één
straal in werking om het nog steeds voortvretende
vuur in de werkplaats te blusschen.

Tegen zulk een overmacht was het vuur
niet opgewassen, terwijl bovendien de meest
brandbare materialen verbrand waren, zoodat
het vuur zienderoogen minderde.

De winkel was toen geheel uitgebrand en
tusschen de zwartgeblakerde omlijstingen van
étalage en deurstijlen lagen de verwrongen
overblijfselen van motoronderdeelen, terwijl
midden in den winkel een geheel door het
vuur vernielde machine lag.

Om de nablussching grondig ter hand te
kunnen nemen moesten de plafonds omlaag
gehaald worden, zoodat het winkelhuis geheel
vernield is.

## Autogarages gespaard

Doordat het vuur zich vrijwel uitsluitend
voortplantte naar de zijde van de Middellandstraat,
zijn de garages in de **Jan Sonjéstraat**,
die aan de werkplaats grenzen, gespaard
gebleven. Vlak achter de werkplaats
is een garage van de autoverhuurinrichting
A.R.O., terwijl daarnaast de garage van den
heer H.P. Take is gevestigd. In laatstgenoemde
garage stonden een zevental auto's,
die door een chauffeur van de A.R.O. naar
buiten gereden werden.

Vijf motorrijwielen en een hoeveelheid
onderdeelen zijn een prooi der vlammen
geworden. Behalve de winkel werden de
bovengelegen étages licht door het vuur
beschadigd.

Alle schade wordt door verzekering gedekt.
De volgorde van aankomst van het materieel
was: de slangenwagens 31, 28, 32, 56,
49 en gereedschapswagen I.
