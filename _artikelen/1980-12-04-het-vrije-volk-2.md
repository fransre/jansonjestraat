---
toegevoegd: 2021-04-29
gepubliceerd: 1980-12-04
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960694:mpeg21:a0521"
onderwerpen: [cafe, actie]
huisnummers: [15]
kop: "Bewoners protesteren tegen café-overlast"
---
# Bewoners protesteren tegen café-overlast

(Van een onzer verslaggevers)

Rotterdam — De bewoners
van de **Jan Sonjéstraat**, gesteund
door de actiegroep Middelland
hebben voorlopig kunnen
verhinderen, dat een café in
hun straat met een keuken
wordt uitgebreid.

De raadscommissie voor
Ruimtelijke Ordening was dinsdag *2 december 1980*,
na het, aanhoren van de
protesten tegen de uitbreiding
en de overlast, die het café al jaren
veroorzaakt, van oordeel,
dat de klachten thuishoren bij
de afdeling algemene zaken van
de gemeente.

Deze afdeling zal nu, samen
met de politie, onderzoeken of
de overlast van het café inderdaad
zo groot is, dat de vergunning
moet worden ingetrokken.

Zolang dit onderzoek nog niet
tot resultaat heeft geleid, krijgt
het café geen bouwvergunning
om een keuken aan de zaak te
bouwen.

De overlast bestaat volgens de
bewoners vooral uit lawaai tijdens
de nachtelijke uren van de
weekeinden. 's Morgens vroeg is
het café het centrum van koppelbazen,
die vooral autolawaai
en parkeeroverlast veroorzaken.
