---
toegevoegd: 2024-07-30
gepubliceerd: 1940-01-10
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002314:mpeg21:a0016"
onderwerpen: [bedrijfsruimte, fiets]
huisnummers: [36]
kop: "Rijwielstalling"
---
# Rijwielstalling

ter overname aangeboden.
's Avonds na zes
uur. Adres: **Jan Sonjéstraat** 36.
