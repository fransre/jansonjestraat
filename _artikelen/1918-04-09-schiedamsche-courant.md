---
toegevoegd: 2021-04-30
gepubliceerd: 1918-04-09
decennium: 1910-1919
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1918-04-09/edition/0/page/3"
onderwerpen: [diefstal]
kop: "Per vrachtwagen S S."
---
# Per vrachtwagen S S.

Enkele dagen geleden werd uit een pakhuis
in de **Jan Sonjéstraat** te Rotterdam
een partij cacao, vet, peperkorrels enz*ovoort* gestolen
ter waarde van ƒ2300. De partij
werd aangehouden in een vrachtwagen van
de Staatsspoor.

De politie heeft den dief thans opgespoord.

Gebleken is, dat getracht werd het ontvreemde
over te brengen naar het Maasstation
en dat de verdachte den besteller
der Staatsspoor had aangenomen om vóór
hij in dienst ging met zijn wagen, nog
een particulier vrachtje te vervoeren.
