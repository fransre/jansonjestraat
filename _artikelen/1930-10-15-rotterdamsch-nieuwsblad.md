---
toegevoegd: 2024-08-06
gepubliceerd: 1930-10-15
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164649051:mpeg21:a00157"
onderwerpen: [reclame]
huisnummers: [7]
kop: "Parijzer modehuis"
---
# Parijzer modehuis

levert chique Dames, en Heerenkleeding
naar de laatste mode, vanaf FL. 1. p*er* week.

Lage prijzen. Gesloten huis.

**Jan Sonjéstraat** 7B, Zijstraat, Middellandstraat.

50526 20
