---
toegevoegd: 2021-04-29
gepubliceerd: 1915-10-01
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296125:mpeg21:a0072"
onderwerpen: [bedrijfsinformatie]
huisnummers: [36]
achternamen: [sas]
kop: "Stadsnieuws."
---
# Stadsnieuws.

3e. aan den heer F.J.H. Sas om in het
pand aan de **Jan Sonjéstraat** n*ummer* 36a op
te richten een inrichting tot het vervaardigen
van saccharinetablettten, met gebruikmaking
van 4 electromotoren en 2
gasovens;
