---
toegevoegd: 2024-03-05
gepubliceerd: 1952-04-19
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000326093:mpeg21:a00160"
onderwerpen: [motor, tekoop]
huisnummers: [38]
achternamen: [gelton]
kop: "Harley Davidson"
---
# Harley Davidson

te koop,
nieuwe motor, nieuwe banden.
Te bevr*agen* P. Gelton, **Jan Sonjéstraat** 38.
Tel*efoon* 34158, van 8-18
uur.
