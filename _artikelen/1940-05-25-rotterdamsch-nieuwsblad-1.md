---
toegevoegd: 2024-07-29
gepubliceerd: 1940-05-25
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002396:mpeg21:a0042"
onderwerpen: [werknemer]
huisnummers: [36]
achternamen: [kerstein]
kop: "Wie helpt"
---
# Wie helpt

netten varensgezel, 24 j*aar*
weer aan werk aan de
wal of Binnenvaart. Br*ieven*
W. Kerstein, **Jan Sonjéstraat** 36,
R*otter*dam.
