---
toegevoegd: 2024-08-01
gepubliceerd: 1938-09-21
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164697018:mpeg21:a00255"
onderwerpen: [werknemer]
huisnummers: [20]
kop: "Krantenjongen"
---
# Krantenjongen

gevraagd om dagelijks
half uur Couranten bezorgen.
Loon 50 cent p*er*
Week. Aanmelden 7 uur
**Jan Sonjéstraat** 20b.
