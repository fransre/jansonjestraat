---
toegevoegd: 2021-04-30
gepubliceerd: 1948-05-11
decennium: 1940-1949
bron: Rotterdamsch Parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164870109:mpeg21:a00082"
onderwerpen: [cafe]
huisnummers: [15]
achternamen: [ringeling, van-klaveren]
kop: "Cafe — Intiem"
---
A*an*s*taande* Donderdagmiddag *13 mei 1948*

2 uur opening

# Cafe — Intiem

Koos Ringeling

**Jan Sonjéstraat** 15.

Prima consumptie,

Heinekens Bieren.

Beleefd aanbevelend,

J.J. Ringeling

v*oor*h*een* C. van Klaveren.
