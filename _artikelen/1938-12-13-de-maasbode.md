---
toegevoegd: 2024-08-01
gepubliceerd: 1938-12-13
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000193017:mpeg21:a0158"
onderwerpen: [actie]
huisnummers: [30]
achternamen: [kofflard]
kop: "Wordt lid van de Katholieke Staatspartij!"
---
# Wordt lid van de Kath*olieke* Staatspartij!

Aanmelding voor 1 Januari 1939!

Er is velerlei in de huidige wereld, waartegen
geest en hart van den katholieken
Nederlander (en niet van hem alleen gelukkig)
in opstand komen. Welk onheil blinde
staatszucht brouwen kan, zien wij letterlijk
voor onze oogen en na een verontwaardigd
protest welt in ons hart de bede, dat toch de
Nederlandsche samenleving voor zulke geweldenarij
moge behoed blijven.

Bij zulk protest en bij die bede mogen wij
het intusschen niet laten. Het zou weinig
baten, indien wij niet, door een hechte politieke
eenheid te vormen, met succes de wacht
konden betrekken rond ons eigen erf. Dat
kan niet beter geschieden dan indien zooveel
mogelijk alle Nederlandsche katholieken
zich aansluiten bij de Kath*olieke* Staatspartij.

Heusch, het is niet voldoende, dat katholiek
Nederland een millioen stemmen uitbrengt.
Door een massale organisatie, ook
op politiek gebied, moet het kracht bijzetten
aan zijn positie in ons staatkundig leven.

De katholieken van Rotterdam denken
op dit laatste punt veel te laks. Het is en
blijft verkeerd, zich afzijdig te houden van
de politieke organisatie.

Reeds in het belang van een goeden gang
van zaken bij de komende dubbele verkiezingen
van 1939 is het gewenscht, dat zooveel
mogelijk allen aan de voorbereidende werkzaamheden
deelnemen: de samenstelling
van de groslijst voor staten- en raadsstembus
en de vaststelling der definitieve candidatenlijS*in*t

Maar wil men daaraan medewerken en
aldus zijn rechtmatigen invloed uitoefenen
op de vertegenwoordiging in staten en raad,
dan is het noodzakelijk, zich vóór den 1en Januari *1939* a*an*s*taande*
op te geven voor het lidmaatschap.

Men talme dus niet langer en zende zijn
naam en adres aan een der onderstaande
afdeelings-secretarissen van „Recht en Orde”:

~~S*in*t Lambertus: J. Willemsen, Gelderschekade 23;~~
~~S*in*t Barbara: W.W. Reys, Boezemsingel 15b;~~
~~H*eilige* Kruisvinding: J.M. Engwirda, Treek 26;~~
~~H*eilige* Michael: H. Harte, Quintstraat 24b;~~
~~H*eilige* Martelaren van Gorcum: H.L. v*an* d*e* Roer, Steven Hoogendijkstr*aat* 35;~~
H*eilige* Elisabeth: J. M. Kofflard, **Jan Sonjéstraat** 30;
~~S*in*t Willibrord: P*rotestantse* G*ezindte* Pattijn, Veelzigtstraat 29a;~~
~~S*in*t Hildegardis: G. J. F. Groothoff, Heer Kerstantstraat 92a;~~
~~H*eilige* Nicolaas: H. Malten, Schiedamscheweg 278b;~~
~~H*eilige* Familie. A.J.J. van Vlijmen, Gordelweg 199;~~
~~H*eilige* Dominicus: M.M. Bronmans, Keizerstraat 22;~~
~~H*eilige* Antonius v*an* Padua: C. v*an* d*er* Pas, R. Rottekade 63;~~
~~O*nze* L*ieve* Vr*ouwe* v*an* d*e* H. Rozenkrans: J A. Burcksen, Schefferstraat 21b;~~
~~H*eilige* Laurentius: H.E. v*an* d*er* Brule, Nieuwstraat 33;~~
~~H*eilige* Rosalia: H.J. van Leeuwen, Oppert 95;~~
~~H*eilige* Theresia: H. Nortier, Zeisstraat 14b;~~
~~H*eilige* Franciscus v*an* Assisië en Marg. Maria Alacoque: A.C. Weygertse, Transvaalstraat 24b;~~
~~H*eilige* Bonifacius: Corvelyn, Streefkerkstraat 9b;~~
~~H*eilige* Hart: S. Suyker Pzn., Kruisstraat 27;~~
~~S*in*t Joseph: J.B. Weykamp, Diergaardesingel 59;~~
~~S*in*t Antonius Abt: A.B. Schiphorst, Bruijnstraat 46 ;~~
~~O*nze* L*ieve* Vrouw Onbevl*ekt* Ontvangen: Ph. Kanters, Westzeedijk 48;~~
~~O*nze* L*ieve* Vrouw van Lourdes: J.H.C. Jansen, Meeuwenstraat 14b.~~
