---
toegevoegd: 2024-05-20
gepubliceerd: 1937-08-18
decennium: 1930-1939
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1937-08-18/edition/0/page/2"
onderwerpen: [ongeval, auto]
achternamen: [bothof]
kop: "Dagelijksche ongevallen."
---
# Dagelijksche ongevallen.

Op den Nieuwe Binnenweg bij den Heemraadssingel
is de 74-jarige J. Bothof, wonend
**Jan Sonjéstraat**, bij het oversteken van den
weg aangereden door een auto, bestuurd
door P.J. W. uit de Taandersstraat. De oude
heer smakte tegen den grond, brak het
linker been en liep een gapende hoofdwonde
op. Per auto van den G*emeentelijke* G*eneeskundige* D*ienst* is hij naar het
ziekenhuis Coolsingel vervoerd en aldaar opgenomen.
Zijn toestand is ernstig.
