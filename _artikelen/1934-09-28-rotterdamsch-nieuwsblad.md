---
toegevoegd: 2024-08-04
gepubliceerd: 1934-09-28
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164673031:mpeg21:a00046"
onderwerpen: [radio, tekoop]
huisnummers: [9]
achternamen: [hoogmolen]
kop: "Radio."
---
# Radio.

Ter overname geheel
ingebouwd. Spotprijs:
ƒ29, tevens Bekapath.
met 40 pracht platen
ƒ9.50. Hoogmolen,
**Jan Sonjéstraat** 9b.
