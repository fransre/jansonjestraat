---
toegevoegd: 2024-08-11
gepubliceerd: 1922-12-30
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494535:mpeg21:a0051"
onderwerpen: [bedrijfsinformatie]
huisnummers: [2]
kop: "Directie Bureau „Studio”."
---
Compliment van den dag gewenscht door

# Directie Bureau „Studio”.

Advertentiën, Reclame en Verzekerings-Bureau, met speciale afdeeling
Incasso, Informatiën, Wissels Incasseeren, enz*ovoort*. Telefoon 9245.

Kantoor: **Jan Sonjéstraat** 2.

Dir*ectie*: W. Swaanenbeek.

10267 20
