---
toegevoegd: 2024-08-11
gepubliceerd: 1924-03-03
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000201238:mpeg21:a0044"
onderwerpen: [auto, ongeval]
kop: "Aanrijding."
---
# Aanrijding.

Op den Coolsingel liep de 78-jarige
E. M. wonende in de **Jan Sonjéstraat** door eigen
onoplettendheid tegen het spatbord van een passeerende
auto, bestuurd door A.J. K. De man
werd tegen den grond geslingerd en brak ten gevolge
daarvan het rechterschouderblad. Per auto
van den geneeskundigen dienst werd hij naar het
ziekenhuis vervoerd en aldaar ter verpleging opgenomen.
