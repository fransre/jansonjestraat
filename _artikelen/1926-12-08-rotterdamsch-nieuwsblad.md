---
toegevoegd: 2024-08-08
gepubliceerd: 1926-12-08
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010513860:mpeg21:a0164"
onderwerpen: [ongeval]
achternamen: [hoogstraten]
kop: "In het Ziekenhuis aan den Coolsingel"
---
# In het Ziekenhuis aan den Coolsingel

is gisteren *7 december 1926* de 55-jarige kras J. Hoogstraten,
uit de **Jan Sonjéstraat**, verbonden,
die bij het aan boord gaan van
het in de Parkhaven liggende s*toom*s*chip* „Imber”
door de gladheid van de loopplank
op de kade was gevallen en den rechterenkel
had ontwricht. Na te zijn verbonden
is hij naar huis vervoerd.
