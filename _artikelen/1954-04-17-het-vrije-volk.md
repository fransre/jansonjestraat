---
toegevoegd: 2024-07-25
gepubliceerd: 1954-04-17
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951889:mpeg21:a0085"
onderwerpen: [auto, tekoop]
huisnummers: [20]
kop: "Zündapp"
---
Te koop een

# Zündapp

200 cc,
met Steib zijspan, geh*eel* in pr*ima*
conditie. **Jan Sonjéstraat** 20a.
