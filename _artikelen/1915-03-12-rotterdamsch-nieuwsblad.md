---
toegevoegd: 2024-08-13
gepubliceerd: 1915-03-12
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000203214:mpeg21:a0077"
onderwerpen: [woonruimte, tehuur]
huisnummers: [27]
kop: "Te Huur aangeboden"
---
# Te Huur aangeboden

~~Een ruim Benedenhnis met Tuin,
bev*attende* 7 Kamers, klein en groot, per 1 April,
a ƒ375.— per jaar,~~

~~Snellinckstraat N*ummer* 14.~~

~~Een vrije 2e Etage à ƒ22.— p*er* maand
tegen 1 April *1915* of later,~~

~~Snellinckstraat N*ummer* 16.~~

~~Een 1e Etage à ƒ19.— p*er* maand tegen
1 April *1915* of later,~~

~~Snellinckstraat N*ummer* 43.~~

Een vrij Bovenhuis à ƒ22.— per
maand, per 1 April *1915*,

**Jan Sonjéstraat** N*ummer* 27.

~~Een ruim Benedenhuis à ƒ24.— p*er*
maand tegen 1 Mei *1915* of vroeger,~~

~~van Oosterzeestraat N*ummer* 61.~~

Adres: I. Akkerman J*unio*r, Essenburgsingel 4.
Teleph*oon* 3100.

16255 70
