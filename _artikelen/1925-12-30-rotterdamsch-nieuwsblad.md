---
toegevoegd: 2024-08-10
gepubliceerd: 1925-12-30
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495663:mpeg21:a0261"
onderwerpen: [bedrijfsinformatie]
huisnummers: [36]
achternamen: [werner]
kop: "C. Werner van Lunteren,"
---
Veel heil en zegen wordt toegewenscht
aan Familie, Vrienden en
Begunstigers, bij de intrede van
het nieuwe jaar, door

# C. Werner v*an* Lunteren,

**Jan Sonjéstraat** 36.

Electr*ische* Wasch- en Strijkinrichting.
