---
toegevoegd: 2024-08-13
gepubliceerd: 1918-06-07
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000190715:mpeg21:a0008"
onderwerpen: [diefstal]
kop: "Rechtszaken."
---
# Rechtszaken.

Rotterdamsche rechtbank.

Zitting van 6 Juni *1918*.

(Vervolg.)

Heden werden veroordeeld:

J.J. K., groentenkoopman, alhier, wegens diefstal
van goederen uit een pakhuis, aan de **Jan Sonjéstraat**,
tot 6 maanden gevangenisstraf.
