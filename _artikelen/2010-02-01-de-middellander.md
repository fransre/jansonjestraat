---
toegevoegd: 2023-01-09
gepubliceerd: 2010-02-01
decennium: 2010-2019
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2010-02-01-de-middellander-origineel.jpg"
onderwerpen: [straat]
achternamen: [van-der-ploeg]
kop: "De buurt bestuurt in de Jan Sonjéstraat en oomstreken"
---
# De buurt bestuurt in de **Jan Sonjéstraat** e*n* o*mstreken*

Bewoners aan de **Jan Sonjéstraat**
mogen in 2010 in het
kader van het project ‘de
buurt bestuurt’ bepalen
waar de politie, wijkteam
Middelland, 100 uur aan
gaat besteden. Deze uren
zijn extra bovenop de reguliere
werkzaamheden van de
politie en uiteraard op het
gebied van het verbeteren
van de veiligheid en leefbaarheid
in de buurt.

In de **Jan Sonjéstraat** is een
bewonersgroep, waaronder
een voorzitter en een secretaris,
die zich inzetten voor activiteiten.
Tijdens een overleg
met en groep afgevaardigden
van de bewonersgroep is
met buurtagent Ferry Steegman
het aantal in te zetten
activiteiten bepaald op:

## Drugs- en jeugdoverlast

\+ Overlast prostitute in/rondom
horecagelegenheden

\+ Toezicht bij het jaarlijkse
evenement ‘Dagje van Plezier’

\+ Structurele geluidsoverlast
in de straat

\+ Communicatie; spreekuur
buurtagent, bijwonen bewonersoverleg,
voor- en achteraf
informeren van in te zetten
uren in het kader van het project
de buurt bestuurt.

\+ Calamiteiten op afspraak:
gereserveerde uren indien er
zich onverhoopt zaken voordoen.

Indien mogelijk worden de bewoners
voor en/of achteraf
geïnformeerd indien de activiteiten
in het kader van de
buurt bestuurt plaatsvinden.
Achteraf worden ook de resultaten
bespoken. Deze terugkoppeling
vindt zowel persoonlijk
als via de email
plaats.

Mocht u nog vragen hebben
over dit project, spreek ons
dan gerust aan. Tot ziens in
uw buurt!

*Onderschrift bij de foto*
Jan van der Ploeg
