---
toegevoegd: 2024-07-29
gepubliceerd: 1940-02-01
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002333:mpeg21:a0280"
onderwerpen: [familiebericht]
huisnummers: [9]
achternamen: [van-duin]
kop: "Dankbetuiging."
---
# Dankbetuiging.

Ondergetekende betuigt hiermede
haar welgemeenden dank aan de „Fatum” S.A.
(belge) d'assurance contre
les accidents te Brussel, voor de
prompte uitkeering, haar aldaar gedaan
in verband met een haar man
overkomen doodelijk ongeval.

Tevens betuigt zij haar dank aan
de firma Le Large te Rotterdam,
Weste Wagenstraat 32, Tel*efoon* 56752,
voor haar bemiddeling in deze aangelegenheid.

Rotterdam, 27 Januari 1940.

(w.g.)

Wed*uwe* W.H.P.M v*an* Duin-Reichardt.

**Jan Sonjéstraat** 9, Rotterdam.

13049 20
