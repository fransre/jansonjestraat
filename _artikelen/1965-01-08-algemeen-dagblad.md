---
toegevoegd: 2024-07-23
gepubliceerd: 1965-01-08
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002826006:mpeg21:a00117"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [41]
kop: "Stencilmachines"
---
# Stencilmachines

ƒ75,— en ƒ100,— per stuk.

BICO, **Jan Sonjéstraat** 41.
