---
toegevoegd: 2023-04-25
gepubliceerd: 1928-01-27
decennium: 1920-1929
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1928-01-27/edition/0/page/1"
onderwerpen: [ongeval, auto]
kop: "Aanrijding"
---
# Aanrijding

Gisteravond *26 januari 1928* te ongeveer 8 uur is de heer A. B.
uit de **Jan Sonjéstraat** te Rotterdam, met
zijn Ford-auto tegen een ijzeren paal gereden
op den hoek van de Nieuwe Haven en de
Oranjebrug. De auto werd ernstig beschadigd
o*nder* a*ndere* werd het rechterwiel vernield, terwijl de
motor defect geraakte.

Door bemiddelling van de Fordcompanie
aan de Willemskade alhier is het rijtuig weggesleept.
