---
toegevoegd: 2024-07-29
gepubliceerd: 1941-05-01
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002598:mpeg21:a0008"
onderwerpen: [huisraad, tekoop]
huisnummers: [31]
achternamen: [morel]
kop: "Gasfornuis"
---
# Gasfornuis

ter overname
aangeb*oden*, gebr*uikt*, 4 p*its*
fornuis Adres: Morel,
**Jan Sonjéstraat** 31A.
