---
toegevoegd: 2024-07-24
gepubliceerd: 1958-11-28
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010953294:mpeg21:a0078"
onderwerpen: [muziek, tekoop]
huisnummers: [19]
achternamen: [roodenburg]
kop: "platenwisselaar"
---
Te koop Philips

# platenwisselaar

met kast, als nieuw.
Tot 20 uur. Roodenburg, **Jan Sonjéstraat** 19b,
R*otter*dam.
