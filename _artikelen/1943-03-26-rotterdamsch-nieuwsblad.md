---
toegevoegd: 2024-07-26
gepubliceerd: 1943-03-26
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003451:mpeg21:a0038"
onderwerpen: [familiebericht]
huisnummers: [12]
achternamen: [termos]
kop: "Christiaan Franciscus Antoine Termos,"
---
Eenige en algeemene
kennisgeving

Heden overleed nog onverwacht
mijn beste Man

# Christiaan Franciscus Antoine Termos,

in den ouderdom van 58 j*aa*r

R.M. Termos-Bosbaan.

Rotterdam, 25 Maart 1943.

**Jan Sonjéstraat** 12B.

Geen bezoek.

Geen bloemen.

De teraardebestelling zal
plaats hebben Maandag *29 maart 1942* a*an*s*taande*
kwart over één op de Algemeene Begraafplaats „Crooswijk”.
Vertrek van het sterfhuis
te half één.
