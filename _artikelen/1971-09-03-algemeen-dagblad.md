---
toegevoegd: 2024-07-22
gepubliceerd: 1971-09-03
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002868003:mpeg21:a00179"
onderwerpen: [woonruimte, tekoop]
kop: "Pand te koop,"
---
# Pand te koop,

staande **Jan Sonjéstraat**.
Vrij boven- en benedenhuis,
waarvan het bovenhuis
bestaande uit 5 kamers,
keuken, badkamer met ligbad
en zonbalkon leeg wordt opgeleverd.
Huur benedenhuis plm.
ƒ100.— per maand. Prijs voor
geheel pand ƒ32.000.—. Te bevragen
Leeuwenburgh, Goudsesingel 47,
Rotterdam.

AD14
