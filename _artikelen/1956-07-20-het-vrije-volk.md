---
toegevoegd: 2024-07-24
gepubliceerd: 1956-07-20
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952570:mpeg21:a0086"
onderwerpen: [kleding, tekoop]
huisnummers: [8]
kop: "heren-winterjas,"
---
Aangeb*oden* geheel nieuwe pracht

# heren-winterjas,

fl*inke* maat,
heeft gekost ƒ195- voor ƒ100.—.
Te zien na 8 uur: **Jan Sonjéstraat** 8a.
