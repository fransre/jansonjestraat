---
toegevoegd: 2024-08-02
gepubliceerd: 1937-10-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164687045:mpeg21:a00116"
onderwerpen: [woonruimte, tehuur]
huisnummers: [18]
kop: "Benedenhuis"
---
# Benedenhuis

**Jan Sonjéstraat** 18,
voor net gezin te huur,
1 Dec*em*b*er* *1937* Voorkamer, 2
kl*eine* Tusschen-, Achterkamer,
uitgebouwde Zijkamer,
Keuken, gr*ote* Kelder,
geschikt Werkplaats.
