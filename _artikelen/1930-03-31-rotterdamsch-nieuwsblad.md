---
toegevoegd: 2024-08-06
gepubliceerd: 1930-03-31
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164646034:mpeg21:a00084"
onderwerpen: [huisraad, tekoop]
huisnummers: [11]
kop: "zinken bad"
---
Tegen billijken prijs
te koop

# zinken bad

met warm waterapparaat
„Economie”. Adr*es*
**Jan Sonjéstraat** 11b.
