---
toegevoegd: 2024-07-23
gepubliceerd: 1964-11-07
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955297:mpeg21:a0166"
onderwerpen: [familiebericht]
huisnummers: [30]
achternamen: [smeets]
kop: "Yvonne en Johnny"
---
Zondag 8 november *1964* wordt onze
tweeling

# Yvonne en Johnny

8 jaar.

Familie Smeets

**Jan Sonjéstraat** 30a
