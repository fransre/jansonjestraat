---
toegevoegd: 2024-08-13
gepubliceerd: 1916-11-20
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296615:mpeg21:a0200"
onderwerpen: [familiebericht]
huisnummers: [12]
achternamen: [lutteken]
kop: "de Weduwe J. van der Kruit,"
---
Heden overleed na een korte
ongesteldheid, onze geliefde
Moeder, Groot-, Overgrootmoeder
en Zuster, Mejuffrouw

# de Wed*uwe* J. van der Kruit,

geb*oren* M. van der Vlies,

in den ouderdom van 88 jaar.

Uit aller naam.

S.M. Lutteken-v*an* d*er* Kruit.

E.G. Lutteken.

Rotterdam, 17 November 1916.

**Jan Sonjéstraat** 12B.

Eenige en algemeene kennisgeving.

19
