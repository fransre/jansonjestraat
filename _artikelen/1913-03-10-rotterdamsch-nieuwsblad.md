---
toegevoegd: 2024-08-14
gepubliceerd: 1913-03-10
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010222405:mpeg21:a0181"
onderwerpen: [bedrijfsinformatie]
huisnummers: [32]
achternamen: [wieman]
kop: "H. Wieman,"
---
# H. Wieman,

Kleermaker, is verhuisd
van **Jan Sonjéstraat** 32
naar Doedesstraat 80c, bij
de Vierambachtsstr*aat*
