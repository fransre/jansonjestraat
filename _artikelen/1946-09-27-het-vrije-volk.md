---
toegevoegd: 2024-07-26
gepubliceerd: 1946-09-27
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955033:mpeg21:a0054"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [wassenaar]
kop: "Anna Maria Petronella Wassenaar-van Aalst"
---
Heden overleed tot onze
diepe droefheid, na een langdurig,
smartelijk, doch geduldig
gedragen lijden mijn
beste Vrouw en onze lieve
Moeder, Behuwd- en Grootmoeder,
Zuster, Behuwdzuster
en Tante

# Anna Maria Petronella Wassenaar-van Aalst

in de leeftijd van bijna 62 j*aar*

Uit aller naam:

A. Wassenaar S*enio*r.

Rotterdam, 25 Sept*ember* *19*46

**Jan Sonjéstraat** 33b.

Liever geen bloemen.

De crematie zal plaats hebben
Maandag 30 Sept*ember* *1946* in het
crematorium te Velzen, na
aankomst van trein 13.20 u*ur*,
halte Driehuis-Westerveld.
