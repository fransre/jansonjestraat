---
toegevoegd: 2024-07-23
gepubliceerd: 1965-02-05
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002826030:mpeg21:a00120"
onderwerpen: [tekoop, reclame]
huisnummers: [41]
kop: "motorzaag"
---
Te koop in goede staat
verkerende

# motorzaag

aangedreven door benzinemotor
ƒ500,—.

„BICO” — **Jan Sonjéstraat** 41B
Rotterdam — 010-250226.
