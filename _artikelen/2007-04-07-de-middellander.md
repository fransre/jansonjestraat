---
toegevoegd: 2023-01-08
gepubliceerd: 2007-04-07
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2007-04-07-de-middellander-origineel.jpg"
onderwerpen: [vegen]
achternamen: [lutgerink]
kop: "Jong geleerd, oud gedaan… (althans, dat hopen we)"
---
# Jong geleerd, oud gedaan… (althans, dat hopen we)

Elke eerste zaterdag van de
maand vegen volwassenen
en een paar kinderen de
**Jan Sonjéstraat** schoon.
Afgelopen keer, op Paaszaterdag
7 april *2007*, was er een
speciale actie: een groep
van ongeveer 20 kinderen
zorgde voor versterking.

Het was een vrolijk *(gezicht)* om de kinderen
met een rood-witte pet
druk in de weer te zien met
bezems en knijpers; het resultaat
zag er op z'n paasbest
uit.

Na afloop trokken we met z'n
allen naar het Dierenlandje.
Daar dronken we koffie en limonade,
aten wat lekkers en
bleven even gezellig napraten.

De Paashaas had eieren op
het landje verstopt dus dat
werd rennen en eieren zoeken.

Wanneer een kind zegt: „Ik
vind het leuk om in de **Jan Sonjéstraat**
te wonen omdat
we vaak leuke dingen doen”
dan weet je weer waar we het
voor doen!

Tineke Lutgerink
