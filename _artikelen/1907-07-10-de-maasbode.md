---
toegevoegd: 2024-08-14
gepubliceerd: 1907-07-10
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000189495:mpeg21:a0038"
onderwerpen: [huisraad, tekoop]
huisnummers: [29]
kop: "Een net salon-ameublement"
---
# Een net salon-ameublement

wegens plaatsgebrek ter overname aangeboden.
Adres: **Jan Sonjéstraat** 29 (beneden),
bij de Middellandstraat.

850C
