---
toegevoegd: 2024-02-21
gepubliceerd: 2002-07-26
decennium: 2000-2009
bron: De Havenloods
externelink: "https://www.jansonjestraat.nl/originelen/2002-07-26-de-havenloods-origineel.jpg"
onderwerpen: [bewoner]
huisnummers: [13]
achternamen: [van-hooijdonk, lutgerink]
kop: "„Ik zou liever willen dat ik 72 werd”"
---
Eeuwfeest voor oudste bewoner **Jan Sonjéstraat**, Jan van Hooijdonk

# „Ik zou liever willen dat ik 72 werd”

door Ben van der Hee

Middelland — Al
tweeënzeventig jaar woont
Jan van Hooijdonk in de
**Jan Sonjéstraat**. Binnenkort
wordt hij honderd en
dat moet natuurlijk op gepaste
wijze gevierd worden.
De actieve straatbewoners
organiseren op zaterdag 29 juni *2002*
een receptie voor Van Hooijdonk.
De jarige zelf
verblijft op dit moment in
het Reumaverpleeghuis in
Hillegersberg, maar ziet erg
uit naar het feest in zijn
straat.

Eén van de organisators van het
feest, Aad Lutgerink, vertelt wat
de plannen zijn. „We houden van
twee tot vier 's middags een receptie
voor zijn huis, nummer
dertien. Veel langer zou te vermoeiend
zijn voor d*e* h*ee*r Van Hooijdonk.
We hebben de hele
familie uitgenodigd, bekenden
en natuurlijk de straatbewoners.”
Lutgerink, van huis uit maatschappelijk
werker, verklaart de
oorsprong van dit initiatief: „Een
paar jaar geleden heb ik beloofd
dat als hij honderd zou worden,
ik iets zou organiseren, en belofte
maakt schuld. Bovendien is
het een leuke gelegenheid voor
de straatbewoners om elkaar
weer eens te ontmoeten.” De
straatbewoners hebben ook geld
ingezameld voor een cadeau. Besloten
is om niet iets te kopen
maar met het geld uitstapjes te
financieren voor d*e* h*ee*r Van Hooijdonk.
„Bijvoorbeeld een
dagje naar de dierentuin,” legt
Lutgerink uit.

Natuurlijk zal er ook een vertegenwoordiger
van het (deel)gemeentebestuur
aanwezig zijn op
de heugelijke dag. Tom Harreman,
voorzitter van de deelgemeente
Delfshaven, zal een
speech houden. Verder is er gezorgd
voor een draaiorgel om de
middag muzikaal te omlijsten.
Bij een feest horen slingers en
vlaggen, ook daar zal voor gezorgd
worden. Er zullen vlaggen
hangen van alle twintig nationaliteiten
die in de straat vertegenwoordigd
zijn.

De jubilaris zelf organiseert nu
natuurlijk niet mee, maar dat is
in het verleden wel anders geweest.

D*e* h*ee*r Van Hooijdonk spreekt met
veel plezier over zijn jonge jaren.
„Toen ik een jaar of tien, elf was
zat ik op school, maar de relatie
met de meester liep niet zo lekker.
Eén keer in de week hadden
we gymnastiek van een speciale
gymleraar. De meester ging dan
schriften nakijken en riep af en
toe een leerling bij zich. Op een
gegeven moment stond ik bij de
meester en daar werd de gymleraar
boos om, hij zei tegen de
meester: „Wat u hier doet, moet
u op school doen!” Vanaf dat
moment mocht ik niet meer mee
gymmen met m'n eigen klas.”
Dat deze meester niet zijn grootste
vriend was blijkt ook uit het
voorval met zijn achternaam.
Van Hooijdonk schreef zelf altijd
zijn naam met ‘ij’, terwijl de
meester van mening was dat het
toch echt met een ‘y’ gespeld
diende te worden. „Op een zekere
dag was ik het school gaan zo
zat dat ik wilde gaan werken. Dat
mocht op zich wel van de hoofdmeester,
als ik maar een baas kon
vinden waar ik aan de slag kon.
Gelukkig kon ik een betrekking
krijgen bij een boekwinkel, dus
kon ik weg van school.” Als kind
ging Van Hooijdonk ook wel
mee met zijn vader naar de Westerkerk
op de Kruiskade en volgde
ook zondagschool. Later verwaterde
dat bezoek een beetje en
kwam hij eigenlijk alleen nog op
hoogtijdagen in de kerk. „Je had
wel wat anders aan je hoofd, er
moest geld verdiend worden.”

Op zeventienjarige leeftijd trad
Van Hooijdonk in dienst bij Van Duin,
een meubelzaak aan de
Rochussenstraat. Er waren in die
tijd veel werklozen en er was
veel armoede. Om toch in dienst
te kunnen blijven werd de volgende
oplossing verzonnen. „Je
moest de ene week wel werken
en de week erop weer niet. Ik
hoefde gelukkig niet te kuieren,
zo noemde men dat, dus ik kon
gewoon alle weken werken. Totdat
mijn collega's daarover gingen
zeuren bij de baas. Het gevolg
was dat ik drie weken moest
gaan kuieren. Op een zondag
liep ik de directeur van Vroom
en Dreesman, waar wij veel voor
werkten, tegen het lijf en die
vroeg waarom ik al drie weken
niet geweest was. Ik legde uit dat
ik moest kuieren en dat vond die
man van V&D onbegrijpelijk dat
ze iemand als mij lieten lopen.
Hij vroeg me die maandagochtend
langs te komen en dat heb ik
gedaan. Hij bood me aan al het
werk door mij te laten doen en
vroeg of ik ergens kon werken.”
In de tussentijd was Van Hooijdonk
verhuisd naar de **Jan Sonjéstraat**
en had de beschikking over
een souterrain, wat uitstekend
geschikt was voor dit doel. Op
deze manier begon het eigen bedrijf,
waar later wel zeven mensen
in dienst traden. Een bedrijfsauto
werd overgenomen
van de melkboer die naar Australië
emigreerde.

In de Tweede Wereldoorlog was
het minder goed toeven in de
stad. Er werden regelmatig bombardementen
uitgevoerd, eerst
door de Duitsers, daarna door
de geallieerden. De bewoners van
de **Jan Sonjéstraat** hebben Joodse
gezinnen en volwassen mannen
weggevoerd zien worden en
in de winter van *19*44-*19*45 is zelfs
een man uit de straat overleden
door de honger.

De toen bijna veertigjarige
Van Hooijdonk besloot met wat andere
straatbewoners om zich niet
alles te laten gezeggen door de
bezetters. Zo moesten de burgers
hun telefoontoestel inleveren.
Een man uit de straat was handig
met telefoons en demonteerde
die van Van Hooijdonk, die het
toestel vervolgens verstopte.
„Toen kwamen ze om de telefoon
op te halen, maar die was er natuurlijk
niet. Ze hebben heel het
huis doorzocht, maar gelukkig
hebben ze het toestel niet gevonden.”
De dag waarop de oorlog
over was liet Van Hooijdonk de
telefoon weer ophangen. „Terwijl
die man het toestel aan het
ophangen was, werd er al gebeld:
het stadhuis. Ze vroegen wat ik
met een telefoon deed, of ik niet
van de verkeerde kant was geweest.
Toen ze hoorden hoe de
vork in de steel stak feliciteerden
ze me; ik was de eerste Rotterdammer
met telefoon.”

## De straat

„Ik ben gek op de straat en op
mijn huis,” zegt Van Hooijdonk
met de nodige weemoed in zijn
stem. „Ik zou graag weer eens
thuis zijn, als ik binnenstap komen
alle herinneringen terug.
Aan mijn vrouw, die inmiddels
overleden is, de kinderen en het
werken in de kelder.” Hoewel hij
niet geheel ontevreden is over de
verzorging in het verpleeghuis
waar hij nu zit, is zijn grote wens
om nog één dag per week aan de
**Jan Sonjéstraat** te verblijven. „Ik
voel me één met m'n huis.”

De betrokkenheid met de straat
is bij d*e* h*ee*r Van Hooijdonk altijd al
groot geweest. Hij zorgde er persoonlijk
voor dat mensen de
straat schoon hielden en dat een
ieder zich fatsoenlijk gedroeg.
Als automobilisten bijvoorbeeld
het eenrichtingsverkeersbord negeerden
ging hij midden op de
weg staan om ze terug te sturen.
Over de properheid van de straat
zegt hij: „Vroeger werd het koper
gepoetst, de pui schoongemaakt
en de stoep geschrobd.
Vandaag de dag is dat helaas niet
meer zo vanzelfsprekend. Als ze
een flesje leegdrinken dan gooien
ze het gewoon op straat.”

Toen er in het jaar 2000 een
boekje, ‘Even terug in de tijd’,
werd samengesteld over honderd
jaar **Jan Sonjéstraat**, heeft d*e* h*ee*r
Van Hooijdonk daar ook zijn
medewerking aan verleend.
Naast de verhalen die hij kon
vertellen over vroeger tijden
heeft hij ook foto's uit het privéalbum
ter beschikking gesteld.

## Leven na de honderd

Een paar dagen geleden hebben
d*e* h*ee*r Van Hooijdonk en andere
bewoners van het verpleeghuis
de avondvierdaagse gereden. Lopen
was er niet meer bij helaas,
maar toch waren het mooie ritten
en leverde het bovendien nog een
medaille op ook. Stilzitten is iets
waar Jan van Hooijdonk moeite
mee heeft. Hij houdt zich nog
graag bezig met klaverjassen,
dammen, domino's en zelfs zingen.
Hij moet wel bekennen dat
alles moeilijker gaat, „Ik zou liever
willen dat ik tweeënzeventig
werd. Het slijt allemaal, de ene
keer heb ik hier pijn de andere
keer daar.” Ook het leven in een
verpleeghuis is niet altijd even
makkelijk. „Ik kan niet zelf bepalen
wanneer ik in en uit bed ga
en dat is vervelend. Soms lig ik
een half uur te wachten voordat
ik uit bed kan.” Zijn grootste
wens voor na de honderd is dat
hij langzaam afscheid kan nemen
zowel van de straat als van
de familie.

Mensen als Jan van Hooijdonk
hebben Rotterdam groot gemaakt,
daar mag de stad trots op
zijn en hen koesteren zolang zij
nog leven.
