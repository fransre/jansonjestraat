---
toegevoegd: 2024-08-13
gepubliceerd: 1918-04-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296980:mpeg21:a0091"
onderwerpen: [diefstal]
kop: "Naar het Huis van Bewaring"
---
# Naar het Huis van Bewaring

is overgebracht de koopman J.J. K., verdacht
van diefstal van zeep, cacao, kwattareepen,
peperkorrels en vet, ter waarde
vaa ƒ2300 door middel van braak ontvreemd
uit het pakhuis van F.J. D. aan
de **Jan Sonjéstraat**. Hij zou een besteller
der S*taats*S*poor* aangenomen hebben om deze goederen
te vervoeren naar het Maasstation
doch gelijk men weet, was deze met den
wagen aangehouden.
