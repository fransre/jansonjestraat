---
toegevoegd: 2024-07-30
gepubliceerd: 1939-08-08
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164702033:mpeg21:a00082"
onderwerpen: [woonruimte, tehuur]
huisnummers: [36]
kop: "bovenhuis,"
---
Te huur vrij

# bovenhuis,

**Jan Sonjéstraat** 36
ƒ32.50, ~~1e Etage Vierambachtsstraat 111,
Huurprijs ƒ32.50 p*er* m*aand*~~
Tel*efoon* 56093-31594.
