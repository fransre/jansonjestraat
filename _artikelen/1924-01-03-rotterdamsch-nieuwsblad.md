---
toegevoegd: 2024-08-11
gepubliceerd: 1924-01-03
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494687:mpeg21:a0182"
onderwerpen: [verloren]
huisnummers: [26]
achternamen: [mager]
kop: "Het succes van „Kleintje”."
---
# Het succes van „Kleintje”.

Het Nieuwsblad-Kleintje zet zijn
vruchtbaren arbeid ook in het nieuwe
jaar met frisschen moed voort. Hij had
den 2en Januari *1924* alweer twee successen
te boeken:

De heer H. Mager, **Jan Sonjéstraat** 26,
verloor een schaats, die hem, reeds een
half uur na het verschijnen van het
Nieuwsblad, waarin hij een Kleintje
had geplaatst, werd terugbezorgd door
Herman Steingröver, Van Speykstraat 135.
