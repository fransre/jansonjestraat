---
toegevoegd: 2024-08-12
gepubliceerd: 1921-05-19
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493747:mpeg21:a0096"
onderwerpen: [huispersoneel]
huisnummers: [24]
achternamen: [bosman]
kop: "Een dagmeisje"
---
# Een dagmeisje

gevraagd. **Jan Sonjéstraat** 24.
Bosman.
