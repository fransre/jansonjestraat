---
toegevoegd: 2024-07-24
gepubliceerd: 1959-11-27
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164907050:mpeg21:a00197"
onderwerpen: [huisraad, reclame]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Alles voor uw woning."
---
# Alles voor uw woning.

Koop
uw S*in*t-Nicolaasgeschenken bij
Joh. v*an* Hooijdonk, o*nder* a*ndere* tafelkleden
v*an*a*f* ƒ15,75, stoelkussens
v*an*a*f* ƒ35, per stel. Joh.L. van Hooijdonk.
Engelsestraat 145.
tel*efoon* 36612 (oud Mathenesse)
**Jan Sonjéstraat** 13b, tel*efoon* 53880.
