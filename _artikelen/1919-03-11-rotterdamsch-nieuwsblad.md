---
toegevoegd: 2024-08-13
gepubliceerd: 1919-03-11
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010995668:mpeg21:a0082"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [22]
achternamen: [spitters]
kop: "draadnagels,"
---
Te koop aangeboden

# draadnagels,

maat 9/17, à 70 c*en*ts p*er*
kilo. J.W. Spitters,
**Jan Sonjéstraat** 22.
