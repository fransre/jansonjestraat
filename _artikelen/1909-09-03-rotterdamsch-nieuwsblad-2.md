---
toegevoegd: 2024-08-14
gepubliceerd: 1909-09-03
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197980:mpeg21:a0086"
onderwerpen: [werknemer]
huisnummers: [4]
kop: "Schoenmakerij."
---
# Schoenmakerij.

Terstond gevraagd een
net Schoenmakers-Halfwas,
goed kunnende repareeren.
Loon vijf of
zes gulden, naar bekwaamheid
en vast
werk. Adres Middellandstraat 78
of **Jan Sonjéstraat** 4.
