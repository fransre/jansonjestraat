---
toegevoegd: 2021-05-03
gepubliceerd: 1956-10-24
decennium: 1950-1959
bron: Algemeen Indisch dagblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010896290:mpeg21:a0153"
onderwerpen: [familiebericht]
huisnummers: [29]
achternamen: [oeij]
kop: "Jaap K.G. Oeij en Elizabeth Westerop"
---
# Jaap K.G. Oeij en Elizabeth Westerop

hopen elkaar het H*eilig* Sacrament, van het Huwelijk
toe te dienen op Zaterdag 27 October *1956* a*an*s*taande* in de
Parochiekerk van S*in*t Barbara, Crooswijkseweg 38,
waarna tot hun intentie een gezongen H*eilige* Mis zal
worden opgedragen.

Rotterdam, Crooswijkse Singel 16A

Scheveningen, Prins Mauritslaan 65

Toekomstig adres:

**Jan Sonjéstraat** 29A

Rotterdam.
