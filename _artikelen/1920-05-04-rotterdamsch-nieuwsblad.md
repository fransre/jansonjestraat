---
toegevoegd: 2024-08-13
gepubliceerd: 1920-05-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493939:mpeg21:a0082"
onderwerpen: [bedrijfsinventaris, tekoop, dieren]
huisnummers: [38]
achternamen: [weier]
kop: "2 werkhonden"
---
# Goede films

te koop 5, 10 en 12
cents per M*eter*, alles moet
weg. E.J. Weier, **Jan Sonjéstraat** 38,
Telef*oon*
N*ummer* 10084.
