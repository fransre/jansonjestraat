---
toegevoegd: 2023-01-08
gepubliceerd: 2008-09-11
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2008-09-11-de-middellander-origineel.jpg"
onderwerpen: [kunst, schilder]
kop: "Kunst in de Jan Sonjéstraat"
---
# Kunst in de **Jan Sonjéstraat**

Op zaterdag 11 oktober *2008*
is in de **Jan Sonjéstraat**
het kunstwerk
‘Heel de wereld is mijn
vaderland’ van André
Smits feestellijk onthuld.
De muurschildering is
een initiatief van de
straatgroep in samenwerking
met het Centrum Beeldende Kunst (CBK)
en de Stichting Bevordering van Volkskracht.
