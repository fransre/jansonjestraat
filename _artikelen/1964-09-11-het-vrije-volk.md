---
toegevoegd: 2024-07-23
gepubliceerd: 1964-09-11
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955249:mpeg21:a0054"
onderwerpen: [kleding, tekoop]
huisnummers: [22]
achternamen: [nederveen]
kop: "herenjas."
---
Te koop aangeb*oden* bruin lederen

# herenjas.

C. Nederveen,
**Jan Sonjéstraat** 22b. (zijstr*aat*
Middellandstr*aat*).
