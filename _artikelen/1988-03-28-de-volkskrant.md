---
toegevoegd: 2024-07-21
gepubliceerd: 1988-03-28
decennium: 1980-1989
bron: De Volkskrant
externelink: "https://resolver.kb.nl/resolve?urn=ABCDDD:010856683:mpeg21:a0125"
onderwerpen: [bellebom]
kop: "Er is geen studentenleven zonder God"
---
# „Waarom mot die bom weg? Hij lèg hier toch lekker?”

De „operatie Bellebom” is tot in de puntjes voorbereid. Op alles is
gerekend bij de evacuatie van drieduizend woningen in de wijk
Middelland. Behalve op de bewoners: negen poezen en zeven vogeltjes
zitten in de Weena-hal, waar ruimte is geschapen voor de opvang van
zevenhonderd huisdieren. En het opvangcentrum voor hun baasjes is al
net zo leeg. Rotterdam ruimt een bom op en burgemeester Peper
handen.

Zondagmorgen *27 maart 1988* half zeven.
Een nog niet geheel wakkere
man loopt met een voetbaltas op
de Westkruiskade in Rotterdam,
op weg naar het Centraal Station.
Het tijdstip is zelfs te vroeg voor de
meest ver weg gelegen wedstrijd van
een Rotterdamse voetbalscheidsrechter.
De man ontvlucht de „operatie-Bellebom”,
het onschadelijk maken en afvoeren
van een vijfhonderd kilo zware Britse
vliegtuigbom die 43 jaar geleden per
ongeluk is gedropt in achtertuinen van
de Bellevoysstraat en de **Jan Sonjéstraat**,
in de Rotterdamse wijk Middelland.

In de Bellevoysstraat valt even later
weinig te merken van de op handen
zijnde evacuatie. In een straal van driehonderd
meter moeten zevenduizend
mensen hun huis uit, maar tot acht uur
heerst hier en in omliggende straten
stilte. Wel rijden al dierenambulances
rond, naar later blijkt geheel vergeefs,
slechts negen poezen en zeven vogeltjes
worden in de Weena-hal opgevangen,
terwijl daar ruimte is geschapen voor
zevenhonderd huisdieren.

Veel dieren worden thuisgelaten, zo
wordt aangenomen. Dat geldt ook voor
de vijfendertig wurgslangen die huizen
in het souterrain van een enthousiaste
biologie-leraar in de Bellevoysstraat. De
politie zal dat verblijf later, bij de controle
op de woningen, onbetreden laten,

Van de grote bruine beer die volgens
geruchten in de wijk verblijft, blijkt
geen spoor wanneer politie, Energiebedrijf
en Gemeentewerken na half 9
begonnen zijn aan de controle van de
ontruimde panden die moeten
worden verzegeld. Het blijkt eigenlijk
te gaan om twee wasbeertjes die een
ex-circusartiest in de wijk ooit heeft
gehad. Eén is overleden en de andere in
een dierentuin beland.

De Pakistaanse familie Masood is zondagochtend *27 maart 1988*
niet extra vroeg opgestaan,
zeven uur. Om half negen staan Masood
en twee van z'n drie kinderen in de
deuropening, wachtend op de controle.
Het plan is om een dagje naar Brabant te
gaan. Het duurt even voordat de controleteams
bij het huis van Masood arriveren.

De meeste mensen moeten geruime
tijd geduld oefenen. Dat komt ook doordat
velen al extra vroeg paraat zijn.
Maar aan de andere kant valt het controleren
van de woningen tegen. Soms
neemt het wel een kwartier in beslag,
terwijl was gerekend op zeven minuten
per woning. Dan moeten er deuren worden
opengebroken. Meestal blijkt er niemand
thuis, een enkele keer ook komt
een slaperig uitziende bewoner naar
buiten stommelen, gewekt door het lawaai.
Een keer wordt een breekijzer
ingezet bij een woning waarvan de sleutel
wèl is achtergelaten. De verantwoordelijke
overheidsdienaar arriveert even
later bij de kapotte voordeur: „Hier is de
sleutel.”

Ook de gemeente zelf zorgt voor onvoorzien
oponthoud. Vanwege de stadsvernieuwing
heeft zij nogal wat panden
eigenhandig dichtgetimmerd. Nu moeten
de schotten worden verwijderd en
na controle herbevestigd. Daarop was
bij de oefening niet gerekend, terwijl er
toch tientallen van dergelijke panden te
vinden zijn.

Het gebeuk en het bijbehorende
wachten brengt veel van de gemeentelijke
medewerkers in een melige stemming.
Twee van de drie agenten lopen er
werkeloos bij, maar het is altijd nog
beter dan een middagje bewaking bij
Feyenoord-FC Utrecht (een wedstrijd
die overigens op last van burgemeester
Peper is uitgesteld).

De meeste mensen ondergaan de operatie
gelaten, maar een enkele wijkbewoner
laat in goed Rotterdams van zijn
misnoegen blijken: „Waarom mot die
bom nou weg? Hij lég hier toch lekker,
niet dan?”

Enige opwinding ontstaat wanneer
De Autoriteiten zich in de wijk laten
zien: burgemeester Peper en hoofdcommissaris
Blaauw. „Er is een bom voor
nodig om Peper de wijken in te krijgen”,
zegt een wijkbewoner, kennelijk
doelend op de buitenlandse reislust van
Rotterdams eerste burger.

Peper heeft begin dit jaar beterschap
beloofd. Keuvelend en handjesschuddend
gaan ze rond. Een buitenlandse
wijkbewoner die wordt voorbijgelopen
mokt dat Peper alleen oog heeft voor de
Nederlandse ingezetenen. „Ik krijg niet
eens een hand.”

Rotterdam heeft de „operatie-Bellebom”
verheven tot een groots evenement,
dat zes miljoen gulden kost. Mogelijke
verwijten dat het meer gaat om
een grootscheepse oefening dan om een
puur noodzakelijke operatie, worden
weggewoven. De gemeente ontkent dat
het een „operatie-Luchtballon” zou
zijn. Wie is verantwoordelijk wanneer
de bom onverhoeds wél mocht ontploffen?
In West-Duitsland is vorig jaar
's avonds nog een gehele kleuterschool
door zo'n duizendponder weggevaagd.

Toch heeft het er wel van weg, dat de
operatie wat al te groots is opgezet. Niet
iedereen is ervan overtuigd dat deze
grootscheepse evacuatie inderdaad wel
nodig was. De EOD klaart soortgelijke
klussen meerdere keren per jaar zonder
probleem. En zelfs al zou de diep liggende
Bellebom ontploffen, dan nog zouden
alleen maar funderingen worden aangetast,
en zouden er geen huizen worden
weggevaagd.

En opvangcentra voor dieren en mensen
blijven voor een fors deel leeg,
chauffeurs van bussen lummelen evenals
een groot aantal agenten urenlang
rond. Al die ruimte en voorzieningen
zijn gebaseerd op een enquête onder
buurtbewoners naar hun behoefte aan
opvang. In werkelijkheid blijkt die behoefte
heel anders dan op papier.
In de Energiehal in Rotterdam-West
is voor twaalfhonderd mensen ruimte
ingericht, compleet met maaltijden en
vermaak. Vijf pallets lectuur zijn door
een uitgever bezorgd. In eerste instantie
zijn er zestig mensen in de hal, later
groeit hun aantal tot welgeteld 252. De
bussen die intensief hadden moeten rijden
tussen de wijk Middelland en de hal,
hebben merendeels stilgestaan. De dierenambulances
rijden 25 van de 29 keer
vergeefs. Ook hier blijkt de realiteit heel
anders dan de enquête deed vermoeden.

De voor de hand liggende verklaring
is, zo zegt een medewerker van de Sociale Dienst,
dat mensen te elfder ure toch
hebben besloten naar familie te gaan.
Hij geeft ook een andere hypothese. „In
de wijk wonen zo'n driehonderd illegalen,
in huizen en zelfs in auto's. Die
voelen er niet zoveel voor in contact te
komen met de politie. Daarom zijn die
mensen al voor zondagochtend *27 maart 1988* vertrokken
uit de buurt.”

Het Pakistaanse gezin Masood blijkt
toch in de Energiehal te zijn neergestreken.
De kinderen vermaken zich zichtbaar.
Spelletjes en teken- en leesmateriaal
te over, tekenfilms in veelvoud op de
talloze televisieschermen. Masood
woont al vijftien jaar in Rotterdam, en
sinds twee jaar in de Bellevoysstraat.
Naar zijn zin, zegt hij, geen probleem
met al die nationaliteiten. „Alleen die
hondepoep op straat, dat is echt verschrikkelijk.”

Tegen het middaguur is zône-A — het
gebied dat geëvacueerd moet worden —
geheel leeggeveegd door de honderd
ontruimingseenheden van de politie. Zij
hebben 2800 woningen bezocht, en bij
welgeteld 72 woningen een deur moeten
forceren.

Omdat de meeste bewoners van Middelland
aan het verzoek gehoor hebben
gegeven hun auto uit de wijk te rijden,
blijft het aantal weg te slepen wagens
gering. In totaal zijn 55 auto's door de
politie weggetakeld. Hoofdcommissaris Blaauw
zal later op de middag zeggen
dat de eigenaren hun mobiel kunnen
ophalen. Er blijft alleen verwarring bestaan
over de kosten: de ene keer heet
het „kosteloos”, de andere keer wordt
bekendgemaakt dat toch 75 gulden
moet worden betaald om de weggesleepte
auto terug te krijgen.

Om kwart voor één is de wijk leeg, om
tien voor twee geeft burgemeester Peper
(bij een Engelstalige journalist heet
hij „doctor Pepper”) over de regionale
rampenzender Radio Rijnmond het teken
dat de eigenlijke operatie Bellebom
kan beginnen. Peper verspreekt zich
nogal pijnlijk als hij zegt dat „de detonatie”
(de ontploffing!) kan beginnen.
Maar evengoed gaan adjudant
P. Schoots en sergeant J. van Linschoten
van de Explosieven Opruimingsdienst
(EOD) de tien meter diepe put in de
tuintjes achter de Bellevoysstraat in.

Anderhalf uur lang heerst dan stilte.
Van op afstand draaien de twee EOD-mannen
het ontstekingsmechanisme uit
de gaaf bewaarde duizendponder. Het
duurt achttien minuten. Vervolgens
gaat Schoots met een „anti-vonk-punt-tangetje”
de put in om, met de hand, het
duplex slagpijpje los te wurmen. Die
handeling is de meest risicovolle, omdat
het slagpijpje door temperatuursverandering,
vallen of kraken kan exploderen.
In het ergste geval gaat dan de hele
bom mee, en mist Schoots na dertig jaar
dienst alsnog zijn pensioen.

EOD-kapitein L. van Manen zal later
zeggen dat de operatie vlekkeloos en
vooral vlot verliep. Het slagpijpje was
verder gecorrodeerd — aangevreten —
dan prettig was, „zag er erg vervelend
uit”, en is daarom in een naastgelegen
put tot ontploffing gebracht Een uur
later verdringen circa vijftig journalisten
zich op en rond de bom die dan al
naar de achtertuintjes van de Bellevoysstraat
is gehesen. Peper, Schoots en Van Linschoten
poseren zij aan zij. De tweede
EOD'er mag ten overvloede zeggen
dat hij best leuk werk heeft.

Als aan het eind van de middag de
„Bellebom” in een vrachtwagen van de
EOD naar Culemborg wordt gereden,
laat burgemeester Peper weten dat het
Rotterdams college van B*urgemeester* en W*ethouders* nog niet
heeft besloten wat het met de eventueel
andere in de stad verborgen bommen zal
doen. De nu geborgen duizendponder
hoopt hij, „als de EOD met ons mee wil
vibreren”, nog eens leeg terug te ontvangen
opdat hij aan het Historisch Museum
kan worden geschonken.

Henk Blanken

Gert Riphagen

*Onderschrift bij de foto*
De familie Masood moet geruime tijd op de controleteams wachten. Het doorzoeken van huizen duurde wat langer dan verwacht. Foto Guus Dubbelman
