---
toegevoegd: 2023-08-01
gepubliceerd: 1929-07-06
decennium: 1920-1929
bron: Utrechtsch Nieuwsblad
externelink: "https://proxy.archieven.nl/0/70C5F8568E6F4D84BB6ED447F02AE299"
onderwerpen: [diefstal]
kop: "Inbraak in een pettenfabriek."
---
# Inbraak in een pettenfabriek.

Voor de Rotterdamsche rechtbank
hebben terecht gestaan de gebroeders J.J.
en G. K., beiden recidivist en gedetineerd,
wien ten laste wordt gelegd, dat
zij in den nacht van 10 op 11 Mei *1929* tezamen
en in vereeniging, ter uitvoering
van hun voornemen om in de in het
pand Middellandstraat 110 gevestigde
pettenfabriek voorheen Wolf & Norden,
met het oogmerk van wederrechtelijke
toeëigening weg te nemen gelden en
wat verder van hun gading zou blijken
te zijn, zich door middel van verbreking
en in klimming toegang hebben verschaft
tot de fabriek, en aldaar gepoogd
hebben met behulp van een breekijzer,
een boor en een nijptang de brandkast
te openen, zijnde hun voorgenomen misdrijf
niet voltooid door de van hun
wil onafhankelijke omstandigheid, dat
de brandkast uit zoodanig materiaal bestond,
dat zij weerstand bood, en zij
door de komst van de politie gestoord
werden.

De verdachten bekenden volmondig.
Door een leeg pand, waarvan de deur
met een valschen sleutel geopend was,
waren zij op het dak van de pettenfabriek
gekomen. Na verbreking van een
koekoek waren zij op de derde verdieping
aangekomen. Van daar begaven zij
zich naar het kantoor, waar zij de kap
van de brandkast lichtten en deze vervolgens
voorover kantelden. J.J. had
met behulp van de meegebrachte werktuigen
de kast bewerkt, maar deze bood
meer tegenstand, dan men verwacht
had. Opeens had G. ontdekt, dat er buiten
politie stond. Over het dak en door
een ander perceel hadden zij toen getracht
te vluchten, doch toen zij de deur
uitkwamen, waren zij door een politieagent
gegrepen. Armoede was de drijfveer
tot de daad geweest.

Getuige A. Brandel, directeur der pettenfabriek,
had op 10 Mei *1929* zijn zaak in
de beste orde verlaten. 's Nachts omstreeks
2 uur was hij door de politie geroepen.
Een boor, een beitel en een
nijptang lagen bij de brandkast, die
voorover gekanteld was. In de kast had
zich zoo goed als geen geld bevonden.

De inspecteur van politie K.T. v*an* d*er* Wilt
had de telefonische waarschuwing
gekregen, dat er onraad was in de pettenfabriek.
Hij had de verdachten over de daken achtervolgd.

De agent van politie J. Moerman had
beide verdachten gearresteerd, toen zij
uit een pand aan de **Jan Sonjéstraat**
kwamen.

Het O*penbaar* M*inisterie* achtte het ten laste gelegde
bewezen. Ook al heeft J.J. K. alleen aan
de brandkast gewerkt, dan is G. toch
mede schuldig aan de poging tot diefstal.
Toen J.J. K. aan den officier werd
voorgeleid, heeft deze hem de vraag gesteld
hoe het komt, dat hij — toch een
flinke jongen — niet op eerlijke wijze
zijn brood kon verdienen. J.J. had toen
opgemerkt, dat het met een straflijst als
de zijne niet gemakkelijk is, weer aan
werk te komen. Vol lof was hij echter
voor zijn broer G., die hem geholpen
had, zooveel als in zijn vermogen was,
en hij had er spijt van, dat hij deze
mee in het avontuur betrokken had. Het
O*penbaar* M*inisterie* meende er niet aan te kunnen meewerken,
dat J.J. voorwaardelijk wordt
veroordeeld. De kans bestaat echter,
dat hij, wanneer driekwart van zijn
straftijd om is, geholpen zal kunnen
worden, om een nieuw leven te beginnen.
Eisch: 1 jaar gevangenisstraf.

Wat G. betreft, al neemt zijn broer ook
de schuld op zich, hij had aan de verleiding
weerstand moeten bieden en
niet moeten mee gaan. Ook hij heeft al
eenige straffen achter den rug. Tegen
hem eischt het O*penbaar* M*inisterie* 11 maanden gevangenisstraf
met aftrek der preventieve hechtenis.

De verdediger van J.J. K., mr. C.C.
Arnold, kan geheel meegaan met de
overwegingen van het O*penbaar* M*inisterie*, echter acht
pleiter de straf te hoog. Pleiter's indruk
is deze, dat verdachte, die reeds meermalen
gestraft is, moeilijk werk kan
vinden. Toen hij gearresteerd werd, was
hij net 8 maanden in vrijheid. Hij was
toen bij zijn broer thuis en verdiende
wat als koopman in ongeregeld goed.
Door den strengen winter had hij echter
in twee maanden al niets verdiend.
Dit heeft op zijn eergevoel gewerkt. Hij
had het gevoel, dat hij zijn broer niet
langer kon „opeten”. Dit is de aanleiding
tot de daad. Pleiter verzoekt deze
omstandigheid bij het bepalen van de
straf in aanmerking te nemen. Hij
merkt nog op, dat bij een lichte straf de
reclasseeringspogingen het beste resultaat
zullen hebben.

Mr. W.J. Jongeneel, optredende voor
verdachte G. K., wees er op, dat de
rechtbank reeds meermalen menschen,
die reeds meer met den strafrechter in
aanraking zijn geweest, tot voorwaardelijke
straffen heeft veroordeeld. Eén geval
is pleiter zelfs bekend, waarbij de
veroordeelde reeds zeven vonnissen
achter den rug had. Dit geeft hem
moed, te verzoeken, ook hier de mogelijkheid
van een voorwaardelijke straf
onder de oogen te zien.

Uitspraak 11 Juli *1929*.
