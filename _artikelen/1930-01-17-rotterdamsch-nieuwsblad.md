---
toegevoegd: 2024-08-06
gepubliceerd: 1930-01-17
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164645018:mpeg21:a00141"
onderwerpen: [huispersoneel]
huisnummers: [35]
achternamen: [gosschalk]
kop: "Dagdienstbode"
---
# Dagdienstbode

gevraagd in Gezin z*onder* K*inderen*.
Zondags vrij. Adres:
Gosschalk,
**Jan Sonjéstraat** 35a.
