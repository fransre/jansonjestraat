---
toegevoegd: 2021-12-29
gepubliceerd: 1941-01-30
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "http://resolver.kb.nl/resolve?urn=ddd:011002640:mpeg21:pdf"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [3]
achternamen: [verheij]
kop: "zit-slaapkamer"
---
Te huur keurig gemeubileerde
boven-

# zit-slaapkamer

voor 2 à 3 personen.
Prima pension. Billijk.
**Jan Sonjéstraat** 3B, bovenhuis,
Verheij.
