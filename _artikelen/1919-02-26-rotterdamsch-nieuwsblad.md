---
toegevoegd: 2024-08-13
gepubliceerd: 1919-02-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010995657:mpeg21:a0084"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [3]
kop: "Straatzaak"
---
# Straatzaak

in Aardappelen, Groenten,
Fruit ter overname
aangeboden wegens
ziekte. Spoed. **Jan Sonjéstraat** 3a.
