---
toegevoegd: 2024-08-14
gepubliceerd: 1907-07-24
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196877:mpeg21:a0083"
onderwerpen: [werknemer]
huisnummers: [15, 17]
achternamen: [van-uitert]
kop: "Plakmeid."
---
# Plakmeid.

Terstond gevraagd een
flinke Plakmeid bij M. van Uitert,
Stoom- en Strijkinrichting De Adelaar,
**Jan Sonjéstraat** 15-17.
