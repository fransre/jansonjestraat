---
toegevoegd: 2024-08-04
gepubliceerd: 1933-02-28
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164663066:mpeg21:a00150"
onderwerpen: [bedrijfsruimte]
huisnummers: [3, 5]
achternamen: [mager]
kop: "Winkelhuis"
---
# Winkelhuis

met Woning te huur,
**Jan Sonjéstraat** N*ummer* 3.
Huur ƒ9 p*er* week. Bevragen
J.H. Mager,
**Jan Sonjéstraat** 5b, van
2 tot 7 u*ur*
