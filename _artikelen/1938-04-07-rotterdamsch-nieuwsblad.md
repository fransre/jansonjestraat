---
toegevoegd: 2024-08-01
gepubliceerd: 1938-04-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164694033:mpeg21:a00022"
onderwerpen: [bedrijfsruimte]
huisnummers: [2]
kop: "Kantoortje"
---
# Kantoortje

Parterre, geheel vrij,
**Jan Sonjéstraat** 2, hoek
Middellandstr*aat*. Uitst*ekend*
gesch*ikt* voor Loterij,
ƒ12.50 p*er* m*aa*nd. Inl*ichtingen* Studio,
Zuidblaak 2, Tel*efoon* N*ummer* 12961.
