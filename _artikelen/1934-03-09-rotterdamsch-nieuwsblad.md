---
toegevoegd: 2024-08-04
gepubliceerd: 1934-03-09
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164670010:mpeg21:a00162"
onderwerpen: [werknemer]
huisnummers: [15]
achternamen: [van-oosten]
kop: "Biedt zich aan:"
---
# B*iedt* z*ich* a*an*:

Net Meisje, 26 j*aar*,
zoekt Werkhuizen voor
net Kamerwerk, v*an* z*eer* g*oede*
g*etuigen* v*oorzien*, ook voor Schoonmaak.
B. v*an* Oosten,
**Jan Sonjéstraat** 15.
