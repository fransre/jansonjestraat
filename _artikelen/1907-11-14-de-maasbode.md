---
toegevoegd: 2024-08-14
gepubliceerd: 1907-11-14
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000189651:mpeg21:a0019"
onderwerpen: [faillissement]
huisnummers: [17]
achternamen: [basart]
kop: "Faillissementen."
---
# Faillissementen.

Opheffing faillissement,

H.C. Basart, koffiehuishouder, **Jan Sonjéstraat** 17.
