---
toegevoegd: 2024-08-14
gepubliceerd: 1907-09-26
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196933:mpeg21:a0119"
onderwerpen: [bedrijfsruimte]
huisnummers: [42, 44]
kop: "Pakhuis."
---
# Pakhuis.

Groote Pakhuisruimte
te huur met veel licht,
Kantoor, Privaat, Gas- en
Waterleiding en 2
groote inrijpoorten van
3½ M*eter* hoog. Te zien
**Jan Sonjéstraat** N*ummer* 42-44
en Sleutel op N*ummer* 40.
Te bevragen Heemraadssingel 126,
hoek Middellandstraat.
