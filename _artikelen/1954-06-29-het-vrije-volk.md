---
toegevoegd: 2024-07-25
gepubliceerd: 1954-06-29
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951937:mpeg21:a0048"
onderwerpen: [huisraad, tekoop]
huisnummers: [8]
achternamen: [hijnen]
kop: "Gasoven,"
---
# Gasoven,

2 bakruimten, met
4 platen, 80×40 en warmtemeters,
klein model, ƒ75.—. Ook
geschikt voor huisgebruik, tussen
7-8 uur 's avonds. J.W.J. Hijnen,
**Jan Sonjéstraat** 8.
