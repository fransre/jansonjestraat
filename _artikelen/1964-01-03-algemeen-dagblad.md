---
toegevoegd: 2021-05-04
gepubliceerd: 1964-01-03
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002823002:mpeg21:a00117"
onderwerpen: [cafe]
huisnummers: [15]
achternamen: [rueter]
kop: "Café Intiem"
---
# Café Intiem

**Jan Sonjéstraat** 15 — Rotterdam

Wegens restauratie gesloten tot half januari *1964*. Daarna wordt
Café Intiem weer geëxploiteerd door de vorige eigenaresse.
Vanouds weer W.C. Rüter-van den Berge. Gelijktijdig wens ik
al mijn vrienden en kennissen een voorspoedig 1964

Heropening wordt nog nader bekendgemaakt.

AD 17
