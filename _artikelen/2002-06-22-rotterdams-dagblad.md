---
toegevoegd: 2023-01-11
gepubliceerd: 2002-06-22
decennium: 2000-2009
bron: Rotterdams Dagblad
externelink: "https://www.jansonjestraat.nl/originelen/2002-06-22-rotterdams-dagblad-origineel.jpg"
onderwerpen: [actie]
kop: "Diner in verschillende etappes"
---
# Diner in verschillende etappes

Omdat lang niet iedereen vroeg het bed uit wil
op een vrije dag en de trip in Rotterdam al snel
vier tot vijf uur in beslag neemt, kreeg organisator
Marjolijn Masselink vaak de vraag ‘waar
kunnen we wat eten’? In de traditie van de City Safari
bedacht zij een reis langs eethuizen.
Inmiddels doen er aan deze Diner Safari vijfendertig
restaurants mee, variërend van Pakistaans,
Ethiopisch, Nederlands, Oostenrijks,
Indisch, Indonesisch, Marokkaans,
Turks, Italiaans, Japans tot Chinees. Elke
gang van het diner wordt in en ander restaurant
genuttigd en tussendoor is er tijd om het
eten te laten zakken tijdens en wandeling of
een bezoek aan en kunstenaar. Verslag van
een avond eten in vijf etappes.

Gang een: New Delhisoep

Beginpunt is Bazar in de Witte de Withstraat.
Voor Rotterdammers allang geen onbekend
adres meer, maar zeker nog een must voor
mensen van buiten de stad. Na een welkomstwoord
van een van de Safari-organisatoren
wordt hier een kom New Delhisoep geserveerd.
Als het eenvoudige gerecht met uien en
paprika op is, wordt de culinaire reiziger op
pad gestuurd. Met plattegrond, metrokaart en
en SOS-nummer, mocht de stad een jungle
blijken.

Gang twee: Turkse mezze

Het echte voorgerecht wordt gegeten in Dunya Lokanta,
een Turks eethuis in de Proveniersstraat.
Op tafel komen negen verschillende
schaaltjes. Zo kan geproefd worden van
Griekse tarama en tatziki, en Bulgaarse variant
van tatziki — met champignons in plaats
van komkommer —, humus, een Turkse aubergineschotel,
en Koerdische aardappelsalade,
een feta/sambal-spread en walnoten/groenten-mix.
Dit alles geserveerd met een
mandje Turks en Koerdisch brood. De eigenaar
wil de gasten ook graag laten proeven
van zijn andere gerechten, maar dat is niet de
bedoeling. De gedachte achter de eetsafari is
juist dat je op één avond kennismaakt met vele
keukens.

## Intermezzo

Tussen het uitgebreide voorgerecht en de
hoofdmaaltijd biedt het programma en wandeling
door de wijk Middelland. Gids is Nico
van Setten. Tot voor kort woonde Van Setten
op de hoek 1e Middellandstraat/Henegouwerlaan.
Een half jaar geleden pakte hij zijn inboedel
en vertrok naar Tarwewijk op Zuid. Hij
had genoeg van alle ‘levendigheid’ in Middelland
en wilde wel eens in een buurt wonen
waar de kogels niet rondvliegen. Een buurt
zonder overvallen, junks, dealers, smerigheid
en verkeersongevallen. „De Tarwewijk, saai?
Dat zijn jouw woorden.”
Van Setten klaagt graag en veel over Rotterdam,
want zonder Fortuyn is er volgens hem
niemand meer die de stad voor verdere teloorgang
kan behouden. Maar waag het niet om
als bezoeker ook iets kritisch ten berde te
brengen. „Het is een teringstad, maar wel
mijn teringstad,” vindt Nico.
Onlangs liet hij een groep Brabanders de verborgen
kinderboerderij in de Duivenvoordestraat
zien. „Die kippen deugen niet,” was het
commentaar van een Brabantse. Dat had ze
nou niet moeten zeggen, want niemand komt
aan Nico's stad en ook niet aan ‘zijn’ kippen.
Nico maakte er en ‘onvergetelijk’ uitje van.
„Ik moest zeker weten dat zij nóóit, maar dan
ook nóoit meer naar Rotterdam komt.”
De rondwandeling onder leiding van Van Setten
leidt onder andere langs het Henegouwen College,
een school waarvan het plein na alle
drugsoverlast is afgegrendeld en waar camera's
de boel in de gaten houden. Maar hij toont
bezoekers ook de pareltjes van de wijk, de **Jan Sonjéstraat**
bijvoorbeeld. Een echt Opzoomerstraatje
te midden van een verloederde omgeving.
Een huis moet er nu al zo'n driehonderdduizend
euro kosten.

Gang drie: Ethiopische pannenkoeken, ‘endjerra's’

Na Nico wacht de hoofdmaaltijd in Axum, een
Ethiopisch café-restaurant in hartje Delfshaven.
Van buiten oogt het pand overigens nauwelijks
als een restaurant, maar vooral als zo'n
café waar de meesten snel langs zouden benen.
Vitrage, kerstverlichting, een donkere
entree: dat soort werk. Binnen blijken gastheer
en gastvrouw echter uiterst hartelijk.
Omdat bij Axum alleen gegeten kan worden
op reservering en meer mensen zich blijkbaar
niet hebben aangemeld, kookt de kokkin deze
avond speciaal voor de enige twee gasten. Bij
het raam is met een Ethiopisch tafeltje een
knus eethoekje à deux gemaakt. Traditioneel
wordt het eten zonder bestek op een endjerra,
een soort zurige pannenkoek, gelegd. De
maaltijd bestaat uit gestoofd rundvlees, gebakken
lamsvlees, kip, koude groenten en salade.
Vóór het eten is het verplicht de handen
te wassen in een badkamer die aan de shampoo
te zien gewoon in gebruik is.

Gang vier: Knoflookijsje

Het toetje wordt geserveerd in Look. Een restaurant
op de 's Gravendijkwal dat, zoals de
naam al aangeeft, gespecialiseerd is in gerechten
met knoflook, véél knoflook. Ook in het
dessert ontbreekt de smaakmaker niet. Zowel
in het ijs als de chocoladetaart zijn flink wat
tenen verwerkt. Als het bord leeg is, de buik
meer dan goedgevuld, komt directeur Marjolijn Masselink
persoonlijk afscheid nemen
van haar ‘tafelgenoten’.
