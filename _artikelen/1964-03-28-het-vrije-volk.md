---
toegevoegd: 2024-07-23
gepubliceerd: 1964-03-28
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954411:mpeg21:a0275"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [smeets]
kop: "Militairen"
---
# Militairen

Met ontsteltenis las ik over
de brand door het optreden
van militairen, waardoor boer
Bosgoed in Oosterhout zo
zwaar is getroffen. Ook ik heb
mijn herinneringen, aan militairen
in die buurt.

Toen we met vakantie waren
in een kamphuis dat ligt
aan het eind fan de weg van
boer Bosgoed, en dat geheel
omgeven is met prikkeldraad
en aan het hek een bordje
Verboden toegang voor onbevoegden
heeft kwamen op
een dag mijn kinderen gillend
binnenstormen. Er bleken militairen
met groen en zwart
gecamoufleerde gezichten op
het kampterrein aan het oefenen.
Ik heb direct de militaire
politie gebeld in Oosterhout,
die mij vroeg de
commandant te zeggen onmiddellijk
het terrein te ontruimen.

De militairen (er kwamen
er nog meer) verlieten het
terrein dwars door het prikkeldraad
en het korenveld van
boer Bosgoed, alles vertrappend.
's Nachts waren ze
weer present, we hebben geen
oog dichtgedaan. Het jaar
daarop hielden ze weer een
oefening, het leek wel of de
hel losgebarsten was, granaatwerpers,
mitrailleurs,
echt iets voor je vakantie.

We gaan naar dit kamphuis
omdat het aan grote gezinnen
zeer goedkoop verhuurd
wordt. Maar als we dit jaar
weer zo'n last van die militairen
hebben, dan is dit het
laatste jaar.

N.J. Smeets

**Jan Sonjéstraat** 30a

Rotterdam.
