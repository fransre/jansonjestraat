---
toegevoegd: 2023-01-10
gepubliceerd: 1993-01-25
decennium: 1990-1999
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/1993-01-25-de-middellander-origineel.jpg"
onderwerpen: [vegen]
kop: "Veegaktie Jan Sonjéstraat"
---
# Veegaktie **Jan Sonjéstraat**

door Anne Ubbels

Vrijdag 27 november *1992* kreeg de **Jan Sonjéstraat** weer
eens een flinke veegbeurt.
Via de ROTEB hebben bewoners blikken, vegers, bezems en rolcontainers geregeld
om hun straat weer een schoon aanzien te geven. Van alles werd er opgeveegd tot
en met en doorgescheurd rijbewijs toe.
Dat vele handen licht werk maken bleek toen we na ruim een half uur al klaar
waren. Ook de winkel op de hoek en het café in de straat hielpen mee door de
inwendige mens te verzorgen: na afloop werd een kop koffie aangeboden.
