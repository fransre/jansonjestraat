---
toegevoegd: 2024-07-28
gepubliceerd: 1941-11-03
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002809:mpeg21:a0021"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [3]
achternamen: [verheij]
kop: "Jan Sonjéstraat 3B"
---
# **Jan Sonjéstraat** 3B

boven
zit-slaapkamer met
prima pension, terstond
J. Verheij, bovenhuis.
