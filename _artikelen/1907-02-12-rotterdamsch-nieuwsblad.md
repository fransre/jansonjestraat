---
toegevoegd: 2024-08-14
gepubliceerd: 1907-02-12
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196599:mpeg21:a0070"
onderwerpen: [werknemer, fiets]
huisnummers: [42, 43, 44]
kop: "Framevijlers en een lakker"
---
# Framevijlers en een lakker

gevraagd, door Rott*erdamsche* Rijwielenfabriek Joh. Pieterse & Co*mpagnon*
**Jan Sonjéstraat** 42-43-44.

12326 10
