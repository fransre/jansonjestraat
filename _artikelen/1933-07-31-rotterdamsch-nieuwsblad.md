---
toegevoegd: 2024-08-04
gepubliceerd: 1933-07-31
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164666034:mpeg21:a00066"
onderwerpen: [gevraagd]
huisnummers: [15]
achternamen: [van-oosten]
kop: "Vriendin."
---
# Vriendin.

Net Meisje, 25 j*aar* P*rotestantse* G*ezindte*,
zoekt leuke Vriendin
voor gezellige omgang,
net gezin. **Jan Sonjéstraat** 15,
B. v*an* Oosten.
