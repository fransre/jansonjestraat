---
toegevoegd: 2024-08-04
gepubliceerd: 1934-03-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164670007:mpeg21:a00027"
onderwerpen: [woonruimte, tehuur]
kop: "Bovenhuis."
---
# Bovenhuis.

Tegen 1 april *1934*
te huur:

pracht Bovenhuis, Middellandstraat 100
hoek **Jan Sonjéstraat**. Te zien: Donderdag *8 maart 1934*
en Dinsdag *13 maart 1934* 2-4 uur. Inl*ichtingen*: Studio,
Zuid blaak 2, Tel*efoon* 12961.

15748 6
