---
toegevoegd: 2023-08-01
gepubliceerd: 1929-05-13
decennium: 1920-1929
bron: Overijsselsch Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB23:003553090:mpeg21:a00068"
onderwerpen: [diefstal]
kop: "Twee beruchte inbrekers op heeterdaad betrapt."
---
# Twee beruchte inbrekers op heeterdaad betrapt.

Zaterdagnacht *11 mei 1929* hoorde een nachtwaker
die op zijn ronde was, in de pettenfabriek
van de firma W*olf &* N*orden* aan de Eerste
Middellandstraat te Rotterdam glasgerinkel.
Hij waarschuwde de politie, welke
terstond uitgebreide maatregelen nam.
Zoo snel mogelijk werd een groot aantal
agenten naar de Middellandstraat gezonden
en het geheele blok huizen tusschen
Schermlaan en Middellandstraat, in welk
blok de fabriek ligt, werd zorgvuldig afgezet.
Eerst daarna is de politie het pand
binnengegaan. In het kantoor, dat gelijkvloers
ligt werd ontdekt, dat de brandkast
werd aangeboord.

Een ruit vlak naast de brandkast was
stuk en blijkbaar hadden de inbrekers
deze ruit bij hun werk per ongeluk gebroken.
De daders waren evenwel onvindbaar.
Nadat het kantoor doorzocht
was, is de politie naar boven gegaan,
maar al vond men daar ook sporen, die
er op wezen, dat er inbrekers waren geweest,
de gezochten bleken ook daar niet
meer te zijn. Ten slotte is de politie op het
dak geklommen. Het openstaan van een
dakvenster had trouwens al het vermoeden
gewekt, dat de dieven langs dien weg
waren gevlucht. Toen men nauwelijks op
het dak was, zag men op eenigen afstand
twee mannen zich snel verwijderen over
het dak van een pand aan de **Jan Sonjéstraat**.
Kort daarna zagen de in die straat
op post staande agenten twee mannen
uit een huis komen. Zij hielden het tweetal
aan. Het bleken de gebroeders K. te
zijn, n*ame*l*ijk* de 32-jarige J.J. K. en de 37-jarige
G. K., twee oude bekenden van de
politie.

Eerstgenoemde is nog maar sedert kort
uit de gevangenis ontslagen. Bij hun
vlucht zijn de gebroeders K. dwars door
een gazen hor dat in een raam stond geloopen.
Daarna zijn zij door een pand naar
beneden gegaan, waarna zij op straat, gelijk
is gemeld, zijn gearresteerd. Bij huiszoeking
is een compleet stel inbrekerswerktuigen
gevonden. Ook in de fabriek
zijn dergelijke instrumenten aangetroffen.
