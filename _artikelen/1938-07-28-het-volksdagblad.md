---
toegevoegd: 2024-08-01
gepubliceerd: 1938-07-28
decennium: 1930-1939
bron: Het volksdagblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010474173:mpeg21:a0087"
onderwerpen: [ongeval, motor]
kop: "Door motorcarrier aangereden."
---
# Door motorcarrier aangereden.

De 36-jarige J. Rademaker, wonende te
Nesserdijk, werd aldaar door een motorcarrier
aangereden en van de dijk geslingerd.
Hij kwam er gelukkig met enige lichte
verwondingen af. De carrier reed enige
meters verder tegen een lantaarnpaal aan,
waardoor beide beschadigd werden. De bestuurder
van de carrier, de 26-jarige A. W.
uit de **Jan Sonjéstraat**, kwam er ongedeerd
van af.
