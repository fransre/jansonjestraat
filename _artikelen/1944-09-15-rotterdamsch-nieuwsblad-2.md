---
toegevoegd: 2024-07-26
gepubliceerd: 1944-09-15
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003593:mpeg21:a0022"
onderwerpen: [familiebericht]
huisnummers: [38]
achternamen: [vis]
kop: "A. Vis"
---
Heden overleed plotseling
door een noodlottig ongeval
in den leeftijd van 45 j*aa*r
mijn geliefde Echtgenoot,
onze zorgzame Vader, Zoon,
Behuwdzoon, Broeder en
Zwager,

# A. Vis

Uit aller naam:

R. Vis-Janknegt.

Rotterdam, 14 Sept*ember* 1944.

**Jan Sonjéstraat** 38a

De begrafenis zal plaats
hebben Zaterdag *16 september 1944* a*an*s*taande*. Vertrek
van het Ziekenhuis Coolsingel
te 2 uur. Aankomst Crooswijk
ong*eveer* 2.30 uur
