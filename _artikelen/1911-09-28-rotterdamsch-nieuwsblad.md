---
toegevoegd: 2024-08-14
gepubliceerd: 1911-09-28
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010200175:mpeg21:a0066"
onderwerpen: [reclame, schilder]
huisnummers: [23]
achternamen: [koomans]
kop: "Schilderschool."
---
# Schilderschool.

16 October *1911* a*an*s*taande* aanvang van den dag- en avondcursus in het nabootsen van hout- en marmer-decoratie, enz*ovoort*.
Stalen van den vorigen Cursus steeds te bezichtigen. Billijke conditiën.
Vraagt inlichtingen,

Aanbevelend,

J. Koomans, M*eeste*r Hout- en Marmerschilder,

voorheen Zomerhofstraat 80,

thans **Jan Sonjéstraat** 23b.

41022 22
