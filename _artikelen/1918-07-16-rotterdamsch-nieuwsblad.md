---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-16
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297062:mpeg21:a0052"
onderwerpen: [fiets, tekoop]
huisnummers: [3]
achternamen: [ritmeijer]
kop: "Jongensfiets"
---
# Jongensfiets

te koop voor Jongen
10 tot 14 jaar, nieuwe
Binnen- en beste Buitenbanden,
prijs billijk
Ritmeijer, **Jan Sonjéstraat** 3a.
