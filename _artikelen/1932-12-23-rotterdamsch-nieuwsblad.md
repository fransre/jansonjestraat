---
toegevoegd: 2024-08-05
gepubliceerd: 1932-12-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164662061:mpeg21:a00093"
onderwerpen: [bedrijfsinventaris, auto, tekoop]
huisnummers: [2]
achternamen: [smit]
kop: "Blits-carrier"
---
Te koop een

# Blits-carrier

met zeer goeden Motor ƒ45.—.

J. Smit, **Jan Sonjéstraat** 2, Tel*efoon* 31600.

455586
