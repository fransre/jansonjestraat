---
toegevoegd: 2024-08-08
gepubliceerd: 1926-02-12
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495270:mpeg21:a0216"
onderwerpen: [familiebericht]
huisnummers: [19]
achternamen: [de-neef]
kop: "Dirk Wilhelm de Neef,"
---
In plaats van kaarten.

Heden overleed tot onze diepe
droefheid, in zijn Heer en Heiland,
mijn geliefde Echtgenoot, onze
Zoon, Behuwdzoon, Broeder, Behuwdbroeder
en Oom, de Heer

# Dirk Wilhelm de Neef,

In den leeftijd van ruim 30 jaar.

Uit aller naam.

Wed*uwe* G. de Neef,
geb*oren* Buisman,

Rotterdam, 11 Februari 1926.

**Jan Sonjéstraat** 19b.

De teraardebestelling zal plaats
hebben a*an*s*taande* Maandag *15 februari 1926* op de Algemeene
Begraafplaats Crooswijk,
Vertrek van het Ziekenhuis Bergweg
te 2 uur.

23
