---
toegevoegd: 2024-07-26
gepubliceerd: 1946-10-09
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955043:mpeg21:a0031"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [wachsman]
kop: "De Stichting „CABA”"
---
# De Stichting „CABA”

(Commissie van Advies en Beheer
voor Afwezigen)

Verzoekt hen, die iets te vorderen hebben van of
verschuldigd zijn aan na te noemen afwezigen,
dan wel gelden, goederen of andere waarden voor
of van hem/haar in bezit of bewaring hebben of
met hen in eenigerlei rechtsbetrekking staan,
hiervan binnen 8 dagen schriftelijk opgave of
mededeling te doen aan onderstaand adres.
Achterhouden van goederen is strafbaar.

~~André van Dantzig en Rosa van Dantzig-Sanders, Oude Binnenweg 75~~

~~Eduard van Dantzig en Sara van Dantzig-Sanders, Beukelsweg 70;~~

~~Levie Ensel, Bierslootsteeg 8, Vlaardingen;~~

~~Samuel Fekete en Adele Fekete-Farkas, Teilingerstraat 35;~~

~~N.A. Frank en M. Frank-Ensel, Zwaanshals 200;~~

~~Joachim van Gelderen, Henegouwerlaan 396;~~

~~Harry Hachenberg, Warmoezierstraat, daarna Boreelstraat;~~

~~Eliazar Hamburger en Elisabeth Hamburger v*an* d*er* Berg, Schieweg 60A;~~

~~Mevrouw B. van Stralen-Van Straten, tot 1940 gewoond hebbende Maaskade 109B;~~

~~David Tenenbaum, Jan Porcellisstraat 8;~~

Alter Wachsman en Temera Wachsman-Schaufel, **Jan Sonjéstraat** 30;

~~B. Weinberg, Vredehofweg 42;~~

~~Wulf Wurszup en Scheine Dina Salmanowitz, Vlaggemanstr*aat* 39B;~~

~~J. v*an* d*er* Heim en Roselle v*an* d*er* Heim-De Jongh, Mathenesserweg 21~~

~~Wed*uwe* Betty van Kleef-Ensel, Zwaanshals 292B;~~

~~Abraham van Koppelen, Agniesestraat 26;~~

~~Alexander van Koppelen, Noordsingel 24;~~

~~M. Koster, Mathenesserlaan 229;~~

~~Marcus Lagrand, Schieweg 11A;~~

~~Mevrouw R. Polak-Koster, Burg. Meineszlaan 25;~~

~~Jacques Poppers en E. Poppers-Cohen, Burg. Meineszlaan 76B;~~

~~Henri Marc Ronkel, Burg. Meineszlaan 121;~~

~~Suzanna Roodenburg-Sanders, Jan Porcellisstraat 8;~~

~~Abraham v*an* d*er* Sluijs en Chr. v*an* d*er* Sluijs-Ensel, Zwaanshals 200;~~

De secretaris

Rotterdam, 7 October 1946.

Graaf Florisstraat 36.
