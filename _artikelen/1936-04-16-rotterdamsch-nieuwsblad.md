---
toegevoegd: 2024-08-03
gepubliceerd: 1936-04-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164682049:mpeg21:a00049"
onderwerpen: [woonruimte, tehuur]
kop: "Vrij bovenhuis"
---
# Vrij bovenhuis

te huur aangeboden, **Jan Sonjéstraat**,
5 Kamers, Keuken, Zolder
m.w.l., geheel gerestaureerd.
Huurprijs billijk. Te bevr*agen* Provenierssingel 79a.

18911 7
