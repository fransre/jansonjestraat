---
toegevoegd: 2024-08-04
gepubliceerd: 1933-10-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164667049:mpeg21:a00154"
onderwerpen: [familiebericht]
huisnummers: [20]
achternamen: [balck]
kop: "2e etage,"
---
Volstrekt eenige Kennisgeving.

Heden overleed door een noodlottig
ongeval ons innig geliefd Zoontje,
Broertje, Kleinzoontje en Neefje,

# Arie Adolf Balck,

op den leeftijd van 8 jaren.

Uit aller naam;

A.P. Balck.

M.L. Balck-Groen.

M.L. Balck.

J.A. Balck.

Margrietje Balck.

Rotterdam, 10 October 1933.

**Jan Sonjéstraat** 20a.

De teraardebestelling zal plaats
hebben Zaterdag *14 oktober 1933* a*an*s*taande* vanuit het Gem*eentelijke* Ziekenhuis Coolsingel,
te c*irc*a één uur
op de Begraafplaats Crooswijk.
