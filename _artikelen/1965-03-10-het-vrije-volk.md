---
toegevoegd: 2024-07-23
gepubliceerd: 1965-03-10
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955407:mpeg21:a0067"
onderwerpen: [auto, tekoop]
huisnummers: [8]
achternamen: [wight]
kop: "Fiat 1100 *19*57,"
---
Wegens niet halen van rijbewijs,

# Fiat 1100 *19*57,

ƒ625. T. Wight,
**Jan Sonjéstraat** 8a, tel*efoon* 253697.
