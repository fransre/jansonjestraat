---
toegevoegd: 2024-07-26
gepubliceerd: 1947-07-16
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010949734:mpeg21:a0046"
onderwerpen: [bedrijfsinformatie]
huisnummers: [8]
achternamen: [hijnen]
kop: "Johannes Wilhelmus Jacobus Hijnen"
---
De Tuchtrechter voor de Prijzen te Rotterdam, brengt
ter algemene kennis, dat hij

# Johannes Wilhelmus Jacobus Hijnen

van beroep: banketbakker, wonende te Rotterdam,
**Jan Sonjéstraat** 8, zaakdoende te Rotterdam,
Gouvernestraat 84

bij vonnis van 27 Juni 1947 ter zake van het tegen te hoge
prijs verkopen van spritskoeken heeft veroordeeld tot betaling
van een geldboete groot ƒ750.—, met bevel tot
openbaarmaking van dit vonnis in de pers op kosten
van de veroordeelde.
