---
toegevoegd: 2024-08-03
gepubliceerd: 1937-03-01
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164688001:mpeg21:a00249"
onderwerpen: [ongeval, fiets]
achternamen: [hulsker]
kop: "Ongevallen"
---
# Ongevallen

Gisteren *28 februari 1937* is de 34-jarige B.J. Hulsker, wonende
**Jan Sonjéstraat**, bij het oefenen op de wielerbaan
in de Nenijtohal gevallen. Met een
hoofdwond en een hersenschudding werd hij
in het Ziekenhuis aan den Bergweg opgenomen.
