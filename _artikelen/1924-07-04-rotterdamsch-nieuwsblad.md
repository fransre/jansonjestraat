---
toegevoegd: 2024-08-11
gepubliceerd: 1924-07-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495089:mpeg21:a0141"
onderwerpen: [verloren]
huisnummers: [29]
achternamen: [hoogmolen]
kop: "„Kleintje” met sigarettenkoker."
---
# „Kleintje” met sigarettenkoker.

Mevr*ouw* V*an* d*e* Ven, 's Gravendijkwal 65, die
een zilveren sigarettenkoker had verloren,
plaatste een „Kleintje” in het Nieuwsblad.
Het onmiddellijk gevolg was, dat de
sigarettenkoker haar werd terugbezorgd
door den eerlijken vinder, den heer Hoogmolen,
**Jan Sonjéstraat** 29, voor wien de
Nieuwsblad-medaille voor betoonde
eerlijkheid is ter beschikking gesteld.

Eere wien eere toekomt.
