---
toegevoegd: 2024-08-06
gepubliceerd: 1930-04-26
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164646063:mpeg21:a00095"
onderwerpen: [familiebericht]
huisnummers: [13]
achternamen: [barents]
kop: "Albartus Barents en Maria van der Vegt"
---
1880-1930

12 Mei *1930* hopen onze geliefde
Ouders:

# Albartus Barents en Maria v*an* d*er* Vegt

hunne 50-jarige Echtvereeniging
te herdenken.

Dat zij nog lang voor elkaar
en voor ons gespaard mogen
blijven is onze oprechte wensch.

Hunne dankbare Kinderen.

Thuis: 28 April *1930* tot 12 Mei *1930*,

**Jan Sonjéstraat** 13.

21
