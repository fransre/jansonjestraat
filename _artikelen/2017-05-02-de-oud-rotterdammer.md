---
toegevoegd: 2021-12-28
gepubliceerd: 2017-05-02
decennium: 2010-2019
bron: De Oud-Rotterdammer
externelink: "https://www.deoudrotterdammer.nl/archief/dor/2017/week18_2017/files/assets/basic-html/index.html#21"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [wachsman, kofflard]
kop: "Herdenking"
---
# Herdenking

Wij woonden in de **Jan Sonjéstraat** 30A.
Het bovenhuis. Ik ben geboren in 1934.
Beneden ons, op 30B, woonde
een joodse familie. De familie Wachsman.
Een gezin met twee kinderen
van mijn leeftijd. Benno, geboren op
23 augustus 1933 en Itjoe, geboren op
16 september 1935. Benno was mijn
vriendje. We speelden samen en soms
mocht ik mee eten. Grote matzes. We
konden met de hele familie bij hen
in de kelder schuilen als de bommen
vielen. Als we samen gingen lezen in
zijn ‘Bijbel’, deed hij zijn keppeltje
op. Ik knoopte dan een zakdoek op
mijn hoofd. We speelden veel buiten,
met veel vriendjes, tot 7 uur. Dan
kwam zijn moeder naar buiten, klapte
in haar handen, en riep: „Benno, Itjoe,
binnen komen, ies kenoeg for
fandaag.” Tot die ochtend in 1943,
's morgens om 5 uur. Toen zijn ze
weggehaald. Wat heb ik gehuild. Itjoe,
de jongste, was een blond jongetje.
Mijn ouders boden aan hem onder
te brengen bij familie in Limburg.
Ze deden dat niet, omdat de moffen
hadden gezegd, dat de gezinnen bij
elkaar moesten blijven. Dat was beter
voor de eindbestemming. Samen zijn
en werken. De eindbestemming was
Sobibor. Ze zijn daar omgebracht
30 april 1943. We blijven aan hen
denken.

Frans Kofflard

franskofflard@gmail.com

*(Zie <https://wachsman.nl> voor meer informatie over de familie Wachsman)*
