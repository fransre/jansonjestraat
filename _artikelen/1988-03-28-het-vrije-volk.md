---
toegevoegd: 2021-04-29
gepubliceerd: 1988-03-28
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962741:mpeg21:a0128"
onderwerpen: [bellebom]
kop: "Is operatie ‘Bellebom’ een blauwdruk voor 'n nucleaire ramp?"
---
# Is operatie ‘Bellebom’ een blauwdruk voor 'n nucleaire ramp?

Een zucht van verlichting ging
er gisteren *27 maart 1988* door de stad toen die twee
dappere mannen van de Explosieven Opruimingsdienst (EOD),
de adjudant P. Schoots
en sergeant J.C. Linschoten,
de ‘Bellebom’ op professionele
wijze hadden gedemonteerd. Maar, zo
vragen velen zich met mij af, was al
die drukte rondom die bom nu wel nodig?
Die evacuatie van 7.000 wijkbewoners
en het verplichte ‘huisarrest’
van 14.000 buurtbewoners?

C.F. van 't Hof uit de Rotterdamse
**Jan Sonjéstraat** schrijft ons
hierover: „Als de demontering van de
‘Bellebom’ niet direct slaagt, injecteren
de mannen van de EOD de bom
met stollingsvloeistof, waardoor het risico
‘nihil’ is, zegt het hoofd van het
bureau Rampenbestrijding M.G. de
Ruiter. Waarom kan dat niet meteen
worden gedaan zonder al die poespas
van evacueren en binnenblijven? Dat
lijkt mij heel wat goedkoper.

Het is nu wel duidelijk dat de
gemeente Rotterdam al vanaf het begin
bezig is met deze evacuatie van
duizenden mensen en dit wel een leuke
oefening vindt, die het wel leuk
doet in de rest van Nederland. Alleen
jammer dat de bewoners van de **Jan Sonjéstraat**,
die nu al vanaf oktober *1987*
hun woongenot ernstig bedorven zien,
met een bosje bloemen worden afgescheept.”
Aldus C.F. van 't Hof.

Toen ik van de week het verhaal
over de operatie ‘evacuatie-Bellebom’
vertelde aan mijn Griekse
vriend, de schrijver Yannis Vadzias,
die in verband met Film International
altijd enkele maanden naar Rotterdam
komt, was zijn eerste spontane reactie:
„Het is onmogelijk dat voor een
dergelijke bom zoveel mensen hun
huizen moeten verlaten en nog eens
duizenden thuis moeten blijven. Daar
zit veel meer achter. Ik denk dat ze
een blauwdruk nodig hadden voor een
evacuatieplan bij een nucleaire ramp,
bijvoorbeeld bij een van jullie kerncentrales
in Borssele of Petten.” Vadzias
is niet alleen schrijver, maar ook
een man die, in dienst van Buitenlandse Zaken
van de Griekse regering, zowat
de hele aarde heeft afgereisd en
dus echt wel weet wat er in de wereld
te koop is.

Interessant is ook het verhaal
van H. Ens, ingenieur te Den Haag. In
de NRC van afgelopen maandag *22 maart 1988*
schreef hij een ingezonden stuk onder
de kop: ‘Verkeer minder veilig dan
een blindganger.’ In dit verhaal vraagt
Ens zich af: „Er zijn veel verkeerssituaties
waarin een voetganger die de
straat oversteekt een aanmerkelijk
groter risico loopt. De vraag is hoe
men kan motiveren dat er maandenlang
wordt gewerkt aan een plan tot
evacuatie van 7.000 mensen, en dat
daarvoor 2.000 ambtenaren worden
ingezet.”

Toen ik gisteren *27 maart 1988* ir. Ens uiteindelijk
na ‘2× bellen’ aan de lijn kreeg
vroeg ik aan hem of Nederland verlegen
zat om ervaringen met een evacuatieplan
na een nucleaire ramp.

Ir. Ens: „Laat iedereen maar
zelf zijn conclusies trekken. Maar ik
vind wel dat je de burgers de ware reden
moet vertellen van zo'n groots
opgezet oefenschema. Dat hebben ze
mijns inziens niet gedaan. Vanochtend *28 maart 1988*
discusseerde ik daarover via de VARA-radio
met de chef van de EOD. Die
zei dat ze met de bomoperatie het risico
tot ‘nul’ wilden beperken. Maar
hij vergat daarbij twee dingen: het risico
van de evacuatie van mensen in
bussen, die mogelijk op een kruispunt
tegen een benzinetankwagen konden
aanrijden.

Ik vind het een heel slechte besluitvorming
van het gemeentebestuur
van Rotterdam dat ze akkoord zijn gegaan
met het terugbrengen van het risico
tot ‘nul’. Want een veiligheidsrisico
is nooit terug te brengen tot nul.

Als ik in de buurt van de Bellevoysstraat
had gewoond, behalve dan
in de paar panden waar de bom vlakbij
lag, had ik geweigerd mijn huis uit
te gaan. Ik had mij beroepen op onze
grondwettelijke vrijheid en daar had
de rechter eerst uitspraak over moeten
doen.

De burgemeester had ook niet
de bevoegdheid te eisen dat de mensen
hun huis uit moeten en de politie
niet het recht deuren van bewoners te
forceren. Alleen een officier van Justitie
kan zo'n bevel uitvaardigen. Overigens
vraag ik mij af wat het Rotterdamse
college van B. en W. gaat besluiten
wanneer er nog 26 van dergelijke
bommen boven water komen.
Krijgen we dan nog 26 van dergelijke
absurde geldverslinde operaties?” Aldus
ir. H. Ens.
