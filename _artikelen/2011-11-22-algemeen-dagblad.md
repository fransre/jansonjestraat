---
toegevoegd: 2023-01-10
gepubliceerd: 2011-11-22
decennium: 2010-2019
bron: Algemeen Dagblad
externelink: "https://www.jansonjestraat.nl/originelen/2011-11-22-algemeen-dagblad-origineel.jpg"
onderwerpen: [gregory]
huisnummers: [17]
achternamen: [halman]
kop: "Tophonkballer gedood door eigen broer"
---
# Tophonkballer gedood door eigen broer

Ruzie over muziek leidt tot fatale steekpartij

Rotterdam — De Haarlemse tophonkballer Gregory Halman (24) is gisterochtend *21 november 2011*
vermoord in een Rotterdamse woning. Hij werd neergestoken en overleed ter
plekke. De polite arresteerde zijn twee jaar jongere broer Jason op verdenking
van de steekpartij. Aan het fatale incident ging een ruzie vooraf over te harde
muziek.

Sander Sonnemans

In het Nederlandse honkbalcircuit
is geschokt gereageerd, nieuwzenders
over de hele wereld meldden de
dood van Halman.
De te harde muziek in de woning
van Jason aan de **Jan Sonjéstraat** in
Rotterdam-West was gisterochtend *21 november 2011*
rond half zes de aanleiding van een
ruzie tussen de twee broers.
Gregory, die in de bovenwoning
bij een vriendin verbleef, was het
kabaal zat en was naar beneden gegaan
om zijn broer tot de orde te
roepen. Jason pikte het commentaar
kennlijk niet en drong korte tijd
later de woning van de vrouw binnen,
maakte weer ruzie met zijn oudere
broer en stak hem daarbij neer.
Gregory's vriendin belde het
alarmnummer, maar de hulp kwam
te laat. Vier ambulancebroeders en
twee politieagenten probeerden de
sportheld te reanimeren, maar de
inspanningen waren tevergeefs.
Jason werd in de woning gearresteerd,
de vrouw moest mee naar het
bureau om een getuigenverklaring
af te leggen.
In de woning is gisteren *21 november 2011* de hele
dag minitieus sporenonderzoek gedaan
door de forensische opsporing.
In de loop van de middag werd het
ontzielde lichaam van Halman uit
de woning gehaald. Een overbuurvrouw,
die alleen anoniem wil reageren,
is getuige geweest van de gebeurtenis.
„Ik was al wakker en hoorde rond
vijf uur buiten plotseling op een
deur bonken,” zegt de vrouw die nog
zichtbaar geschrokken is. „Hij stond
maar op de voordeur te slaan. ‘Doe
open, doe open, maak de deur open,
laat me erin’, was wat hij steeds
maar riep. Ik wilde net tegen hem
gaan zeggen dat hij ook bij mij
mocht slapen, maar kwam aan de
overkant iemand naar beneden en
de deur ging open, waarna Jason
naar binnen ging. Toen was alles
weer rustig. Een half uurtje later
klonk buiten weer kabaal. Jason liep
buiten te schreeuwen. ‘Ik mis mijn
broer! Ik ben alleen!’, brulde hij.
Toen kwam ineens de politie. Jason
werd tegen de grond gewerkt, kreeg
klappen en handboeien om.”

## Rustige jongen

„Normaal een hele rustige jongen,
die ook altijd netjes gedag zei als je
hem tegenkwam,” reageert een
buurman, die evenmin met zijn
naam in de krant wil. „Je zag Jason
vaak in zijn raamkozijn zitten en dan
zwaaide hij naar je als hij je zag. Dat
zijn broer Gregory een groot honkballer
was en in Amerika dik verdiende,
viel niet aan hem op. Hij
had geen kapsones en pronkte niet
met mooie auto's of zo.” Halman,
een rechtshandige slagman die
sinds 2006 in 22 duels voor Oranje
uitkwam, genoot internationale bekendheid.
Als zoon van ex-international
Eddy Halman debuteerde hij
op zijn 16de als achtervanger bij de
Haarlemse hoofdklasser Kinheim.
In 2004 verhuisde hij naar Amerika,
waar hij een profcontract tekende
bij de Seattle Mariners. Gregory verbleef
enkele weken in Nederland
vanwege de European Big League Tour.
Ook in de VS is geschokt gereageerd
op de dood van de honkballer.
„Dit doen ongelooflijk pijn,” zegt
Halmans zaakwaarnemer Greg Nicotera.
„Hopelijk vinden we de
kracht te leren leven met het verlies
van zo'n enthousiaste, goedaardige,
genereuze en betrouwbare jongen.
We bidden voor zijn nabestaanden
en geliefden.”
Seattle Mariners-voorzitter Howard Lincoln
en hoofdcoach Jack Zduriencik
hebben hem zien uitgroeien
‘tot een gepassioneerde en
getalenteerde honkballer’.
„Hij had een aanstekelijke lach
waarmee hij je tegemoet kwam in
het clubhuis. En hij was een geweldige
teamgenoot. Onze gedachten
en gebeden gaan uit naar zijn familie.”

*Onderschrift bij de foto*
De politie onderzoekt de woning waar tophonkballer Gregory Halman (foto rechts) werd doodgestoken.
Zijn broer Jason (foto links) wordt ervan verdacht Gregory te hebben gedood. Foto Rob Jelsma
