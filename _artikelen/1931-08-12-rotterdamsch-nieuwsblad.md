---
toegevoegd: 2024-08-05
gepubliceerd: 1931-08-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164654049:mpeg21:a00043"
onderwerpen: [huisraad, tekoop]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "Te koop:"
---
# Te koop:

Strijkkachel, merk Bins
en Peerless Glansmachine,
wegens overcompleet,
in goeden staat.
L. v*an* d*en* Bos, **Jan Sonjéstraat** 36.
