---
toegevoegd: 2024-08-14
gepubliceerd: 1909-06-30
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197466:mpeg21:a0079"
onderwerpen: [huispersoneel]
huisnummers: [26]
kop: "Dienstmeisje,"
---
Gevraagd een net

# Dienstmeisje,

P*rotestantse* G*ezindte*, pl*us*m*inus* 14 j*aar*. **Jan Sonjéstraat** 26a,
's avonds na
6 uur.
