---
toegevoegd: 2024-08-13
gepubliceerd: 1917-05-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296531:mpeg21:a0067"
onderwerpen: [werknemer, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "schilders"
---
Bekwame

# schilders

gevraagd. E.J. Weier
**Jan Sonjéstraat** 38. Telefoon 10084.
