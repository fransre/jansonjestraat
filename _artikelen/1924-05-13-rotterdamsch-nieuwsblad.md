---
toegevoegd: 2024-08-11
gepubliceerd: 1924-05-13
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494793:mpeg21:a0066"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [38]
kop: "Foto,"
---
# Foto,

daglicht-, gaslicht-,
Postkaarten, Akronpapier,
Hauffplaten, enz*ovoort*
aangeboden. Ook aan
Amateurs, moet weg.
**Jan Sonjéstraat** 38a.
