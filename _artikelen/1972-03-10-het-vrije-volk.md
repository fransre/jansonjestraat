---
toegevoegd: 2021-04-29
gepubliceerd: 1972-03-10
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957773:mpeg21:a0263"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Filmclub gesloten: „rendez-vous”"
---
# Filmclub gesloten: „rendez-vous”

Rotterdam — Op last van
burgemeester en wethouders is
gisteren *9 maart 1972* de filmclub Angelique
aan de **Jan Sonjéstraat** gesloten.
Er werd volgens de politie rendez-vous
gegeven. Eigenaar van
de club is A.L.
