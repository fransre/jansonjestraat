---
toegevoegd: 2021-05-04
gepubliceerd: 1946-11-05
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955066:mpeg21:a0045"
onderwerpen: [cafe, tehuur]
huisnummers: [15]
achternamen: [van-klaveren]
kop: "Zaal"
---
# Zaal

voor repetitie, vergadering,
clubavond, feest of bruiloft
met tapvergunning.

Gratis alle soorten glaswerk,
koffie- en theekoppen

C. v*an* Klaveren, **Jan Sonjéstraat** 15,
telefoon 30682.
