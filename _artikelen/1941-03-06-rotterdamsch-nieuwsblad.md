---
toegevoegd: 2024-07-29
gepubliceerd: 1941-03-06
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002678:mpeg21:a0063"
onderwerpen: [huisraad, tekoop]
huisnummers: [3]
kop: "Te koop"
---
# Te koop

massief eiken theekast,
Queen Anne model,
ƒ17.50, groot vloerkleed,
pluche. 3½-4½,
ƒ10 Plaatsgebrek **Jan Sonjéstraat** 3B,
bovenh*uis*.
