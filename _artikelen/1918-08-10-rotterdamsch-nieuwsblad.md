---
toegevoegd: 2024-08-13
gepubliceerd: 1918-08-10
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297084:mpeg21:a0051"
onderwerpen: [werknemer]
huisnummers: [44]
kop: "Reiziger gevraagd"
---
# Reiziger gevr*aagd*

goed ingevoerd bij H*eren*
Boekhandelaren, en bekendheid
van Cartonnagewerk
is vereischte.
Adres **Jan Sonjéstraat** 44a.

41202 8
