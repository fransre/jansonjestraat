---
toegevoegd: 2024-02-21
gepubliceerd: 1996-12-09
decennium: 1990-1999
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/1996-12-09-de-middellander-origineel.jpg"
onderwerpen: [bewoner]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Oudste inwoner Jan Sonjéstraat ingenomen met prijs"
---
# Oudste inwoner **Jan Sonjéstraat** ingenomen met prijs

De **Jan Sonjéstraat** is in de prijzen gevallen bij de uitreiking
van de Opzoomerprijs voor Delfshaven. Als genomineerde
wonnen de bewoners van de **Jan Sonjéstraat**
duizend gulden door maandelijks met z'n allen de
straat schoon te maken, waardoor iedereen zich verantwoordelijk
voor de straat is gaan voelen.

Daarnaast hebben zij de straat
opgevrolijkt met plantenbakken. Dat
deze activiteiten de saamhorigheid
in de straat bevorderen, kan de
oudste bewoner, meneer Van Hooijdonk,
beamen. Hij woont al 67
jaar in de straat. Boven woonde hij
met zijn vrouw en drie dochters. In
de kelder had hij een stoffeerderswerkplaats
met zeven man personeel.
Later verhuisde de zaak naar
de Engelsestraat. In de loop der
jaren heeft Van Hooijdonk allerlei
veranderingen meegemaakt. Als
schooljongen — hij ging naar school
in de Oostervantstraat — ging hij met
vriendjes over sloten springen die
grensden aan boerderijen aan de
tegenwoordige Beukelsdijk.

„Dan werden we nog weleens achterna
gezeten door de boer,” herinnert
hij zich. „Het was allemaal land.
Op de Middellandstraat stonden
geen huizen. Dat was de polder. Een
ruimte had je! Op het Henegouwerplein
gooiden ze al het as op het
plein en dan had je een hele grote
berg, waar de mensen hun kooltjes
uit zaten te zoeken.”

De Tweede Wereldoorlog staat Van Hooijdonk
nog helder voor de
geest. Naast zijn huis, waar hij met
zijn vrouw en drie dochters woonde,
verrees een café dat druk werd
bezocht door Duitse soldaten. Hij
maakte mee hoe de huizen verduisterd
moesten worden en hoe
onschuldige mensen op de
Middellandstraat werden neergeschoten.
Voor zijn ogen werd het
Joodse Ziekenhuis aan de Claes de
Vrieselaan ontruimd.

„Bij de grote brand van het bombardement
heb ik als hulp in de straat
gestaan,” herinnert hij zich. „Dan
kwamen de mensen allemaal spullen
brengen. De mensen uit het ziekenhuis
werden natuurlijk verplaatst
allemaal, dus dan hadden ze dekens
nodig en slopen en kussens. Ik stond
dat op de hoek van de straat te verzamelen
en dat werd dan naderhand
weggehaald. Dan zag ik hoe de
gewonden en de zieken in auto's
werden weggevoerd. Je werd op
een afstand gehouden. De Duitsers
liepen met geweren in de aanslag. Ik
ben in dat ziekenhuis verschillende
keren op visite geweest, bij kennissen.
Jarenlang heb ik als kind zijnde
een Joods vriendje gehad.”

Sommige huizen uit de straat, die
rond de eeuwwisseling is gebouwd,
heeft Van Hooijdonk zien afbreken
en zien vervangen door nieuwe.
Ook de bevolking heeft hij zien veranderen.
De welgestelden heeft hij
naar de buitenwijken weg zien trekken,
om plaats te maken voor een
gemêleerde bevolking van verschillende
nationaliteiten.

In zijn herinnering Opzoomerden
bewoners van de straat al voordat
het woord was uitgevonden: „De
stoepjes en de bellen werden gepoetst.
Dat zie je tegenwoordig niet
meer,” constateert Van Hooijdonk,
die toegeeft dat het met de gezamenlijke
schoonmaakakties in de
straat wel weer de goede kant uit
gaat. Ondanks zijn hoge leeftijd
draagt hij hier nog steeds aan bij:
„Ik doe nog graag mee. Mijn eigen
straatje veeg ik ook altijd nog. Ik kan
niet tegen vuil. Misschien ben ik daar
een beetje te precies in,” zegt Van Hooijdonk,
die vindt dat de Roteb
ook wel iets vaker zou mogen vegen:
„Om de twee weken is te weinig.”
Toch zou hij het maandelijks vegen
met zijn medebewoners niet willen
missen: „We gaan goed met elkaar
om. Ik vind het gezellig.”

*Onderschrift bij de foto*
De H*ee*r van Hooijdonk zou het maandelijks vegen met de medebewoners
niet willen missen.

foto: Sjoerd Nicolaï
