---
toegevoegd: 2024-07-29
gepubliceerd: 1941-03-18
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002688:mpeg21:a0019"
onderwerpen: [werknemer, fiets]
huisnummers: [36]
achternamen: [wessels]
kop: "Flinke nette Jongen"
---
# Flinke nette Jongen

gevraagd als leerling
rijwielhersteller, gelegenheid
om vlug te bekwamen
als zoodanig.
E. Wessels. **Jan Sonjéstraat** 36.
