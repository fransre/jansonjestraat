---
toegevoegd: 2024-08-06
gepubliceerd: 1930-03-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002035:mpeg21:a0026"
onderwerpen: [bedrijfsruimte]
huisnummers: [38]
kop: "Zaal"
---
# Zaal

te huur voor Bruiloften
en Partijen met inbegrip
van Pianist ƒ35.
**Jan Sonjéstraat** 38. Telef*oon* 30654.
