---
toegevoegd: 2023-01-21
gepubliceerd: 1973-10-31
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://schiedam.courant.nu/issue/VV/1973-10-31/edition/0/page/2"
onderwerpen: [angelique, werknemer]
huisnummers: [33]
kop: "assistentes."
---
Gevraagd voor nieuwe club

# assistentes.

Intern mogelijk.
Alleen persoonlijk aanmelden
**Jan Sonjéstraat** 33a,
010-256188

R16
