---
toegevoegd: 2024-08-13
gepubliceerd: 1917-07-30
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296672:mpeg21:a0098"
onderwerpen: [werknemer]
huisnummers: [6]
achternamen: [de-jong]
kop: "Heren coiffeurs."
---
# H*eren* coiffeurs.

Er biedt zich aan
een Noodhulp, voor onbepaalden
tijd. Uitstekend
met het vak bekend,
v*an* g*oede* g*etuigen* v*oorzien*. Adres: P. de Jong,
**Jan Sonjéstraat** 6a.
