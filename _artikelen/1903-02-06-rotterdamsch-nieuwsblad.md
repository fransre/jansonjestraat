---
toegevoegd: 2023-01-21
gepubliceerd: 1903-02-06
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010178747:mpeg21:a0001"
onderwerpen: [diefstal]
kop: "Diefstalkroniek."
---
# Diefstalkroniek.

Van een in
aanbouw zijnd pand aan de **Jan Sonjéstraat**
is een looden pijp van 8 Meter ontvreemd.
