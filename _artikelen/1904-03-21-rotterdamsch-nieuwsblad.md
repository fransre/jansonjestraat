---
toegevoegd: 2021-04-29
gepubliceerd: 1904-03-21
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202209:mpeg21:a0071"
onderwerpen: [woonruimte, tehuur]
huisnummers: [40]
kop: "Zoo hier, zoo daar."
---
# Zoo hier, zoo daar.

Voor nette lieden met
klein gezin te huur: mooi
Bovenhuis, **Jan Sonjéstraat**
N*ummer* 40, voor ƒ19; idem
N*ummer* 18 en 20 ruime Benedenhuizen
voor ƒ16; ~~Pijnackerplein
N*ummer* 1 een 2de
Etage voor ƒ17, en Blommerdijkschelaan,
vrij Bovenhuis
voor ƒ18.~~ Adres:
Bergweg 24 en Middellandstraat
N*ummer* 51.
