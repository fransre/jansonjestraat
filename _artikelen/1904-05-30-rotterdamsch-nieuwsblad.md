---
toegevoegd: 2024-08-14
gepubliceerd: 1904-05-30
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202725:mpeg21:a0108"
onderwerpen: [woonruimte, tehuur]
huisnummers: [40]
kop: "Kamers,"
---
Te huur twee ongemeubileerde

# Kamers,

Voorkamer voor ƒ1.50,
Achterkamer voor ƒ1.00,
beide voorzien v*an* Slaapgelegenheid.
Adres: **Jan Sonjéstraat** 40.
