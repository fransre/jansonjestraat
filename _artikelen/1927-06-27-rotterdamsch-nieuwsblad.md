---
toegevoegd: 2024-08-07
gepubliceerd: 1927-06-27
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514024:mpeg21:a0061"
onderwerpen: [woonruimte, tehuur]
huisnummers: [41]
kop: "Te huur."
---
# Te huur.

vrij Bovenhuis. Huurprijs
ƒ30 per maand,
Bezichtigen en bevragen:
**Jan Sonjéstraat** 41a,
hoek Schermlaan.
