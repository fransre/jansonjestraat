---
toegevoegd: 2024-08-11
gepubliceerd: 1922-06-20
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494275:mpeg21:a0027"
onderwerpen: [woonruimte, tekoop]
huisnummers: [14]
kop: "Zelfbewoning."
---
# Zelfbewoning.

Te koop: het pand **Jan Sonjéstraat** N*ummer* 14, bij de Middellandstraat,
te R*otter*dam.

't Vrije Bovenhuis is direct te betrekken.

Te bezichtigen Woensdag *21 juni 1922* en Donderdag *22 juni 1922* van 2½-5 uur.

37196 18
