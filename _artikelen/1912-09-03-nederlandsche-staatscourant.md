---
toegevoegd: 2024-08-13
gepubliceerd: 1912-09-03
decennium: 1910-1919
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000177312:mpeg21:a0015"
onderwerpen: [faillissement]
huisnummers: [39]
achternamen: [assies]
kop: "Gerechtelijke aankondigingen."
---
# Gerechtelijke aankondigingen.

Bij vonnis van de arrondissements-rechtbank te Rotterdam,
dd. 28 Augustus 1912, is K. Assies, aannemer, wonende **Jan Sonjéstraat** 39
te Rotterdam, verklaard in staat van faillissement
en zulks met benoeming van den edelachtbaren heer mr. A. Rietema,
rechter in de arrondissements-rechtbank te Rotterdam,
tot rechter-commissaris, en van den ondergeteekende, wiens
kantoor is aan de Gedempte Bierhaven n*ummer* 8, tot curator.

Rotterdam, 30 Augustus 1912.

De Curator,

Mr. G. J. C. Schilthuis.

[Kosteloos.]

(4566)
