---
toegevoegd: 2024-07-21
gepubliceerd: 1986-06-26
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010948833:mpeg21:a0038"
onderwerpen: [familiebericht]
huisnummers: [34]
achternamen: [hilal]
kop: "Saïd Hilal,"
---
Bij exploit van 23 juni 1986
is ten verzoeke van
Margaretha Jacoba Mink
te Rotterdam gedagvaard

# Saïd Hilal,

v*oor*h*een*
wonende te Rotterdam
**Jan Sonjéstraat** 34A,
thans zonder bekende
woon- of verblijfplaats,
om op maandag 8 september 1986
om 10 uur
v*oor*m*iddag* bij procureur te verschijnen
ter terechtzitting
van de Arr*ondissements* rechtbank
te Rotterdam,
Noordsingel 117, terzake
van een vordering tot
echtscheiding, c.a.,
R. Krebbers, gerechtsdeurwaarder,
Bergsingel 224B, Rotterdam.

TV 477438
