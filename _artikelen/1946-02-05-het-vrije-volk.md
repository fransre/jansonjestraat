---
toegevoegd: 2021-05-04
gepubliceerd: 1946-02-05
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954862:mpeg21:a0026"
onderwerpen: [cafe, tehuur]
huisnummers: [15]
achternamen: [van-klaveren]
kop: "Feest- en Vergaderzaal"
---
# Feest- en Vergaderzaal

aangeboden **Jan Sonjéstraat** 15;
te bereiken met lijn 4, 15, 16 en 22.
C. v*an* Klaveren, Tel*efoon* 30682.
Dagel*ijks* bespr*eken* 10-4 uur.
