---
toegevoegd: 2021-07-24
gepubliceerd: 1961-10-27
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002792049:mpeg21:a00264"
onderwerpen: [woonruimte, tekoop]
huisnummers: [22]
kop: "leeg benedenhuis,"
---
Te koop aangeboden voor klein
bedrijf:

# leeg benedenhuis,

3 kamers, keuken en
douchegelegenheid en 60 m2
bergruimte a*an* d*e* straat. Boven
verhuurd v*oor* ƒ850 p*er* j*aar*. Te bez*ichtigen*
zaterdag *28 oktober 1961* 2-4 uur: **Jan Sonjéstraat** 22.
