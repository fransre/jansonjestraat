---
toegevoegd: 2024-07-23
gepubliceerd: 1965-03-12
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259035011:mpeg21:a00019"
onderwerpen: [bewoner]
huisnummers: [30]
achternamen: [smeets]
kop: "Alleen „Lange Mina” is er nog"
---
# Alleen „Lange Mina” is er nog

„Nog even, dan zullen we jou niet
meer zien,

Je moet ook nog wijken voor lijn 15
en 10.

Herinnering blijft: rooie Naat, Catrien,

Dat wordt in mijn dagboek geschreven;

Mijn steenkoude handen van vis en
ijs,

Jouw vishal was bepaald geen ijspaleis

'k Zal steeds aan je denken, al wordt
ik oud en grijs,

Dat staat in mijn dagboek te lezen.”

Ziedaar een couplet uit een gedicht
van N.J. Smeets (**Jan Sonjéstraat** 30A Rotterdam)
over het verdwijnen
van de Rotterdamse visafslag.

Het doet een beetje Jordaans aan,
dit vers. Uit het begeleidende briefje
blijkt echter, dat het recht uit een
Rotterdams hart komt. Luister maar:

„Ik zelf ben al bijna 20 jaar werkzaam
bij de Gemeentelijke Visafslag,
die zoals u reeds lang bekend is, binnenkort
zijn deuren voorgoed zal
sluiten. Wat wij nooit hadden kunnen
denken zal over enige weken waarheid
zijn. Een koud en tochtig beroep
waar ik in de loop der jaren van ben
gaan houden en dat iets bijzonders
was. Vele vrouwen heb ik zien komen
en ook weer helaas zien gaan, zonder
dat hun plaats werd ingenomen. Alleen
„Lange Mina” (mevrouw M. Bouwman)
is nog als enige visvrouw
over. Maar ook zij komt niet meer zo
heel vaak aan de Vismarkt. Jammer,
heel erg jammer..... en dat in zo'n
wereldstad als Rotterdam”.
