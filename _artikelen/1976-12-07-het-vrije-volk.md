---
toegevoegd: 2021-04-29
gepubliceerd: 1976-12-07
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959406:mpeg21:a0110"
onderwerpen: [cafe, misdrijf]
huisnummers: [15]
kop: "Vijf arrestaties in illegale gokhuizen"
---
# Vijf arrestaties in illegale gokhuizen

(Van een onzer verslaggevers)

Rotterdam — Bij een inval in twee illegale gokhuizen
heeft de Rotterdamse politie vannacht vijf
mensen gearresteerd, speelmateriaal in beslag genomen
en de hand gelegd op 8.000 gulden aan speelgelden.

Met assistentie van een mobiele
eenheid deed de politie
vannacht tegelijkertijd een inval
in een gokhuis aan de Aelbrechtskolk
en in de **Jan Sonjéstraat**.

~~In het eerste huis werden gearresteerd
de eigenaren J.S. (25)
en H.D. (33) uit Schiedam. Ook
de croupier, de 47-jarige J.P.,
werd gearresteerd. In het huis
waren ongeveer 15 spelers.~~

In de **Jan Sonjéstraat** waren
veertien spelers op het moment
van de inval. Gearresteerd werden
de 20-jarige mejuffrouw
M.I., die als coupier optrad, en
de 28-jarige eigenaar J.B.

In beide huizen werd de
speeltafel, de roulettebak en de
drankvoorraad in beslag genomen.
De inval is de tweede grote
actie tegen gokhuizen in korte
tijd. In juni van dit jaar *1976*
werden bij een bliksemactie invallen
gedaan in verschillende
Rotterdamse speelhuizen.
