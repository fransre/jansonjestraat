---
toegevoegd: 2024-08-11
gepubliceerd: 1923-07-28
decennium: 1920-1929
bron: Nieuwe Haarlemsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMNHA03:179027023:mpeg21:a00084"
onderwerpen: [bewoner]
huisnummers: [32]
achternamen: [van-doorn]
kop: "Huisvesting bij opkomst onder de wapenen."
---
# Huisvesting bij opkomst onder de wapenen.

De algemeene vereeniging van Verlofsofficieren
der Nederlandsche land- en zeemacht
heeft een huisvestigings-comité opgericht,
dat op verzoek voor alle verlofsofficieren
van ieder wapen, leden zoowel als niet-leden
der vereeniging, kamers bespreekt
voor de tijd, dat zij voor herhalingsoefeningen
onder de wapenen moeten komen.

Het verlangen daartoe moet worden kenbaar
gemaakt aan den heer G.C. van Doorn,
**Jan Sonjéstraat** 32a, Rotterdam.
