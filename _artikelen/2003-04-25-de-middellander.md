---
toegevoegd: 2024-02-21
gepubliceerd: 2003-04-25
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2003-04-25-de-middellander-origineel.jpg"
onderwerpen: [actie]
kop: "Actie driehoeksborden"
---
# Actie driehoeksborden

Vrijdag 25 april 2003 zijn de opbouwwerksters uit Middelland gewapend met keukenrollen
en glassex op pad geweest om de driehoeksborden schoon te maken in de wijk.

De borden in de Zwaerdecroonstraat,
Snellinckstraat, Hondiusstraat, Lieve Verschuierstraat,
**Jan Sonjéstraat** en het
Henegouwenplein hebben een poetsbeurt
gehad. Het bord in de Volmarijnstraat
wordt op een andere dag onder handen
genomen want door de nieuwbouwwerkzaamheden
kon het bord niet worden
bereikt. Na een aantal uurtjes gepoetst te
hebben zien de driehoeksborden er weer
veel beter uit. Voor de mensen die het niet
weten, u kunt zelf informatie ophangen in
het bord. Het bord dient geopend te worden
met een steek- of dopsleutel nummer 13.
Daarna kunt u het bord benutten voor
bijvoorbeeld:

\- Belangrijke aankondigingen

\- Mededelingen

\- Straatnieuwtjes

\- Opzoomeractiviteiten

\- Of andere feestjes

Maak gebruik van de borden want ze hangen
niet voor niets in de straat!

*Onderschrift bij de foto*
Actie driehoeksborden „met glassex schoongepoetst”
