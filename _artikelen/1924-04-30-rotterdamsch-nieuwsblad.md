---
toegevoegd: 2024-08-11
gepubliceerd: 1924-04-30
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494782:mpeg21:a0164"
onderwerpen: [bewoner]
achternamen: [koomans]
kop: "Bestuursjubileum J. Koomans."
---
# Bestuursjubileum J. Koomans.

Vandaag *30 april 1924* jubileert een bekend vereenigingsman
op brandweergebied, iemand die
als zoodanig zijn sporen, o*nder* a*ndere* bij de Rotterdamsche Brandweervereeniging
„Vriendschap zij ons doel” heeft verdiend.
Immers, heden is het 25 jaar geleden,
dat de heer J. Koomans, geaffecteerde
aan spuit 32 (Volmarijnstraat) werd gekozen
tot 1en voorzitter van deze corporatie,
tot wier eere-voorzitter hij 12 Augustus 1914
werd benoemd. Op 1 Februari 1892
trad hij tot „Vriendschap” toe en
werd op 5 Mei 1895 gekozen tot haar 1en
penningmeester, waarop hij 30 April 1899
deze functie voor die van 1en voorzitter
verwisselde.

In de 29 jaren, dat de Heer Koomans bestuurslid
dezer brandweervereeniging is,
heeft hij veel tot bevordering van haar
bloei bijgedragen. Geen wonder dat èn bestuur
èn leden van „Vriendschap zij ons
doel” deze gelegenheid hebben aangegrepen
hun eere-voorzitter te huldigen. Dit
zal hedenavond *30 april 1924* geschieden. Met medewerking
van het muziekkorps „De Brandweer”
wordt hem een ovatie gebracht
aan zijn woning in de **Jan Sonjéstraat**.
Men zal hiervoor om kwart voor achten
aantreden in de Valkensteeg bij den Groenendaal.
