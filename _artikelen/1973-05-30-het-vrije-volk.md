---
toegevoegd: 2024-07-21
gepubliceerd: 1973-05-30
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958125:mpeg21:a0117"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Club Angelique"
---
# Club Angelique

Nu voor elk wat wils in onze verbouwde zaak met films, show, gratis
drank en als klap op de vuurpijl onze 5 leuke girls die u graag
zullen verwennen.

Ons adres is **Jan Sonjéstraat** 33a (2e zijstraat 1e Middellandstraat).
Telefoon 25.61.88.

R 16
