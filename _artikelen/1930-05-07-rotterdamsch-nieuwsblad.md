---
toegevoegd: 2024-08-06
gepubliceerd: 1930-05-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164647020:mpeg21:a00102"
onderwerpen: [familiebericht]
huisnummers: [13]
achternamen: [barents]
kop: "Dankbetuiging."
---
# Dankbetuiging.

Aangezien het moeilijk is ieder persoonlijk
te bedanken betuigen wij
hiermede onzen hartelijken dank,
voor de zeer vele blijken van belangstelling
bij ons Gouden Huwelijksfeest
ondervonden. Ook onzen hartelijken
dank aan al diegenen, welke
ons verrasten met het zenden van
Prachtbloemstukken en Fruitmanden.

A. Barents.

M. Barents-van der Vegt.

Rotterdam, **Jan Sonjéstraat** 13.

16
