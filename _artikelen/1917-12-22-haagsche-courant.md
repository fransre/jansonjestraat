---
toegevoegd: 2024-08-13
gepubliceerd: 1917-12-22
decennium: 1910-1919
bron: Haagsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000139611:mpeg21:a0010"
onderwerpen: [bedrijfsinventaris, gevraagd]
huisnummers: [8]
achternamen: [van-essen]
kop: "Vaten."
---
# Vaten.

Gevraagd alle soorten eiken
vaten.

Adres: H. van Essen, **Jan Sonjéstraat** 8,
Telef*oon* 12026, Rotterdam.

2063
