---
toegevoegd: 2021-04-29
gepubliceerd: 1903-11-23
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010179292:mpeg21:a0069"
onderwerpen: [woonruimte, tehuur]
huisnummers: [18, 20]
kop: "Mooie Woningen."
---
# Mooie Woningen.

**Jan Sonjéstraat** N*ummer* 42
een ruime Etage, ƒ15 p*er*
maand, idem N*ummer* 18 en 20,
mooie Beneden- en Bovenhuizen
ƒ18 en ƒ20 p*er*
maand. ~~Hillegondastraat
vooraan, vrij Bovenhuisje
ƒ3 per week, idem Winkelhuisje,
zeer geschikt
voor Barbier, Schoenmaker,
Tabak en Sigaren
enz*ovoort*, ƒ3.25 per week~~
