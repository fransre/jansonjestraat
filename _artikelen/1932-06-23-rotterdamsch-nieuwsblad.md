---
toegevoegd: 2024-08-05
gepubliceerd: 1932-06-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164659055:mpeg21:a00161"
onderwerpen: [woonruimte, tehuur]
huisnummers: [23]
kop: "Aangeboden:"
---
# Aangeboden:

Zit-Slaapkamer voor
nette Juffrouw of Verpleegster,
billijken
prijs. Adr*es* **Jan Sonjéstraat** 23a.
