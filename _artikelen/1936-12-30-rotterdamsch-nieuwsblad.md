---
toegevoegd: 2024-08-03
gepubliceerd: 1936-12-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164680066:mpeg21:a00042"
onderwerpen: [huispersoneel]
kop: "Gevraagd:"
---
# Gevraagd:

net Meisje van 7½-4,
met Middageten.
Loon ƒ3. Aanmelden
**Jan Sonjéstraat** 26a.
