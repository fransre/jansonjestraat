---
toegevoegd: 2024-07-30
gepubliceerd: 1939-10-18
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164703039:mpeg21:a00047"
onderwerpen: [werknemer]
huisnummers: [34]
achternamen: [hollink]
kop: "Halfwas Loodgieter"
---
# Halfwas Loodgieter

gevraagd. Aanmelden: F.W.J. Hollink,
**Jan Sonjéstraat** 34, n*a*m*iddags* 7 u*ur*

35650 6
