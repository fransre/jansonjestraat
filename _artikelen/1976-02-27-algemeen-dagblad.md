---
toegevoegd: 2024-07-21
gepubliceerd: 1976-02-27
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002919023:mpeg21:a00278"
onderwerpen: [auto, tekoop]
huisnummers: [10]
achternamen: [wiemer]
kop: "Spanker of Vrijheid."
---
# Spanker of Vrijheid.

F. Wiemer,
**Jan Sonjéstraat** 10b,
R*otter*dam.

R29
