---
toegevoegd: 2024-07-28
gepubliceerd: 1941-08-20
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002729:mpeg21:a0048"
onderwerpen: [huispersoneel]
huisnummers: [38]
achternamen: [jongste]
kop: "Rooms Katholiek Meisje"
---
# R*ooms* K*atholiek* Meisje

b*iedt* z*ich* a*an*,
v*oor* d*ag* en n*acht*, tegen
1 October *1941* of eerder,
goed kunnende koken,
naaien, strijken, 22 j*aar*
Br*ieven* p*er* a*dres* W. Jongste,
**Jan Sonjéstraat** 38C.
