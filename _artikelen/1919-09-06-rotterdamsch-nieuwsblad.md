---
toegevoegd: 2024-03-07
gepubliceerd: 1919-09-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494290:mpeg21:a0075"
onderwerpen: [kinderartikel, kleding, muziek, tekoop]
huisnummers: [10]
kop: "kinderledikant,"
---
Te koop wit

# kinderledikant,

flinke maat, opslaande
hekken, lederen
Handkoffer, 4/4 Viool,
zwart laken Damesmantel
voor bejaarde
Juffrouw. **Jan Sonjéstraat** 10a.
