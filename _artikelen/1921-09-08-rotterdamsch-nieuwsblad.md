---
toegevoegd: 2024-08-12
gepubliceerd: 1921-09-08
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493839:mpeg21:a0109"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [10]
kop: "Kinderwagen."
---
# Kinderwagen.

Te koop roomkleurige
Kinderwagen met
bankje, in zeer goeden
staat; twee complete
Gordijngarnituren. **Jan Sonjéstraat** 10b.
