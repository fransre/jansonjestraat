---
toegevoegd: 2024-08-08
gepubliceerd: 1926-08-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495414:mpeg21:a0065"
onderwerpen: [brand, dans]
huisnummers: [38]
achternamen: [hoogerwerf]
kop: "Brandjes."
---
# Brandjes.

Aanvankelijk onder toezicht van den
brandspuitmeester N. Verhoeven, daarna
onder dat van den brandmeester, den
heer W.B. Wouters, hebben gasten van
spuit 31 gisteravond *3 augustus 1926* 8 uur met de gummislang
een begin van brand gebluscht
in een bergplaats in den danssalon van
D. Hoogerwerf aan de **Jan Sonjéstraat** n*ummer* 38b.
