---
toegevoegd: 2024-08-04
gepubliceerd: 1934-06-27
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164671064:mpeg21:a00201"
onderwerpen: [werknemer]
huisnummers: [15]
achternamen: [van-oosten]
kop: "Biedt zich aan:"
---
# B*iedt* z*ich* a*an*:

beschaafd Meisje, 26 j*aar*
als Werkster, h*ele* of h*alve* M*aandag*,
W*oensdag* Vr*ijdag*. Adres B. v*an* Oosten,
**Jan Sonjéstraat** 15.

