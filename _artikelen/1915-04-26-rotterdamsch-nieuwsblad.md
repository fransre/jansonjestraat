---
toegevoegd: 2024-08-13
gepubliceerd: 1915-04-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295991:mpeg21:a0197"
onderwerpen: [fiets, tekoop]
huisnummers: [38]
achternamen: [huizing]
kop: "Heerenrijwiel"
---
Dubbele Clockenlager
1e klas

# Heerenrijwiel

met nikkelen Velgen
Rijwiel nog nieuw met
Bel en Tasch, voor elk
nemelijk bod, ook
Zondags te zien en te
bevragen bij N.M. Huizing,
**Jan Sonjéstraat** 38c,
moet weg wegens
plaatsgebrek
