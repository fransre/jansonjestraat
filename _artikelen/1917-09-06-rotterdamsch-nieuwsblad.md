---
toegevoegd: 2024-08-13
gepubliceerd: 1917-09-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296705:mpeg21:a0082"
onderwerpen: [huispersoneel, gevraagd]
huisnummers: [41]
achternamen: [baatsen]
kop: "Gevraagd:"
---
# Gevraagd:

nette Vrouw, van 9-11,
voor nette bezigheden,
Dinsdag, Vrijdag, Zaterdag.
Bespreken des
morgens voor 10.
Baatsen, **Jan Sonjéstraat** 41a
Bovenh*uis*.
