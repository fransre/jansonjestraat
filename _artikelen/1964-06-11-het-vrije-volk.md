---
toegevoegd: 2024-07-23
gepubliceerd: 1964-06-11
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955170:mpeg21:a0087"
onderwerpen: [auto, tekoop]
huisnummers: [31]
achternamen: [both]
kop: "VolksWagen"
---
Te koop aangeboden

# V*olks*W*agen*

1951
en Ford Mainline 1952. Both,
**Jan Sonjéstraat** 31.
