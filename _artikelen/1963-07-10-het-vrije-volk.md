---
toegevoegd: 2024-07-23
gepubliceerd: 1963-07-10
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954189:mpeg21:a0192"
onderwerpen: [dieren]
huisnummers: [30]
achternamen: [smeets]
kop: "Dierenrijk"
---
# Dierenrijk

De kinderen van mevr*ouw* N.J. Smeets,
**Jan Sonjéstraat** 30a, kwamen zaterdagmiddag *6 juli 1963*
met een in deze straat loslopende
hond thuis. De hond is bruin
met zwart, rulgharig en het is vermoedelijk
een foxterriër. Wil de eigenaar
deze hond zo spoedig mogelijk afhalen?
