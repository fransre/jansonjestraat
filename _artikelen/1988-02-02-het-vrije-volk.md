---
toegevoegd: 2024-07-21
gepubliceerd: 1988-02-02
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962768:mpeg21:a0149"
onderwerpen: [bellebom]
kop: "Plof"
---
# Plof

Er worden op de lokatie
Bellevoystraat/**Jan Sonjéstraat**
damwanden geheid.
De naaste bewoners moeten
tweemaal een dag hun woning
verlaten. Tijdens het heien
zou zo'n bom kunnen ontploffen.

Op 27 maart *1988* zullen deskundigen
van de EOD de bom
(bommen) demonteren.
De bewoners binnen een cirkel
van 300 meter moeten die
dag met kind, kat en kostbaarheden
hun woning verlaten.
In een iets wijdere kring
dient eenieder de hele dag
binnen te blijven.

Tijdens het demonteren zou
zo'n bom kunnen ontploffen.

En de ene plof is de andere
niet.

Is de ene plof de andere
niet?

Zeker weten?

Of?

Wie 't weet mag het zeggen.

R.M. Fransen, Rotterdam
