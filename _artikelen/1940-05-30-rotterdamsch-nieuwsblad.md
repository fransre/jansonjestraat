---
toegevoegd: 2024-07-29
gepubliceerd: 1940-05-30
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002400:mpeg21:a0072"
onderwerpen: [bedrijfsinformatie]
huisnummers: [13]
kop: "Bond van Kleermakerspatroons, afdeling Rotterdam"
---
# B*ond* v*an* K*leermakerspatroons*, afd*eling* Rotterdam

Het Bestuur van den Bond van Kleermakerspatroons, afd*eling* Rotterdam,
Secretariaat **Jan Sonjéstraat** 13a, brengt ter kennis aan de
Cliëntèle, dat onderstaande Leden door de laatste gebeurtenissen gedwongen
van adres moesten veranderen:

C.W. Borgers, v*oor*h*een* Meent 46,
thans: Sonmanstraat 38. Telefoon 46043.

Blom & Co*mpagnon*, v*oor*h*een* Middensteiger 18,
thans: Molenlaan 86, Hillegersberg. Tel*efoon* 40373.

J.G. van Irsen, v*oor*h*een* Teilingerstraat 69,
thans: Beukelsdijk 63. Telefoon 34044.

G. Kiela, v*oor*h*een* Meent 39-41,
thans: Noordmolenstraat 73.

Ch. Philipse, v*oor*h*een* Diergaardelaan 29,
thans: Jan van Avenesstraat 46, Telef*oon* 46144.

Smit & v*an* d*er* Klein, v*oor*h*een* Leuvehaven 39,
thans: Coolhaven 156, Telef*oon* 36882.

Schoonenberg N.V., v*oor*h*een* Zeevischmarkt 10,
thans: Plantageweg 30. Telef*oon* 54033.

Cliënten worden beleefd verzocht hun adressen op te geven, daar alle
administratie verloren is gegaan.

1435
