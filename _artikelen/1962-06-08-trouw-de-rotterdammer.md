---
toegevoegd: 2021-04-30
gepubliceerd: 1962-06-08
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://schiedam.courant.nu/issue/TR-ROT/1962-06-08/edition/0/page/2"
onderwerpen: [ongeval, auto]
kop: "Bromfietser door winkelruit"
---
# Bromfietser door winkelruit

In de **Jan Sonjéstraat** te Rotterdam
reed de achttienjarige kantoorbediende
G. de Vries uit de Sourystraat donderdagavond *7 juni 1962*
met zijn bromfiets tegen de
winkelruit (3 bij 2 meter) van een kledingmagazijn.

De jongen raakte vermoedelijk de
macht over het stuur kwijt, doordat
hij met te grote snelheid door de bocht
ging. Hij liep door de scherven een grote
diepe wond aan de hals op, waarvoor
men hem naar het Dijkzigtziekenhuis
overbracht.
