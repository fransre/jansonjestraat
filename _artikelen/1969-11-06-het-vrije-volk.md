---
toegevoegd: 2024-07-22
gepubliceerd: 1969-11-06
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957052:mpeg21:a0335"
onderwerpen: [familiebericht]
huisnummers: [30]
achternamen: [smeets]
kop: "Yvonne en Johnny"
---
Ondanks (naar wij hopen) een
korte scheiding, hopen

# Yvonne en Johnny

hun 13e verjaardag te vieren,
op 8 november *1969*. Gordelweg 160.

**Jan Sonjéstraat** 30a,

Fam*ilie* N.J. Smeets.
