---
toegevoegd: 2024-07-22
gepubliceerd: 1972-12-29
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002880023:mpeg21:a00138"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Club Angelique."
---
# Club Angelique.

Ook
tussen de feestdagen continueren
wij ons alom
bekend programma, mocht u
niet aanwezig kunnen zijn,
wensen wij u een voorspoedig 1973.
De directie, **Jan Sónjestraat** 33A,
zijstr*aat* Middellandstr*aat* Rotterdam (C*entrum*). Tel 256188.

VZ16
