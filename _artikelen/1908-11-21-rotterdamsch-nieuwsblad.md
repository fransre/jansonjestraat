---
toegevoegd: 2024-08-14
gepubliceerd: 1908-11-21
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197287:mpeg21:a0114"
onderwerpen: [werknemer]
huisnummers: [16]
achternamen: [schravesande]
kop: "Verstelnaaister"
---
1 Flinke, vlugge

# Verstelnaaister

gevraagd, ook met Costuumwerk
op de hoogte,
voor Woensdag of Donderdag.
Adres bij J. Schravesande, **Jan Sonjéstraat** 16 boven,
bij de Middellandstraat.
