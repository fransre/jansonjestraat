---
toegevoegd: 2024-08-13
gepubliceerd: 1915-03-17
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000203241:mpeg21:a0034"
onderwerpen: [werknemer]
huisnummers: [18]
kop: "Winkeljuffrouw"
---
Een net, Belgisch Meisje,
oud 18 jaar, zag zich
gaarne geplaatst als

# Winkeljuffrouw

of Cassière, sprekende de
Nederlandsche en Fransche
taal. Br*ieven* E.V. L.,
**Jan Sonjéstraat** 18b.
