---
toegevoegd: 2024-08-11
gepubliceerd: 1923-08-08
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494865:mpeg21:a0035"
onderwerpen: [fiets, reclame]
huisnummers: [3]
achternamen: [dommers]
kop: "rijwielen"
---
Carnegie en The Globe

# rijwielen

zijn gegarandeerd, ook
in huurkoop. A. Dommers,
**Jan Sonjéstraat** 3a.
Rijwielhandel.
