---
toegevoegd: 2024-08-14
gepubliceerd: 1910-03-16
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197838:mpeg21:a0095"
onderwerpen: [werknemer]
huisnummers: [28]
kop: "Gevraagd"
---
# Gevraagd

een aankomende Strijkster,
een flink Loopmeisje,
dat het vak kan leeren,
loon 1.75 en een bekwame
Waschvrouw, die
in een Waschinrichting
werkzaam geweest is.
Wasch- en Strijkinrichting
**Jan Sonjéstraat** 28a.
