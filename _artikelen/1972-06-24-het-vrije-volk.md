---
toegevoegd: 2021-04-29
gepubliceerd: 1972-06-24
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957870:mpeg21:a0117"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Filmclub"
---
# Filmclub

Angelique presenteert
geen 10, geen 8, geen
6, maar 4 daverende shows,
m*et* m*edewerking* v*an* Angelique, Helene, Monique
en Francoise. Gezellige
bar met charmante bediening
aanwezig. Nieuwste geluidsfilms,
drank gratis. Geopend
van 's middags 2 tot 5 uur, 's
avonds van 8.40 tot 12 uur.
Het bekende adres, **Jan Sonjéstraat** 33
te Rotterdam
(zijstr*aat* Middellandstr), woonhuis
tel*efoon* 010-256188. VZ16
