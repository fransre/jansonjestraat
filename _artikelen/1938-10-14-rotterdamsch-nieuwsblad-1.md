---
toegevoegd: 2024-03-07
gepubliceerd: 1938-10-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164697038:mpeg21:a00058"
onderwerpen: [verloren]
huisnummers: [39]
achternamen: [rolvers]
kop: "Verloren:"
---
# Verloren:

Briefje van 25 omtrek
Vierambachtstraat. Tegen belooning
terug bezorgen
Rolvers. **Jan Sonjéstraat** 39a.
