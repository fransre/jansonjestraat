---
toegevoegd: 2024-08-03
gepubliceerd: 1935-03-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164676022:mpeg21:a00150"
onderwerpen: [woonruimte, tehuur]
huisnummers: [28]
kop: "Vrij bovenhuis,"
---
# Vrij bovenhuis,

5 Kamers, Keuken, Warande,
Zolder, keurig
in orde, verlaagde
Huurprijs. **Jan Sonjéstraat** 28a.
