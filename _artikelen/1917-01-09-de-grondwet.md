---
toegevoegd: 2024-08-13
gepubliceerd: 1917-01-09
decennium: 1910-1919
bron: De grondwet
externelink: "https://resolver.kb.nl/resolve?urn=MMGARO01:000171673:mpeg21:a0006"
onderwerpen: [diefstal]
kop: "Brutale vetdiefstal."
---
# Brutale vetdiefstal.

Ten nadeele van een der veemen te
Rotterdam is een brutale vetdiefstal gepleegd.
Met valsche sleutels heeft men
zich toegang verschaft tot een bij dat
veem in gebruik zijnd pakhuis aan de
**Jan Sonjéstraat**, waaruit ongeveer 60
vaten vet, elk ter waarde van circa ƒ100,
zijn ontvreemd.

Vermoedelijk zijn ze weggehaald tusschen
5 en 6 uur des avonds, eenige dagen
geleden.

Door de royale manier, waarop de dieven
te werk gingen, kregen omwonenden
niet den minsten argwaan.
