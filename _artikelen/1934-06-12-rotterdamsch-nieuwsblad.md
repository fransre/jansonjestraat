---
toegevoegd: 2024-08-04
gepubliceerd: 1934-06-12
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164671047:mpeg21:a00072"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [17]
kop: "Aangeboden:"
---
# Aangeboden:

ongemeubileerde Zit-Slaapkamer
m*et* Serre,
met of zonder Pension,
netten stand. **Jan Sonjéstraat** 17.
