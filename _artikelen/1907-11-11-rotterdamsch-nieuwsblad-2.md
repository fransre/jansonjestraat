---
toegevoegd: 2022-07-23
gepubliceerd: 1907-11-11
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196971:mpeg21:a0125&2"
onderwerpen: [bedrijfsruimte]
huisnummers: [42, 44]
kop: "Groot Pakhuis."
---
# Groot Pakhuis.

Te huur aan de **Jan Sonjéstraat** 42-44
een
Pakhuis, groot 160 vierkanten
Meter, vrije ruimte
verder Kantoor, groote
Kasten, Privaat en
Gas- en Waterleiding en
twee hooge en breede inrijdeuren,
geschikt voor
de grootste Wagens. Te
bevragen Heemraadssingel,
Hoek Middellandstraat.
