---
toegevoegd: 2021-04-29
gepubliceerd: 1939-10-07
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000194198:mpeg21:a0062"
onderwerpen: [brand, motor]
achternamen: [dekker]
kop: "Uitslaande brand in de Middellandstraat."
---
# Uitslaande brand in de Middellandstraat.

Een steekvlam sloeg uit.

Vanmorgen *7 oktober 1939* om kwart over negen is brand
uitgebroken in den motorhandel van den
heer P. Dekker, op den hoek Middellandstraat
en **Jan Sonjéstraat**.

Toen de eigenaar in de werkplaats achter
den winkel bezig was met het soldeeren van
een benzinetank van een motor sloeg plotseling
een steekvlam uit, die de werkbank
vlam deed vatten. De kleeding van den motorhersteller
vatte vlam, doch de man wist
deze zelf te dooven en naar buiten te snellen.

In de werkplaats met de zeer brandbare
voorwerpen verspreidden de vlammen zich
met groote snelheid en spoedig sloeg het
vuur over naar den winkel, waar de vlammen
voedsel vonden in de triplexbekleeding
van muren en etalage. Met een harden knal
sprongen de ruiten, waardoor het vuur lucht
kreeg en de vlammen nog verder oplaaiden.
Direct na het uitbreken van den brand was
de brandweer gealarmeerd, die spoedig sein
„middelalarm” gaf.

Toen de brandweer arriveerde, sloegen de
vlammen uit het beneden huis langs den
gevel omhoog en had het vuur reeds de daklijst
aangetast. Omdat de bovenverdiepingen
groot gevaar liepen, werd het vuur direct
krachtig aangepakt. Met twee stralen van
slangenwagen 31 en een straal van 28 werd
het vuur bestreden, zoodat het vuur zienderoogen
minderde. Na twintig minuten had men
den brand onder de knie. Het benedenhuis
met winkel en werkplaats was geheel uitgebrand,
terwijl de daklijst gedeeltelijk was
vernield.

De bovenverdiepingen liepen slechts weinig
schade op. Een aantal in den winkel en
werkplaats staande motorrijwielen werd een
prooi der vlammen.

Het pand om den hoek van de **Jan Sonjéstraat**
heeft echter wel schade ondervonden.
Op de eerste étage bewoond door den heer
K.H. Jansen, brande de vloer door en een
deel van de voorkamer werd een prooi der
vlammen.

De blussching stond onder leiding van
onderbrandmeester N. Verhoeven, brandmeester
J. van Onlangs, en hoofdman A.J.
Kruijs.

Mede aanwezig waren de slw. 32, 56, 49,
reservewagen 1 en slw. 34.

De heer Dekker was tegen brand- en waterschade
verzekerd.
