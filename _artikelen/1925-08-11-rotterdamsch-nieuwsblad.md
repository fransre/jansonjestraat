---
toegevoegd: 2024-08-10
gepubliceerd: 1925-08-11
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495546:mpeg21:a0077"
onderwerpen: [huispersoneel]
huisnummers: [46]
achternamen: [van-loon]
kop: "Gevraagd:"
---
# Gevraagd:

een flinke Dienstbode,
voor dag en nacht, g*oed* k*unnende* k*oken*
en van g*oede* g*etuigen* v*oorzien*. Adres:
A. van Loon, **Jan Sonjéstraat** 46,
hoek
Schermlaan.
