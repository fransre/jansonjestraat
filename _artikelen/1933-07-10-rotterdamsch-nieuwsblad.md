---
toegevoegd: 2024-08-04
gepubliceerd: 1933-07-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164666010:mpeg21:a00102"
onderwerpen: [familiebericht]
huisnummers: [5]
achternamen: [mager]
kop: "Johannes Hendricus Mager,"
---
Heden overleed, in de volle zekerheid
des geloofs, na een langdurig
doch geduldig gedragen lijden, onze
innig geliefde Vader, Behuwd- en
Grootvader, Broeder, Behuwdbroeder
en Oom

# Johannes Hendricus Mager,

Wed*uwnaa*r van

Anna Klasina van Raalte,

in den ouderdom van 88 jaar.

Uit aller naam,

C.Ph.Ch. Mager.

H.G.A Henke-Mager.

C.F.E. Henke en Kleindochter.

Rotterdam, 8 Juli 1933.

**Jan Sonjéstraat** 5b.

De begrafenis zal plaats hebben
Woensdag *12 juli 1933* a*an*s*taande* op de Algemeene
Begraafplaats Crooswijk. Vertrek
van het sterfhuis te 1.30 uur.
