---
toegevoegd: 2024-08-14
gepubliceerd: 1906-06-06
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213809:mpeg21:a0127"
onderwerpen: [bedrijfsinformatie]
huisnummers: [21]
achternamen: [rodt]
kop: "Sigarenwinkel,"
---
# Sigarenwinkel,

nette zaak en goede
stand, ter overname aangeboden,
wegens scheepsbetrekking.
Huur ƒ7,
mooie Woning. Adres en
informatie bij den Reiziger
A.E.A. Rodt, **Jan Sonjéstraat** 21,
einde
Kruiskade of Benthuizerstraat 16,
winkel.
Spoed wordt vereischt.
Sigaren, Koffie en Thee
kunnen ook in Depot
blijven. Koopsom goed,
de waarde v*an* d*e* Inventaris.
