---
toegevoegd: 2024-07-20
gepubliceerd: 1930-06-05
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164647040:mpeg21:a00063"
onderwerpen: [woonruimte, tehuur]
huisnummers: [38]
kop: "mooie Eerste Etage,"
---
Te huur:

# mooie Eerste Etage,

**Jan Sonjéstraat** 38.

Huur ƒ35.— per maand. Te bevragen
bij A. Kooij, Lusthofstraat 125,
Telefoon 57226.

34039 11
