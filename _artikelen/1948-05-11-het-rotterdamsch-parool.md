---
toegevoegd: 2024-07-26
gepubliceerd: 1948-05-11
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164870109:mpeg21:a00022"
onderwerpen: [ongeval, fiets, tram]
kop: "Geslipt in de tramrails"
---
# Geslipt in de tramrails

~~Op de Mauritsweg raakte Maandagmiddag *10 mei 1948*
de wielrijder A.H. S.
uit de Ackersdijkstraat met het
voorwiel van zijn fiets inde tramrails.
Hij viel en sloeg met het
hoofd tegen de bijwagen van een
uit de tegengestelde richting komende
tram van lijn 14. Met een
wond aan het achterhoofd, inwendige
kneuzingen en kwetsuren aan
de linkerarm is hij in het Coolsingelziekenhuis
opgenomen.~~

Op de Goudserijweg slipte de
wielrijder F.A. V. uit de **Jan Sonjéstraat**
in de tramrails. De linkervoet
van de man werd overreden
door een juist passerende vrachtauto.
De tenen werden gekneusd.
Het tramverkeer ondervond een
half uur vertraging.

~~De scheepsbewaker F.J. v*an* P. uit
Pernis overkwam op de Westzeedijk
een dergelijk ongeval. Hij
kneusde de rechterknie toen hij
met zijn fiets kwam te vallen. In
het Coolsingelziekenhuis werd de
man opgenomen.~~
