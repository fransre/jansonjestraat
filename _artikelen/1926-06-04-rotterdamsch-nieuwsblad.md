---
toegevoegd: 2024-08-08
gepubliceerd: 1926-06-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495364:mpeg21:a0035"
onderwerpen: [muziek, tekoop]
huisnummers: [12]
kop: "Te koop:"
---
# Te koop:

prima 4/4 Studieviool,
mooien klank, met
Vormkist en bijbehooren.
Billijken prijs,
**Jan Sonjéstraat** 12b,
Bovenh*uis*.
