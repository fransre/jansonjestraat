---
toegevoegd: 2023-07-31
gepubliceerd: 1925-09-29
decennium: 1920-1929
bron: De Harderwijker
externelink: "https://proxy.archieven.nl/0/4C48DD750B9E499294896C384FB91128"
onderwerpen: [bewoner]
kop: "Korte berichten"
---
# Korte berichten

— Donderdagnamiddag *24 september 1925* omstreeks 6 uur
hoorden twee agenten te Rotterdam bij
het passeeren der Oranje Nassau school,
Beukelsdijk hoek Velzenluststraat, luid
geschreeuw van een jongen. Zooveel gelezen
hebbend in den laatsten tijd over
achtergelaten leerlingen, begrepen zij aanstonds
hier ook zoo'n geval voor zich te
hebben. Zij waarschuwden den concierge
van de school, die in de **Jan Sonjéstraat**
woonde, en vonden den 9-jarigen D.M.,
die in de school was achtergelaten en nu
door luid geschreeuw de aandacht van
voorbijgangers trachtte te trekken.
