---
toegevoegd: 2021-05-18
gepubliceerd: 1911-05-01
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000186175:mpeg21:a0042"
onderwerpen: [reclame]
huisnummers: [22]
achternamen: [nolles]
kop: "Henri Nolles."
---
# Henri Nolles.

De 4 reuzenfeesten, meer dan 1000 bezoekers aangeboden door de firma Lap,
Heerenkleeding-magazijn te Rotterdam, zijn gearrangeerd door Henri Nolles.

Groot Succes! — Eerste klas bioscope! (ook voor huisfeesten!) Vermakelijke
goochelkunst! Eigen Décors! Fraaie Attributen!

Henri Nolles — Feestarrangeur.

Veranderd adres: **Jan Sonjéstraat** 22 — Rotterdam.

(Voor kinderen Poppenkast — Zeer amusant!) (427)
