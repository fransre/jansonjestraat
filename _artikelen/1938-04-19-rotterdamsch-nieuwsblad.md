---
toegevoegd: 2024-08-01
gepubliceerd: 1938-04-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164694041:mpeg21:a00105"
onderwerpen: [brand]
huisnummers: [27]
achternamen: [moerman]
kop: "Brandjes."
---
# Brandjes.

Gistermiddag *18 april 1938* 6 uur bluschten gasten van slangenwagen 32,
onder toezicht van den brandmeester,
den heer C. van Bockel, met de gummislang
een begin van brand ten huize van Moerman
aan de **Jan Sonjéstraat** 27.
