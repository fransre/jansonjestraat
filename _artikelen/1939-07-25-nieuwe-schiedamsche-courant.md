---
toegevoegd: 2024-05-20
gepubliceerd: 1939-07-25
decennium: 1930-1939
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1939-07-25/edition/0/page/2"
onderwerpen: [ongeval, auto]
kop: "Goed afgeloopen."
---
# Goed afgeloopen.

Zondagavond *23 juli 1939* om 7 uur heeft in de Witte de Wittstraat
een aanrijding plaats gehad,
waarbij de auto van den Spaanschen consul
de heer L. Perez Munoz zwaar werd beschadigd.
De consul zelf kwam er zonder letsel
af. Deze reed met zijn auto in de Witte de Withstraat
komende van de richting Schiedamschesingel.
Uit de Eendrachtstraat kwam
de limonade-fabrikant C. van K. wonende
**Jan Sonjéstraat** met een gesloten vrachtauto.
Doordat laatstgenoemde den van links komende
personenauto geen voorrang verleende,
werd deze in de flank gegrepen en geheel
ingedrukt. Ook de vrachtauto kreeg
schade. De beide bestuurders echter kwamen
met den schrik vrij.
