---
toegevoegd: 2024-08-11
gepubliceerd: 1924-05-09
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494790:mpeg21:a0033"
onderwerpen: [kleding, tekoop]
huisnummers: [24]
kop: "Smoking."
---
# Smoking.

Te koop aangeboden
een zoo goed als niet
gedragen Smoking, middelmatig
Persoon. Prijs
ƒ30, **Jan Sonjéstraat** N*ummer* 24b.
