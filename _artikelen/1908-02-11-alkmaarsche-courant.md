---
toegevoegd: 2024-05-20
gepubliceerd: 1908-02-11
decennium: 1900-1909
bron: Alkmaarsche Courant
externelink: "https://kranten.archiefalkmaar.nl/issue/ACO/1908-02-11/edition/null/page/1"
onderwerpen: [bewoner]
huisnummers: [41]
achternamen: [rolf]
kop: "Emigratie naar Kanada."
---
# Emigratie naar Kanada.

Door eenige oud-Kanadeesche emigranten is opgericht
de vereeniging „Kanada”, welke zich ten doel
stelt, bestrijding der emigratie naar Kanada.

Ze tracht dit doel te bereiken door het verspreiden
van geschriften, het geven van informatiën, enz*ovoort*.

Nadere inlichtingen worden verstrekt door de heeren
B. Rolf, **Jan Sonjéstraat** 41, Rotterdam, en J.A. Pieterse,
Utrechtsche weg 31 te Arnhem.
