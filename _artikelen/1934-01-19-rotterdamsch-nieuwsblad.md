---
toegevoegd: 2024-08-04
gepubliceerd: 1934-01-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164669021:mpeg21:a00053"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [3]
kop: "Slaapkamer"
---
# Slaapkamer

aangeboden à ƒ2.50 per
week, met of z*onder* Pens*ion*
Op vrij Bovenh*uis* geen
Beroepspension. **Jan Sonjéstraat** 3b,
Westen.
