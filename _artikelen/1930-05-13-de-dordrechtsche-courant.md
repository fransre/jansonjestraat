---
toegevoegd: 2023-08-01
gepubliceerd: 1930-05-13
decennium: 1930-1939
bron: De Dordrechtsche Courant
externelink: "https://proxy.archieven.nl/0/3D626D0365EC4C81B5D496084D234227"
onderwerpen: [faillissement]
huisnummers: [18]
achternamen: [henke]
kop: "Faillissementen."
---
# Faillissementen.

Uitgesproken:

A.W.L. Henke, handelende onder de firma
Agentuur en Commissiehandel „Heweko”, wonende
**Jan Sonjéstraat** 18b, Rotterdam, zaak doende
Beursplein 4, Rotterdam.

Rechter-Comm*issaris* mr. dr. G.L. van Oosten
Slingeland. Cur*ator* mr. A.M. Mooy.
