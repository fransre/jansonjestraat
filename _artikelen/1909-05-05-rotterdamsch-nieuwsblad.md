---
toegevoegd: 2024-08-14
gepubliceerd: 1909-05-05
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197421:mpeg21:a0095"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [24]
achternamen: [boijen]
kop: "Aangeboden"
---
# Aangeboden

op vrij Bovenhuis, een
frissche Slaapkamer met
Pension, voor net Persoon,
met vaste betrekking,
bezigh*eden* buitenshuis
hebbende. Wed*uwe* Boijen
**Jan Sonjéstraat** N*ummer* 24b,
bij de 1e Middellandstr*aat*,
lijn Oudedijk.
