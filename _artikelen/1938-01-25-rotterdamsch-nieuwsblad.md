---
toegevoegd: 2024-08-02
gepubliceerd: 1938-01-25
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164693019:mpeg21:a00200"
onderwerpen: [woonruimte, gevraagd]
huisnummers: [14]
achternamen: [verschoor]
kop: "vrij bovenhuis"
---
Gem*eente*-ambt*enaar* vraagt

# vrij bovenhuis

of Benedenhuis, 5 Kamers,
±ƒ35. Verschoor,
**Jan Sonjéstraat** N*ummer* 14.
