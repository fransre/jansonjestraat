---
toegevoegd: 2024-07-26
gepubliceerd: 1948-06-19
decennium: 1940-1949
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB15:000554143:mpeg21:a00049"
onderwerpen: [woonruimte, tekoop]
huisnummers: [28]
kop: "Afslag"
---
# Afslag

van het pand en erf **Jan Sonjéstraat** 28,
Rotterdam op
Woensdag 23 Juni 1948, 14 u*ur*
in Veilingzaal 5, Beursgebouw,
aldaar, ingang Meent. Inzet
ƒ9400.—. Maximumprijs ƒ9700.—

Not*aris*s*en* H.A.W. Zeijlmans
v. Emmichoven.
