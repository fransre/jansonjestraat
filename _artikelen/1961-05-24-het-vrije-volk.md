---
toegevoegd: 2024-07-23
gepubliceerd: 1961-05-24
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010953539:mpeg21:a0049"
onderwerpen: [bedrijfsruimte]
huisnummers: [33]
achternamen: [van-hulst]
kop: "kelderruimte"
---
Droge

# kelderruimte

voor
opslagplaats of bergruimte. Te
bevr*agen* tussen 19-20 uur. v*an* Hulst
**Jan Sonjéstraat** 33b.
