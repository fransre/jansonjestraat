---
toegevoegd: 2023-03-27
gepubliceerd: 2023-03-26
decennium: 2020-2029
bron: Instagram
externelink: "https://www.instagram.com/p/CqQb3mtMBAe/?igshid=YmMyMTA2M2Y%3D"
onderwerpen: [actie]
huisnummers: [20]
kop: "openrotterdam"
---
# openrotterdam

Wat hartverwarmend! In de **Jan Sonjéstraat** staat een overvol ‘Neem wat je wil’ kastje, voor iedereen die iets nodig heeft. Knuffels, krijtjes, lampen; er staat echt van alles in 💛🧡❤️

Wat zou jij nog neerzetten in het kastje? ⬇️

\#open010 \#rotterdam \#middelland \#west \#neemmeewatjewil \#JanSonjéstraat \#actie \#goodvibes
