---
toegevoegd: 2024-07-26
gepubliceerd: 1943-02-13
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003416:mpeg21:a0007"
onderwerpen: [kinderartikel, gevraagd]
huisnummers: [38]
achternamen: [boehm]
kop: "kinderwagen,"
---
Moderne

# kinderwagen,

liefst
blauwe of witte opvouwbare. **Jan Sonjéstraat** 38a
F.C.J. Böhm.
