---
toegevoegd: 2024-07-28
gepubliceerd: 1941-10-20
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002792:mpeg21:a0029"
onderwerpen: [woonruimte, tehuur]
huisnummers: [38]
achternamen: [stepanenco]
kop: "Te huur"
---
# Te huur

2e étage, 3 gem*eubileerde*
kamers m*et* keukengebr*uik*,
tevens gem*eubileerde* zit-slaapkamer,
huurprijs n*aar* overeenkomst.
I. Stepanenco
**Jan Sonjéstraat** 38c.
