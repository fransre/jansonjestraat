---
toegevoegd: 2024-08-05
gepubliceerd: 1932-03-18
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164658021:mpeg21:a00040"
onderwerpen: [familiebericht]
huisnummers: [14]
achternamen: [verschoor]
kop: "Cor Verschoor en Jo Verhage."
---
Verloofd

# Cor Verschoor en Jo Verhage.

R*otter*dam, 16-3-*19*32.

**Jan Sonjéstraat** 14b.

Hengelo (O*verijssel*)

Tuindorpstraat 101.

10
