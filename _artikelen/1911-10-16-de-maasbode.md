---
toegevoegd: 2024-08-14
gepubliceerd: 1911-10-16
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000186410:mpeg21:a0035"
onderwerpen: [reclame]
huisnummers: [22]
achternamen: [nolles]
kop: "Henri Nolles — Feestarrangeur"
---
# Henri Nolles — Feestarrangeur

**Jan Sonjéstraat** 22, Rotterdam.

Eerste klas bioscope- en goochelvoorstellingen

Henri Nolles, bouwer van „Sprookjesland” en van „Het Vrooliike Rad”,

beide van 10-31 Aug*ustus* *1911* op de tentoonstelling te Arnhem

Dagelijks door duizenden bezocht!
