---
toegevoegd: 2024-03-07
gepubliceerd: 1931-03-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164652020:mpeg21:a00091"
onderwerpen: [werknemer]
huisnummers: [34]
achternamen: [bosselaar]
kop: "Naaimeisje"
---
# Naaimeisje

gevraagd om het Costuumvak
te leeren,
P*rotestantse* G*ezindte*. Aanmelden Bosselaar,
**Jan Sonjéstraat** 34.
