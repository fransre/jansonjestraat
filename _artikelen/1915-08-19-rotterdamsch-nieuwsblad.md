---
toegevoegd: 2024-08-13
gepubliceerd: 1915-08-19
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296088:mpeg21:a0131"
onderwerpen: [werknemer]
huisnummers: [3]
achternamen: [bosveld]
kop: "Jongen,"
---
Terstond gevraagd een
nette

# Jongen,

leeftijd 16 à 17 jaar, bij
G.J.H. Bosveld. **Jan Sonjéstraat** 3A,
Groentenwinkel.
