---
toegevoegd: 2024-08-11
gepubliceerd: 1924-05-06
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494787:mpeg21:a0106"
onderwerpen: [werknemer]
huisnummers: [36]
achternamen: [steenbergen]
kop: "Nette meisjes"
---
# Nette meisjes

gevraagd, Mangel- en
Vouwwerk. Steenbergen,
**Jan Sonjéstraat** 36a.
