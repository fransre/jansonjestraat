---
toegevoegd: 2024-07-24
gepubliceerd: 1956-05-22
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000334041:mpeg21:a00144"
onderwerpen: [reclame]
huisnummers: [34]
achternamen: [raap]
kop: "Voor exclusieve opnamen"
---
# Voor exclusieve opnamen

van Uw bruiloft aan huis, stadhuis,
kerk enz*ovoort* uitsluitend foto Raap, **Jan Sonjéstraat** 34b, telefoon
37613, R*otter*dam. Specialiteit in Kinderfoto's.
