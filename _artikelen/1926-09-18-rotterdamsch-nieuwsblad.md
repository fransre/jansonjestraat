---
toegevoegd: 2024-08-08
gepubliceerd: 1926-09-18
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495453:mpeg21:a0015"
onderwerpen: [woonruimte, tekoop]
huisnummers: [36]
kop: "Jan Sonjéstraat Numero 26 a en b,"
---
De Notarissen

J.M. Slinkert en
P.J.M. Dolk

te Rotterdam,

zijn voornemens bij Voorl*opigen* en Definitieven
Afslag, op Woensdagen 6 en 13 October 1928,
's nam*iddags* 2
uur, in het Notarishuis aldaar, in
het openbaar te verkoopen:

Een pand, bestaande uit Benedenhuis,
ingericht tot Wasscherij
met daarbij behoorende Bovenwoning,
voorts gemetselde loods,
open Plaats, Vrij-Bovenhuis en
erve, te Rotterdam aan de

# **Jan Sonjéstraat** N*ummer* 26 a en b,

kad*astraal* bekend Gem*eente* Delfshaven Sectie D N*ummer* 529,
groot 1 are 2 centiaren.

Zijnde verhuurd: de Wasscherij
met Woning tot 31 Juli 1927 à
ƒ75.— en het Bovenhuis tot 31 Juli 1927
à ƒ40.—, beide p*er* maand of
totaal ƒ1380.— 's jaars.

Grondbel. ƒ58.70.

Straatbel. ƒ22.—.

Betaling kooppenningen 1 November 1926.

Te bezichtigen 4, 5 en 6 October 1926
van 11 tot 4 uur. Permissiebiljetten
en nadere inlichtingen te
bekomen ten kantore van voornoemde
Notarissen, Leuvehaven 335,
te Rotterdam.

46729 39
