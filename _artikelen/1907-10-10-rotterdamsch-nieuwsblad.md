---
toegevoegd: 2024-08-14
gepubliceerd: 1907-10-10
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196944:mpeg21:a0041"
onderwerpen: [werknemer]
huisnummers: [35]
achternamen: [moll]
kop: "Loopmeisje"
---
Een flink

# Loopmeisje

Wasch- en
Strijkinrichting A.J. Moll,
**Jan Sonjéstraat** N*ummer* 35.
