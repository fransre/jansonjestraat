---
toegevoegd: 2024-07-24
gepubliceerd: 1957-09-13
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164898065:mpeg21:a00067"
onderwerpen: [familiebericht]
achternamen: [leen]
kop: "Gouden feest"
---
# Gouden feest

Volgende week woensdag *18 september 1957*
is het feest in de **Jan Sonjéstraat**.
De heer en
mevrouw G. Leen zijn die
dag precies vijftig jaar getrouwd.
Het zijn eenvoudige
mensen, die niet van
veel drukte houden, maar
zo'n feest mag niet onvermeld
blijven. Dat zal het
ook vast niet. Van Rotterdammer
al vast hartelijk
gefeliciteerd.
