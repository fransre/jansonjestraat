---
toegevoegd: 2024-03-07
gepubliceerd: 1922-04-28
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494233:mpeg21:a0121"
onderwerpen: [werknemer, schilder]
huisnummers: [23]
achternamen: [koomans]
kop: "Schilders"
---
# Schilders

gevraagd door J. Koomans,
**Jan Sonjéstraat** 23b.

28597 6
