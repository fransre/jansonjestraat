---
toegevoegd: 2024-08-14
gepubliceerd: 1907-11-04
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196965:mpeg21:a0191"
onderwerpen: [werknemer]
huisnummers: [35]
achternamen: [moll]
kop: "Strijkster."
---
# Strijkster.

Terstond gevraagd een
aankomende Platgoedstrijkster
bij F.J. Moll
**Jan Sonjéstraat** 35.
