---
toegevoegd: 2024-08-13
gepubliceerd: 1920-06-15
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000198515:mpeg21:a0019"
onderwerpen: [diefstal]
kop: "Diefstallen."
---
# Diefstallen.

Ten nadeele van den roomijsfabrikant A.C. H.,
uit de **Jan Sonjéstraat**, is door zekeren venter,
W. v*an* L., een bedrag van ƒ11 benevens een
koperen bel gestolen.
