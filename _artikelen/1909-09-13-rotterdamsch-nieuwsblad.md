---
toegevoegd: 2024-08-14
gepubliceerd: 1909-09-13
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197988:mpeg21:a0082"
onderwerpen: [huisraad, tekoop]
huisnummers: [33]
kop: "Te Koop:"
---
# Te Koop:

twee Petroleumkachels,
zoo goed als nieuw en
volkomen reukloos. **Jan Sonjéstraat** 33,
Bovenh*uis*.
