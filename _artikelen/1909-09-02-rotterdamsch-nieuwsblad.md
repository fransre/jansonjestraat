---
toegevoegd: 2024-08-14
gepubliceerd: 1909-09-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197979:mpeg21:a0094"
onderwerpen: [werknemer]
huisnummers: [20]
achternamen: [paulus]
kop: "Strijkster."
---
Wasch- en Strijkinrichting,
Mejuffr*ouw* Paulus,
**Jan Sonjéstraat** 20A.
Gevraagd voor terstond
een aank*omend* Platgoed- en
Leerling-

# Strijkster.

Aan hetzelfde Adres bestaat
gelegenheid voor
Dames grondig onderwijs
te ontvangen in
glansstrijken en kunnen
nog eenige fijne
Wasschen geplaatst worden.
Het beste Adres
voor Gordijnen te wasschen.
