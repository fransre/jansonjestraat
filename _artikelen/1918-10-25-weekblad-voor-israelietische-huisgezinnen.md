---
toegevoegd: 2024-05-15
gepubliceerd: 1918-10-25
decennium: 1910-1919
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005421043:00001"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [bosman]
kop: "Alida Bosman en Alex Frank."
---
In plaats van Kaarten.

Verloofd:

# Alida Bosman en Alex Frank.

Rotterdam, **Jan Sonjéstraat** 24.

Nijmegen, Lange Hezelstraat 34.

19 October 1918.

י״ג חשון תרע״ט
*13 Cheshvan 5679*
