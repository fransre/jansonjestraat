---
toegevoegd: 2024-08-03
gepubliceerd: 1936-08-25
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164684060:mpeg21:a00216"
onderwerpen: [woonruimte, tehuur]
huisnummers: [35]
kop: "Te huur:"
---
# Te huur:

Benedenhuis, **Jan Sonjéstraat** 35b.
Bevragen:
**Jan Sonjéstraat** 35a, bovenhuis.
