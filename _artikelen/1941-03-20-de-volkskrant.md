---
toegevoegd: 2021-04-29
gepubliceerd: 1941-03-20
decennium: 1940-1949
bron: De Volkskrant
externelink: "https://resolver.kb.nl/resolve?urn=ABCDDD:010843975:mpeg21:a0055"
onderwerpen: [diefstal]
kop: "Thee gestolen"
---
# Thee gestolen

Inbrekers hebben zich in de nacht van
17 op 18 Maart *1941* toegang verschaft tot een
pakhuis aan de **Jan Sonjéstraat**, toebehorende
aan J.H.G. uit Overschie. Er werd
niet minder dan 50 k*ilo*g*ram* thee meegenomen.
