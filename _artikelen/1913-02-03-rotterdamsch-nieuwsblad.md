---
toegevoegd: 2024-08-14
gepubliceerd: 1913-02-03
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010222375:mpeg21:a0167"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [14]
kop: "Kinderwagen,"
---
Te koop aangeb*oden* een

# Kinderwagen,

merk Brennabor, met
porseleinen Duwer,
slechts een paar maal
gebruikt. Te bezichtigen
**Jan Sonjéstraat** 14b.
