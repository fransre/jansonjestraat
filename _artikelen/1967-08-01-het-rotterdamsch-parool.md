---
toegevoegd: 2024-07-23
gepubliceerd: 1967-08-01
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259049027:mpeg21:a00071"
onderwerpen: [straat]
kop: "Gemeentewerken brengt straten op peil"
---
# Gemeentewerken brengt straten op peil

(Van een onzer verslaggeefsters)

Rotterdam, dinsdag *1 augustus 1967*. — De
dienst van gemeentewerken heeft de
bewoners van de Robert Fruinstraat,
Van der Poelstraat, Hendrick Sorchstraat,
Joost van Geelstraat, Jan Porcellistraat,
**Jan Sonjéstraat**, Bellevoystraat,
Jan van Vuchtstraat en
Schermlaan een brief geschreven.
waarin wordt meegedeeld, dat deze
straten weer op het voorgeschreven
peil zullen worden gebracht.

Men zal nog dit jaar met de werkzaamheden,
die ongeveer zes maanden
in beslag nemen, beginnen. In de brief
staat o*nder* a*ndere*, dat de noodzakelijke werkzaamheden
aan de bedrijfsleidingen
worden verricht en dat er nieuwe rioleringen
worden gelegd. Elke leiding
moet worden vernieuwd of op hoogte
gebracht. Nadat deze werkzaamheden
zijn voltooid, zullen de trottoirs weer
op hoogte worden gebracht
De bewoners worden verzocht, indien
zich belangrijke famillegebeurtenissen
voordoen (b*ij*v*oorbeeld* huwelijk of
sterfgeval) tijdig contact op te nemen
met de heer J.J. van Veen, te bereiken
in de directiekeet op het werk,
zodat de woningen in elk geval zo
goed mogelijk bereikbaar zullen zijn.
