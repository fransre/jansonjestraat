---
toegevoegd: 2024-08-03
gepubliceerd: 1936-09-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164685014:mpeg21:a00265"
onderwerpen: [fiets, motor, tekoop]
huisnummers: [10]
kop: "Te koop:"
---
# Te koop:

1 Herenrijwiel met Torpedonaaf,
1 licht Motorrijwiel,
onder 60 k*ilo*g*ram*
merk Gazelle met J.L.O.-motor.
**Jan Sonjéstraat** 10a.
