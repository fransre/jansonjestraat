---
toegevoegd: 2024-08-03
gepubliceerd: 1936-09-24
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164685021:mpeg21:a00089"
onderwerpen: [familiebericht]
huisnummers: [26]
achternamen: [beversluis, mager]
kop: "Pieter Johannes Beversluis,"
---
Heden overleed na een langdurig
lijden onze beste Stiefvader en
Grootvader, de Heer

# Pieter Johannes Beversluis,

Wed*uw*n*aa*r van L.M. Reimus.

in den ouderdom van ruim 84
jaren.

H. Mager.

J.C. Mager-Knöps.

a*an* b*oord* s*toom*s*chip* „Beverwijk”: H.P.J. Mager.

J.G. Mager.

L.M. Mager.

P.J.H. Mager.

Rotterdam, 23 September 1936.

**Jan Sonjéstraat** 26.

De teraardebestelling zal plaats
hebben Zaterdag 26 Sept*ember* *1936* a*an*s*taande* op
de Alg*emene* Begraafplaats te Vreeswijk
te 2.30 uur.

Vertrek van het sterfhuis 12 uur.

31062 25
