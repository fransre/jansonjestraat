---
toegevoegd: 2023-01-10
gepubliceerd: 1999-07-03
decennium: 1990-1999
bron: Rotterdams Dagblad
externelink: "https://www.jansonjestraat.nl/originelen/1999-07-03-rotterdams-dagblad-origineel.jpg"
onderwerpen: [eeuwfeest]
achternamen: [gelton, ladage]
kop: "Opsporing verzocht"
---
# Opsporing verzocht

De **Jan Sonjéstraat**, vernoemd naar de landschapschilder Johannes Gabrielszoon Sonjé (1625-1707),
bestaat op 7 maart 2000
een eeuw. De voorbereidingen voor een feest zijn in volle
gang door de straatgroep Jan Sonjé. Daarnaast stelt men een
boekje samen, waarvoor men speurt naar mensen die verhalen
over de straat kunnen vertellen of oude foto's in bruikleen willen
geven. Voorts worden gegevens gezocht over een danssalon
die zich in de straat bevond alsmede de koffiebranderij van
Gelton, een machinale brei-inrichting van Ladage en nog veel
meer. ~~T. de Does is de inzender van de bijgaande foto van kinderen
van de Putsebocht in Vreewijk. Niet bekend is in welk
jaar deze is gemaakt, maar afgaande op de kleding en de autostep
met luchtbanden zal dat in de jaren vijftig zijn geweest.
De Does is nieuwsgierig naar de namen van de kinderen en ook
naar anekdotes. P.J. Rijnhout zoekt foto's van de Gouvernedwarsstraat
en het pand Gouvernestraat 2, het huis waarin onder
anderen zijn broer, Rigardus Rijnhout (beter bekend als de
Reus van Rotterdam) heeft gewoond. Voorts zoekt hij medeleerlingen
die in de Gaffeldwarsstraat in de klas hebben gezeten
bij juffrouw Vermeulen of de meesters Van Gerdingen en de Bruin.
Reacties uitsluitend schriftelijk aan ‘Rotterdam van toen’.~~

~~Foto collectie T. de Does~~
