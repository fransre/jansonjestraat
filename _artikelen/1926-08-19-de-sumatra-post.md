---
toegevoegd: 2024-08-08
gepubliceerd: 1926-08-19
decennium: 1920-1929
bron: De Sumatra post
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010359679:mpeg21:a0139"
onderwerpen: [ongeval, motor, tram]
kop: "Gevolgen van woest rijden."
---
# Gevolgen van woest rijden.

Op een avond te tien uur is op de Westkruiskade,
nabij den Westersingel een vrij
ernstig motorongeluk gebeurd. De 18-jarige
H.L.W. H. uit de **Jan Sonjéstraat** reed met
zijn motor in de richting van den Coolsingel.
Op de duo zat de 16-jarige J. de M.
uit de Hooidrift. H. reed met groote
snelheid. Op den Westersingei gekomen
kwam juist een motorwagen van lijn 4 aan
gereden. De motorrijder wist niet wat hij
doen moest en wegens zijn groote snelheid
was hij ook niet in staat tijdig te
remmen, met het gevolg, dat hij in volle
vaart tegen de tram reed. Beide jongemannen
bekwamen wonden aan hoofd en beenen.
Zij werden door den G*emeentelijke* G*eneeskundige* *en* G*ezondheidsdienst* naar het
ziekenhuis aan den Coolsingel gebracht
waar zij ter verpleging zijn opgenomen.
Het ongeval is te wijten aan het woeste
en roekelooze rijden van H., die dien keer
voor het eerst motor reed.
