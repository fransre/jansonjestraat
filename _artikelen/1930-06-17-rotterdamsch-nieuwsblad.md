---
toegevoegd: 2024-08-06
gepubliceerd: 1930-06-17
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002110:mpeg21:a0088"
onderwerpen: [woonruimte, tehuur]
huisnummers: [23]
kop: "Aangeboden:"
---
# Aangeboden:

voor net Jongmensch
Kost en Inwoning, vrije
Slaapkamer, Gezin zonder Kinderen.
**Jan Sonjéstraat** 23a.
