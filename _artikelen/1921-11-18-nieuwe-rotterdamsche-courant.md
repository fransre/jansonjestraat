---
toegevoegd: 2024-08-12
gepubliceerd: 1921-11-18
decennium: 1920-1929
bron: Nieuwe Rotterdamsche Courant
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010025774:mpeg21:a0164"
onderwerpen: [familiebericht]
huisnummers: [46]
achternamen: [van-loon]
kop: "Jan Willem,"
---
Geboren:

# Jan Willem,

Zoon van A. van Loon en A.H. v*an* Loon-Marchant.

Rotterdam, 17 November 1921.

**Jan Sonjéstraat** 46.

68627.6
