---
toegevoegd: 2024-08-05
gepubliceerd: 1932-09-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164661015:mpeg21:a00063"
onderwerpen: [auto, tekoop]
huisnummers: [2]
achternamen: [smit]
kop: "Chevrolet 1930,"
---
# Chevrolet 1930,

kleur beige, zeer goed
in orde, direct van Particulier,
zeer billijk. J. Smit,
**Jan Sonjéstraat** 2,
Tel*efoon* 31600, na zes uur
N*ummer* 43339.
