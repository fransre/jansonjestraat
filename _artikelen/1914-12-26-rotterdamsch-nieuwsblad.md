---
toegevoegd: 2024-08-13
gepubliceerd: 1914-12-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295817:mpeg21:a0180"
onderwerpen: [woonruimte, gevraagd]
huisnummers: [15]
achternamen: [geens]
kop: "Gevraagd:"
---
# Gevraagd:

gemeubeld Kwartier, 3
Bedden, 1 Zitplaats en
Keuken met prijsopgave.
Geens, **Jan Sonjéstraat** 15b.
