---
toegevoegd: 2024-08-14
gepubliceerd: 1912-04-23
decennium: 1910-1919
bron: Het vaderland
externelink: "https://resolver.kb.nl/resolve?urn=MMKB23:001493053:mpeg21:a00105"
onderwerpen: [reclame]
huisnummers: [22]
achternamen: [nolles]
kop: "Goochelen!!"
---
# Goochelen!!

Moderne goochelkunst! Fraaie apparaten! Poppenkastvertooningen,
beschaafd en zeer amusant. Eerste klasse bioscope voor salon en zaal.

Henri Nolles,

Feestarrangeur Werkzaam voor de eerste families van om land.

**Jan Sonjéstraat** 22 — Rotterdam.

Avis!! Bioscope-vertooningen in huis, zeer billijk! Zonder gevaar! Zeer amusant!
