---
toegevoegd: 2023-08-01
gepubliceerd: 1929-05-13
decennium: 1920-1929
bron: De avondpost
externelink: "https://resolver.kb.nl/resolve?urn=MMKB27:017910069:mpeg21:a00068"
onderwerpen: [diefstal]
kop: "Vlucht over de daken verijdeld."
---
# Vlucht over de daken verijdeld.

Een nachtwaker van een particulieren
nachtveiligheidsdienst te Rotterdam hoorde
Zaterdagnacht *11 mei 1929* gerucht in de pettenfabriek
van de firma W*olf &* N*orden* in de
Middellandstraat aldaar. Hij waarschuwde
onmiddellijk de politie, die een
afzetting formeerde rondom het gebouw.
Daarna ging zij het gebouw binnen,
waar zij in het kantoor twee inbrekers
op heeterdaad betrapte bij het
forceeren van de brandkast. De twee
mannen vluchtten naar de daken, vanwaar
zij poogden door een pand in de
**Jan Sonjéstraat** op straat te komen.
Zij stuitten echter op de politie-afzetting.
Een agent schoot in de lucht, waarop
de beide inbrekers bleven staan en
gearresteerd konden worden. Het zijn de
gebr*oeders* K., 31- en 37 jaar oud, beiden bekende
inbrekers. Eén van hen is eerst
kort geleden uit de gevangenis ontslagen.
In het kantoor is alles overhoop gebaald.
De brandkast was echter niet geopend.
Een groote hoeveelheid inbrekerswerktuigen
werd gevonden.
