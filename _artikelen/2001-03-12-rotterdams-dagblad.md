---
toegevoegd: 2023-01-11
gepubliceerd: 2001-03-12
decennium: 2000-2009
bron: Rotterdams Dagblad
externelink: "https://www.jansonjestraat.nl/originelen/2001-03-12-rotterdams-dagblad-origineel.jpg"
onderwerpen: [eeuwfeest]
huisnummers: [36]
achternamen: [van-hooijdonk, acioz, van-der-ploeg, gelderblom, lutgerink]
kop: "Jubileum Sonjéstraat voor het nageslacht bewaard"
---
# Jubileum **Sonjéstraat** voor het nageslacht bewaard

Rotterdam — Ongeveer zestig bewoners
en oud-bewoners van de
**Jan Sonjéstraat** hebben zaterdag *10 maart 2001*
het boekje waarmee zij het honderdjarig
bestaan van hun straat
afgelopen jaar hebben gevierd, ingemetseld
in de muur van het
pand 36a. Het boekje werd in een
plastic koker gestopt, waarna de
koker in een special daarvoor in
de muur geboord gat werd geschoven.
Daarna werd het gat
dichtgemetseld en voorzien van
en gedenkplaat van messing en
koper. Op nummer 36a woont de
aannemer Acioz. Afgesproken is
dat wanneer hij ooit het pand verkoopt,
in de verkoopacte wordt
opgenomen dat het kostbare kleinood,
dat de herinnering aan het
honderdjarig bestaan voor het
verre nageslacht moet waarborgen,
in de muur verborgen zit. Zo
kan er ook in de toekomst rekening
worden gehouden met wat er
achter de gedenkplaat verborgen
gaat.
De festiviteiten voor het honderdjarig
bestaan van de **Jan Sonjéstraat**
in de wijk Middelland hebben
een jaar geduurd. Het inmetselen
van het boekje markeerde
het einde van een jaar vol feestelijkheden
en festiviteiten in deze
trotse zijstraat van de Middellandstraat,
die ruim een eeuw geleden
genoemd werd naar de
landschapsschilder Jan Gabriël
Sonjé.

## Voorbeeld

De bewoners van de **Jan Sonjéstraat**
zijn er trots op dat zij wonen
waar ze wonen, en dat willen ze
weten ook. „Het zijn mensen die
op een fantastische wijze met hun
woonstraat bezig zijn,” zei de
Delfshavense deelraadsvoorzitter
Harreman een jaar geleden toen
de festiviteiten van start gingen.
Hij roemde de bewoners om de
hechte banden die zij met elkaar
hebben en noemde de **Jan Sonjéstraat**
een voorbeeld voor de gehele
deelgemeente.
Een speciale rol was er zaterdagmiddag *10 maart 2001*
weggelegd voor de oudste
bewoner van de straat, de 98-jarige
J. van Hooijdonk, die de eer te
beurt viel de gedenksteen te mogen
onthullen. Van Hooijdonk
woont al zeventig jaar in de straat
en is daarmee het geheugen en
het geweten van de straat. Veel
van zijn herinneringen en van het
fotomateriaal dat hij in de loop
der tijden heeft verzameld, is terecht
gekomen in het jubileumboek
dat fotograaf Jan van der
Ploeg en historicus Oscar Gelderblom
over de straat hebben gemaakt.
„Dit is een goede gelegenheid om
elkaar weer eens te ontmoeten,
ook de mensen die hier vroeger
hebben gewoond,”
zei mede-organisator
A. Lutgerink. „We willen
graag laten zien dat we een goede
buurt zijn, dat de gezelligheid
hier op een hoog peil staat.”

*Onderschrift bij de foto*
Feest in de **Jan Sonjéstraat**. In rijm melden de bewoners er heel tevreden te zijn. Links de 98-jarige J. van Hooijdonk. Foto Dick Sluijter
