---
toegevoegd: 2024-08-05
gepubliceerd: 1933-01-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164663008:mpeg21:a00137"
onderwerpen: [bedrijfsinformatie]
huisnummers: [4, 17]
kop: "Jan Sonjéstraat Numeros 4a-b en 17a-b,"
---
Notarissen P.W.M. Esser,
te Rotterdam, en
H.J.J. Verhoeff,
te Avenhorn,

zullen bij voorl*opige* en defin*itieve* Afslag op
Woensdagen 11 en 18 Januari 1933,
's nam*iddags* 2 uur in het Notarishuis aan
de Gelderschekade te Rotterdam, in
het openbaar verkoopen:

I. Het pand, bestaande uit Winkel
met Magazijnruimte en Bovenwoning,
vrij Bovenhuis en Erf, te
Rotterdam, aan de

Eerste Middellandstraat N*ummer* 98a-b,

groot 111 c*enti*A*ren*.

Verhuurd: Winkel met gedeeltelijke
Magazijnruimte, voor ƒ30.— per
week; Bovenwoning voor ƒ30.— en
vrij Bovenhuis voor ƒ50.— per m*aa*nd,
totaal ƒ2520.— p*er* j*aar*; Magazijnruimte
gedeeltelijk in eigen gebruik.

II & III. Twee panden, bestaande
elk uit Garage met vrij Bovenhuis,
open Plaats en Erf, te Rotterdam,
aan de

# **Jan Sonjéstraat** N*ummer*s 4a-b en 17a-b,

resp*ectievelijk* groot 103 en 81 c*enti*A*ren*.

Verhuurd: Bovenhuis N*ummer* 4 voor
ƒ5.— per week met contract; Bovenhuis
N*ummer* 17 voor ƒ40.— per m*aa*nd;
de Garages in eigen gebruik.
