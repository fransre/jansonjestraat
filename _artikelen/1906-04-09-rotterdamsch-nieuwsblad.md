---
toegevoegd: 2024-08-14
gepubliceerd: 1906-04-09
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213763:mpeg21:a0217"
onderwerpen: [werknemer]
huisnummers: [37]
achternamen: [burggraaf]
kop: "Tuinman."
---
# Tuinman.

Gevraagd een Halfwas,
v*an* g*oede* g*etuigen* v*oorzien*. Zich aan te
melden bij A. Burggraaf,
**Jan Sonjéstraat** 37, Rotterdam.
