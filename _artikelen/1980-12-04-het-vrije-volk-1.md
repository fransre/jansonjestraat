---
toegevoegd: 2023-03-07
gepubliceerd: 1980-12-04
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960694:mpeg21:a0486"
onderwerpen: [bulldog]
kop: "‘De methadonbus is stervensbegeleiding’"
---
Bulldog vangt jeugdprostituées op

# ‘De methadonbus is stervensbegeleiding’

Van een onzer verslaggeefsters

Rotterdam — „Zonder erbij na te denken steekt
zij de nieuwe sigaret op die ze zojuist van een vriendje
heeft gekregen. Na een poosje verdwijnen de zorgen en
komt er een ongekend vredig gevoel Voor in de plaats.
Hé, kan ze nog net uitbrengen. Heb jij iets in mijn sigaret
gedaan?

Jimmy buigt zich over haar heen. Ja meisje, een
beetje „smack”. Maar, maak jezelf geen zorgen. Laat
dat maar aan Jimmy over. Geniet maar van dat heerlijke
gevoel dat je in heaven bent.

Na anderhalve week is het
meisje compleet verslaafd aan
de heroine. Haar „lieve vriend
Jimmy” ondergaat een totale
gedaanteverwisseling. Hij is nu
Jimmy de pooier, dealer annex
gebruiker.

Hij laat het meisje gedwongen
de hoer spelen om zijn en haar
heroinegebruik te bekostigen.
Als ze weigert, kan ze klappen
krijgen. Als ze niet genoeg geld
binnenbrengt, dan krijgt ze geen
heroine. Ze heeft er over gedacht
om terug te gaan naar
haar ouders, maar ze durft niet.
Ze wil haar ouders geen schande
aan doen en bovendien zou Jimmy
haar nooit laten gaan!”

Dit jonge meisje is een van de
vele heroinehoertjes die Rotterdam
telt. Ze is terechtgekomen
in De Bulldog.

Deze stichting heeft aan de
's-Gravendijkwal 161 een pand
voor de opvang van jonge verslaafde
meisjes. Het opvang-annex
open jongerencentrum
heeft begin november *1980* zijn deuren
hier geopend.

## Etage

Daarvoor moest De Bulldog
het doen met een gehuurde etage
in de **Jan Sonjéstraat** die zo
slecht was dat aan opvang weinig
kon worden gedaan.

De Bulldog probeert deze
groep meisjes en jongens een
eigen plekje te geven. Het centrum
is dag en nacht open. Er
kan koffie gedronken, gepraat,
gewassen, geslapen of uitgehuild
worden.

Rinus van Klaveren van De Bulldog
is destijds als speeltuinwerker
in aanraking gekomen
met jongeren die „buiten de
boot vielen” en van het een in
het ander gekomen. Hij is nu al
zo'n vijf jaar bezig met de opvang
van drugsverslaafden en
jeugdprostituées. Ze weer
drugs-vrij krijgen gaat altijd op
basis van vrijwilligheid.

In de drie grote steden zijn
naar schatting 800 aan heroine
verslaafde jeugdprostituées.
Speciale opvang is er niet voor
ze. In kindertehuizen worden ze
niet geplaatst uit angst dat anderen
ook aan de heroine geraken.

Om wildgroei tegen te gaan,
wil de overheid verslaafde
meisjes tot achttien jaar die nu
in leven blijven door te hoereren,
gedwongen opnemen in een
kliniek in Loosduinen.

Rinus van Klaveren noemt
het opsluiten van de heroinehoertjes
ronduit belachelijk.

## Zinloos

„Een gedwongen opname is
zinloos. Ze worden daar alleen
maar gedrild. Eenmaal vrij
gaan ze meteen in Rotterdam
weer de hoer spelen. En bovendien
vind ik het een gevaarlijke
ontwikkeling. Nu zijn het jonge,
verslaafde meisjes, maar burgemeester
Van der Louw gebruikte
laatst al de term „zwaar verslaafden”.
Dat zou in kunnen
houden dat de weg geopend is
om alle verslaafden op te pakken.
En de alcoholisten én iedereen
die zich afwijkend gedraagt.
Iedere deskundige weet toch dat
instituten als Rekken waar kinderen
gedwongen worden opgenomen,
niet werken.”

De Bulldog zit expres dicht bij
„het wereldje”. De jeugdprostituées
vinden hier een huiselijke
sfeer. Er is geen harde lijn zoals
in een kliniek. Ze hoeven ook
niet gemotiveerd te zijn, maar
als ze het even niet meer zien
zitten dan is De Bulldog er. Eten,
zich wassen of slapen.

Zijn ze geslagen door een
pooier of een dealer, dan kunnen
ze bij ons terecht. Door die
lage drempel, blijven ze komen.
Lichamelijk zijn ze er vaak belabberd
aan toe, maar het eerste
dat de hulpverleners doen is de
meisjes proberen tot rust te laten
komen en binnen te houden
zodat ze kunnen wennen aan
een normale dagindeling. Contact
met ouders kan ook weer
gemaakt worden.

Honderd procent resultaat
boekt de Bulldog niet. Hoogstens
tien van de honderd meisjes
„redt” het en wordt heroinevrij.
„Natuurlijk nemen velen
weer de benen, maar ze komen
toch steeds terug en proberen
het opnieuw.”

„Wat wij doen is niet méér
uitzichtloos als wat er aan opvang
is bij de stichting Alcohol
en drugs of de afkickcentra.

Als de meisjes en jongens dat
afkickprogramma gemist hebben
of niet meer mee mogen
doen omdat zij bijvoorbeeld drie
keer weer „gebruikt” hebben,
dan kunnen ze nog naar De Bulldog.
We praten pas over afkicken
als ze tot rust gekomen
zijn.”

„Wij zijn al lang op deze wijze
bezig met het helpen van deze
groep kinderen die nu plotseling
maar opgesloten moeten worden
of ze willen of niet, omdat ‘er
geen andere mogelijkheid is’.

In die kliniek in Loosduinen
komt twintig man personeel en
daar is voor de begeleiding van
één patiënt 400 gulden per dag
uitgetrokken. Rotterdam en Den Haag
willen de kliniek volgend
jaar openen. Maar ons werk
wordt bij de lancering van dit
plan doodgezwegen.”

Waarom?

„Omdat men tegenover derden
niet kan verkopen dat met
weinig mankracht en geld De Bulldog
draait. En dat terwijl
uit het hele land mensen komen
om te zien hoe wij werken en
hulp verlenen.

Weet je wat het is,” zegt Rinus
berustend, „wij vormen een experiment.
Wij heten alternatief,
maar het is natuurlijk lariekoek
om alternatief altijd te associëren
met weinig geld en weinig
mankracht.

De Bulldog is een noodzaak.
Je moet juist beginnen met de
ongemotiveerde heroinehoertjes
een plek te geven waar ze
terecht komen. Hoewel het natuurlijk
te gek is dat wij met die
kinderen nog geen uitstapje
kunnen maken, omdat er geen
geld voor is. Ik neem er nu drie
mee in mijn eigen wagen voor
een uitje.”

De Bulldog krijgt een vergoeding
van 25 gulden per cliënt,
daarvan gaat ƒ7,50 als zakgeld
haar de verslaafde. „Het eten alleen
al kost ƒ12,50 per persoon,
dan blijft er nog vier gulden
over voor onderhoud aan het
pand. Onze spullen zijn allemaal
tweedehands. Vorig jaar wilden
we in een pand een begeleid kamerproject
beginnen. Het is afgeketst
op de huur. Driehonderd
in de maand was te duur.”

## Lapmiddel

„Laten ze De Bulldog in Rotterdam
een eerlijke, kans geven,”
zegt Van Klaveren. „Het is
natuurlijk een lapmiddel en
zonder geld voor de hulpverleners
zwaar werk. Velen zijn
overspannen weggegaan. Wat
wil je ook als je ieder dubbeltje
moet omkeren voor je iets aanschaft!”

In plaats van een gedwongen
opname in de kliniek zijn de
Bulldogwerkers voorstander
van een reorganisatie in de kindertehuizen
en de tehuizen van
de Voogdij en Kinderbescherming.

„Als kinderen weglopen uit
tehuizen of van ouders, dan is er
een gapend gat. Er is nauwelijks
opvang. De kindertehuizen
moeten anders gaan werken. Nu
worden de kinderen altijd in
een hoek gedreven: ze zwerven
en waar ze dan aan bloot staan!

Waarom zou je in kindertehuizen
niet evenveel personeelsleden
hebben als kinderen?
Daarmee zou je al een hoop ellende
besparen en veel weglopen
voorkomen.”

Methadon als afkickmogelijkheid
voor heroine wijzen de
stafleden van De Bulldog af.
‘Methadon was het heilige middel,
maar in feite is het een dubbele
verslaving want het maakt
afhankelijk. Het gaat bovendien
niet alleen om de methadon; er
is ook nog de verslaving aan het
ritueel: het spuiten.’

## Afgeschreven

Er zijn er genoeg die op de
Baan methadon halen en 's
avonds heroine spuiten. De methadonbus
is een soort stervensbegeleiding.
Wie naar de bus
gaat, wordt in feite door de instellingen
afgeschreven. Het is
toch waanzin om een kind van
vijftien jaar af te schrijven?

Kritiek op de strenge afkickprogramma's
is er ook bij de
Bulldog. „Als je drie keer heroine
gebruikt hebt, mag je niet
meer meedoen. Die kinderen
zijn dan weer vogelvrij. Maar
een arts zegt tegen een kankerpatiënt
toch ook niet: „Ik behandel
u niet meer als ik zie dat
u één sigaretje heeft gerookt.””

*Onderschrift bij de foto*
Rinus van Klaveren:
„Gedwongen
opname
heroinehoertjes
is zinloos”.
