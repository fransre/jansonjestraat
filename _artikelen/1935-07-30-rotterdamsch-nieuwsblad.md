---
toegevoegd: 2024-08-03
gepubliceerd: 1935-07-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164678034:mpeg21:a00167"
onderwerpen: [auto, tekoop]
huisnummers: [20]
kop: "Opeltje 1930"
---
# Opeltje *19*30

Coach 4-P., 2 m.w.b.
prima ƒ175. Stalen Motorboot.
Kajuit orig*ineel*.
Bootmotor, goed loopend
ƒ160. **Jan Sonjéstraat** 20a.
