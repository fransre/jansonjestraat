---
toegevoegd: 2024-05-19
gepubliceerd: 1920-03-02
decennium: 1920-1929
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1920-03-02/edition/0/page/4"
onderwerpen: [familiebericht]
huisnummers: [34]
achternamen: [bosselaar]
kop: "Margaretha Johanna Bosselaar"
---
Eenige en algemeene
kennisgeving.

Heden overleed, zacht en kalm,
na een langdurig, zeer geduldig
gedragen lijden, onze innig geliefde
Dochter, Zuster en Schoonzuster

# Margaretha Johanna Bosselaar,

in den ouderdom van 30 jaar.

Wed*uwe* A. Bosselaar-v*an* d*er* Linden

A. Bosselaar.

J.C. Noordzij-Bosselaar.

A. Noordzij J*unio*r.

Geen bloemen.

Verzoeke van rouwbeklag verschoond
te blijven.

Rotterdam, 2 Maart 1920.

**Jan Sonjéstraat** 34b.

1555 27
