---
toegevoegd: 2024-08-13
gepubliceerd: 1913-06-21
decennium: 1910-1919
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000170634:mpeg21:a0013"
onderwerpen: [faillissement]
huisnummers: [20]
achternamen: [van-dulken]
kop: "Gerechtelijke aankondigingen."
---
# Gerechtelijke aankondigingen.

Bij vonnis der arrondissements-rechtbank te Rotterdam, van
18 Juni 1913, is het faillissement van J.M. van Dulken, reiziger,
wonende te Rotterdam aan de **Jan Sonjéstraat** n*ummer* 20a, opgeheven.

De Curator,

Mr. H.J.F. Heyman.

[Kosteloos.]

(3560)
