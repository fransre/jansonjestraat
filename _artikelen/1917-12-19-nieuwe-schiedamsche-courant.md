---
toegevoegd: 2024-05-20
gepubliceerd: 1917-12-19
decennium: 1910-1919
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1917-12-19/edition/0/page/3"
onderwerpen: [diefstal]
kop: "Uit Rotterdam."
---
# Uit Rotterdam.

In verband met de inbraak bij de gebr*oeder*s
Groosjohan, waar voor een waarde van ƒ19.000
aan zijden stoffen is gestolen en waarvoor reeds
3 personen zijn gearresteerd, zijn gister-avond *18 december 1917*
door de rechercheurs P. Vroomman en J. Kruivenhoorn
nog aangehouden de los-werkman E. v*an* W.,
zonder vaste woonplaats, de los-werkman
W. M., wonende Nadorststraat en zekere J. de M.,
wonende **Jan Sonjéstraat**.
