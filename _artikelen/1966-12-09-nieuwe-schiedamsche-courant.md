---
toegevoegd: 2021-04-30
gepubliceerd: 1966-12-09
decennium: 1960-1969
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1966-12-09/edition/0/page/10"
onderwerpen: [woonruimte, tekoop]
huisnummers: [5]
kop: "Flink dubbel woonhuis"
---
# Flink dubbel woonhuis

Gelegen aan **Jan Sonjéstraat** 5a en 5b te Rotterdam, netjes
bewoond. Huren ƒ2466,—. Lasten ƒ222,—. Prijs ƒ20.000,—, direct
van eigenaar, Molenstraat 26, Zundert, tel*efoon* (01696)2363.
