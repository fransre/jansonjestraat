---
toegevoegd: 2021-04-30
gepubliceerd: 1917-10-19
decennium: 1910-1919
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1917-10-19/edition/0/page/3"
onderwerpen: [diefstal]
kop: "In beslag genomen:"
---
# In beslag genomen:

In Rotterdam gaat men voort met het
in beslag nemen van voorraden.

Aangehouden is een sleeperswagen beladen
met 5 balen, 7 vaten en 3 kisten
stijfsel, 7 balen tabak, 22 kisten vermicelli,
2 kisten en 5 pakken lucifers. Deze
goederen waren afkomstig uit een pakhuis
in de **Jan Sonjéstraat** aldaar, waar men
nog opgeslagen vond 3 kisten slaolie, 26
doozen kwatta, 25 pakken condorzeep en
een doos zeep, toebehoorende aan een
koopman te Zevenbergen, alsmede 5 kisten
thee, 24 pakken vet, 3 kisten vet,
60 pakken zeeppoeder, 15 pakken condorzeep,
50 kisten gecondenseerde melk, 4½
baal karnemelkzeep, 310 pakken karnemelkzeep
en 2 kisten zeep, alles eigendoem van
een koopman te Rotterdam.

De voor de regeering in bezit genomen
goederen worden geschat en een bon voor
de waarde van het gemiddelde van twee
schattingen wordt aan den vroegeren houder
der waren gegeven.

De in bezit genomen goederen worden
ter beschikking der bevolking of bedrijven
gesteld, tegen prijzen, die de bepaalde bedragen
niet te boven gaan.
