---
toegevoegd: 2023-08-01
gepubliceerd: 1933-11-18
decennium: 1930-1939
bron: De Dordrechtsche Courant
externelink: "https://proxy.archieven.nl/0/0C5832890B6140469353168AE6F928AB"
onderwerpen: [werknemer]
huisnummers: [39]
achternamen: [dekkers]
kop: "Vestenvak."
---
# Vestenvak.

Gevraagd aankomende Machinestiksters
en eenige Leerlingen. S.A. Dekkers
**Jan Sonjéstraat** N*ummer* 39a, Rotterdam.

1717
