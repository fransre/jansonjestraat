---
toegevoegd: 2021-09-02
gepubliceerd: 1899-05-06
decennium: 1890-1899
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010177757:mpeg21:a0060"
onderwerpen: [straat]
kop: "Straten-aanleg in den Coolpolder."
---
# Straten-aanleg in den Coolpolder.

Burg*emeester* en Weth*ouders* hebben aan den Raad doen
toekomen een plan tot aanleg van straten
bewesten de Bellevoysstraat in samenwerking
met de heeren M. Zaaijer c*um* s*uis*. Deze straten
zullen worden aangelegd op een terrein in den
Coolpolder, dat, uit drie kampen land bestaande,
zich uitstrekt bewesten de aan de Bellevoysstraat
gelegen erven van den Ouden Binnenweg
tot aan de wetering, waarlangs
de Middellandstraat ontworpen is. Over den
middelsten kamp zal de Claes de Vrieselaan
worden doorgetrokken ter breedte van 30
meter. Verder worden doorgetrokken de
Schermlaan tot aan de Claes de Vrieselaan en
de Schietbaanlaan over alle drie de kampen
terwijl nog worden aangelegd een 20 meter
breede straat, aan welker verlengde het archiefterrein
uitkomen zal *(de latere Robert Fruinstraat)*. De Middellandstraat
zal door twee straten, resp*ectievelijk* 12 en 13
m*eter* breed, aan de Schermlaan verbonden worden
*(de latere Jan Porcellisstraat en **Jan Sonjéstraat**)*,
en de Schermlaan aan de Schietbaanlaan
door een straat van 15 m*eter* breed *(de latere Jan van Vuchtstraat)*. Eindelijk op
den westelijken kamp een straat van 15 m*eter*
van het ontworpen pleintje aan de verlenging
der Claes de Vrieselaan naar de Schietbaanlaan *(de latere Joost van Geelstraat)*.
Voor de uitvoering van dat plan is becijferd
een bedrag van ƒ137.700, te dragen
door de heeren Zaaijer c*um* s*uis* en ƒ19.700 te dragen
door de gemeente. Voor rekening van
eerstgenoemden komen dan alle kosten voor
zoover de straatbreedte niet grooter is dan
25 m*eter* en daaronder is ook begrepen de stratenaanleg
aan beide zijden van het plein tot
over 12½ m*eter* uit de rooiing. Voor rekening
der gemeente komt de aanleg over meer dan
25 m*eter* breedte en van het middengedeelte
van het plein, samen over p*lus*m*inus* 3950 centiaren.
Bovendien verzoeken de heeren Zaaijer c*um* s*uis*
voor die 3950 centiaren grond, die zij ter
wille van verkeersbelangen afstaan, een vergoeding
van ƒ1,50 per centiare. Burg*emeester* en
Weth*ouders* stellen naar aanleiding hiervan den
Raad voor, onder voorbehoud dat door het
polderbestuur de vereischte slootdempingen
worden toegestaan, medewerking te verleenen
tot de uitvoering van het hierboven omschreven
stratenplan en in beginsel te besluiten
tot de overneming der daartoe behoorende
straten; goed te keuren dat deze van gemeentewege
worden aangelegd tegen betaling door
de heeren Zaaijer c*um* s*uis* van ƒ137.700 en dat de
Gemeente voor den aanleg van p*lus*m*inus* 3950 centiaren
straat en plein bestede ƒ19.700 voorts
voor den afstand dier p*lus*m*inus* 3950 centiaren aan
de heeren Zaaijer c*um* s*uis* betale ƒ1,50 per centiare.
