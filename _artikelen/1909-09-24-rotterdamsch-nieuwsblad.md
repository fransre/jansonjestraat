---
toegevoegd: 2024-08-14
gepubliceerd: 1909-09-24
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197998:mpeg21:a0152"
onderwerpen: [huisraad, tekoop, pension]
huisnummers: [24]
kop: "Aangeboden"
---
# Aangeboden

op vrij Bovenhuis frissche
Zit- en Slaapkamer
aan de straat, voor net
Pensoon met vaste positie,
b*ezigheden* b*uitens*h*uis* h*ebbende*, met
degelijk Pension. **Jan Sonjéstraat** 24b,
Bovenhuis.
Tevens te koop een
drielichts Gaskroon, uitstekend
geschikt voor
Winkel of groote Kamer.
Br*ieven* fr*anco* N*ummer* 2071, Bur*eau* v*an* d*it*
Blad.
