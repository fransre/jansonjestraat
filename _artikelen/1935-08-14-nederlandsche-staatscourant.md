---
toegevoegd: 2024-08-03
gepubliceerd: 1935-08-14
decennium: 1930-1939
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000163612:mpeg21:a0006"
onderwerpen: [bedrijfsinformatie]
huisnummers: [6]
achternamen: [bosma]
kop: "Naamlooze vennootschappen."
---
# Naamlooze vennootschappen.

Krachtens besluit, genomen in de op 13 Augustus 1935 gehouden
algemeene vergadering van aandeelhouders der naamlooze
vennootschap: Bouwmaatschappij Adriaan Goekooplaan N.V.,
gevestigd te 's Gravenhage, is die vennootschap met ingang van
gemelden datum ontbonden, met aanwijzing van den ondergeteekende
als eenig liquidateur.

H.G. Bosma,

**Jan Sonjéstraat** 6, Rotterdam.

(3682/8)
