---
toegevoegd: 2024-08-12
gepubliceerd: 1921-05-10
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493740:mpeg21:a0066"
onderwerpen: [huisraad, tekoop]
huisnummers: [29]
kop: "verhuizing"
---
Wegens

# verhuizing

te koop massief eiken
Salon-ameublement
ƒ100. Te zien na 5 u*ur*
**Jan Sonjéstraat** 29b.
