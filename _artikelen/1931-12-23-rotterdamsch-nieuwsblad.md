---
toegevoegd: 2024-08-05
gepubliceerd: 1931-12-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164656055:mpeg21:a00117"
onderwerpen: [brand]
huisnummers: [29]
achternamen: [hoogmolen]
kop: "Brandjes."
---
# Brandjes.

Gasten van slangenwagen 28 bluschten
hedenmorgen *23 december 1931* kwart voor elf onder toezicht
van den brandspuitmeester, den
heer W.N. Bolk en van den brandmeester,
den heer L.J.A. van Dieten, met emmers
water een begin van brand in de
woning van M.H. Hoogmolen aan de
**Jan Sonjéstraat** 29, waar door fel stoken
brand was ontstaan aan een schoorsteenmantel.
