---
toegevoegd: 2024-07-23
gepubliceerd: 1961-07-06
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010953576:mpeg21:a0032"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [46]
achternamen: [woudenberg]
kop: "kostganger"
---
Nette

# kostganger

gevraagd.
huiselijk verkeer, goede verzorging.
Mevr*ouw* Woudenberg, **Jan Sonjéstraat** 46.
