---
toegevoegd: 2021-04-29
gepubliceerd: 2017-02-25
decennium: 2010-2019
bron: Algemeen Dagblad
externelink: "https://www.ad.nl/rotterdam/wijk-middelland-is-voorbeeld-voor-de-rest-van-rotterdam-br~a6c7d9d4/"
onderwerpen: [verkeer]
achternamen: [jager]
kop: "Wijk Middelland is voorbeeld voor de rest van Rotterdam"
---
# Wijk Middelland is voorbeeld voor de rest van Rotterdam

Reportage \| In de wijk Middelland in Rotterdam-Delfshaven voeren bewoners met de gemeente experimenten uit om hun buurt veiliger te maken, ook voor het verkeer. Het moet leiden tot een mooiere wijk, met misschien wel een ‘droomstraat’.

Nog niet eens zo lang geleden was Middelland onderdeel van het beruchte wilde westen van Rotterdam. Junks en hoeren bepaalden het straatbeeld, veel huizen waren dichtgetimmerd met luiken.

Alles bij elkaar was het gebied in Delfshaven bepaald geen aanbeveling om te gaan bezoeken, laat staan om er te komen wonen. De krant bracht deze week aan bezoekje aan Middelland.

De situatie van toen is inmiddels veranderd, vertellen Robert Jager en Dolf Klijn, bewoners van de wijk. Samen trekken zij nu aan een plan dat Middelland, en dan heel specifiek het gedeelte tussen de Middellandstraat en Mathenesserlaan en de 's-Gravendijkwal en Claes de Vrieselaan, moet opknappen.

De vorig jaar november *2016* begonnen proef is gedoopt tot Veilig, veiliger Middelland. „We zijn begonnen met drie bezorgde vaders. Eerst in twee straten, nu al in twaalf”, vertelt Jager, die zelf in de **Jan Sonjéstraat** woont.

## Verkeersveiligheid

Hij kreeg al gauw steun van buurman Dolf Klijn uit de Jan Porcellisstraat. Met een bijdrage van bijna 40.000 euro uit het gemeentelijke City Lab 010-potje besloten zij de verkeersveiligheid in de wijk aan te pakken. In juni *2017* moet er een eindrapport op tafel liggen.

„Eigenlijk komt het idee uit Antwerpen”, bekent Jager. „Daar bestaat het fenomeen Droomstraat al langer. Het houdt in dat je een straat voor een paar dagen afsluit om er leuke dingen te doen.”

Een eerste inventarisatie leverde maar liefst honderd ideeën op, diept Dolf Klijn op. „Natuurlijk was niet alles uitvoerbaar. We hebben tien experimenten over, daarmee zijn we aan de slag gegaan.”

Aan de hand van metingen moet blijken of auto's minder hard rijden. Daarnaast zullen enquêtes duidelijk dienen te maken of maatregelen helpen of juist hinder opleveren.

Dus gaat in het Middelland vandaag de dag over drempels, extra borden (ook van Dick Bruna), as-verspringingen van de straten en belemmeringen in de vorm van bloembakken. Met al deze maatregelen moet de wijk veiliger voor het verkeer worden.

Ook is het idee dat bezoekers ‘van buiten’ ontmoedigd worden om hun auto in de buurt te parkeren. „Het is soms ook wennen hoor, niet alleen voor ons, ook voor de gemeente”, gaat Jager verder. Dat merkten de twee toen het schrappen van zes parkeerplaatsen in de wijk niet overal in goede aarde viel.

## Betaald parkeren

„We hebben hier al betaald parkeren, ook is het gebied een 30 km-zone”, legt hij uit over de verkeerssituatie. De tijden dat er betaald moet worden voor het stallen van een wagen, liggen nu tussen 09.00 en 18.00 uur. „Dat is anders geweest, het liefst zouden we tot 21.00 uur hebben”, vult Klijn aan.

Als doel hebben ze gesteld dat de pakweg duizend bewoners zich veilig voelen. „In een Droomstraat kunnen kinderen spelen, kunnen ouderen veilig oversteken en heb je geen overlast van verkeer.”

De plannenmakers Jager en Klijn werden begin dit jaar verrast door interesse van de gebiedscommissie Prins Alexander, dat al enige tijd kampt met parkeerproblemen in de Kruidenbuurt. Omdat bewoners in Ommoord betaald parkeren afwijzen, ging het wijkbestuur op zoek naar andere ideeën. Zo stuitte Prins Alexander (Oost) op het experiment in Middelland in West, de andere kant van de stad.

Jager en Klijn zijn vereerd met de belangstelling uit Prins Alexander. „Het zou mooi zijn als dit experiment leerstof oplevert die overal in de stad gebruikt kan worden, of het nu oost, west, zuid of pakweg Pernis is.” Ze roepen alle bewoners in Middelland op mee te denken en met voorstellen te komen.

Ze hebben wel een tip voor Ommoord. „Bekijk de problemen goed per straat en let op de verkeersstromen. Er is echt maatwerk nodig.”

De twee Middellanders hebben gemerkt dat de gemeente Rotterdam en zeker ook burgemeester Ahmed Aboutaleb er veel aan gelegen is de pilot te laten slagen. „Aboutaleb ziet dit gebied als een proefvijver.”

De tijd van de junks, hoeren en later ook van de Poolse en Bulgaarse nummerborden is voorbij, Middelland is al lang geen no go area meer, stellen de bewoners.

„Het is nu juist een opkomende buurt”, durft Dolf Klijn wel aan. „Ik woonde in Utrecht, had eerst zo mijn vraagtekens bij Rotterdam, maar wil nu niet meer weg.” Ook Robert Jager prijst zijn woonwijk. „Ik ken hier bijna iedereen in de straat.”

Hij onderstreept de verbetering met de gestegen huizenprijzen. „Destijds kostte een woning 70.000 euro, nu is dat al 3,5 ton…”

*(Zie <https://veilig-veiliger-middelland.nl> voor het eindrapport van het project Veilig, Veiliger Middelland)*
