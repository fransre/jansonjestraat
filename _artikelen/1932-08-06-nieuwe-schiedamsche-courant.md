---
toegevoegd: 2024-05-20
gepubliceerd: 1932-08-06
decennium: 1930-1939
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1932-08-06/edition/0/page/8"
onderwerpen: [familiebericht]
huisnummers: [19]
achternamen: [wessels]
kop: "Johannes Gerardus Wessels"
---
Eenige en algemeene kennisgeving.

Heden overleed tot onze diepe
droefheid, na kortstondig
doch geduldig lijden, voorzien
van de H*eilige* Sacramenten der
Stervenden, onze lieve zorgzame
Vader, Behuwd-, Groot- en
Overgrootvader, de Heer

# Johannes Gerardus Wessels

Weduwnaar van Mevrouw

Helena Johanna de Winter

in den ouderdom van 82 jaar.

Familie Wessels.

Rotterdam, 4 Augustus 1932.

**Jan Sonjéstraat** 19a.

36027 20
