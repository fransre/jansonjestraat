---
toegevoegd: 2024-08-11
gepubliceerd: 1923-05-31
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000188504:mpeg21:a0037"
onderwerpen: [bedrijfsinformatie]
huisnummers: [2]
kop: "Bureau „Studio”"
---
Telefoon-wijziging

# Bureau „Studio”

1e Middellandstraat 100.

Ingang Kantoor **Jan Sonjéstraat** 2.

Vanaf 1 Juni *1923* nieuw nummer 31248.

43523 20
