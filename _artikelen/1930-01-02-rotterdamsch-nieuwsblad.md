---
toegevoegd: 2024-08-06
gepubliceerd: 1930-01-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164645001:mpeg21:a00112"
onderwerpen: [huispersoneel]
huisnummers: [31]
achternamen: [moll]
kop: "Gevraagd:"
---
# Gevraagd:

jonge Vrouw voor Wasschen
en Werken, 4 dagen
per week. Moll,
**Jan Sonjéstraat** 31b.
