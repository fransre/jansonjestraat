---
toegevoegd: 2024-07-28
gepubliceerd: 1941-10-15
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002788:mpeg21:a0010"
onderwerpen: [muziek, tekoop]
huisnummers: [37]
kop: "Te koop"
---
# Te koop

piano, te bezichtigen
dagelijks voor
6 uur 's avonds. **Jan Sonjéstraat** 37b.
