---
toegevoegd: 2024-07-25
gepubliceerd: 1952-12-23
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000357095:mpeg21:a00079"
onderwerpen: [reclame]
huisnummers: [34]
achternamen: [raap]
kop: "Foto's van Uw kind"
---
# Foto's van Uw kind

bij U thuis gemaakt, zijn altijd de meest
geslaagde. Speciaal voor de feestweken. Drie verschillende
briefkaarten ƒ4.50, zes verschillende briefkaarten ƒ7.50 met
een gratis vergroting naar keuze. Foto Raap, **Jan Sonjéstraat** 34b,
Telef*oon* 37613, Rotterdam.
