---
toegevoegd: 2024-08-10
gepubliceerd: 1925-01-22
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495003:mpeg21:a0104"
onderwerpen: [huispersoneel]
huisnummers: [17]
kop: "Dagmeisje"
---
# Dagmeisje

gevraagd in klein gezin.
Zondags vrij. **Jan Sonjéstraat** 17.
