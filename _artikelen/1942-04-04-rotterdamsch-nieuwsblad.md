---
toegevoegd: 2024-03-07
gepubliceerd: 1942-04-04
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003118:mpeg21:a0030"
onderwerpen: [huispersoneel]
huisnummers: [20]
achternamen: [blind]
kop: "Dagmeisje"
---
# Dagmeisje

P*rotestantse* G*ezindte*. 14-16 j*aar*
Blind. **Jan Sonjéstraat** 20.

P1331
