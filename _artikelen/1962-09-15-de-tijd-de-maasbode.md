---
toegevoegd: 2024-07-23
gepubliceerd: 1962-09-15
decennium: 1960-1969
bron: De Tijd De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011232982:mpeg21:a0005"
onderwerpen: [familiebericht]
huisnummers: [25]
achternamen: [van-aalst]
kop: "J.G. van Aalst en J.W.E. van Aalst-Gudden"
---
Op woensdag 19 september *1962*
a*an*s*taande* hopen onze geliefde
ouders en grootouders

# J.G. van Aalst en J.W.E. van Aalst-Gudden

de dag te herdenken dat zij
vóór 60 jaar in het huwelijk
traden.

Om 8 uur wordt er in de S*in*t
Ellsabethkerk, Mathenesserlaan,
een gezongen H*eilige* Mis
opgedragen te hunner intentie.

Rotterdam, 19 sept*ember* 1962

**Jan Sonjéstraat** 25a.

Hun dankbare kinderen
en kleinkinderen.

Woensdag *19 september 1962* geen ontvangdag.
