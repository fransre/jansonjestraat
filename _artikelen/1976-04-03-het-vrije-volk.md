---
toegevoegd: 2021-05-18
gepubliceerd: 1976-04-03
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959175:mpeg21:a0125"
onderwerpen: [woonruimte, tekoop]
huisnummers: [18]
kop: "huis,"
---
Te koop van pens.fonds

# huis,

**Jan Sonjéstraat** 18, Rotterdam.
Dubbel benedenhuis
leeg en vrij te aanvaarden. Bovenwoning
met aparte ingang
verhuurd voor ƒ1977,— per
jaar. Koopprijs ƒ47.500,— k*osten* k*oper*
voor het gehele pand. Zeer
hoge hypotheek mogelijk. Benodigd
eigen geld pl*us*m*inus*
ƒ6.500,—. Inl*ichtingen* Aurora, tel*efoon* 010-246829.

R14a
