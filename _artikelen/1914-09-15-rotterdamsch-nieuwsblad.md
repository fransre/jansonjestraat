---
toegevoegd: 2024-08-13
gepubliceerd: 1914-09-15
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295663:mpeg21:a0045"
onderwerpen: [familiebericht]
huisnummers: [39]
achternamen: [van-wingerden]
kop: "Louis."
---
Geboren:

# Louis.

Zoon van

J. Corbet Lzn.

en

L. Corbet-van Wingerden.

**Jan Sonjéstraat** 39a.

8
