---
toegevoegd: 2024-03-07
gepubliceerd: 1919-05-28
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010333555:mpeg21:a0064"
onderwerpen: [werknemer]
huisnummers: [42]
achternamen: [ladage]
kop: "Timmerman."
---
# Timmerman.

Halfwas Timmerman
gevraagd. Adres:
B. Ladage, **Jan Sonjéstraat** 42a.
