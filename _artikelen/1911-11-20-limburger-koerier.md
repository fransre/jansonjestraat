---
toegevoegd: 2024-08-14
gepubliceerd: 1911-11-20
decennium: 1910-1919
bron: Limburger koerier
externelink: "https://resolver.kb.nl/resolve?urn=MMKB23:001187100:mpeg21:a00054"
onderwerpen: [reclame]
huisnummers: [22]
achternamen: [nolles]
kop: "De Passie-Spelen!!"
---
# De Passie-Spelen!!

fraai gekleurd, met begeleiding van schoone, religieuze
pathefoonmuziek, vertoond, door Henri
Nolles' Bioscoop!

Voorts komische films! Met groot succes opgetreden
voor de R*ooms* K*atholieke* Militairen-Ver*eniging* te 's Hertogenbosch!
Moderne goochelkunst! Fraaie apparaten!
Oostersche decors!

Henri Nolles. **Jan Sonjéstraat** 22, Rotterdam.

7744.
