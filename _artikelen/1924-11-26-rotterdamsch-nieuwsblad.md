---
toegevoegd: 2024-08-11
gepubliceerd: 1924-11-26
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495211:mpeg21:a0234"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [termijn]
kop: "Bets Molenaar. M. Termijn."
---
In plaats van kaarten.

Verloofd:

# Bets Molenaar. M. Termijn.

Rob. Fruinstraat 11.

**Jan Sonjéstraat** 33.

Rotterdam, 25-11-*19*24.

9
