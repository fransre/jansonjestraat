---
toegevoegd: 2024-08-14
gepubliceerd: 1904-10-26
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213475:mpeg21:a0144"
onderwerpen: [huisraad, kleding, muziek, tekoop]
huisnummers: [44]
kop: "Te Koop:"
---
# Te Koop:

een vierkant Kacheltje, een
Zomer- en Winterpelerine,
en in ruil voor een Gitaarciter
met 32 bladen een Orgeltje
of een 2 rijer Harmonika
of Fonograaf. Te zien
**Jan Sonjéstraat** 44, 2 maal
bellen (Verl*engde* Binnenweg).
