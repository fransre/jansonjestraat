---
toegevoegd: 2024-07-26
gepubliceerd: 1943-02-01
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003405:mpeg21:a0019"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Meubelmaker,"
---
# Meubelmaker,

leerling meubelmaker
en carrierjongen.
Aanmelden: **Jan Sonjéstraat** 36.
