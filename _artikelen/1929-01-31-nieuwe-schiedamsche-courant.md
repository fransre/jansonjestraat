---
toegevoegd: 2024-05-19
gepubliceerd: 1929-01-31
decennium: 1920-1929
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1929-01-31/edition/0/page/8"
onderwerpen: [woonruimte, tehuur]
huisnummers: [9]
kop: "Benedenhuis te huur"
---
# Benedenhuis te huur

**Jan Sonjéstraat** 9b. Te bevragen
bovenhuis.

6806
