---
toegevoegd: 2022-07-23
gepubliceerd: 2020-09-29
decennium: 2020-2029
bron: Algemeen Dagblad
externelink: "https://www.ad.nl/rotterdam/twee-jongens-van-17-en-18-jaar-opgepakt-voor-breken-van-jukbeen-van-man-die-ze-aansprak-op-dealen~ae0da7cf/"
onderwerpen: [misdrijf]
kop: "Twee jongens van 17 en 18 jaar opgepakt voor breken van jukbeen van man die ze aansprak op dealen"
---
# Twee jongens van 17 en 18 jaar opgepakt voor breken van jukbeen van man die ze aansprak op dealen

Een jongen van 17 jaar uit Spijkenisse en een 18-jarige Rotterdammer
zijn aangehouden door de politie voor het ernstig mishandelen van een
man uit Rotterdam-West. Het slachtoffer sprak de jongens aan op
dealen en werd vervolgens bewusteloos geschopt.

Het voorval gebeurde op 14 juli *2020* van dit jaar toen de man zijn woning verliet
om op de 1e Middellandstraat een pakje sigaretten te kopen. Op de hoek
van de **Jan Sonjéstraat** en de 1e Middellandstraat zag hij drie jongens staan.
Volgens de man waren ze drugs aan het dealen.

Omdat hij dit niet wil hebben in zijn wijk, maakte hij een filmpje van de
jongens. Zij zagen dit en vroegen hem ernaar. De man legde uit dat hij niet
wil dat ze drugs dealen. De jongens wilden dat hij het filmpje zou
verwijderen, maar dat wilde hij pas doen als hij met hun ouders had
gesproken.

## Bewusteloos

De jongens deden vervolgens alsof ze hem meenamen naar hun ouders en
liepen via de Duivenvoordenstraat naar de Oostervantstraat. Daar sloten ze
hem in. Ze probeerden zijn telefoon af te pakken en dreigden hem te slaan.
De man legde vervolgens een van de jongens op de grond, waardoor hij zelf
ook op straat kwam te liggen. De andere twee jongens bleven op het hoofd
van de man intrappen totdat hij bewusteloos was. Omstanders grepen in,
belden een ambulance en hielpen het slachtoffer. De verdachten gingen er
ondertussen vandoor.

In het ziekenhuis bleek onder andere dat het jukbeen van de man was
gebroken. Ook moest zijn voorhoofd worden gehecht. Door het
politieonderzoek zijn de jongens op het spoor gekomen. Op camerabeelden
werden ze herkend door een agent. Ze zijn afgelopen vrijdag *25 september 2020* opgepakt. De
verdachten zijn afgelopen maandag *28 september 2020* voorgeleid aan de rechter-commissaris,
die heeft besloten dat ze nog veertien dagen moeten blijven in afwachting
van hun rechtszaak.

*Onderschrift bij de foto*
Foto ter illustratie. © ANP
