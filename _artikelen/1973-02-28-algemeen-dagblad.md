---
toegevoegd: 2024-07-21
gepubliceerd: 1973-02-28
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002877024:mpeg21:a00191"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Nieuw showprogramma"
---
Club „Angelique”

# Nieuw showprogramma

Open 's middags van 14.00-17.00. Entree ƒ30.—.

's Avonds van 20.30-01.00 entree ƒ45.—

Elke avond party-avond, ook zaterdags. **Jan Sonjéstraat** 33a. Rotterdam
(C*entrum*) tel*efoon* 256188.

VZ16
