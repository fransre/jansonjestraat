---
toegevoegd: 2024-05-16
gepubliceerd: 1934-08-03
decennium: 1930-1939
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005427083:00001"
onderwerpen: [reclame, schilder]
huisnummers: [37]
achternamen: [wassenaar]
kop: "Huisschilder A. Wassenaar"
---
# Huisschilder A. Wassenaar

Aannemer van alle voorkomende burgerwerken

**Jan Sonjéstraat** 37b — Rotterdam
