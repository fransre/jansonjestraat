---
toegevoegd: 2023-03-07
gepubliceerd: 1979-04-20
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960131:mpeg21:a0175"
onderwerpen: [bulldog]
kop: "Actievoerders nemen ambtenarenluch"
---
Protest tegen uitblijven uitkeringen

# Actievoerders nemen ambtenarenluch

(Van een onzer verslaggevers)

Rotterdam — „Oh
was ik maar bij moeder
thuis gebleven, dan had ik
nu geen honger”, zongen
gisteren *19 april 1979* zo'n dertig jongeren
in de kantine van
het Stadskantoor aan het
Haagse Veer. Zij protesteerden
tegen de lange
wachttijden van nooduitkeringen
voor minderjarigen
die uit hun ouderlijk
huis zijn vertrokken
en geen werk hebben.

„We hebben onze buik vol
van crisisuitkeringen, verstrekt
door de Gemeentelijke Sociale Dienst (GSD)
van Rotterdam, aan werkloze
minderjarigen via overleg
met hun ouders,” was te lezen
op de pamfletten die de
jongeren uitdeelden.

Daarom besloten zij massaal
te genieten van een
„ambtenarenlunch” voor
prijzen die de helft zijn van
die in een commerciële
snackbar. De actievoerders
noemden een bal gehakt
„een broodje Van der Louw”
en een uitsmijter werd betiteld
als „een broodje Den Dunnen”.

Woordvoerder Aart van Boeien:
„We eten hier omdat het
goedkoop en lekker is. Dat komt
goed uit omdat we weinig of
geen geld hebben. We moeten te
lang wachten op onze uitkering.
Geldgebrek mag voor de GSD
geen excuus zijn, gezien de
goedkope lunches die de ambtenaren
hier krijgen. Als ze voor
zichzelf zo goed zorgen, mogen
ze dat voor ons ook best doen.”

Een maand geleden hebben de
actievoerders een brief gestuurd
aan de directie van de GSD.
„Die brief hebben we inderdaad
ontvangen en is nog steeds in
behandeling bij onze klachtencommissie.
Ik betreur het dat de
jongeren zo ongeduldig zijn. We
leggen zo'n brief heus niet naast
ons neer,” aldus een woordvoerder
van de GSD.

Sommige actievoerders hebben
bij „de Bulldog”, een open
jongerencentrum voor minderjarigen,
een onderdak gevonden.
Hun aantal groeit en daarom wil
de Bulldog uitbreiden. Een pand
aan de **Jan Sonjéstraat** zou al
beschikbaar kunnen zijn als het
ministerie van C*ultuur,* R*ecreatie en* M*aatschappelijk Werk* de Bulldog
subsidie zou geven. Er zijn al
mondelinge toezeggingen gedaan,
maar het geld blijft uit.
Op 25 april *1979* zullen de jongeren
van de Bulldog besluiten of ze
de aanvraag voor subsidiëring
van de uitbreiding nog langer
zullen afwachten.

*Onderschrift bij de foto*
Zonder problemen bestelden de jongeren hun (goedkope)
lunch in het zelfbedieningsrestaurant van het
Stadskantoor.
