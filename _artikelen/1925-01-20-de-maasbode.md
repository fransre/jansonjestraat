---
toegevoegd: 2024-08-10
gepubliceerd: 1925-01-20
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000194081:mpeg21:a0099"
onderwerpen: [woonruimte, tehuur]
huisnummers: [21]
kop: "Ongemeubileerde achterkamer"
---
# Ongem*eubileerde* achterkamer

aangeb*oden* met warande, desnoods
gebr*uik* van k*euken* en bergpl*aats*, voor een
dame bij alleenwonende wed*uwe* **Jan Sonjéstraat** 21b.

2719
