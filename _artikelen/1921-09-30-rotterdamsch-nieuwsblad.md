---
toegevoegd: 2024-08-12
gepubliceerd: 1921-09-30
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493858:mpeg21:a0152"
onderwerpen: [bedrijfsinformatie]
huisnummers: [17]
kop: "Publieke Verkoopingen"
---
# Publieke Verkoopingen

In het Notarishuis aan de Gelderschekade
te Rotterdam.

Donderdag 29 september *1921*.

Definitieve Afslag:


Door de Notarissen Klootwijk & Verlinden,
Haringvliet Z.z. 84.

Pand, Open Plaats en Erf, **Jan Sonjéstraat** n*ummer* 17,
trekg*eld* ƒ11.400. Daarop opgehouden.
