---
toegevoegd: 2024-05-14
gepubliceerd: 1940-05-03
decennium: 1940-1949
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005424070:00001"
onderwerpen: [dans]
huisnummers: [23]
achternamen: [pijpeman]
kop: "Joodsche Dansclub"
---
# Joodsche Dansclub

De J.D.C. zal op Donderdag 9 Mei *1940* a*an*s*taande* de
eerste clubavond houden in het Dans-Instituut Frenk
aan de v*an* Oldenbarneveltstraat 47. Op dezen
avond zal het bestuur aan de leden en introducé's
het nieuwe zomerprogramma voorleggen
en bestaat er gelegenheid tot aanmelding als
lid. Het Secretariaat van de J.D.C. is gevestigd
bij den heer J. Pijpeman, **Jan Sonjéstraat** 23.
tel*efoon* 33577.
