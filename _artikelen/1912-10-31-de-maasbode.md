---
toegevoegd: 2024-08-14
gepubliceerd: 1912-10-31
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000185856:mpeg21:a0046"
onderwerpen: [bewoner]
huisnummers: [36]
achternamen: [schuddebeurs]
kop: "Land- en zeemacht."
---
# Land- en zeemacht.

Evenals voorgaande jaren zal het Nederlandsch
Gymnastiekverbond weder een examen houden
in de oefeningen voor verkorten diensttijd voor
militieplichtigen en wel op Dinsdag 5 November *1912*
's avonds te half 8 in de Exercitieloods aan
den Crooswijkschen Weg te Rotterdam.

De oefeningen zijn volgens het nieuwe programma
van voorgeoefendheid, bedoeld in het
1e lid van art*ikel* 70 der Militiewet en bestaan uit:
staafoefeningen, vrije oefeningen, loopen, springen,
gewichtheffen, steenstooten, staafwerpen,
klimmen, optrekoefening, en evenwichtsoefeningen.

De aanmelding moet geschieden bij den heer
P. Schuddebeurs, **Jan Sonjéstraat** 36, uiterlijk
tot Maandag 4 November *1912*.
