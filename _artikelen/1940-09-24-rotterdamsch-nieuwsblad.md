---
toegevoegd: 2024-07-29
gepubliceerd: 1940-09-24
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002465:mpeg21:a0024"
onderwerpen: [cafe, bedrijfsinventaris, tekoop]
huisnummers: [15]
achternamen: [van-klaveren]
kop: "Te koop"
---
# Te koop

witte liter-Grapefruit
flesschen, Kroonkurken,
kratten, machines, Carrier
bedrijfskl*eding*. Spoed.
Spotprijs. v*an* Klaveren.
**Jan Sonjéstraat** 15.
