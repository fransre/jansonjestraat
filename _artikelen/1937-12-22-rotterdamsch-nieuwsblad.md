---
toegevoegd: 2024-08-02
gepubliceerd: 1937-12-22
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692045:mpeg21:a00263"
onderwerpen: [huisraad, tekoop]
huisnummers: [31]
achternamen: [morel]
kop: "Te koop:"
---
# Te koop:

goed brandende Vulkachel,
geheel compleet
met Plaat. Morel,
**Jan Sonjéstraat** 31A.
