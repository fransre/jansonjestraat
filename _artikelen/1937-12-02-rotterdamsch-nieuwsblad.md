---
toegevoegd: 2024-08-02
gepubliceerd: 1937-12-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692028:mpeg21:a00143"
onderwerpen: [auto, tekoop]
huisnummers: [15]
achternamen: [van-opstal]
kop: "Ford T te koop,"
---
# Ford T te koop,

gesloten Bestelwagen,
1500 k*ilo*g*ram* met rolluiken
z*o* g*oed* a*ls* n*ieuw*. Banden, prima
motor ƒ70. v*an* Opstal
**Jan Sonjéstraat** 15.
