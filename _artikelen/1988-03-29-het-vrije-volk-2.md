---
toegevoegd: 2021-04-29
gepubliceerd: 1988-03-29
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962738:mpeg21:a0208"
onderwerpen: [bellebom]
kop: "Geforceerde deuren gratis vernieuwd"
---
# Geforceerde deuren gratis vernieuwd

Rotterdam — De gemeente
Rotterdam zal geen kosten in rekening
brengen voor het wegslepen van 55 auto's
en het vernieuwen van de 72 deuren,
die tijdens de operatie Bellebom
werden geforceerd. Ook de 22 jongeren
die zondagmiddag *27 maart 1988* door de politie in de
‘gevaren-zone’ werden aangehouden
zullen hier verder geen nadelige gevolgen
van ondervinden.

„De mensen hebben in 99,9 percent
van de gevallen zo goed meegewerkt,
dat we het hierbij hebben gelaten,”
verklaart een woordvoerder van
de gemeente.

Overigens bleek achteraf dat de
politie een aantal woningen voor niets
had opengebroken. Volgens de woordvoerder
was dit te wijten aan de soms
wat stroeve samenwerking tussen de diverse
gemeentelijke diensten en de Mobiele Eenheid.
„De ME is een strakke
organisatie die zeer snel werkt. Die jongens
hadden soms de deur al ingeramd
waarna Gemeentewerken pas verscheen
met de sleutel.”

Vandaag *29 maart 1988* zullen de nieuwe deuren
worden geplaatst. Ruim vijfhonderd
mensen meldden zich met vragen bij
het Gemeentelijk Energiebedrijf (GEB)
nadat het gas zondagmiddag *27 maart 1988* weer was
aangesloten. Berichten dat iemand in
sector B was overvallen en een aantal
huizen in sector A was opengebroken
worden door de politie ontkend.

„Tijdens de schouw na de demontage
van de bom,” zegt een politie-woordvoerster,
„leek het erop dat
hier en daar was ingebroken. Eenmaal
binnen bleek echter niets vermist. Er
zijn bij ons wat dat betreft ook geen
meldingen binnengekomen.”

De tien meter diepe kuil in de
tuinen tussen de Bellevoysstraat en de
**Jan Sonjéstraat** zal ‘laag voor laag’
wordt gevuld. De benodigde grond zal
vrijkomen bij het graven naar een vermoedelijke
Duitse blindganger aan de
Boezembocht. De damwanden, die in
december vorig jaar *1987* in de grond werden
geheid, blijven in de bodem zitten. De
stalen constructie zal drie meter onder
het maaiveld worden afgezaagd en vervolgens
met een laag grond worden bedekt.
Het herstellen van de tuintjes gaat
ongeveer een half miljoen gulden kosten.
