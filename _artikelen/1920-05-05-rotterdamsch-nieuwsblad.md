---
toegevoegd: 2024-08-13
gepubliceerd: 1920-05-05
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493940:mpeg21:a0071"
onderwerpen: [tekoop]
huisnummers: [3]
kop: "2 werkhonden"
---
Te koop

# 2 werkhonden

en Fruitmanden. **Jan Sonjéstraat** 3a,
na 7
uur.
