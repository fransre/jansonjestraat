---
toegevoegd: 2024-08-08
gepubliceerd: 1926-10-10
decennium: 1920-1929
bron: Nieuwe Rotterdamsche Courant
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010028677:mpeg21:a0048"
onderwerpen: [woonruimte, tekoop]
huisnummers: [38]
kop: "Jan Sonjéstraat Numero 38,"
---
De Notaris Frans L. Hartong
te Rotterdam,

zal op Donderdag 21 October 1926
bij voorloopigen en op Donderdag 26 October 1926
bij
eindafslag, telkens des namiddags 2
uur, in het Notarishuis aan de
Gelderschekade te Rotterdam,

verkoopen:

Het dubbel pand en erve
te Rotterdam, aan de

# **Jan Sonjéstraat** N*ummer* 38,

kad*astraal* gem*eente* Delfshaven, Sectie D, n*ummer*
2700, groot 1.93 aren.

Grond- en straatbelasting 1926
ƒ101.46 en ƒ42.75.

Verhuurd: het benedengedeelte
voor ƒ40. p*er* w*eek* tot 31 Augustus 1936,
de 2 1e et*ages* ieder voor ƒ30.—
p*er* m*aand* en de 2 2e et*ages* ieder voor ƒ29
p*er* m*aand*, totaal ƒ3496.— p*er* j*aar*.

Aanvaarding en betaling der
kooppenningen 1 December 1926.

Bezichtiging 18, 19 en 20 October 1926
van 10-4 uur.

Inlichtingen en toegangsbewijzen te
verkrijgen ten kantore van genoemden
Notaris, Mathenesserlaan 217.

46950/32
