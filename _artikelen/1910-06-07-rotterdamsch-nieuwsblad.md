---
toegevoegd: 2024-08-14
gepubliceerd: 1910-06-07
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197905:mpeg21:a0118"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Reiziger"
---
# Reiziger

gevraagd tegen zeer
hooge Provisie, bij gebleken
geschiktheid vast
salaris. Te spreken
's morgens 9 uur. **Jan Sonjéstraat** 36a,
Pakhuis
nabij de Middellandstr*aat*
