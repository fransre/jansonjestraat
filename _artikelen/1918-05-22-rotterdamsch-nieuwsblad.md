---
toegevoegd: 2024-08-13
gepubliceerd: 1918-05-22
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297015:mpeg21:a0099"
onderwerpen: [diefstal]
kop: "Met wagen en al verdwenen."
---
# Met wagen en al verdwenen.

A.C. H., wonende **Jan Sonjéstraat**, heeft,
aangifte gedaan, dat men te zijnen nadeele
een ijswagen en een bedrag van ƒ8 verduisterd
heeft.
