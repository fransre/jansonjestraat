---
toegevoegd: 2024-07-25
gepubliceerd: 1954-11-03
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164888055:mpeg21:a00099"
onderwerpen: [huispersoneel]
huisnummers: [33]
achternamen: [spitters]
kop: "Rooms Katholiek dagmeisje"
---
Gevr*aagd*

# R*ooms* K*atholiek* dagmeisje

voor hele
dagen, meerdere hulp aanwezig.
Mevr*ouw* Spitters, **Jan Sonjéstraat** 33,
tel*efoon* 34142.
