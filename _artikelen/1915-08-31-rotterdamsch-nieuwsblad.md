---
toegevoegd: 2024-08-13
gepubliceerd: 1915-08-31
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296098:mpeg21:a0104"
onderwerpen: [gevraagd]
huisnummers: [12]
achternamen: [lutteken]
kop: "Vlaggen."
---
# Vlaggen.

Gevraagd ter overname
één vlag rood wit blauw
pl*us*m*inus* 3 M*eter*. Eén vlag Oranje,
idem, een Vlaggestok.
Aanbiedingen aan G.E. Lutteken,
**Jan Sonjéstraat** 12b.
