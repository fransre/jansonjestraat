---
toegevoegd: 2024-08-06
gepubliceerd: 1930-04-01
decennium: 1930-1939
bron: Nieuwe provinciale Groninger courant
externelink: "https://resolver.kb.nl/resolve?urn=MMRHCG04:234995077:mpeg21:a00008"
onderwerpen: [bewoner]
huisnummers: [5]
achternamen: [roodenburg]
kop: "Examens."
---
# Examens.

Het examen ter verkrijging van een akte van bekwaamheid
als onderwijzeres aan een bewaarschool
(akte A), uitgaande van de Gemengde Commissie
voor Bewaarschoolakte-examens voor de gemeenten
Rotterdam, 's-Gravenhage, Arnhem, Groningen en
Schiedam wordt gehouden in Juni *1930* en Juli *1930*. Men kan
zich aanmelden voor 22 April *1930* bij den secretaris der
commissie, W.J. Roodenburg, **Jan Sonjéstraat** 5a te
Rotterdam.
