---
toegevoegd: 2024-07-23
gepubliceerd: 1963-04-06
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002814032:mpeg21:a00224"
onderwerpen: [bedrijfsruimte]
huisnummers: [41]
kop: "bedrijfsruimte"
---
Te koop

# bedrijfsruimte

Opp*ervlakte* c*irc*a 35 M²

gelegen **Jan Sonjéstraat** 41
bij Middellandstraat.

Huur bovenhuis ƒ60,50 per
maand, voorzien van zware
metalen stellingen 1 mei *1963* te
betrekken.

Telefoon K 10 / 18 12 55.
