---
toegevoegd: 2024-08-03
gepubliceerd: 1935-03-21
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164676024:mpeg21:a00144"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [34]
kop: "Café"
---
# Café

te koop, Centrum, ƒ80
contant, zonder schuld,
wegens Betrekking. **Jan Sonjéstraat** 34b.
