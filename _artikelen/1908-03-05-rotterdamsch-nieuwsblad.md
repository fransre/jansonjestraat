---
toegevoegd: 2024-08-14
gepubliceerd: 1908-03-05
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994755:mpeg21:a0170"
onderwerpen: [straat]
kop: "Grond"
---
# Grond

voor aanvulling kosteloos
verkrijgbaar op het
werk **Jan Sonjéstraat**,
hoek Schermlaan. Ook
te bevragen Volmarijnstraat 105.
