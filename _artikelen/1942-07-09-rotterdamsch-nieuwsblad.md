---
toegevoegd: 2024-03-07
gepubliceerd: 1942-07-09
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002891:mpeg21:a0011"
onderwerpen: [kleding, tekoop]
huisnummers: [36]
achternamen: [augustijn]
kop: "Bruine damesmantel,"
---
# Br*uine* d*ames*mantel,

oude kw*aliteit* ƒ60,
z*o* g*oed* a*ls* n*ieuw* H. Augustijn, **Jan Sonjéstraat** 36B.

A991
