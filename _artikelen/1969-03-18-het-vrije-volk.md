---
toegevoegd: 2024-07-22
gepubliceerd: 1969-03-18
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956858:mpeg21:a0080"
onderwerpen: [huispersoneel]
huisnummers: [38]
achternamen: [uilenbroek]
kop: "Werkster"
---
Nette

# werkster

gevr*aagd* 3 ochtenden
p*er* w*eek*. Zelfst*andig* kunnende
werken. Uilenbroek, **Jan Sonjéstraat** 38,
R*otter*dam. Tel*efoon* 238170.
