---
toegevoegd: 2023-01-12
gepubliceerd: 1988-03-26
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962736:mpeg21:p009"
onderwerpen: [bellebom]
achternamen: [van-brenkelen]
kop: "Operatie-Bellebom: onzin of noodzakelijk kwaad?"
---
# Operatie-Bellebom: onzin of noodzakelijk kwaad?

Spanning stijgt in ‘gevaren-zone’

Rotterdam — Zoveel
hoofden, zoveel zinnen. De één
vindt „al dat gedoe” rond de
operatie-Bellebom pure onzin
en sensatie, de ander spreekt
van „een noodzakelijk kwaad”.
Maar hoe men ook mag denken
over de demontage van de
duizendponder, iedereen binnen
een straal van driehonderd
meter rond de vliegtuigbom
in de Bellevoysstraat zal
morgenochtend *27 maart 1988* voor één dag
huis en haard moeten verlaten.
En iedereen in het gebied daar
omheen zal het grootste deel
van de dag binnenshuis moeten
vertoeven of eveneens ergens
anders onderdak zien te
regelen. In het opvangcentrum
in de Energiehal, bij familie of
kennissen buiten de ‘gevaren-zone’
of elders voor een gezellig
dagje uit.
De gemeente Rotterdam
heeft in samenwerking met diverse
andere diensten alles in
het werk gesteld de 21.000 bewoners
in het stadsdeel rond
de Bellebom zo goed mogelijk
voor te lichten. Vorig jaar september *1987*
werd al een zogenaamd
ontruimingsplan opgesteld
en kregen een aantal
werkgroepen de opdracht de
zes miljoen gulden kostende
operatie ‘op de rails’ te zetten.
Toch is deze golf van informatie
aan een enkeling voorbijgegaan.
Zoals een mevrouw
in de Aleidisstraat, die nog
nooit van de Bellebom had gehoord,
en enkele Roemeense
inwoners van de Bellevoysstraat,
die het slechts met
„wat vage verhalen” moesten
doen. „Hét probleem is,” zegt Belu Mihai
in gebrekkig Engels,
„dat de, informatie die door de
gemeente werd rondgestuurd
in het Nederlands was. Daar
begrepen we dus weinig van.
Uiteraard hadden we ook aan
de Nederlandse kranten niets.
Via andere buitenlandse mensen
hebben we inmiddels te horen
gekregen dat we zondag *27 maart 1988* de
wijk uit moeten. Waarheen?
Geen idee, misschien een stukje
lopen op Zuidplein.”
M.G. de Ruiter van het Bureau Rampenbestrijding:
„Die
mensen zullen we zo snel mogelijk
benaderen. We dachten
echt dat we nu iedereen op de
hoogte hadden gebracht. Die
Roemeense mensen zullen we
dus op korte termijn persoonlijk
inlichten.”

## ‘Spookwijk’

Naarmate de operatie-Bellebom
haar hoogtepunt nadert
stijgt de spanning in de Rotterdamse
wijken die het meest bij
het gebeuren zijn betrokken.
Vooral in Middelland, dat morgen *27 maart 1988*
voor één dag een ‘spookwijk’
wordt, is de Bellebom
zo'n beetje het gesprek van de
dag. „Als wij 's ochtends de buren
op straat tegenkomen,”
zegt mevrouw L M. van Brenkelen
uit de **Jan Sonjéstraat**,
„tellen we als het ware de dagen
af. Dan is het zoiets als:
goedemorgen, nog zes dagen
en dan is het zover…”
De familie Van Brenkelen
heeft vanaf het prille begin direct
met de operatie-Bellebom
te maken gehad. Vanuit de
huiskamer konden zij zien hoe
de Explosieven Opruimingdienst
(EOD) in de weer was
om het projectiel op te sporen
en hoe eind december *1987* de gigantische
damwanden in de
grond werden geslagen. Van Brenkelen:
„In het begin waren
we vooral angstig. Tijdens het
boren van die gaten, noodzakelijk
om de bom op te sporen,
leefde hier continu de vrees
dat ze die duizendponder zouden
raken. Later kwam nog
eens de herrie van die waterpompen
en werden houten
schotten voor de ramen getimmerd.
De operatie loopt nu gelukkig
op haar eind, want het
begint allemaal aardig te vervelen.”

## ‘Ontdekker’

Wie niet haar buik vol heeft,
van de Bellebom zélf, maar
wél van alle toestanden eromheen,
is mevrouw H.J. Ottenberg.
Sinds ze hier en daar
werd gebombardeerd tot ‘ontdekker’
van de Bellebom heeft
ze een niet aflatende stroom
journalisten en andere geïnteresseerden
over de vloer gehad
die haar het hemd van het
lijf vroegen. Voor alle duidelijkheid
legt ze uit dat zij het
niet is geweest die de aanzet
tot de operatie Bellebom heeft
gegeven.
„De politie kwam hier aan
de deur,” luidt de officiële lezing
„om te vragen of iemand
ndertijd die bom had zien vallen.
Daarvóór had men elders
in de wijk naar zo'n blindganger
gezocht, maar nooit gevonden.
Mijn ouders hebben dat ding in *19*44 inderdaad in de tuin
van de buren terecht zien komen
en dat aan mij verteld. Op
aanraden van de politie heb ik
vervolgens die brief aan het
college van burgemeester en
wethouders geschreven.”
„Voor ons,” gaat ze verder,
„was het jarenlang een vaststaand
feit dat die bom nooit
zou kunnen exploderen. Later
werd pas duidelijk dat de bom
nog wel degelijk kon afgaan.
Aangezien de precieze plaats
van de blindganger niet bekend
was liepen er dus mensen
risico, vooral met het oog op
de renovatie die op stapel
staat. Er zouden zelfs mensenlevens
mee gemoeid kunnen
zijn. Met die wetenschap zou ik
niet rustig meer kunnen slapen.”
Uit de door de Gemeentelijke Gezondheidsdienst (GGD)
uitgevoerde enquête onder de
zevenduizend inwoners van het
‘evacuatie-gebied’ blijkt dat de
meeste betrokkenen hun zaakjes
het liefst zelf regelen.
Ruim duizend evacuées wensen
gebruik te maken van de
opvang in de Energiehal, zo'n
vijftig mensen hebben verzocht
om speciaal vervoer en
‘slechts’ vijftig huisdieren worden
ondergebracht in het tijdelijke
asiel in de Weena-IJshal.
De meeste honden en katten,
maar ook de vijftig slangen
van onderwijzer M. Termeer
in de Bellevoysstraat, blijven
dus gewoon thuis.

## In de stress

„Het liefst,” zegt de slangenliefhebber,
„was ik zélf ook
thuis gebleven. Er kan niet zoveel
misgaan, maar als het
glas van zo'n terrarium om de
een of andere reden stuk zou
gaan ben ik er nu niet bij om
die slangen terug te stoppen.
En als zo'n python eenmaal
ontsnapt, zit 'ie zó op straat.”
Naast de vijf boa's, de vijf
pythons en ettelijke slanke
boa's en rattenslangen beschikt
Termeer over een aantal
moeilijk vervoerbare leguanen.
„Die dieren,” zegt Termeer,
„schieten meteen in de
stress als je met ze naar buiten
gaat. Zo'n leguaan eet dan
geheid niets meer. Door de
Rampenbestrijding is me benadrukt
dat ik me over diefstal
geen zorgen hoef te maken.
De wijk wordt hermetisch
afgesloten. Bovendien heeft de
dienst in dit kader een verzekering
van vijftig miljoen gulden
afgesloten…”
Een aantal café-houders in
‘sector A’ verbaast zich over
het feit dat men hen — ondanks
de verder overvloedige
informatie — niet persoonlijk
heeft benaderd. R. Pronk, eigenares
van een café in sector
A en één in sector B: „Wel raar
allemaal. We krijgen het gemis
aan omzet niet vergoed,
want er is volgens de gemeente
sprake van onheil van buitenaf.
We hebben verder echter
niets officieels gehoord. Moet
je eens kijken als de belastingen
omhoog gaan. Dan zijn ze
er wel zó bij.”
„Te weinig informatie?” reageert
De Ruiter van het bureau
Rampenbestrijding, „nou,
daar geloof ik niets van. Er
zijn diverse berichten de deur
uitgegaan, de kranten staan er
bol van, de speciale Bellebomkrant
is huis-aan-huis bezorgd
en we hebben een enquête gehouden.
Bedrijven, dus ook café's,
hebben zelfs een aparte
brief ontvangen. Die voorlichting
is al in een heel vroeg stadium
zo goed mogelijk geregeld.”
Voor autoverhuurder L.H. Wolders,
die zijn bedrijf op
steenworpafstand van de Bellebom
runt, is het morgen *27 maart 1988*
vooral improviseren geblazen.
De wagens die vandaag *26 maart 1988* voor
één dag de deur uit gaan kunnen
tenslotte morgen *27 maart 1988* niet worden
ingeleverd en zullen dus
pas maandagochtend *28 maart 1988* terugkomen.
Verder zullen alle niet
verhuurde wagens de wijk uit
moeten. Voor Wolders een reden
te meer vandaag *26 maart 1988* zoveel
mogelijk zaken te doen.
„Ik zit hier al tweeëndertig
jaar,” vertelt hij, „en al die tijd
heeft die bom practisch naast
de deur gelegen. Er is nooit
wat gebeurt. Wat mij betreft
hadden ze 'm dus gewoon op
die plek kunnen laten liggen.
Bovendien: ik werk al zes dagen
in de week en heb dus weinig
trek ook zondagochtend *27 maart 1988*
nog eens een keertje op te draven
om hier de noodzakelijke
dingen te regelen.”
De Ruiter: „We hebben uiteraard
goed overwogen wanneer
we die Bellebom zouden gaan
ruimen. Bekend is dat de EOD
het liefst niet op zondag werkt.
Maar als je verder alle voors
en tegens op een rijtje zet,
kwam 27 maart *1988* gewoon als
gunstig uit de bus. Alleen kerken
en, inderdaad, die café's
ondervinden dan last. Maar we
hebben de groep gedupeerden
zo klein mogelijk weten te houden.”
Ook in ‘sector B’ — de binnenblijvers
dus — wordt op
verschillende manieren tegen
de operatie Bellebom aangekeken.
De familie Van der Mark,
eigenaar van hotel Traverse
aan de 's-Gravendijkwal 70,
zegt de zondag *27 maart 1988* beslist niet met
plezier tegemoet te zien. „We
mogen vandaag *26 maart 1988* geen nieuwe
gasten meer inschrijven,” zegt
mevrouw van der Mark. „Alle
gasten die eerder komen moeten
we vertellen dat we in de
gevarenzone zitten en dat iedereen
morgen *27 maart 1988* het hotel om
8.00 uur moet verlaten. Daar
trapt natuurlijk niemand in.”
Mevrouw Van Brenkelen uit
de **Jan Sonjéstraat** tenslotte:
„Er hangt ons nogal wat boven
het hoofd. Het maakt wat dat
betreft niet uit of je nu een
kind of een volwassene bent.
Als je het goed beschouwt,
komt het morgen *27 maart 1988* allemaal
neer op de vaste hand van die
ene EOD-meneer. Ik heb er het
volste vertrouwen dat hij erin
slaagt die bom te demonteren.
Maar het is en blijft mensenwerk.”

*Onderschrift bij de foto*
J.P. van Brenkelen uit de **Jan Sonjéstraat**: „Het begint allemaal aardig te vervelen.” (Foto Tieleman van Rijnberk)

*Onderschrift bij de foto*
Onderwijzer M. Termeer uit de Bellevoysstraat met een van mijn
slangen: „Het liefst was ik ook zélf thuisgebleven.” (Foto Fons
Evers)

*Onderschrift bij de foto*
L.H. Wolders is met zijn autoverhuurbedrijf in de Bellevoysstraat
bijna ‘buurman’ van de Bellebom. „Wat mij betreft kan de bom gewoon
blijven liggen,” is zijn mening. (Foto Tieleman van Rijnberk)

*Onderschrift bij de foto*
De familie Van der Mark, eigenaar van hotel Traverse
aan de 's-Gravendijkwal (‘sector B’), schrijft vandaag *26 maart 1988*
geen nieuwe gasten in. (Foto Erno Wientjes)
