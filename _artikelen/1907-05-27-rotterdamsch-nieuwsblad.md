---
toegevoegd: 2024-08-14
gepubliceerd: 1907-05-27
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196683:mpeg21:a0093"
onderwerpen: [tekoop]
huisnummers: [29]
kop: "Fransch"
---
# Fransch

Duitsch Engelsch, Wiskunde
en de andere vakken
van onderwijs. Privaat- en
Cursuslessen
door een bevoegd Hoofdonderwijzer.
Billijke
conditiën. **Jan Sonjéstraat** 29,
bij de Middellandstraat.
