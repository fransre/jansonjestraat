---
toegevoegd: 2024-08-01
gepubliceerd: 1938-05-11
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164695008:mpeg21:a00085"
onderwerpen: [woonruimte, tehuur]
huisnummers: [18]
kop: "benedenhuis,"
---
Mooi

# benedenhuis,

**Jan Sonjéstraat** 18. voor
2 kl*eine* Tussch*en*-, Achter-,
Zijkamer, Keuken en
Kelder. Te zien van 3-6.
Bevragen: Bovenhuis
