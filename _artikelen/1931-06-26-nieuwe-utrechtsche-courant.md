---
toegevoegd: 2024-08-05
gepubliceerd: 1931-06-26
decennium: 1930-1939
bron: Nieuwe Utrechtsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMUTRA04:253387079:mpeg21:a00039"
onderwerpen: [familiebericht]
huisnummers: [23]
achternamen: [koomans]
kop: "Hendrik Zeilstra en Magdalena Elizabeth Koomans"
---
Ondertrouwd:

# Hendrik Zeilstra en Magdalena Elizabeth Koomans

Rotterdam, 25 Juni 1931.

Ontvangdag: Woensdag 1 Juli *1931* a*an*s*taande*
7.30-9.30 uur, **Jan Sonjéstraat** 23b.

Huwelijksbevestiging D*eo* V*olente* 9 Juli *1931*,
2.30 uur, in de Nieuwe Zuiderkerk,
door den WelEerw*aarde* Heer Ds. F.C. Meyster.
