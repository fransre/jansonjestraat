---
toegevoegd: 2024-08-13
gepubliceerd: 1913-11-20
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295864:mpeg21:a0137"
onderwerpen: [reclame]
huisnummers: [12]
kop: "Stukadoors."
---
# Voorschotten

worden verstrekt, in
kleine bedragen aan
huisgezinnen, die zich
laten inschrijven in een
solied Begrafenisfonds,
5 p*ro*C*en*t rente en zonder
borg. Zich te vervoegen
**Jan Sonjéstraat** 12a, in
de 1e Middellandstraat,
van 3-5 uur, behalve
Maandag en Zaterdag.
