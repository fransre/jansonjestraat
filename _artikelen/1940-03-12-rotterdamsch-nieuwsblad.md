---
toegevoegd: 2024-07-29
gepubliceerd: 1940-03-12
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002292:mpeg21:a0036"
onderwerpen: [boek, gevraagd]
huisnummers: [22]
achternamen: [de-kan]
kop: "wereld-atlas."
---
Te koop gevraagd n*ummer*
1 en 2 van de Nieuwsblad

# wereld-atlas.

Adres: K.A. de Kan,
**Jan Sonjéstraat** 22, Rotterdam.
