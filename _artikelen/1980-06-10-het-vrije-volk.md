---
toegevoegd: 2021-04-29
gepubliceerd: 1980-06-10
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010960558:mpeg21:a0048"
onderwerpen: [cafe]
huisnummers: [15]
achternamen: [kok]
kop: "Kennisgeving"
---
# Kennisgeving

De burgemeester van Rotterdam
maakt bekend dat
de heer N. Kok een verzoek
heeft ingediend om
vergunning voor het vergroten
met een aanbouw
van het café in het pand
**Jan Sonjéstraat** 15.

Het bouwplan is in overeenstemming
met het voor
dit gebied in voorbereiding
zijnde bestemmingsplan.

Burgemeester en wethouders
overwegen de bouwvergunning
te verlenen
met toepassing van artikel 50, lid 8, van de Woningwet.

In verband hiermee ligt
het bouwplan in het tijdvak
van 11 juni *1980* tot en met
24 juni 1980 op werkdagen
van 9.00 uur tot 16.00 uur
ter inzage op de secretarie-afdeling
Ruimtelijke Ordening, Stadsvernieuwing en Volkshuisvesting
(stadshuis, kamer 210).

Gedurende dit tijdvak kan
een iedere hiertegen bij
burgemeester en wethouders
schriftelijk bezwaren
indienen.

Rotterdam, 10 juni 1980.

De burgemeester voornoemd,

J.G. van der Ploeg, loco.
