---
toegevoegd: 2024-08-05
gepubliceerd: 1932-10-06
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164661040:mpeg21:a00174"
onderwerpen: [huispersoneel]
huisnummers: [44]
kop: "Net meisje"
---
# Net meisje

gevr*aagd*, 14 of 15 jaar,
netjes werken vereischt,
goed v*oor* Kinderen.
Aanm*elden* na 7 uur.
**Jan Sonjéstraat** 44, ben*eden*
