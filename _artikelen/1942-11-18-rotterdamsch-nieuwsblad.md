---
toegevoegd: 2024-07-26
gepubliceerd: 1942-11-18
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003004:mpeg21:a0065"
onderwerpen: [diefstal, fiets]
huisnummers: [37]
achternamen: [van-balgooi]
kop: "Invaliden-rijwiel ontvreemd."
---
# Invaliden-rijwiel ontvreemd.

De heer P. van Balgooi, wonende **Jan Sonjéstraat** 37,
begaf zich Zondagmorgen *15 november 1942*
j*ongst*l*eden* per rijwiel naar de R*ooms* K*atholieke* Kerk aan den
Westzeedijk. Naar zijn gewoonte plaatste
hij de fiets in het portaal. Na afloop van
den dienst, kwam hij tot de ontstellende
ontdekking, dat zijn vervoermiddel, dat
speciaal voor hem was gebouwd in verband
met zijn invaliditeit, was verdwenen.
Deze diefstal is des te verwerpelijker,
omdat de dief aan den bouw van het rijwiel
moest hebben gezien, dat het aan een
invalide toebehoorde, die door dit verlies
wel bijzonder gedupeerd is.
