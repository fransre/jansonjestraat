---
toegevoegd: 2024-08-05
gepubliceerd: 1931-07-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164654003:mpeg21:a00119"
onderwerpen: [woonruimte, tehuur]
huisnummers: [39]
kop: "vrij bovenhuis"
---
Te huur,

# vrij bovenhuis

**Jan Sonjéstraat** 39,
ƒ45 p*er* maand. Te bevragen
Hendri*c*k Sorchstraat 44.
