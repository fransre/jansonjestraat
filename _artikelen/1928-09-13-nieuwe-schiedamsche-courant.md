---
toegevoegd: 2024-05-20
gepubliceerd: 1928-09-13
decennium: 1920-1929
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1928-09-13/edition/0/page/7"
onderwerpen: [woonruimte, tekoop]
huisnummers: [38]
kop: "Publieke verkoopingen"
---
# Publieke verkoopingen

te Rotterdam.

In het Notarishuis aan de Geld*erse* Kade.

Op Woensdag 12 Sept*ember* *1928*, des namiddags ten 2 uur.

Eindafslag.

Dubb*el* pand en erf, Albregt Engelmanstraat 34a-c.
In bod op ƒ15.500. Daarop verkocht.

Pand en erf, **Jan Sonjéstraat** 38a-b. In bod
op ƒ30.300. Daarop verkocht.
