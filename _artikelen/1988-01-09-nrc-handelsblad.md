---
toegevoegd: 2024-07-21
gepubliceerd: 1988-01-09
decennium: 1980-1989
bron: NRC Handelsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBNRC01:000030312:mpeg21:a0028"
onderwerpen: [bellebom]
kop: "Operatie Bellebom"
---
Werkstad-Stadswerk

# Operatie Bellebom

Door Markus Meulmeester

Rotterdam, 9 jan*uari* *1988* —
„Het wordt een gigantische
operatie”, zegt het hoofd van
het gemeentelijk bureau rampenbestrijding
M.G. de Ruiter.
Al weken is hij dagelijks bezig
met de voorbereiding van „operatie
Bellebom” op zondag 27
maart waarbij ongeveer 2000
ambtenaren en 21.000 bewoners
van de wijk Middelland
betrokken zullen zijn.

Aanleiding is het op 29 november 1944
uitgevoerde bombardement
van de Britse luchtmacht
op een gebouw van de
Duitse Sicherheitsdienst aan de
Heemraadsingel. Enkele niet
ontplofte projectielen kwamen
in de tuinen van de woningen
aan de Bellevoysstraat en **Jan Sonjéstraat**
terecht. Dat bombardement
kostte toen 66 Rotterdammers
het leven.
De gemeente werd in mei
van het vorige jaar *1987* op de aanwezigheid
van de bommen gewezen
door een wijkbewoonster.
Archiefonderzoek, aangevuld
met getuigenverhoren
rechtvaardigde nadere studie
door de Explosieven opruimingsdienst
(EOD).

De Ruiter: „Een eerste onderzoek
van de EOD in
september/oktober *1987* leverde
geen resultaat op. Een tweede
onderzoek, een zogeheten dieptedetectie
met plasticpijpen
waarin met een sonde tot op
een diepte van veertien meter
kan worden gemeten, toonde
aan dat op zes tot tien meter op
verschillende plaatsen ijzer in
de grond zit.”

Explosieven-opruimer adjudant
H.F. de Bruin denkt dat er
een zogeheten duizendponder
met 250 kilo springstof, een
stuk van een staart en een onontplofte
raket in de grond liggen.
„Vanaf de zevende dag
hadden we met de dieptedetectie
resultaat. Zo'n duizendponder
is ongeveer 1,20 meter lang
en 60 centimeter dik. Niet gering.
Maar het kan ook nog een
vijfhonderdponder, een koffieketel
of regenpijp zijn. Zeker
weten we dat pas als we het
zien. In Purmerend en Haarlem
dachten we ook dat er iets zat,
maar er was niks. Dat risico lopen
we hier ook.”

## Huis uit

Maar er wordt wel degelijk
rekening met de aanwezigheid
van zware bommen gehouden.
Aanstaande woensdag *13 januari 1988* wordt in
de tuinen begonnen met het
heien van stalen damwanden
voor het graven van bouwputten.
De directe omwoners (ongeveer
25 gezinnen) moeten die
dag hun woning verlaten. Een
groot deel van de woningen in
de Bellevoysstraat staat al wegens
renovatie leeg. De kans
bestaat dat bij het heien van de
damwanden een bom wordt geraakt
die dan spontaan tot explosie komt.

De Ruiter: „Voor een ontploffing
op die diepte hoeven
we niet erg bang te zijn. De
schokgolf kan echter de heipalen
van de funderingen treffen
en de kans is dan groot dat ze
breken waardoor de daarop gebouwde
woningen geheel of gedeeltelijk
instorten. Daarom
moeten de mensen hun woning
uit. Later in de maand moet dat
nog een keer omdat er dan weer
een wand vlakbij een projectiel
wordt geheid. Met het graven
van de bouwput kan pas worden
begonnen als het grondwater is
weggepompt.”

## Bejaarden

Als op zondag 27 maart *1988* de
bommen worden gedemonteerd
moeten 7000 bewoners binnen
een straal van 300 meter voor
een dag worden geëvacueerd.
Onder hen zijn bijna 500 bejaarden
in de leeftijd van 65 tot
99 jaar en een aantal minder validen
en zieken. Nog eens
14.000 mensen binnen een diameter
van 1,2 kilometer mogen
die dag wegens gevaar voor
scherven bij een explosie niet
de straat op of moeten elders
onderdak zien te vinden. Ook
alle auto's moeten van de straat.
Daarvoor zijn buiten de gevarenzone
een aantal garages gereserveerd.

Projectleider De Ruiter:
„Het wordt de grootste evacuatie
in de stad sinds jaren. Zeventien
peletons ME, brandweer,
GGD, gemeentewerken,
sociale dienst, R*otterdamse* E*lectrische* T*ram* en ambtenaren
van het stadhuis doen er
aan mee. Het evacueren zal
moeilijk gaan omdat we toch
rekening moeten houden met
twintig verschillende nationaliteiten
die in de wijk wonen.
Toch willen we dat iedereen op
de hoogte is. In de buurtwinkel
houden we twee keer per week
een spreekuur waar vooral vragen
worden gesteld over wat er
met de huisdieren zoals honden,
katten, schildpadden, slangen,
kanariepieten en papegaaien
moet gebeuren. Wij vragen
de mensen die dieren maar een
dag alleen te laten.”

Zo'n tweeduizend mensen
zullen met bussen naar de
Energiehallen worden gebracht
waar men de dag door kan
brengen. Daar zal voor maaltijden
en drank worden gezorgd.
Toch is het volgens De Ruiter
niet de bedoeling dat daar een
„soort circus” wordt georganiseerd.
„Dat trekt natuurlijk ook
mensen uit andere wijken aan.
Wij voeren wat dat betreft een
ontmoedigingsbeleid. Zo'n
veertien dagen voor de ontruiming
houden we nog huis-aan-huis
een enquête om te kijken
wie er met de GGD naar ziekenhuis
of verzorgingshuis gebracht
moet worden.”

De operatie, zo schat De Ruiter,
zal in totaal zes miljoen
gulden kosten. Tevens is een
verzekering afgesloten om eventuele
schade tot 50 miljoen gulden
te dekken. „Onze doelstelling
is alles zo soepel mogelijk
te laten verlopen en de hinder
tot een minimum te beperken.”
Maar wat zal er gebeuren als de
Explosieven opruimingsdienst
er niet in slaagt binnen een dag
de bommen te ontmantelen? De Ruiter:
„Dan komen we in de
problemen. Achter in ons hoold
houden we daar wel rekening
mee. Maar laten we het niet hopen.”
