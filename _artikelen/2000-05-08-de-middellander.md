---
toegevoegd: 2023-01-12
gepubliceerd: 2000-05-08
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2000-05-08-de-middellander-origineel.jpg"
onderwerpen: [eeuwfeest, fiets]
huisnummers: [30]
achternamen: [van-hooijdonk, lutgerink, gelderblom, van-der-ploeg, knulst, spitters, lassooij, poot, wachsman]
kop: "Jan Sonjéstraat 100 jaar"
---
# **Jan Sonjéstraat** 100 jaar

door Ineke Westbroek

„In deze straat lukt het om mensen die heel erg van
elkaar verschillen bij elkaar te krijgen.” Een grote
kracht van de honderdjarige **Jan Sonjéstraat**, vindt Aad Lutgerink,
die al eenendertig jaar in de straat woont.
Als onderstreping van zijn stelling wapperden in de **Jan Sonjéstraat**
de vlaggen van alle nationaliteiten
van mensen die er wonen.

Uit het gedenkboekje ‘Even terug in
de tijd’, dat is samengesteld door de
straatgroep Jan Sonjé en geschreven
en geïllustreerd is door de bewoners
Oscar Gelderblom en Jan van der Ploeg,
valt op te maken dat de
mensen in de **Jan Sonjéstraat** zowel
in het heden als in het verleden een
hechte groep hebben gevormd.
Tijdens de jubileumreünie, die de
straatgroep op 11 maart *2000* in het
Trefcentrum organiseerde, werd
het eerste exemplaar uitgereikt aan
de oudste bewoner, de heer Jan van Hooijdonk (97).

Even terug in de tijd beschrijft de
afgelopen honderd jaar **Jan Sonjéstraat**
en geeft een beeld van
de straat anno nu. Veel voormalige
bewoners hebben meegewerkt aan
het boek, door oude foto's aan te
leveren. De oud-bewoners, die verspreid
over het hele land blijken te
wonen, reageerden op oproepen in
kranten, die de straatgroep had
geplaatst. De familie Knulst, die van
1925 tot 1935 met negen broertjes
en zusjes op 33b woonde, vond het
leuk om gehoor te geven aan de
oproep voor foto's en deelname
aan de reünie. Zij hebben hun
straatje nooit kunnen vergeten. De
heer Knulst, die tegenwoordig in
Berkel en Rodenrijs woont, weet
nog precies alle bedrijfjes in de
straat, zoals de boordenwasserij
waar zijn vader de boorden liet
stijven. Ook de garen- en bandzaak
van de gezusters Ladage ligt hem
nog vers in het geheugen: „Daar
was een blinde muur. Als we daar
met ballen tegenaan gooiden, kwamen
ze naar buiten, want die ballen
belandden wel eens tegen de
ramen.”
Zijn zus (zij woont alweer vele
jaren in Schiebroek) weet nog hoe
zij 's avonds, gezeten op de tuintrap,
urenlang over de schutting
met haar buurmeisje zat te kletsen:
„We hadden het over van alles,
over school, over jongens. Ik heb
hier wel een stuk of vier vrijers
gehad.”

## Aanvaringen met oom agent

Touwtjespringen, fietsen, ‘koten’,
slagballen, van put naar put lopen
en natuurlijk voetballen, dat waren
de favoriete spelletjes in de **Jan Sonjéstraat**.
Het probleem rond
voetballende jongens in een te kleine
straat blijkt van alle tijden te zijn.
Een oud-bewoner, die zijn jeugd in
de straat heeft doorgebracht, herinnert
zich de aanvaringen met politieagenten
van het dichtbij gelegen
bureau Duivenvoordestraat: „Dan
had je de mogelijkheid dat je balletje
werd afgepikt. Die agent kwam
altijd de straat in op een fiets met
een hele grote bel: ping pang! Op
de wijs van die bel riepen wij dan:
‘De lul!’”
Dierbare herinneringen koesteren
de ex-bewoners aan de chique
mevrouw Spitters, die zich niet te
goed voelde om urenlang het
springtouw te draaien, met om zich
heen een grote kluit touwtjespringende
kinderen.
Nog steeds voelen ouders in de **Jan Sonjéstraat**
zich betrokken bij alle
kinderen in de straat. Een
Kaapverdiaanse oud-bewoonster,
een maand geleden verhuisd naar
IJsselmonde („We hadden een groter
huis nodig met een tuin, in een
rustiger buurt waar kinderen veilig
kunnen spelen”) denkt met weemoed
aan het gevoel van onderlinge
saamhorigheid, waarmee ouders
en kinderen er samen op uittrokken:
„We gingen samen naar het
park, deden spelletjes. Al die leuke
dingen mis ik. Wat dat betreft is het
jammer dat we weg zijn gegaan.”

## Plantdagen en straatdiners

Een grote rol bij het bevorderen
van het saamhorigheidsgevoel spelen
de activiteiten van de in 1991
opgericht straatgroep, die straatdiners
en gezamenlijke plantdagen
organiseert. Samen met Aad Lutgerink
en Don Lassooij speelt
Jan Poot een centrale rol bij deze
activiteiten. Hij woont sinds 1954 in
de straat. Oorspronkelijk komt hij
uit Veendam. „Er is veel veranderd
in de straat”, blikt hij terug, „vroeger
woonden er alleen maar
Nederlanders, nu leven wij met
verschillende culturen.” Een ander
opvallend verschil: „Vroeger had je
alleen maar een fiets, die je gemakkelijk
kwijt kom, nu hebben we allemaal
een auto.” Zijn mooiste herinnering:
„Toen ik mijn eigen huis
kocht en hier kwam wonen met
mijn gezin. Een waanzinnig goed
huis voor die prijs. Het bewijs: ik
woon er nog!”

## Angstige ogenblikken

Even terug in de tijd wijdt ook een
hoofdstuk vaan de **Jan Sonjéstraat** tijdens
de Tweede Wereldoorlog.
Beschreven wordt hoe bewoners
zoveel mogelijk hun gewone leven
probeerden voort te zetten. Ook in
de **Jan Sonjéstraat** eiste de oorlog
slachtoffers. Zoals de joodse bewoner
van 35a, die werd afgevoerd
nadat bleek dat niemand in de
straat hem durfde te laten onderduiken.
Het zoontje van de kleermaker *(Wachsman)*
op 30b, Ietje (voluit Isaac *(Ilan)*)
werd wel een helpende hand
gereikt, maar zijn ouders konden
niet van hem scheiden. Niemand uit
dit gezin keerde terug.
De angstige ogenblikken tijdens
bombardementen — zoals de voor
de Sicherheitsdienst bestemde
‘Bellebom’ — bleven veel oud-bewoners
hun leven lang bij. „Mijn moeder
en ik waren samen thuis toen
we hoorden schieten”, vertelt een
oud-bewoner, die de oorlog als
kind meemaakte, „die bom viel
schuin achter onze woning. Mijn
moeder en ik stonden in een hoekje
in een nis. Zo hebben wij elkaar
vastgehouden. Was hij ontploft, dan
had ik dit waarschijnlijk nooit kunnen
vertellen.”

*Onderschrift bij de foto*
De stemming zat er goed in tijdens het lied
