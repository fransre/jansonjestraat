---
toegevoegd: 2024-07-24
gepubliceerd: 1957-05-09
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010952904:mpeg21:a0051"
onderwerpen: [dieren, huisraad, tekoop]
huisnummers: [3]
achternamen: [romeijn]
kop: "aquarium"
---
Te koop

# aquarium

80×50×35 m*et*
planten, verwarming, lichtbak
en tafel ƒ25. Romeijn, **Jan Sonjéstraat** 3b.
