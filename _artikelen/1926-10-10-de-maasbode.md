---
toegevoegd: 2022-07-23
gepubliceerd: 1926-10-10
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000196385:mpeg21:a0116"
onderwerpen: [woonruimte, tekoop]
huisnummers: [36]
kop: "Jan Sonjéstraat numero 36"
---
Het pand en erve te Rotterdam
aan de

# **Jan Sonjéstraat** n*ummer* 36

zal op Woensdag 13 October 1926,
's nam*iddags* 2 uur in het Notarishuis aldaar,
door de Notarissen J.M. Slinkert
en P.J.M. Dolk definitief
worden afgeslagen. Het perceel staat
in trekgeld op ƒ11.500.—.

65778 10
