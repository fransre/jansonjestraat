---
toegevoegd: 2024-08-08
gepubliceerd: 1926-09-13
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495448:mpeg21:a0086"
onderwerpen: [werknemer]
huisnummers: [36]
achternamen: [werner]
kop: "strijkster"
---
Bekwaam Overhemd-

# strijkster

gevraagd. Electrische
Wasch- en Strijkinrichting.
C. Werner v*an* Lunteren,
**Jan Sonjéstraat** 36.
