---
toegevoegd: 2024-08-05
gepubliceerd: 1932-12-22
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164662060:mpeg21:a00058"
onderwerpen: [bedrijfsinformatie]
kop: "Onderlinge Vrienden."
---
# Onderlinge Vrienden.

Zaterdagavond *17 december 1932* heeft de Rotterd*amsche* Brandweervereeniging
„Onderlinge Vrienden” van geaffecteerden
van slangenwagen 56 haar halfjaarlijksche
cabaretavond gehouden in de zaal
„Kuyt” aan de **Jan Sonjéstraat**.

Nadat de voorzitter, de heer F.A. Priem, het
feest met een kernachtige rede had geopend,
voerde het duo Harry Wolff een even fraai als
varieerend programma van vermakelijkheden
uit. Menigmaal daverde het in de zaal van de
pret.

Een prettig bal besloot het geslaagde feest.
