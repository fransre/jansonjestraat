---
toegevoegd: 2024-08-14
gepubliceerd: 1907-04-08
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196644:mpeg21:a0112"
onderwerpen: [gevraagd]
huisnummers: [18]
kop: "Ter Overname"
---
# Ter Overname

gevraagd een Mangel en
een Waterfornuis van 90
à 100 L*iter* inhoud. Adres:
Wasch- en Strijkinrichting
**Jan Sonjéstraat** 18,
nabij de Schietbaanlaan.
