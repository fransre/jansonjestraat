---
toegevoegd: 2024-08-14
gepubliceerd: 1907-02-08
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196596:mpeg21:a0089"
onderwerpen: [huispersoneel]
huisnummers: [11]
kop: "Dagmeisje"
---
Gevraagd een flink

# Dagmeisje

in een gezin van 2 Personen,
goed kunnende
werken en wasschen.
Aanbieding in persoon:
**Jan Sonjéstraat** 11, Bovenhuis.
