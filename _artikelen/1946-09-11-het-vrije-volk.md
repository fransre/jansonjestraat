---
toegevoegd: 2024-07-26
gepubliceerd: 1946-09-11
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955019:mpeg21:a0022"
onderwerpen: [bedrijfsruimte]
huisnummers: [34]
kop: "Gedeelte souterrain"
---
# Gedeelte souterrain

te huur voor werk- of
bergplaats. **Jan Sonjéstraat** 34b.
