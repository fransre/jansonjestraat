---
toegevoegd: 2024-08-13
gepubliceerd: 1915-09-21
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296116:mpeg21:a0157"
onderwerpen: [kunst]
huisnummers: [5]
achternamen: [roodenburg]
kop: "Nationale Vereeniging voor de Volkszang"
---
# Nationale Vereen*iging* v*oor* d*e* Volkszang

Afd*eling* Rotterdam,

(gesteund door het Alg*emeen* Ned*erlandsch* Verb*ond*)

De Volkszangavonden
zullen wederom aanvangen op
Woensdag 6 October 1915,
's avonds van 8-9 uur, in de
Zaal van de Dames Blom, Stationsweg 37a.

G. den Hartogh, Voorzitter,
's Gravendijkwal 49b.

W.J. Roodenburg, Secretaris,
**Jan Sonjéstraat** 5.

Mej*uffouw* C. Scholten,
Leidster der Zangavonden,
's Gravendijkwal 49b.

39668 17
