---
toegevoegd: 2024-07-21
gepubliceerd: 1988-01-29
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003063024:mpeg21:a00004"
onderwerpen: [bellebom]
kop: "Bloemen na heigeweld"
---
# Bloemen na heigeweld

Mevrouw De Jong
uit de **Jan Sonjéstraat**
in de Rotterdamse wijk
Middelland kreeg gisteren *28 januari 1988*,
evenals de bewoners
van 25 andere huizen
in de buurt, een bos
bloemen als goedmakertje
voor de overlast
die het heien in hun
achtertuinen had veroorzaakt.

Daar moest een stalen
damwand worden geslagen
in verband met
de zoekactie naar twee
Engelse vliegtuigbommen,
die daar al sinds
de Tweede Wereldoorlog
zouden liggen.

Als de bommem inderdaad
worden gevonden,
zal de Explosieven Opruimingsdienst
die op
27 maart *1988* onschadelijk
maken. Zo'n 7.000 wijkbewoners
zullen dan
hun huis uit moeten,
terwijl zo'n 14.000 anderen
in de iets wijdere
omgeving een dag lang
binnen zullen moeten
blijven. (Foto Peter
van Es)
