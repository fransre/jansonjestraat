---
toegevoegd: 2024-05-14
gepubliceerd: 1940-02-16
decennium: 1940-1949
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005424059:00001"
onderwerpen: [dans]
huisnummers: [23]
achternamen: [pijpeman]
kop: "Joodsche Dansclub"
---
# Joodsche Dansclub

Hier ter stede is opgericht de Joodsche Dansclub J.D.C. Rotterdam.
Bestuurderen zijn de
heeren N. Drielsma, Voorzitter, J. Pijpeman,
1e Secretaris, **Jan Sonjéstraat** 23, tel*efoon* 33577 en
B. Hammelburg, Penningmeester.
