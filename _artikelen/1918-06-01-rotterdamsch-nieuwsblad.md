---
toegevoegd: 2021-04-29
gepubliceerd: 1918-06-01
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297024:mpeg21:a0116"
onderwerpen: [diefstal]
kop: "Zeep, chocolade, peper,"
---
# Zeep, chocolade, peper,

en plantenvet werden ontvreemd uit een pakhuis
uit de **Jan Sonjéstraat**. Voor dezen diefstal
stond de 21-jarige groentenkoopman J.J. K.
terecht, die met het vrachtje op 5 April *1918* l*aatst*l*eden*
werd aangehouden, toen hij het per as vervoerde.
De bereden politie was door een jongen gewaarschuwd
en had het rijtuig in galop over
de keien achtervolgd. Beklaagde ontkende den
diefstal. Hij was enkel de „uitvoerende hand”
van een onbekende geweest, zei hij.

Het O*penbaar* M*inisterie* eischte 1 jaar gevangenisstraf,
waarna mr. Masthoff voor vrijspraak pleitte.
