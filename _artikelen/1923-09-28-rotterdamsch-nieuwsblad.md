---
toegevoegd: 2024-08-11
gepubliceerd: 1923-09-28
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494909:mpeg21:a0172"
onderwerpen: [diefstal, fiets]
kop: "De onvermoeide zwijnenjagers."
---
# De onvermoeide zwijnenjagers.

~~Uit een pand aan de Maasstraat is van
W. V., uit de Osseweistraat, een rijwiel gestolen.~~

Ten nadeele van don rijwielhandelaar J. D.,
uit de **Jan Sonjéstraat**, is door iemand,
die zich „Jan Van Trigt” noemde, een rijwiel
verduisterd. Door denzelfden zwijnenjager
werd vervolgens, thans onder den
naam van „Jan De Vos” een rijwiel gehuurd
bij J.C. L. aan de S*in*t Mariastraat,
die eveneens niets meer van hem hoorde.
