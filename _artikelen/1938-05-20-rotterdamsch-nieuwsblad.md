---
toegevoegd: 2024-08-01
gepubliceerd: 1938-05-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164695016:mpeg21:a00078"
onderwerpen: [fiets, tekoop]
huisnummers: [5]
achternamen: [oostenveld]
kop: "Damesrijwiel,"
---
# Damesrijwiel,

lamp, nieuwe Binnenbanden,
Terugtrapnaaf,
Handrem. Lage Prijs. Te
bevr*agen* Oostenveld,
**Jan Sonjéstraat** 5.
