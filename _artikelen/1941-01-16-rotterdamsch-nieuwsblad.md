---
toegevoegd: 2024-07-29
gepubliceerd: 1941-01-16
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002623:mpeg21:a0021"
onderwerpen: [kleding, tekoop]
huisnummers: [3]
kop: "Te koop"
---
# Te koop

zwarte Seals-mantel,
nog prima ƒ27.50, maat 46.
**Jan Sonjéstraat** 3b.
bovenhuis.
