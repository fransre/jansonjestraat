---
toegevoegd: 2024-08-07
gepubliceerd: 1927-09-19
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514095:mpeg21:a0085"
onderwerpen: [huisraad, tekoop]
huisnummers: [31]
achternamen: [van-den-ende]
kop: "Emaille fornuis"
---
# Emaille fornuis

te koop aangeboden,
in prima staat, vasten
prijs 17.50. A. van den Ende,
**Jan Sonjéstraat** 31a.
