---
toegevoegd: 2024-08-07
gepubliceerd: 1927-03-03
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010513930:mpeg21:a0131"
onderwerpen: [werknemer]
huisnummers: [41]
kop: "hulp-monteur"
---
B*iedt* z*ich* a*an* Automobiel-

# hulp-monteur,

v*an* g*oede* g*etuigen* v*oor*z*ien*, 3 jaar in 't
vak. **Jan Sonjéstraat** 33.
Tel*efoon* 37568.
