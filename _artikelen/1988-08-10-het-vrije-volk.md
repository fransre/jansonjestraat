---
toegevoegd: 2021-04-29
gepubliceerd: 1988-08-10
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962863:mpeg21:a0141"
onderwerpen: [cafe, misdrijf]
huisnummers: [15]
kop: "Man gebruikt hakbijl in bar"
---
# Man gebruikt hakbijl in bar

Rotterdam — De 32-jarige eigenaresse
van een bar aan de **Jan Sonjéstraat**
in Rotterdam is ernstig gewond
in het ziekenhuis Dijkzigt opgenomen
nadat een 55-jarige klant haar met een
hakbijl had geslagen. De man, een inwoner
van Rotterdam, is inmiddels door
de politie aangehouden. De politie weet
niet waarom de man de vrouw heeft geslagen.
