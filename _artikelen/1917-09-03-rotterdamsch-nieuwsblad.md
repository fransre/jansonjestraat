---
toegevoegd: 2024-08-13
gepubliceerd: 1917-09-03
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296702:mpeg21:a0095"
onderwerpen: [huisraad, tekoop]
huisnummers: [14]
achternamen: [boslooper]
kop: "Te koop:"
---
# Te koop:

notenhouten Garderobe
en een Inmaakpot, inhoud
25 pond boter. Te
bezichtigen bij J. Boslooper,
**Jan Sonjéstraat** N*ummer* 14a.
