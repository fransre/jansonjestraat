---
toegevoegd: 2024-08-14
gepubliceerd: 1909-12-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010198057:mpeg21:a0133"
onderwerpen: [tekoop, dieren]
huisnummers: [36]
achternamen: [van-biezen]
kop: "Raskonijnen,"
---
# Raskonijnen,

Marmotten. Te koop
Brandneuzen, Black and
Tans, witte Vlaamsche
Reuzen en Zilverkonijnen,
zeer billijk, voor 1
gulden, 3 halfwas Marmotten.
Adres v*an* Biezen,
**Jan Sonjéstraat** 36a.
Te bevragen des
middags van 3 tot 6 uur.
