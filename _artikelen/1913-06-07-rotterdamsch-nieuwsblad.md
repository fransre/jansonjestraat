---
toegevoegd: 2024-08-14
gepubliceerd: 1913-06-07
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010222477:mpeg21:a0083"
onderwerpen: [huisraad, tekoop]
huisnummers: [19]
kop: "Cylinderbureau."
---
# Cylinderbureau.

Ter overname aangeboden
een modern massief
eiken Cylinderschrijfbureau
met veel
berging en een massief
noten Vitrine, Theetafel
met geslepen ruiten, door
omstandigheden voor billijken
prijs. Te bezichtigen
**Jan Sonjéstraat** 19b
tusschen Middellandstr*aat*
en Schermlaan.
