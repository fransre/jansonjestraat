---
toegevoegd: 2024-08-10
gepubliceerd: 1925-05-18
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495478:mpeg21:a0121"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [termijn]
kop: "C.M. Termijn-Hofman,"
---
Voor de vele blijken van belangstelling
ondervonden na het overlijden
van onze geliefde Vrouw en
Moeder

# C.M. Termijn-Hofman,

betuigen wij U onzen hartelijken dank.

J. Termijn en Kinderen.

R*otter*dam, Mei 1925.

**Jan Sonjéstraat** 33.

10
