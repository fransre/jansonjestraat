---
toegevoegd: 2024-03-07
gepubliceerd: 1909-05-13
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197428:mpeg21:a0134"
onderwerpen: [verloren]
huisnummers: [4]
achternamen: [van-duuren]
kop: "Gevonden."
---
# Gevonden.

Tegen Advertentie-onkosten
terug te bekomen
één gekleurd Overhemd
en vier hooge Boorden,
gemerkt, waarschijnlijk
door Wasscherij verloren
in de Middellandstraat.
Adres **Jan Sonjéstraat** 4,
J.G.A. v*an* Duuren.
