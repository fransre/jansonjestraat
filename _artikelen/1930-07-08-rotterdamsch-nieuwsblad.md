---
toegevoegd: 2024-08-06
gepubliceerd: 1930-07-08
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164648009:mpeg21:a00130"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [valster]
kop: "Pieter Valster."
---
Dankbetuiging.

Hiermede betuigt onderget*ekende* haren
hartelijken dank voor de groote belangstelling
en medegevoel, ondervonden
bij het overlijden en beäarden
van onzen geliefden Man, Vader, Behuwd- en
Grootvader,

# Pieter Valster.

Mede namens verdere familie.

Wed*uwe* P. Valster-van Beek.

**Jan Sonjéstraat** 24, R*otter*dam.

14
