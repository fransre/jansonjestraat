---
toegevoegd: 2024-08-13
gepubliceerd: 1912-03-23
decennium: 1910-1919
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000177628:mpeg21:a0011"
onderwerpen: [faillissement]
huisnummers: [21]
achternamen: [slik]
kop: "Gerechtelijke aankondigingen."
---
# Gerechtelijke aankondigingen.

Bij vonnis der arrondissements-rechtbank te Rotterdam, dd.
20 Maart 1912, is J.H. Slik, stukadoor, wonende **Jan Sonjéstraat** 21B,
te Rotterdam, verklaard in staat van faillissement,
met benoeming van den edelachtbaren heer mr. C. Star Busmann,
rechter in gemelde rechtbank, tot rechter-commissaris, en van
ondergeteekende tot curator.

Rotterdam, 21 Maart 1912.

Nieuwehaven 93.

Mr. W. Nolen,

Advocaat en Procureur.

[Kosteloos.]

(1574)
