---
toegevoegd: 2024-08-05
gepubliceerd: 1932-04-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164658061:mpeg21:a00087"
onderwerpen: [woonruimte, tehuur]
huisnummers: [22]
kop: "Te huur:"
---
# Te huur:

per 1 Juni *1932* a*an*s*taande* **Jan Sonjéstraat** 22,
Benedenhuis het Souterrain
en Tuin, bevattende
boven Voor-, Tusschen- en
Achterkamer Beneden
een groote en kleine
Kamer. Huurprijs
ƒ40 p*er* m*aand* Te bevragen
Bovenhuis.
