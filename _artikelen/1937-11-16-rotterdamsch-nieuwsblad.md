---
toegevoegd: 2024-08-02
gepubliceerd: 1937-11-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692014:mpeg21:a00075"
onderwerpen: [huisraad, fiets, muziek, tekoop]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "Te koop:"
---
# Te koop:

1 Nieuwe Damesfiets,
groot Schrijfbureau Duitsche
Piano, Eet-, Ontbijt- en Theeservies.
Wegens vertrek n*aar* Indië.
L. v*an* d*en* Bos, **Jan Sonjéstraat** 36.
