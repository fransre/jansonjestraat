---
toegevoegd: 2024-08-13
gepubliceerd: 1915-10-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296129:mpeg21:a0144"
onderwerpen: [tekoop]
huisnummers: [28]
achternamen: [de-mink]
kop: "Postzegels"
---
# Postzegels

te koop aangeboden
voor beginnende en
meergevorderde Verzamelaars.
Ook ruiling. Tevens
inkoop van alle
soorten. Billijkst Adres.
J. de Mink. **Jan Sonjéstraat** 28B.
