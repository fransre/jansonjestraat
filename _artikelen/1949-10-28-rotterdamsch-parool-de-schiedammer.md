---
toegevoegd: 2021-04-30
gepubliceerd: 1949-10-28
decennium: 1940-1949
bron: Rotterdamsch Parool / De Schiedammer
externelink: "https://schiedam.courant.nu/issue/SP/1949-10-28/edition/0/page/4"
onderwerpen: [werknemer]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "Heren Kleermakers gevraagd"
---
# H*eren* Kleermakers gevraagd

voor thuiswerk, grootwerker,
klasse B, halfwas of naaister
voor atelier. Jac. A.B. van Hooijdonk,
**Jan Sonjéstraat** 13,
tel*efoon* 37180.
