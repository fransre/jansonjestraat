---
toegevoegd: 2024-07-29
gepubliceerd: 1940-10-10
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002479:mpeg21:a0114"
onderwerpen: [misdrijf, radio]
kop: "Een huis vol familie"
---
Politierechter

# Een huis vol familie

Schoonmama is „de zure bom”

Plaats van handeling: de **Jan Sonjéstraat**,
een pand met twee étages. De
eerste étage wordt door mama met haar
ongetrouwde dochter en zoon bewoond,
de tweede étage door haar getrouwde
dochters.

Deze drie gezinnen leven sinds lang
op een gespannen voet. Zij treiteren en
negeren elkaar van 's morgens tot
's avonds. Als schoonmama b*ij*v*oorbeeld* naar
haar radio zit te luisteren, wordt het
plotseling verdacht stil in den aether…

Dan gaat de zoon de trappen op naar
den zolder, klimt op het dak en ontdekt,
dat de antennnedraad is doorgeknipt…

Er wordt dan op de tweede verdieping
hartelijk gelachen. Er is vreugde.

Doet de familie op de tweede étage
de straatdeur dicht, dan trekt de familie
op de eerste étage deze weer open.
Komen de schoonzoons hun schoonmama
tegen, dan loopen ze haar bijna
de trap af. Zij is hun nachtmerrie, de
zure bom, die beiden onpasselijk maakt.
Ook de vrouwen liggen met haar moeder
overhoop. Kortom, dit huis vol familieleden
is een huis van verschrikking.

Op een dag, dat de antenne doorgesneden
was en de ongetrouwde zoon op
den gemeencshappelijken zolder aan het
herstellen was gegaan, was de schoonzoon-banketbakker
naar boven geloopen… Er waren te voren reeds woorden
gevallen en toen hij voetstappen op
de zoldertrap hoorde en het grijze bebrilde
hoofd van zijn schoonmoeder
ontwaarde, zeide hij hatelijk:

— Zoo vuile… en toen volgde het
verschrikkelijke woord… daar heb je
dat oude loeder óók.

Gisteren *9 oktober 1940*, toen dit beleedigingszaakje
voor den rechter diende, vroeg deze aan
den banketbakker, of hij iets tegen zijn
schoonmama had.

— 't Is een serpent, antwoordde deze,
maar dat ééne woord heb ik niet gezegd.
Dat liegt ze.

Toen kwam schoonmama. Deze sprak
steeds over „mijnheer mijn schoonzoon”.
Toen ze op den zolder was gekomen,
had hij gevraagd, doelende op
den aan de antenne werkenden zoon:

— Ben je soms bang, dat ik hem opvreet?

— Als je soms honger hebt, kan je
van mij nog wel een boterham krijgen,
had schoonmama gerepliceerd.

— Die heb ik van jou niet noodig.

De rechter, die schoonmama de keuze
had gelaten niet tegen haar schoonzoon
onder eede te getuigen, gaf zijn poging
daartoe spoedig op.

— Och ja, ik kan het me begrijpen,
U wilt hier even uw hart luchten.

Ze praatte over haar getrouwde
dochters. Nou ja… hier te kort en
daar te lang…

De rechter sprak, dat in Amerika de
filmsterren ook haar eigen leven leefden.

— Nu ja en toen had hij haar op die
gemeene manier uitgescholden.

De banketbakker zat steeds ontkennend
te schudden in de zondaarsbank.

— Zij heeft een groote fantasie, betoogde
hij minachtend.

— U heeft geen eerbied voor uw
schoonmoeder, verweet de rechter.

— Ik niet? Hoe kan ik voor haar
hoogachting hebben? Elken dag ruzie.

Dan kwam de 19-jarige ongetrouwde
dochter, die ook over haar zwagers en
zusters een boekje open deed en ook zij
had de gemeene scheldwoorden gehoord.

Maar de banketbakker had zijn vrouw
meegebracht, die wel tegen haar moeder
wilde getuigen, hoewel de rechter
moeite deed er haar van af te houden.

— Want kijk eens, — legde de magistraat
uit — het geldt hier slechts een
bagatel en er komen hier méér vrouwen,
die een meineed plegen om hun mannen
te helpen en dan voor langen tijd naar
de gevangenis verhuizen.

Deze dochter wilde echter getuigen
tégen haar moeder, die haar man —
zooals zij zich ladylike uitdrukte —
„het vreten uit zijn muil had gehaald”.
Maar de scheldwoorden had zij niet
gehoord, dus mocht ze gaan zitten naast
haar mama en zuster. Toen er nog een
knoop bleek te zijn in het verhoor,
moest ze opnieuw voor het hekje komen
en daarbij mocht zij ook even haar hart
luchten.

Maar de edelachtbare wist het niet
wat voor moeder zij had. Was het niet
verschrikkelijk om een dochter aan te
wrijven, dat ze niet fatsoenlijk was? En
dan door de heele buurt.

De „buurt” — vertegenwoordigd door
talrijke **Jan Sonjéstraat**dames — stond
op de tribune. Men genoot er.

De rechter veroordeelde den banketbakker
ten slotte — conform den eisch
— tot een tientje boete of vier dagen
brommen.

— Dat is ongehoord, vond de banketbakker.
Het zijn allemaal leugens van
„mevrouw mijn schoonmoeder”. Ze
liegt door de heele familie en staat er
voor bekend.

Daarmede was dit gerechtelijk drama
afgeloopen. De plaats van den banketbakker
werd door zijn zwager ingenomen,
die meubelmaker is. Ook hij had
— met een kleine variatie — zijn schoonmoeder
eveneens diep beleedigd in haar
eer. Hij had er bij gevoegd:

— Je bent uit Amerika geknokt…

De rechter kon hem gelooven of niet,
maar die schoonmoeder — intusschen
voor het hekje getreden met vlammende
blikken — stond daar nu wel poeslief,
maar je moest haar in werkelijkheid
eens zien. Dan keek ze zoo zedig
niet, dat loeder.

De ongetrouwde zoon kwam bevestigen,
dat hij den meubelmaker de woorden
tegen zijn moeder had hooren uiten.

Rechter: — Heb je zijn lippen zien
bewegen, toen hij de heerlijke woorden
door den aether deed vloeien?

— Jawel edelachtbare. Ik heb het gezien.
Toen kwam de banketbakker als getuige
a décharge en gooide er een
schepje op, natuurlijk ten voordeele van
zijn zwager-meubelmaker.

Hij vertelde, dat er altijd herrie en
schelden was en zijn schoonmoeder hem
soms met een bezem te lijf wilde.

Hij mocht ook gaan zitten, doch koos
een andere bank, zoodat hij niet op dezelfde
zat als zijn gehate schoonmama.

Ook kwam de banketbakkersvrouw
weer als getuige en opnieuw, hekelde
zij het gedrag van haar moeder, die
haar uitmaakte, waarvoor zij zélf was
gescholden.

De rechter zuchtte diep en veroordeelde
ook den meubelmaker wegens
beleediging van zijn schoonmama tot
eenzelfde boete.

Op den Noordsingel wachtten de familieleden
elkaar op…

Het verdere verloop zal wel spoedig
weer in de rechtszaal behandeld worden.
