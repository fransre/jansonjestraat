---
toegevoegd: 2024-07-28
gepubliceerd: 1941-10-30
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002801:mpeg21:a0014"
onderwerpen: [huisraad, tekoop]
huisnummers: [3]
kop: "Te koop"
---
Te koop insteekhaard.
plaat ƒ30.—. Goed
brandend, wegens pl*aats*-gebrek.
**Jan Sonjéstraat** 3B
(bovenhuis).
