---
toegevoegd: 2024-08-06
gepubliceerd: 1930-05-05
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164647005:mpeg21:a00170"
onderwerpen: [huispersoneel]
huisnummers: [35]
kop: "Dagmeisje"
---
# Dagmeisje

gevraagd, leeftijd 16 j*aar*,
Gezin z*onder* K*inderen*. Zaterdagmiddag
en Zondag den
geheelen dag vrij. **Jan Sonjéstraat** 35a.
