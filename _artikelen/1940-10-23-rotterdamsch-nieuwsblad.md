---
toegevoegd: 2024-07-29
gepubliceerd: 1940-10-23
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002496:mpeg21:a0008"
onderwerpen: [bedrijfsruimte, tehuur]
huisnummers: [34]
kop: "Repetitielokaal"
---
# Repetitielokaal

of Werkplaatsje te
huur, electr*isch* licht, gas,
str*omend* water, stookgel*egenheid*.
**Jan Sonjéstraat** 34B.
