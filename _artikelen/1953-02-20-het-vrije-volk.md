---
toegevoegd: 2024-07-25
gepubliceerd: 1953-02-20
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951479:mpeg21:a0051"
onderwerpen: [huisraad, tekoop]
huisnummers: [24]
achternamen: [rijswijk]
kop: "wasmachine"
---
Te koop electr*ische*

# wasmachine

(Velo) t*egen* e*lk* a*annemelijk* b*od* of ruilen voor
keukengeyser Rijswijk. **Jan Sonjéstraat** 24a,
Rotterdam.
