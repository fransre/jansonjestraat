---
toegevoegd: 2021-04-29
gepubliceerd: 1913-09-24
decennium: 1910-1919
bron: Particuliere collectie
externelink: "https://www.jansonjestraat.nl/originelen/img-20200430-wa0001-wa0002.jpg"
onderwerpen: [bewoner]
huisnummers: [22]
achternamen: [de-smit]
kop: "Waarde Familie de Vijlder!"
---
# Waarde Familie de Vijlder!

Ziehier onze woning en
het gehele huisgezin
behalve den baas zelf.

N*ummer* 1 is Anna

N*ummer* 2 is Wilhelm

N*ummer* 3 is Marietje

N*ummer* 4 is Piet

en Johan staat links van Piet

Binnenin de voorkamer zit
Roza, de grootste, met Lena,
de jongste, en Moe zit achter
de gordijnen.

24 september 1913

P.J. de Smit
