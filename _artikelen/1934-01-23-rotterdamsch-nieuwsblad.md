---
toegevoegd: 2024-08-04
gepubliceerd: 1934-01-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164669025:mpeg21:a00172"
onderwerpen: [huispersoneel]
huisnummers: [27]
kop: "Net meisje"
---
# Net meisje

gevraagd bij Dame alleen
van 9½-3 uur.
Aammelden van 9-12
's v*oor*m*iddag* **Jan Sonjéstraat** 27,
Bovenhuis.
