---
toegevoegd: 2024-08-05
gepubliceerd: 1932-10-17
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164661052:mpeg21:a00086"
onderwerpen: [woonruimte, gevraagd]
huisnummers: [8]
achternamen: [disse]
kop: "Gevraagd:"
---
# Gevraagd:

Woning voor Weduwe
m*et* Dochter, ƒ5 à ƒ6,
Westen, 1ste Etage of
Ben*eden* of ongem*eubileerde* Voork*amer*
Br*ieven* Disse, **Jan Sonjéstraat** 8a.
