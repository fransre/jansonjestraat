---
toegevoegd: 2024-08-07
gepubliceerd: 1928-07-09
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514340:mpeg21:a0054"
onderwerpen: [werknemer, fiets]
huisnummers: [17]
kop: "Jongen"
---
# Jongen

gevraagd met Transportfiets
kunnende rijden
ƒ8.00 p*er* w*eek* **Jan Sonjéstraat** 17.
