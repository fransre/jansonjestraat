---
toegevoegd: 2024-08-03
gepubliceerd: 1936-01-13
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164681014:mpeg21:a00037"
onderwerpen: [verloren]
huisnummers: [7]
achternamen: [klok]
kop: "Verloren:"
---
# Verloren:

Bankbiljet ƒ10, 2en Kerstdag.
Bootje Schiemond,
lijn 15 Centraal Station.
Belooning. **Jan Sonjéstraat** 7,
Wed*uwe* J. Klok.
