---
toegevoegd: 2024-08-05
gepubliceerd: 1932-01-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164657025:mpeg21:a00088"
onderwerpen: [werknemer]
huisnummers: [34]
achternamen: [bosselaar]
kop: "Naaimeisje"
---
# Naaimeisje

gevraagd, circa 14 j*aar*,
om kosteloos Naaien te
leeren, P*rotestantse* G*ezindte* of g*oede* g*etuigenis* a*an*m*elden*
Bosselaar, **Jan Sonjéstraat** 34.
