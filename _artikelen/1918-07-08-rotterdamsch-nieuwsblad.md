---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297055:mpeg21:a0111"
onderwerpen: [bedrijfsinformatie]
huisnummers: [15]
achternamen: [boer]
kop: "A.E. Boer"
---
Groothandel in Spiegel- en Vensterglas

# A.E. Boer

Opening van het Filiaal

**Jan Sonjéstraat** 15a, b, c,

Telefoon N*ummer* 14332

op maandag 8 juli 1918.

Hoofdkantoor en magazijnen
te Schiedam:

Broersveld 76-82. — Tel*efoon* N*ummer* 239.

36474 60
