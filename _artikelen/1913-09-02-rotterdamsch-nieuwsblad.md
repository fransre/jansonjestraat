---
toegevoegd: 2024-08-13
gepubliceerd: 1913-09-02
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295728:mpeg21:a0063"
onderwerpen: [werknemer]
huisnummers: [29]
kop: "Huisnaaister"
---
# Huisnaaister

voor Verstelwerk gevraagd,
bekend met het
maken van nieuw Ondergoed,
voor 1 dag om
de 14 dagen. Adres **Jan Sonjéstraat** 29A,
Boven.
