---
toegevoegd: 2024-07-20
gepubliceerd: 1930-05-24
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164647028:mpeg21:a00099"
onderwerpen: [huispersoneel]
huisnummers: [34]
kop: "jonge werkster"
---
Nette

# jonge werkster

gevraagd, liefst voor
den Vrijdag. Aanmelden
's avonds 7-9 uur
**Jan Sonjéstraat** 34a.
