---
toegevoegd: 2024-08-05
gepubliceerd: 1932-11-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164662012:mpeg21:a00200"
onderwerpen: [bedrijfsinformatie]
huisnummers: [2]
achternamen: [smit]
kop: "J. Smit,"
---
Oproeping.

Allen, die iets te vorderen hebben
of onder berusting hebben van wijlen
den Heer

# J. Smit,

Willem Nagellaan 37, Hillegersberg,
zaak doende **Jan Sonjéstraat** 2, Rotterdam,
gelieven daarvan opgave te
doen vóór 14 November *1932* a*an*s*taande* aan Accountantskantoor
J. Roodenburg,
1e Middellandstraat 98b, R*otter*dam.

41719 15
