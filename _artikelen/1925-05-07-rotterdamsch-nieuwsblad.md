---
toegevoegd: 2024-08-10
gepubliceerd: 1925-05-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495469:mpeg21:a0237"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [termijn]
kop: "Clinia Margaretha Termijn-Hofman,"
---
Heden overleed tot onze groote
droefheid na een smartelijk en
langdurig lijden onze lieve Vrouw,
Moeder, Aanbehuwd- en Grootmoeder,

# Clinia Margaretha Termijn-Hofman,

in den ouderdom van ruim 64 jaar.

J. Termijn S*enio*r.

J. Termijn J*unio*r.

J. W. Termijn-v. Beveren.

Maassluis,

L. Termijn.

M. Termijn-de Vries.

A.H. Termijn.

W.J. v*an* d*er* Gaag-Termijn.

R. v*an* d*er* Gaag.

C.J. Termijn.

New-York,

P. Termijn.

J. Termijn.

M. Termijn en Verloofde

en Kleinkinderen.

Rotterdam, 7 Mei 1923.

**Jan Sonjéstraat** 33b.

82
