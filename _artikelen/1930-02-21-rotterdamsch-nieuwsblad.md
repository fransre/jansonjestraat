---
toegevoegd: 2024-08-06
gepubliceerd: 1930-02-21
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164645058:mpeg21:a00102"
onderwerpen: [familiebericht]
huisnummers: [14]
achternamen: [van-der-meer]
kop: "Geertje Wullechina van der Meer-Niekamp,"
---
Algemeene Kennisgeving.

Heden ontsliep zacht en kalm,
na een langdurig lijden, onze
geliefde Vrouw, Moeder, Behuwdmoeder,
Dochter, Zuster, Behuwdzuster
en Tante

# Geertje Wullechina van der Meer-Niekamp,

in den ouderdom van 56 jaar.

Uit aller naam,

D. van der Meer.

F. van der Meer.

H.W. van der Meer-van den Brink.

A. Niekamp.

Rotterdam, 20 Februari 1930.

**Jan Sonjéstraat** 14a.

De begrafenis zal plaats hebben
Maandag *24 februari 1930* a*an*s*taande* op „Crooswijk”.
Vertrek van het woonhuis om 2uur.

26
