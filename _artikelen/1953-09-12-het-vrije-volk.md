---
toegevoegd: 2024-07-25
gepubliceerd: 1953-09-12
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951601:mpeg21:a0158"
onderwerpen: [bewoner]
huisnummers: [11]
achternamen: [jansen]
kop: "Mijnheer de Redacteur"
---
# Mijnheer de Redacteur

## R*otterdamse* E*lectrische* T*ram*-personeel

In ons blad is gewag gemaakt van
kabaal, dat op de personeelsvergadering
van de R*otterdamse* E*lectrische* T*ram* is gemaakt. Waarom
wordt er in H*et* V*rije* V*olk* altijd over E*enheids* V*ak*c*entrale*-elementen
gesproken in zulke gevallen?
Ik vindt dit kleinzielig.

Als de R*otterdamse* E*lectrische* T*ram* thans een duiventil is
geworden, dan komt dat, omdat er
beschamend lage lonen worden betaald,
zoals het „Algemeen Dagblad”
vermeldde.

P. Jansen

**Jan Sonjéstraat** 11a

Rotterdam

(E*enheids* V*ak*c*entrale*'ers stonden op die avond pamfletten
te verspreiden. In die pamfletten
werden de vergaderingbezoekers
opgewekt de eis te stellen, dat
15 October *1953* de streefdatum zou moeten
zijn. Op die vergadering is die eis
toen ook gesteld en dat ging met
het vermelde kabaal gepaard. Was de
conclusie van H*et* V*rije* V*olk* dus onjuist? Als
inzender wat meer van de praktijken
dezer lieden kende uit eigen ervaring,
dan zou hij het woord „kleinzielig”
niet zo gauw gebezigd hebben. En dan
zou hij ook weten, dat het „Algemeen Dagblad”
er altijd een danig plezier
in heeft met verkeerde voorstelling
van zaken haar arbeiders-vijandigheid
te demonstreren, hetgeen dit
blad ook nu met het noemen van
loonbedragen bij de R*otterdamse* E*lectrische* T*ram* weer heeft
gedaan. Red. H*et* V*rije* V*olk*)
