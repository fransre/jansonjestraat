---
toegevoegd: 2024-07-21
gepubliceerd: 1988-03-15
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962726:mpeg21:a0194"
onderwerpen: [bellebom]
kop: "‘Operatie Bellebom’: grootste evacuatie na watersnoodramp"
---
Middelland op 27 maart *1988* verlaten stadswijk

# ‘Operatie Bellebom’: grootste evacuatie na watersnoodramp

Door Marcel Potters

Rotterdam — Het is de grootste
evacuatie in Nederland sinds de watersnoodramp
van 1953, de ‘verhuizing’
van zevenduizend bewoners uit
de Rotterdamse wijk Middelland tijdens
de ‘operatie Bellebom’ op 27
maart *1988*. Het tijdelijke vertrek is één
van de voorzorgsmaatregelen in het
kader van het onschadelijk maken
van een Britse vliegtuigbom in de
Bellevoysstraat.

Duizend agenten van de Rotterdamse
politie zijn die dag *27 maart 1988*
op de been om de operatie in
goede banen te leiden. Daarnaast
worden nog eens duizend
vrijwilligers en vertegenwoordigers
van andere gemeentelijke
diensten ingezet, om de betrokkenen
te verplaatsen en
onderdak te bieden.

Verder zal de PTT een speciale
nood-telefooncentrale inrichten
en fungeert de Energiehal
als opvangcentrum
voor de evacuées. De totale
kosten van de actie, die zich
onder de verantwoordelijkheid
van burgemeester dr A. Peper
van Rotterdam zal voltrekken,
worden geschat op zes miljoen
gulden.

## Drukgolf

De veiligheidsmaatregelen
die rond de demontage van de
Engelse vliegtuigbom worden
genomen behoren tot de meest
ingrijpende waarmee de Explosieven Opruimingdienst (EOD)
tot dusver te maken
heeft gehad. Volgens kapitein
L.P.G. van Maren van deze
dienst zijn deze maatregelen
zeker niet overbodig.

„Als de Bellebom,” zegt hij,
„op zo'n tien meter onder het
maaiveld zpu exploderen is
daar aan de oppervlakte weinig
van te merken. Wél ontstaat
er een zijwaartse drukgolf
die in de directe omgeving
de heipalen van de woningen
beschadigt. Een groot deel van
die panden zal daarbij zeker
instorten.”

Burgemeester Peper hierover:
„Het is een zeer ongewone
operatie waarvoor zelfs het
toepassen van een speciale
noodverordening noodzakelijk
was. We konden die bom uiteraard
niet zomaar laten liggen;
dat zou wat slordig zijn geweest.
Toen eenmaal bekend
was dat het ding zich daar bevond
wisten we al snel wat de
gevolgen konden zijn als die
bom zou ontploffen. De huidige
operatie is het logische gevolg
van deze wetenschap. Hopelijk
is het wel de laatste keer dat
een dergelijke omvangrijke actie
moet plaatsvinden.”

## Staartstuk

De vijfhonderd kilo zware
Bellebom werd vorig jaar november
met behulp van sonarapparatuur
in één van de tuinen
tussen de **Jan Sonjéstraat**
en de Bellevoysstraat ontdekt.
De Britse vliegtuigbom was
daar op 29 november 1944, tijdens
een aanval op een pand
van de Sicherheitsdienst (SD)
aan de Heemraadssingel, door
een Typhoon-bommenwerper
gedropt maar niet ontploft.

Het projectiel boorde zich in
de zachte grond, stuitte op een
dikke boomstam en sloeg volgens
de EOD ‘over de kop’. Het
inmiddels geborgen staartstuk
werd daarbij van de rest van
de bom gescheiden.

De in totaal één meter tachtig
lange ‘blindganger’ bevindt
zich volgens experts van de
EOD in een perfecte staat. De
voorschriften van de EOD luiden
dat tijdens het demonteren
van de bom alle burgers binnen
een straal van driehonderd
meter rond het projectiel
hun woning moeten verlaten.
Nog eens veertienduizend
mensen, die op driehonderd tot
zeshonderd meter van de bom
wonen, mogen een groot deel
van de bewuste zondag *27 maart 1988* niet
buiten komen. De Rotterdamse
politie zal het eerstgenoemde
gebied na de evacuatie hermetisch
afgrendelen.

Hoofd Rampenbestrijding M.G. de Ruiter:
„Eén van de zaken
waarmee we al direct hadden
te maken was het feit dat
er in dit gebied zo'n 22 nationaliteiten
zijn te vinden. Het was
voor ons dus moeilijk iedereen
duidelijk te maken waar het
precies om ging. Daarnaast
kent dit deel van Rotterdam
veel drugsverslaafden en illegalen.
Ook gegevens waarmee
we nadrukkelijk rekening
moesten houden.”

## Bruine beer

Een huis-aan-huis-enquête,
die onder de bewoners van de
drieduizend betrokken panden
is gehouden, leverde onder
meer gegevens op over het
aantal mensen dat op 27 maart *1988*
speciaal vervoer behoeft en de
aanwezige huisdieren. Het bureau
Rampenbestrijding kreeg
daarbij niet alleen meldingen
van katten en honden, maar
ook van gifslangen, wurgslangen
en één bewoner beweerde
er zelfs een bruine beer op na
te houden.

Geadviseerd wordt de huisdieren
zoveel mogelijk thuis te
laten of een logeeradres te vinden
bij ‘een dierenvriend’. Wie
dat niet lukt kan gebruik maken
van de tijdelijk als asiel
ingerichte Weena IJshal.

De Rotterdamse politie wijst
erop dat de straten in het geëvacueerde
gebied — in de speciale
Bellebom-krant aangeduid
als de rode sector — vrij
moeten zijn van auto's. De parkeergarage
van het Europointgebouw
zal op 26 en 27 maart *1988*
gratis toegankelijk zijn voor
auto's uit dit gebied. Wagens
die achterblijven worden door
de wegsleepdienst afgevoerd
naar het Hertenkamp bij De Doelen
en de Veilingweg.

De gemeente Rotterdam
heeft een speciaal telefoonnummer
in het leven geroepen
voor nadere informatie over de
operatie Bellebom. Dat infonummer
is 06-022.67.22

*Onderschrift bij de foto's*
De politie
moet de paarden
uit de
manege weghalen
voordat
de bom gedemonteerd
wordt. (foto
De Jong en
Van Es)

„Als de bom ontploft zullen
panden instorten,”
zegt kapitein L.P.G. van Maren
van de Explosieven Opruimingsdienst.
(foto ANP)
