---
toegevoegd: 2024-07-29
gepubliceerd: 1940-07-19
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002443:mpeg21:a0056"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Aangeboden"
---
# Aangeboden

ongemeubileerd 1 grote
kamer, vaste wastafel,
aaneengrenzende badkamer
ƒ4, voor 1 persoon
op 3de étage. v*an* Veghel's bemiddelingsbureau.
**Jan Sonjéstraat** 20B.
