---
toegevoegd: 2024-08-11
gepubliceerd: 1923-08-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494864:mpeg21:a0065"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [38]
achternamen: [westeneind]
kop: "kinderwagen,"
---
Te koop:

# kinderwagen,

merk „Patria”, spotprijs
15 gulden. Adres:
Westeneind, **Jan Sonjéstraat** 38c.
