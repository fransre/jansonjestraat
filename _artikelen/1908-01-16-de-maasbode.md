---
toegevoegd: 2024-08-14
gepubliceerd: 1908-01-16
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000188836:mpeg21:a0015"
onderwerpen: [faillissement, schilder]
kop: "Faillissementen."
---
# Faillissementen.

Opheffing faillissement.

L. Peperzak, huisschilder Bellevoystraat 57,
zaak doende **Jan Sonjéstraat** 45, failliet verklaard
18 December *1908* j*ongst*l*eden*
