---
toegevoegd: 2024-08-14
gepubliceerd: 1913-03-12
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010222407:mpeg21:a0187"
onderwerpen: [kunst]
huisnummers: [5]
achternamen: [roodenburg]
kop: "Nationale Vereeniging voor den Volkszang."
---
# Nationale Vereen*iging* voor den Volkszang.

Gesteund door het Alg*emeen* Ned*erlandsch* Verb*ond*.

Bijeenkomst

tot het aanleeren van mooie oorspronkelijke
Nederl*andsche* Liederen (op
gehoor) op

Woensdag 12 Maart *1913*

's avonds 8 uur, in de Bovenzaal

Bécude, Noordsingel 39.

Het bestuur.

Secr*etaris* de Heer J.W. Roodenburg

**Jan Sonjéstraat** 5.

Toegangsbew*ijzen* aan de zaal ƒ0.25.
