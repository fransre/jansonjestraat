---
toegevoegd: 2024-03-07
gepubliceerd: 1942-03-19
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003105:mpeg21:a0018"
onderwerpen: [huisraad, kleding, tekoop, dieren]
huisnummers: [29]
achternamen: [augustijn]
kop: "Te koop"
---
# Te koop

2 zw*arte* lederen
fauteuils, 1 spiegel, 1 p*aar*
br*uine* d*ames*-schoenen maat 38
1 konijnenhok b. d. H. Augustijn,
**Jan Sonjéstraat** 29B.
