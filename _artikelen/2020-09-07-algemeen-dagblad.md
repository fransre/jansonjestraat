---
toegevoegd: 2021-04-29
gepubliceerd: 2020-09-07
decennium: 2020-2029
bron: Algemeen Dagblad
externelink: "https://www.ad.nl/rotterdam/straatbewoners-slaan-alarm-over-poging-doodslag-intimidatie-en-drugshandel~ab022dc5/"
onderwerpen: [misdrijf]
kop: "Straatbewoners slaan alarm over poging doodslag, intimidatie en drugshandel"
---
# Straatbewoners slaan alarm over poging doodslag, intimidatie en drugshandel

Mishandelingen, straatintimidatie, agressie en drugshandel; een groep bewoners van de Rotterdamse wijk Middelland smeekt de gemeente Rotterdam om actie te nemen tegen de volgens hen onveilige leefsituatie en het geweld in en rondom de **Jan Sonjéstraat** en de 1e Middellandstraat. Ze willen cameratoezicht of een samenscholingsverbod.

Ruim drie jaar geleden sloegen bewoners ook al alarm, toen vanwege doodsbedreigingen en ingegooide ruiten bij een straatbewoner. Volgens de bewoners is de situatie nu opnieuw geëscaleerd. Een poging tot doodslag op één van de bewoners heeft de emmer doen overlopen.

## Schoppen

Deze bewoner zou door jongeren tegen de grond zijn gewerkt, nadat hij hen aansprak op hun drugshandel. Hij kreeg vervolgens schoppen tegen zijn hoofd en belandde volgens de bewoners met ernstige fracturen in het ziekenhuis. Het herstel gaat maanden duren, schrijven de bewoners.

De escalatie is volgens hen ‘het onvermijdelijke gevolg van het uitblijven van effectief ingrijpen en de jarenlange spanning tussen de bewoners en de agressieve en criminele groepen jongens en mannen op de hoek van onze straat’.

„De jonge mannen die dagelijkse op de hoek van onze straat te vinden zijn, staan hier met een duidelijk doel; drugsverkoop”, staat in de brief. „Wij als straatbewoners zijn talloze keren getuige geweest van de door deze jongeren openlijk bedreven drugshandel. Op straat, maar ook vanuit verschillende auto's en busjes, worden op klaarlichte dag, terwijl de kinderen uit de straat buitenspelen, meerdere transacties gedaan.”

## Lantaarnpaal

Onder meer het sluiten van een kroeg en het plaatsen van een extra lantaarnpaal hebben volgens de bewoners geen effect gehad; de drugsverkoop gaat er onverminderd verder. Ook voelen jonge vrouwen zich geïntimideerd door seksistische opmerkingen vanuit de groep jongeren. Het gevoel van angst en onveiligheid overheerst bij de bewoners. Zij willen dat de gemeente het ‘hangen’ op de hoek van de straat onmogelijk maakt. ‘De maat is vol’.
