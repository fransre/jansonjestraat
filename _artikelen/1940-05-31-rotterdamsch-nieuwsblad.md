---
toegevoegd: 2024-07-29
gepubliceerd: 1940-06-18
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002401:mpeg21:a0035"
onderwerpen: [huisraad, tekoop]
huisnummers: [10]
achternamen: [haarlemmer]
kop: "Te koop"
---
# Te koop

1 tafel en 4 stoelen,
vaste prijs ƒ12,50. L. Haarlemmer,
**Jan Sonjéstraat** 10a.
