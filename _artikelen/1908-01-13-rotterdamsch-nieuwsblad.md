---
toegevoegd: 2024-08-14
gepubliceerd: 1908-01-13
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994711:mpeg21:a0092"
onderwerpen: [fiets, tekoop]
huisnummers: [39]
achternamen: [van-uitert]
kop: "Spotprijs."
---
# Spotprijs.

Door bijzondere omstandigheden
wordt een
splinternieuw Heerenrijwiel,
voorzien van
Bel, Pomp, Tasch met
gereedschap en Banden
met nog 12 maanden
garantie, voor spotpr*ijs*
verkocht. Te bezichtigen.
A. v*an* Uitert,
**Jan Sonjéstraat** 39,
R*otter*dam.
