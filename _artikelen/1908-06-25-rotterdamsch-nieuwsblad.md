---
toegevoegd: 2024-08-14
gepubliceerd: 1908-06-25
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010994841:mpeg21:a0031"
onderwerpen: [reclame]
huisnummers: [26, 28]
achternamen: [hummelman]
kop: "Wasscherij en strijkinrichting"
---
Gevestigd

# Wasscherij en strijkinrichting

Gez*usters* Hummelman.

28-26 **Jan Sonjéstraat** 28-26, bij de Middellandstraat.

Bevelen zich beleefd aan voor groote en fijne wasschen, alsmede
alle voorkomende goederen. Vlugge bediening. Billijke prijzen.
Goederen tegen brandschade verzekerd.

25825 30
