---
toegevoegd: 2024-07-23
gepubliceerd: 1960-11-29
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164911051:mpeg21:a00088"
onderwerpen: [cafe, werknemer]
huisnummers: [15]
achternamen: [rueter]
kop: "buffetjuffrouw,"
---
Gevraagd

# buffetjuffrouw,

vakkennis
geen vereiste. Mevr*ouw* Rüter,
café Intiem, **Jan Sonjéstraat** 15,
tel*efoon* 30682.
