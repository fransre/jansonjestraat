---
toegevoegd: 2024-08-09
gepubliceerd: 1926-01-28
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000193716:mpeg21:a0066"
onderwerpen: [faillissement]
huisnummers: [29]
achternamen: [hoogmolen]
kop: "Faillissementen"
---
# Faillissementen

Opgegeven door Van der Graaf & Co*mpagnon*
(Afd*eling* Handelsformatiën.)

Opgeheven:

Rotterdam, 27 Januari *1926*.

H. Hoogmolen, **Jan Sonjéstraat** 29.

