---
toegevoegd: 2024-05-16
gepubliceerd: 1929-02-08
decennium: 1920-1929
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005425110:00001"
onderwerpen: [familiebericht]
huisnummers: [17]
achternamen: [bosman]
kop: "Annie Bosman en Bernard Cohen"
---
Verloofd:

# Annie Bosman en Bernard Cohen

Rotterdam,

**Jan Sonjéstraat** 17

Goudschestraat 23

7 Februari 1929.
