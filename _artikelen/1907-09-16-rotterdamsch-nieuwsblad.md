---
toegevoegd: 2024-08-14
gepubliceerd: 1907-09-16
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196924:mpeg21:a0066"
onderwerpen: [bedrijfsruimte]
huisnummers: [42, 44]
kop: "Groot Pakhuis."
---
# Groot Pakhuis.

Terstond te huur een
groot Pakhuis, breed 9
en diep 20 Meter, voorzien
van dubbel stel Inrijdeuren,
breed 2.60 en
hoog 3.50 Meter en verder
van Kantoor, veel
licht. Privaat en Gas- en
Waterleiding. Voor alle
doeleinden uitstekend
geschikt en dagelijks te
zien **Jan Sonjéstraat** 42-44.
Sleutel op n*ummer* 40.
