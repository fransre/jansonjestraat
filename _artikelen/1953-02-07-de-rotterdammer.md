---
toegevoegd: 2024-05-20
gepubliceerd: 1953-02-07
decennium: 1950-1959
bron: De Rotterdammer
externelink: "https://krantenbankzeeland.nl/issue/kra/1953-02-07/edition/0/page/258"
onderwerpen: [woonruimte, tekoop]
huisnummers: [14]
kop: "Eindafslag"
---
# Eindafslag

op Woensdag 11 Febr*uari* *19*53 om
14 uur in de Koopmansbeurs,
Coolsingel te R*otter*dam van het
huis en erf aan de

**Jan Sonjéstraat** 14.

In trekgeld ƒ8600.—.

Verhey en Hoeneveld,
Notarissen
