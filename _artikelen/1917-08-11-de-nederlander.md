---
toegevoegd: 2024-08-13
gepubliceerd: 1917-08-11
decennium: 1910-1919
bron: De Nederlander
externelink: "https://resolver.kb.nl/resolve?urn=MMKB15:000670203:mpeg21:a00047"
onderwerpen: [reclame]
huisnummers: [38]
achternamen: [weier]
kop: "Excelsior-bioscoop"
---
# Excelsior-bioscoop

1e klasse bioscoop- en projectie voorstellingen

Waarbij U gegarandeerd wordt een beschaafde
en Christelijke uitlegging. Pakkende en gewilde
propaganda voor drankbestrijding, natuurtafereelen
en films op het gebied van
Zending en Wetenschap. Door het geheele
land tegen billijke conditiën te ontbieden.

Directeur: E.J. Weier, Rotterdam

**Jan Sonjéstraat** 38 — Telefoon 10084
