---
toegevoegd: 2024-07-26
gepubliceerd: 1942-12-31
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003039:mpeg21:a0022"
onderwerpen: [woonruimte, tekoop]
huisnummers: [36]
kop: "in het openbaar verkoopen:"
---
Notaris Mr. I. van der Waal
te Rotterdam zal op Woensdagen
6 en 13 Januari 1943, telkens des
nam*iddags* 2 uur in de Nieuwe Koopmansbeurs
te Rotterdam

# in het openbaar verkoopen:

De navolgende
panden te Rotterdam resp
~~Nieuwe Binnenweg 218 en 269;
Vierambachtsstraat 111, Eerste
Middellandstraat 25;~~ **Jan Sonjéstraat** 36;
Maximumprijzen resp*ectievelijk*
~~ƒ22.700.—; ƒ20.850.— ; ƒ26.900.—;
ƒ25.950.—; en~~ ƒ12.700.—. De panden
zijn inmiddels uit de hand
te koop. Inl*ichtingen* verkrijgbaar ten
kantore van gen*oemde* notaris 's Gravendijkwal 133,
Tel*efoon* 33539.
