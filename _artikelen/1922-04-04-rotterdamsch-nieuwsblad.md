---
toegevoegd: 2024-08-11
gepubliceerd: 1922-04-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494214:mpeg21:a0106"
onderwerpen: [werknemer]
huisnummers: [38]
achternamen: [gogarn]
kop: "Een jongen"
---
# Een jongen

gevraagd voor de Beddenmakerij.
Zich persoonlijk aan te melden
bij de Firma J.H.A. Gogarn,
**Jan Sonjéstraat** 38.

24435 10
