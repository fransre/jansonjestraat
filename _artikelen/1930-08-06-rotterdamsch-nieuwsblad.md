---
toegevoegd: 2024-08-06
gepubliceerd: 1930-08-06
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002133:mpeg21:a0058"
onderwerpen: [huispersoneel]
huisnummers: [3]
kop: "huishoudster,"
---
Gevraagd, tegen 15
Aug*ustus* *1930*

# huishoudster,

in klein Gezin, niet beneden
30 jaar. **Jan Sonjéstraat** 3b,
bovenhuis.
