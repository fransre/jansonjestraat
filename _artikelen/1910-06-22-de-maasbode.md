---
toegevoegd: 2021-05-04
gepubliceerd: 1910-06-22
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000186097:mpeg21:a0031"
onderwerpen: [reclame]
huisnummers: [15, 17]
kop: "Aan de dames."
---
# Aan de dames.

Door uitbreiding genoodzaakt — en door te voldoen aan ons streven
prachtvol werk te blijven leveren, wordt onze

waschinrichting omgebouwd tot een electrische.

Alle ingekomen aanvragen ter bespreking van condities worden in volgorde
behandeld.

Met hoogachting en dank,

Electrische Wasscherij „Hollandia” **Jan Sonjéstraat** 15-17, Rotterdam.

P.S. Als u wenscht te veranderen van Wasscherij, schrijf dan spoedig s*'il* v*ous* p*laît*.

(60)
