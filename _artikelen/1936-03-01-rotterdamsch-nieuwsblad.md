---
toegevoegd: 2024-08-03
gepubliceerd: 1936-03-01
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164681069:mpeg21:a00021"
onderwerpen: [woonruimte, tehuur]
huisnummers: [38]
kop: "Te huur:"
---
# Te huur:

ruime 1e Etage, 3 Kamers,
Keuken, Warande,
ƒ6.50 p*er* w*eek* **Jan Sonjéstraat** 38.
Bevr*agen*
Not*aris* Wijsenbeek-mr. Hioolen
Mauritsweg 11. Tel*efoon* 56204.
