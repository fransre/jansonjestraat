---
toegevoegd: 2024-08-03
gepubliceerd: 1935-09-10
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164679010:mpeg21:a00124"
onderwerpen: [woonruimte, tehuur]
huisnummers: [7, 9]
kop: "Vrij bovenhuis"
---
# Vrij bovenhuis

te huur, **Jan Sonjéstraat** 7.
Bevr*agen* N*ummer* 9.
