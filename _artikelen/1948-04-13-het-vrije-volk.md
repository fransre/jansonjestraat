---
toegevoegd: 2024-07-26
gepubliceerd: 1948-04-13
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010949508:mpeg21:a0039"
onderwerpen: [huispersoneel]
huisnummers: [18]
achternamen: [van-eijk]
kop: "Nette werkster"
---
# Nette werkster

in gezin
voor Vrijdag. Van Eijk, **Jan Sonjéstraat** 18b.
