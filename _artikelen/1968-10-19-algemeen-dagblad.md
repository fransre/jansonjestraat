---
toegevoegd: 2021-05-18
gepubliceerd: 1968-10-19
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002836042:mpeg21:a00507"
onderwerpen: [woonruimte, tekoop]
huisnummers: [14]
kop: "Te koop aangeboden te Rotterdam, voor zelfbewoning"
---
# Te koop aangeboden te Rotterdam, voor zelfbewoning

**Jan Sonjéstraat** 14

bovenhuis vrij te aanvaarden, benedenhuis verhuurd
voor ƒ901,80 p*er* j*aar*. Koopsom ƒ19.500 k*osten* k*oper*.
