---
toegevoegd: 2024-07-20
gepubliceerd: 1964-02-06
decennium: 1960-1969
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259028031:mpeg21:a00069"
onderwerpen: [woonruimte, tekoop]
huisnummers: [30, 32, 34]
kop: "Huizenveiling"
---
# Huizenveiling

Openbare verkoping van onroerende goederen
door de N.V. Notarishuis op 5 februari 1964
in de Blauwe Zaal van de Koopmansbeurs
te Rotterdam.

Definitieve afslag.
Not*aris* R.A. v*an* d*er* Poll:
1\. pand en erf Rodenrijselaan 40, inzet ƒ19.800, afslag ƒ19.800.
Not*aris* L.A.J. Schuurs en Th.J.J.M. Havermans:
2\. pand en erf Ruilstraat 34, inzet ƒ18.000,
3\. pand en erf Ruilstraat 36, inzet ƒ18.800,
4\. pand en erf Ruilstraat 38, inzet ƒ19.000, afslag tezamen ƒ56.300;
5\. pand en erf De Jagerstraat 54, inzet ƒ19.600, afslag ƒ19.600;
6\. pand en erf De Jagerstraat 60, inzet ƒ20.000, afslag ƒ20.010;
7\. pand en erf De Jagerstraat 62, inzet ƒ20.200, afslag ƒ20.200;
8\. pand en erf De Jagerstraat 56, inzet ƒ21.000,
9\. pand en erf De Jagerstraat 58, inzet ƒ22.000, afslag tezamen ƒ43.600.

Voorlopige afslag.
Not*aris* mr. T. Leopold en H.A. Heitkamp, Arnhem:
10\. pand en erf **Jan Sonjéstraat** 30 ƒ17.400;
11\. pand en erf **Jan Sonjéstraat** 32 ƒ18.600;
12\. pand en erf **Jan Sonjéstraat** 34 ƒ18.000.
Not*aris* mr. T. Leopold:
13\. pand en erf Blankenburgerpark 150 ƒ25.600;
14\. pand en erf Burg*emeester* Meineszlaan 119 ƒ44.000;
15\. pand en erf Davidsstraat 21 ƒ15.400.
