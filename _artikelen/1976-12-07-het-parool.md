---
toegevoegd: 2021-04-29
gepubliceerd: 1976-12-07
decennium: 1970-1979
bron: Het Parool
externelink: "https://resolver.kb.nl/resolve?urn=ABCDDD:010840041:mpeg21:a0075"
onderwerpen: [cafe, misdrijf]
huisnummers: [15]
kop: "Politie doet inval illegale gokhuizen"
---
# Politie doet inval illegale gokhuizen

Rotterdam (ANP) — De Rotterdamse
politie heeft in de nacht van
maandag *6 december 1976* op dinsdag *7 december 1976*, geassisteerd door
de mobiele eenheid, invallen gedaan
in twee illegale gokhuizen, één aan de
Aelbrechtskolk en één in de **Jan Sonjéstraat**.
In totaal werden vijf mensen
gearresteerd, drie exploitanten en twee
croupiers, onder wie een vrouw. Er is
ongeveer ƒ2.000 in beslag genomen.
In beide huizen waren in het geheel
32 spelers aanwezig.
Sinds medio vorig jaar *1975* treedt men
in Rotterdam regelmatig hard op tegen
de gokhuizen. Rotterdam is één van de
gemeenten, die zich hebben aangemeld
als vestigingsplaats voor een legaal
casino.
