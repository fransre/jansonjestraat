---
toegevoegd: 2024-07-25
gepubliceerd: 1953-10-26
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951728:mpeg21:a0181"
onderwerpen: [ongeval, auto]
achternamen: [torenga]
kop: "Veel verkeer naar Stadion"
---
# Veel verkeer naar Stadion

De Rotterdamse politie heeft gistermiddag *25 oktober 1953*
het verkeer van en naar
het Feijenoord-Stadion in goede banen
geleid. Voor de wedstrijd reden
7500 auto's 200 bussen, 7000 fietsen en
tientallen trams naar het Stadion.
Per trein arriveerden 15000 toeschouwers,
per tram 8000. Voor en na de
westrijd gebeurden een zevental ongelukken
in en om de Maastunnel.
Lichte materiële schade en enige
stagnatie was het gevolg. Op de Henegouwerlaan
werd bovendien de 64-jarige
opperman J.H. Torenga uit
de **Jan Sonjéstraat** bij het oversteken
door een auto gegrepen. Hij brak een
been en liep een hersenschudding en
een hoofdwond op.

Twee uur na de wedstrijd was het
weer rustig in de stad. De politie
slaakte een zucht van verlichting....
