---
toegevoegd: 2024-08-07
gepubliceerd: 1928-02-17
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514223:mpeg21:a0201"
onderwerpen: [muziek, tekoop]
huisnummers: [10]
kop: "Te koop:"
---
# Te koop:

Phonograaf, met Hoorn
en 65 Platen. Adres:
**Jan Sonjéstraat** 12a.
