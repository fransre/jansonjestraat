---
toegevoegd: 2023-01-21
gepubliceerd: 2000-06-19
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2000-06-19-de-middellander-origineel.jpg"
onderwerpen: [geraniumdag]
kop: "De Jan Sonjéstraat is weer in de bloemetjes gezet."
---
# De **Jan Sonjéstraat** is weer in de bloemetjes gezet.

*Onderschrift bij de foto*
