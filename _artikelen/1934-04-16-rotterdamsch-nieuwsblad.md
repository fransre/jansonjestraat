---
toegevoegd: 2024-08-04
gepubliceerd: 1934-04-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164670051:mpeg21:a00076"
onderwerpen: [woonruimte, tehuur]
huisnummers: [13, 18]
kop: "Benedenhuis"
---
# Benedenhuis

te huur, Voor-, Tusschen-, Achterkamer.
Keuken, Kelder en
Tuin, Huur ƒ35 p*er* m*aand*
**Jan Sonjéstraat** 13.
Bevragen op 18a.
