---
toegevoegd: 2021-04-29
gepubliceerd: 1906-08-17
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213871:mpeg21:a0003"
onderwerpen: [ongeval]
huisnummers: [17]
achternamen: [van-uitert]
kop: "Stadsnieuws."
---
# Stadsnieuws.

Hedenmorgen *17 augustus 1906* 9 uur werd in
het Ziekenhuis verbonden de 28-jarige Anna H.Th. van Vreeswijk,
huisvrouw van
M. van Uitert, wonende **Jan Sonjéstraat**
n*ummer* 17. De vrouw was bij het mangelen van
linnengoed met haar rechterhand tusschen
een der mangelrollen beklemd geraakt en
had zich daarbij ernstig verwond.
