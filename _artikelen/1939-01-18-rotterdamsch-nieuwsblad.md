---
toegevoegd: 2024-08-01
gepubliceerd: 1939-01-18
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164699014:mpeg21:a00239"
onderwerpen: [auto, tekoop]
huisnummers: [20]
achternamen: [paulus]
kop: "Te koop:"
---
# Te koop:

Peugeot 1937, type 402,
in prima staat, wegens
omstandigheden. Lage
prijs. Te bevragen H. Paulus,
**Jan Sonjéstraat** 20.
