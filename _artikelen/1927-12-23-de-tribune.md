---
toegevoegd: 2024-08-07
gepubliceerd: 1927-12-23
decennium: 1920-1929
bron: De tribune
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010469164:mpeg21:a0039"
onderwerpen: [ongeval, tram]
kop: "Door de koude bevangen."
---
# Door de koude bevangen.

Woensdagavond *21 december 1927* omstreeks 12 uur werd de
19-jarige dienstbode A. de R., uit de **Jan Sonjéstraat**,
toen zij in een tramwagen van lijn 7
op den Schiedamschedijk zat, door de koude
bevangen. Zij werd naar het politiebureau
in de Groote Pauwensteeg overgebracht en
daar door een verpleger van den Geneeskundigen
Dienst behandeld.
