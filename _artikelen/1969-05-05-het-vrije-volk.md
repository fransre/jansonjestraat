---
toegevoegd: 2024-07-22
gepubliceerd: 1969-05-05
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956918:mpeg21:a0032"
onderwerpen: [cafe, werknemer]
huisnummers: [15]
kop: "Werkster"
---
Gevraagd

# werkster

voor
schoonhouden café, ook tappen
achter bar. Iedere dag van
9-14 uur. Tel*efoon* 010-230682. Café
Intiem, **Jan Sonjéstraat** 15, Rotterdam.
