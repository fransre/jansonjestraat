---
toegevoegd: 2024-08-12
gepubliceerd: 1922-02-10
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494070:mpeg21:a0136"
onderwerpen: [huisraad, tekoop]
huisnummers: [21]
kop: "Te koop:"
---
# Te koop:

een gevoerde Ulster,
Pool. voor ƒ20, Een
koperen Gaslamp voor
ƒ8. **Jan Sonjéstraat** 21b
