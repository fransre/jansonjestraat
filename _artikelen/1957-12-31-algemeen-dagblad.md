---
toegevoegd: 2024-07-24
gepubliceerd: 1957-12-31
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000353077:mpeg21:a00018"
onderwerpen: [reclame]
huisnummers: [32]
achternamen: [van-der-velden]
kop: "A. van der Velden"
---
Exclusieve reclame-artikelen

# A. van der Velden

**Jan Sonjéstraat** 32 — Rotterdam — Telefoon 51637

P.F.

Specialiteit sleutelkettingen
