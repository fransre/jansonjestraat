---
toegevoegd: 2021-05-03
gepubliceerd: 1908-07-11
decennium: 1900-1909
bron: Maas- en Scheldebode
externelink: "https://krantenbankzeeland.nl/issue/mas/1908-07-11/edition/0/page/7"
onderwerpen: [reclame, fiets]
huisnummers: [40]
kop: "Rijwielen 31,50."
---
# Rijwielen 31,50.

Reclame-Rijwielen met 1 jaar garantie, goede Banden 31,50; dito
Kogelfreewheel, stuur twee remmen 37,25 Onze bekende Roland-Rijwielen,
met of zonder Freewheel Peters Unionbanden 45,—. Roland N*ummer* 2, hoogfijne
1e klasse Machine, gemaakt van Engelsche fittings, 65,—. Roland N*ummer* 3,
de beste Machine in den handel, met Bakkerbanden, concuurreert
met de duurste merken, 85,—; fijne referentiën van Cliënten, die jarenlang
onze Machines berijden, voorhanden. Rijwielbanden. Wij houden een
werkelijke reuzencollectie Banden in voorraad, niet denkbeeldig. Binnenbanden
1,50. Excelsior 2,—. Peters Union 1 jaar garantie 2,50. Jackson
3,50. Bakker 4,50. Buitenbanden 2,— tot 8,50.

Voor de Rijwielhandelaars hebben wij extra lage prijzen. Vraagt onze
geïllustreerde Prijscourant met speciale prijzen, korting op iedere Prijscourant
v. Grossiers. Eigen Rijwielfabriek. **Jan Sonjéstraat** 40
Rotterdam. Emailleer- en Vernikkelinrichting, Frames, Wielen,
Sturen enz Roland Rijwiel Comp. Binnenweg 112 b.d.
Eendrachtsstraat Rotterdam, (geen klein Winkeltje.)

Telefoon 5270.

Hoofdagent voor Flakkee J.G. Noordijk Dirksland.
