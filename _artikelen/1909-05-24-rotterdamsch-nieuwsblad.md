---
toegevoegd: 2024-08-14
gepubliceerd: 1909-05-24
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197435:mpeg21:a0102"
onderwerpen: [woonruimte, tehuur]
huisnummers: [12]
kop: "Te Huur"
---
# Te Huur

in de **Jan Sonjéstraat** 12
een groot Souterrainhuis,
bevattende Voorkamer,
Slaapkamer, Achterkamer,
Keuken, Warande
en Tuin en Benedenkamer,
prachtigen
stand, bij de Middellandstraat,
huurprijs
ƒ19 per maand. Te bevragen
N. v*an* Leeuw,
v*an* d*er* Poelstraat 33A,
Rotterdam.
