---
toegevoegd: 2024-08-05
gepubliceerd: 1932-07-26
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164660029:mpeg21:a00075"
onderwerpen: [muziek, tekoop]
huisnummers: [32]
achternamen: [appel]
kop: "Piano."
---
# Piano.

Door omstandigheden
aangeboden een zwarte
Piano, merk Steinbach
tegen Spotprijs. P. Appel,
**Jan Sonjéstraat** N*ummer* 32.
