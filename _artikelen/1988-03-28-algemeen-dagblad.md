---
toegevoegd: 2024-07-20
gepubliceerd: 1988-03-28
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003283023:mpeg21:a00136"
onderwerpen: [bellebom, radio]
kop: "‘Eigenlijk best gezellig’"
---
# ‘Eigenlijk best gezellig’

De politie past op het huis

Alleen weer
werkt tegen bij
ontruiming wijk

door Gerard Bergers

Rotterdam — De Rotterdamse
wijk Middelland, zondagmorgen *27 maart 1988* om acht
uur. Meest gehoorde klachten: ‘Wat een
beestenweer’ en ‘koud hè?!’
Het hoost bakken uit de hemel en het is koud.
Politiemensen met wollen mutsen op en handschoenen
aan; kleumende bewoners in de deuropening
van hun huizen; de operatie-Bellebom
komt moeizaam op gang.

Op de hoek Claes de Vrieselaan-Middellandplein
laat een van de vele paarden van de bereden
brigade wat vallen op het trottoir en door de
Bellevoysstraat schalt uit de luidsprekers van
een politiewagen: „Dames en heren, goedemorgen,
dit is de politie. Wilt u alstublieft uw huizen
verlaten.”

## Wachten

Op de stoep van zijn huisje, op nog geen vijftig
meter van de plaats waar de Bellebom ligt, zegt
de 67-jarige Van Moerland; „Ik zal toch nog even
wachten. Je wéét het nooit. Ik heb hem op 29 november 1944
zien komen, en ik wil hem zien gaan
ook. Om nooit te vergeten. Een kwam er in de
dakgoot van dat huisje daar en de andere achter
in de tuinen. Jawel — die in de dakgoot is ontploft,
maar die andere niet.”

Naast hem de 75-jarige L.J. Smits: „Dat was
allemaal de Twééde Wereldoorlog. Maar ik heb
de Eerste nog meegemaakt. Dat was andere
koek. Toen had je geen blindgangers. Nee meneer,
de bommen waar ze toen mee gooiden waren
geen zure bommen om het zo maar te zeggen.”

Op de radio is een anonieme
mevrouw aan het woord: „Ik
wou dat dat wijf het lazerus
had gekregen”, zegt ze. Met
‘dat wijf’ bedoelt ze mevrouw
H. Offenberg, die de politie
over de aanwezigheid van de
bom heeft getipt toen er bij de
renovatie in de Bellevoysstraat
graafmachines
werden ingezet. Bang voor het
leven van de machinisten, vertelde
mevrouw Offenberg wat
zovelen al jaren wisten — dat
er een onontplofte bom in de
tuintjes achter de huizen
moest liggen. Maar een aantal
mensen in de buurt heeft haar
dat niet in dank afgenomen.

„Leuk begin van de zomertijd,
dat weer”, zegt de anonieme
dame op de radio. „Bovendien
moet je nu nóg eerder je
nest uit.” In de **Jan Sonjéstraat**
klinken op dat moment de eerste
hamerslagen. Politie en gemeentewerken
timmeren er
aan de weg, want in een van de
huisjes wordt niet opengedaan.
Via een opengebroken raam
klimmen twee agenten naar
binnen, om er even later uit te
komen met een duidelijk geflipte
bewoner en zijn vriendin.
Waarom ze niet open hadden
gedaan ? „Ja, waarom wel!”
En vervolgens, tot ieders hilariteit:
„Bellebom? Nooit van
gehoord!”

## Handschoenen

Een aantal politiemensen
heeft over de wollen wanten
plastic handschoenen aangetrokken.
„Je komt van alles tegen
in die huisjes”, zegt er een,
„vuile spuiten van drugsverslaafden
en veel meer vieze
troep. Maar eerlijk gezegd, het
valt ons nóg mee.”

Op de hoek van het Middellandplein
wordt met vereende
krachten een voormalige wijnhandel
opengebroken, omdat
de politie er het gas wil laten
afsluiten en wil kijken of er
zich niemand meer binnen bevindt.
De bewoner zal zich wel
nooit meer melden, want het
pand blijkt een illegaal speelhol
te bevatten.

## Kraakpanden

Alles bij elkaar knallen zo'n
vijfenzeventig keer hamers,
bijlen, breekijzers en koevoeten
op de vroege zondagmorgen *27 maart 1988*
om panden open te breken.
Het zijn voornamelijk renovatie- en
kraakpanden die in de
loop van de tijd door de politie
zorgvuldig zijn dichtgetimmerd,
maar nu geopend moeten
worden.

„Hoe loopt 't in zône A?”,
vraagt iemand die kennelijk
met de leiding is belast wat
zorgelijk over de politieradio.
Het antwoord is al even mistroostig:
„Het zit wat tegen.
Komt door het slechte weer.”

Dezelfde bron maakt melding
van op de Beukelsdijk
aangevoerde koffie. „Maar we
hebben er niks aan”, zegt de
politieradio, „want ze zijn de
bekertjes, de melk en de suiker
vergeten.” Even later wordt er
ook nog om lepeltjes gevraagd.

## Tafeltjes

In de Energiehal, op veilige
afstand van de Bellebom, zitten
dan zo'n tweehonderd mensen
aan de lectuurtafeltjes, in
de tv-hoek of gewoon kip of kabeljauw
te eten, want de ruim
tweeduizend ambtenaren die
door het ‘rampgebied’ trekkende
burgemeester Peper
heeft ingezet, hebben niets aan
het toeval overgelaten. Wie
niet van vlees houdt krijgt vis
en voor wie het geen van beide
is, schenkt de keuken vruchtensap.

De twee mensen om wie eigenlijk
alles draait, adjudant
Schoots en sergeant Linschoten
van de Explosieven Opruimings Dienst,
zijn dan afgedaald
in de elf meter diepe ‘koker’
waarin de Bellebom, nu
vrij van het grondwater, ligt.
Ze hebben niet veel gezegd.
Schoots, zorgvuldig aan zijn
shaggie trekkend: „Natuurlijk
heb ik lekker geslapen, waarom
niet?” En Linschoten, bijgenaamd
De Lange: „Alleen
hadden ze ons wel wat beter
weer mogen geven.”

Het lijkt een fluitje van een
cent.

## Gespannen

Maar Silla de Kok van het
aannemingsbedrijf Monschouwer
uit Heerjansdam weet wel
beter: „Ik heb ze drie maanden
meegemaakt hier in de
keet toen wij het voorbereidende
werk deden om de bom zo
goed en zo kwaad als dat ging
bloot te leggen. Ze zijn anders
vandaag *28 maart 1988*. Ze zijn gespannen
van hun haren tot hun tenen”,
zegt ze zorgelijk.

Op de 's Gravendijkwal tuurt
dan een blote vrouwelijke etalagepop
met het bordje ‘Leeg
hè?!’ over de onwezenlijk lege
wijk.

## Oordoppen

Hans Boodt van het Etalageburo,
zoals zijn zaak heet,
heeft haar op straat gezet om,
zoals hij zegt, de mensen toch
nog wat te laten lachen. En het
bedrijf Groen Rubber, aan de
Eerste Middellandstraat, dat
normaliter condooms verkoopt,
is blijkens de aanbeveling
in de etalage overgestapt
op oordoppen, voor het geval
dat.

Terwijl mensen van het Rode Kruis
in de Bajonetstraat
een Surinaams meisje met waterpokken
in een ambulance
tillen schettert een paar straten
verder uit de luidsprekers
van café Middelland het door
een handige zanger haastig
uitgebrachte ‘Bellebomlied’.
Uitbater Jan Hanssen, samen
met aanhang en ondanks de
vroege zondagmorgen *27 maart 1988* al of
nóg aan de pils: „We zijn vannacht
opengebleven en we hoeven
pas tegen één uur de wijk
uit. Ja, ik heb best aardig zaken
gedaan. Pilsjes, uitersmijtertjes
en zo voor de heel vroege
klanten. Nee, ons zal je niet
horen.”

Buiten stoppen politiemensen
een dronken voorbijganger,
die de afgesloten wijk in
wil, veiligheidshalve maar in
een politiewagen, terwijl een
44-jarige vrouw die halsstarrig
weigert haar woning te verlaten
en met een beitel dreigt,
naar een bureau wordt gebracht.

Beneden sleutelen even later
Schoots en Linschoten aan de
ontsteker van de bom.

Als de bewoners tegen de
avond hun huizen weer in mogen
blijken de zegeltjes die de
politie ter controle op de deuren
plakte nog heel en zijn ook
de afgegeven huissleutels niet
zoekgeraakt.

„Eigenlijk best gezellig, zo'n
dag”, zegt er een.

„Alleen jammer van het
weer.”

## Operatie kost 6 miljoen

Van een onzer
verslaggevers

Rotterdam
— De zes miljoen
gulden, die de operatie-Bellebom
heeft gekost, kan
worden onderverdeeld
in de volgende
posten: Aannemer Monshouwer
uit Heerjansdam
krijgt voor het
voorbereidende
werk 4,5 miljoen
gulden. De EOD
die de bom onschadelijk
maakte
brengt twee ton in
rekening en de
ambtelijk coördinator
van de operatie
ontvangt
70.000 gulden. De
evacuatiekosten
bedragen 100.000
gulden; eenzelfde
bedrag gaat naar
de GGD, terwijl
voor verzekeringskosten
2 ton zijn
uitgetrokken. Het
terugbrengen in de
oorspronkelijke
staat van het terrein
waar de bom
heeft gelegen kost
nog eens een half
miljoen gulden.

## Bellebom wordt waarschijnlijk een museumstuk

Van een onzer verslaggevers

Rotterdam — De ‘Bellebom’,
die gistermiddag *27 maart 1988*
door adjudant P. Schoots en
sergeant J.C. Linschoten
van de Explosieven Opruimings Dienst
onschadelijk
is gemaakt, krijgt waarschijnlijk
een plaatsje in het
Historisch Museum van Rotterdam.
Burgemeester Peper
gaat proberen de bom,
waar de springstof nog uitgehaald
moet worden, voor
de Maasstad te behouden.

„Het is een uiterst gaaf
exemplaar; een van de mooiste
die we ooit onschadelijk
hebben gemaakt”, zeiden
gistermiddag *27 maart 1988* adjudant
Schoots en sergeant Linschoten
in koor. „Alle letters en
cijfers staan er nog op en
zelfs de kleurenbandjes die
de lading aangeven zijn nagenoeg
ongeschonden.”

### Slagpijpje

Volgens beiden is de demontage
snel, maar niet zonder
gevaar verlopen. „Het
duplex slagpijpje was zó
gammel”, aldus kapitein
Van Maren die de operatie
leidde, „dat het direct in een
put naast de bom tot ontploffing
is gebracht. Pas daarna
is de Bellebom zelf opgetakeld.
De demontage begon
om 13.50 uur; om 14.55 was
de bom boven en om 17.00
uur reed-ie in een vrachtwagen
Rotterdam uit op weg
naar Culemborg voor verdere
behandeling.”

Daarna ontstond in de inmiddels
vrijgegeven Bellevoysstraat
een waar ‘bomtoerisme’. Terwijl de eerste
bewoners terugkeerden naar
hun huizen verdrongen honderden
mensen zich rond de
plaats waar de blindganger
uit 1944 bijna 44 jaar op een
diepte van elf meter heeft
gelegen.

De wijk Middelland had de
hele dag een onwezenlijke
aanblik geboden: Verlaten
winkelstraten en woonwijken,
waaruit ook alle auto's
waren verwijderd.

De EOD studeert nu samen
met TNO op de mogelijkheden
om meer bommen
te demonteren. „Ze liggen
her en der in het land en zeker
ook in Rotterdam”, aldus
de commandant van de
EOD. Volgens burgemeester
Peper ligt er in elk geval één
aan de Boezembocht in de
Maasstad, maar zijn van
‘zestien melders’ ook aanwijzingen
over andere bommen
binnengekomen. Sommige
van de meldingen betreffen
een en dezelfde bom, aldus
burgemeester Peper.

### Oproep

Peper had de bevolking
gistermorgen *27 maart 1988* via het lokale
radiostation Radio Rijnmond
opgeroepen ‘het gezellig
te houden’. Hij was langdurig
in de wijk om er met
bewoners te praten. Na afloop
toonde hij zich uiterst
tevreden. Zó zelfs, dat hij op
nog geen tien centimeter afstand
van de inmiddels omhoog
gebrachte Bellebom
een sigaartje opstak. Maar
volgens de mensen van de
EOD kon dat geen kwaad.
Zelf schrok Peper er een
beetje van. „Temeer”, zo zei
hij, „omdat twee jaar geleden
bij een soortgelijke demontage
in West-Berlijn de
bom tot ontploffing kwam.
Er werd toen een kleuterschool
finaal weggeblazen”,
aldus Peper. Hij trapte het
sigaartje toch maar uit, op
nog geen vijf centimeter
voor de punt van de bom.

## Bewoners tevreden over aanpak

Van een onzer verslaggevers

Rotterdam — Het merendeel
van de bewoners dat
vanwege de Bellebom zijn
huis heeft moeten verlaten
zegt tevreden te zijn over de
aanpak en de voorlichting van
de gemeente.

Vooral onder de vele bewoners
van buitenlandse afkomst
toonden gisteren *27 maart 1988* bewondering
voor de zorgvuldige informatievoorziening
en de inzet van
tolken tijdens de operatie.

Dat blijkt uit een representatieve
steekproef die gisteren *27 maart 1988*
onder 105 geëvacueerde bewoners
van de wijk Middelland is
gehouden door enquêteurs van
de Rijksuniversiteit in Leiden
en de Erasmus Universiteit in
Rotterdam.

Volgens de leider van dit crisisonderzoeksteam,
prof. dr. U. Rosenthal,
spreekt uit de resultaten
groot vertrouwen in
de overheid. „Niemand heeft
een poging gedaan zijn antiek
tijdelijk ergens anders te brengen.
Daaruit bleek het vertrouwen
in de goede afloop”, aldus
Rosenthal.

Bijna de helft van de bewoners
noemde de speciale Bellebomkrant
van de gemeente als
de belangrijkste en beste bron
van informatie. Opmerkelijk
was hoe weinig mensen zich
lieten voorlichten via Radio Rijnmond:
slechts vijf procent.

Volgens de onderzoekers gaf
het tijdstip van evacuatie aanleiding
tot veel klachten. Sommigen
moesten ruim vier uur
wachten alvorens zijn de betrokken
straten konden verlaten.

*Onderschrift bij de foto's*

Lachende gezichten, kwispelende honden en de politie
die op het huis past. (Foto's Ton den Haan)

De Energiehal rekende
op zo'n duizend mensen. Er
kwamen er amper 350. De
rest ging naar familie of kennissen.

De oudste bewoners van de wijk Middelland verlaten hun huis. Het zijn de 91-jarige W.J. de Paus
en de een jaar oudere D. Hannewijk. Beiden hebben de bom destijds zien vallen. Gisteren *27 maart 1988*
stortten tientallen verslaggevers en fotografen zich op het bejaarde paar. „Het lijkt wel
of we net getrouwd zijn”, aldus De Paus.

Welkom staat er op dit pandje, maar de politie moest het wel openbreken.

Waar het allemaal om ging: De Bellevoysstraat. Verlaten huizen, en wachten op de klap die niet kwam.
