---
toegevoegd: 2021-04-29
gepubliceerd: 1918-05-31
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000190695:mpeg21:a0052"
onderwerpen: [diefstal]
kop: "Diefstal in de Jan Sonjéstraat."
---
# Diefstal in de **Jan Sonjéstraat**.

Terecht stond J.J. K. 21 jaar groentenkoopman
wonende en gedetineerd alhier, beklaagd op 5
April *1918* l*aatst*l*eden* te zamen en in vereeniging met één of
meer andere personen uit een pakhuis aan de **Jan Sonjéstraat**
te hebben weggenomen 4 kisten Sultana-zeep,
28 doozen kwatta-chocolade-reepen, 13
pakken verschillende soorten zachte zeep, 51 doozen
toiletzeep en een zak peperkorrels en een zak
met doozen plantenvet, alles toebehoorende aan
P.J. Dietvors en L. Rosenboom.

Bekl*aagde* verklaart de goederen uit het pakhuis
geladen te hebben op last van een ander.

De voerman A. Gevaerts, verklaart, dat er den
vorigen avond *4 april 1918* iemand ten zijnen huize is geweest
om den volgenden morgen *5 april 1918* een vrachtje te vervoeren
van de **Jan Sonjéstraat** naar het Maasstation.
Wie zijn lastgever was, wist hij niet, en was ook
zijn gewoonte niet om te vragen.

De agent van politie J. Ponse verklaart dat ze
door een jongen van omstreeks 12 jaar die de
eigenaars van het pakhuis kende gewaarschuwd
waren, dat vreemde menschen aan het pakhuis
geweest waren, en de goederen in een soort van
bestelwagen als van de spoor hadden geladen. In
draf zijn ze toen weggereden en zagen op de
Kruiskade een soort wagen met twee paarden bespannen
die ongewoon hard reed. Ze hebben den
wagen en bekl*aagde* aangehouden, en de ontvreemde
goederen gevonden.

De officier achtte het feit bewezen en eischte
1 jaar gevangenisstraf.

Mr. J.P.J. Mashoff, de verdediger, vroeg na een
breedvoerig pleidooi, vrijspraak.
