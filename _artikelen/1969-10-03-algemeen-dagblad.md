---
toegevoegd: 2024-07-22
gepubliceerd: 1969-10-03
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002853003:mpeg21:a00098"
onderwerpen: [familiebericht]
huisnummers: [9]
achternamen: [touw]
kop: "Cornelis Marinus Touw"
---
Met droefheid geven wij u kennis, dat na een
langdurig geduldig gedragen lijden van ons is
heengegaan mijn innig geliefde Man, onze lieve
Broer, Zwager en Oom

# Cornelis Marinus Touw

op de leeftijd van 59 jaar.

Mede namens verdere Familie:

C. Touw-Mijsbergh

Rotterdam, 1 oktober 1969

**Jan Sonjéstraat** 9b

Mijn Man is opgebaard in de Rouwkamer van
de Mathenesserlaan 470.

Bezoek en gelegenheid tot condoleren donderdag *2 oktober 1969*
van 20.30 uur tot 21.00 uur en vrijdag *3 oktober 1969* van
19.00 uur tot 19.30 uur.

De begrafenis zal plaatsvinden zaterdag 4 oktober *1969* a*an*s*taande*
op de Algemene Begraafplaats „Hofwijk” te Overschie.

Vertrek vanaf het woonhuis om 12 uur. Aankomst
aan de Begraafplaats te c*irc*a 12.20 uur.
Na de plechtigheid is er gelegenheid tot condoleren
in de Aula op de Begraafplaats.
