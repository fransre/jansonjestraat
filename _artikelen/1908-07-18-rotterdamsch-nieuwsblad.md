---
toegevoegd: 2024-03-07
gepubliceerd: 1908-07-18
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197183:mpeg21:a0125"
onderwerpen: [verloren]
huisnummers: [5]
kop: "Verloren"
---
# Verloren

Dinsdagmiddag *14 juli 1908* j*ongst*l*eden*, 3 u*ur*,
vermoedelijk Middellandstraat,
's Gravendijkwal,
een fluweelen Ceintuur
met Zeeuwsche Knoopen,
geen zilver. Tegen belooning.
Adres **Jan Sonjéstraat** 5.
