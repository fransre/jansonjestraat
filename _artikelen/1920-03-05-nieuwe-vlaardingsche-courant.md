---
toegevoegd: 2024-08-13
gepubliceerd: 1920-03-05
decennium: 1920-1929
bron: Nieuwe Vlaardingsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMSAVL02:000487029:mpeg21:a00006"
onderwerpen: [familiebericht]
huisnummers: [34]
achternamen: [bosselaar]
kop: "Margareta Johanna Bosselaar,"
---
Enige en algemeene kennisgeving.

Heden overleed zacht en kalm,
na een langdurig geduldig gedragen
lijden, onze innig geliefde Dochter,
Zuster en Schoonzuster

# Margareta Johanna Bosselaar,

in den ouderdom van 30 jaar.

Wed*uwe* A. Bosselaar-v*an* d*er* Linden.

A. Bosselaar.

J.C. Noordzij-Bosselaar

A. Noordzij Jz.

Geen bloemen.

Verzoeke van rouwbeklag verschoond
te blijven.

Rotterdam, 2 Maart 1920.

**Jan Sonjéstraat** 34b.

1727 22
