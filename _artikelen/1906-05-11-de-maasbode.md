---
toegevoegd: 2024-08-14
gepubliceerd: 1906-05-11
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000191106:mpeg21:a0008"
onderwerpen: [straat]
kop: "Stadsnieuws."
---
# Stadsnieuws.

Van gemeentewege wordt in de
Schermlaan tegenover de **Jan Sonjéstraat**
een telefoontoren geplaatst.
