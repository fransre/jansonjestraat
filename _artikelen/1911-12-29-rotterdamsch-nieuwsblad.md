---
toegevoegd: 2021-05-18
gepubliceerd: 1911-12-29
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010200259:mpeg21:a0100"
onderwerpen: [reclame]
huisnummers: [22]
achternamen: [nolles]
kop: "Bioscope-Operateur!"
---
# Bioscope-Operateur!

Volledige opleiding, theoretisch en practisch in 4 weken!
Behandeling van kalklicht, booglampen, motor-generator enz*ovoort*!
Alzoo een vak in 4 weken!

Eerste Rotterdamsche Operateurschool. Directie: Henri Nolles,
Feestarrangeur. Bioscope-installateur der Concertzaal
„De Vereeniging” e*n* a*nderen*.

52632 24

Zich aan te melden: **Jan Sonjéstraat** 22, Rotterdam.
