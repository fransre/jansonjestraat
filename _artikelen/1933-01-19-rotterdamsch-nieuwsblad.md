---
toegevoegd: 2024-08-05
gepubliceerd: 1933-01-19
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164663021:mpeg21:p00014"
onderwerpen: [bedrijfsinformatie]
huisnummers: [4, 17]
kop: "Publieke verkoopingen."
---
# Publieke verkoopingen.

In het Notarishuis aan de Gelderschekade
te Rotterdam.

woensdag 18 januari *1933*

Eindafslag:

Door de Notarissen P.W.M. Esser,
Schiedamschesingel 33a en H.J.J. Verhoeff
te Avenhorn

Pand en erf, 1ste Middellandstraat 98a b, in
bod op ƒ18.800.

Id., **Jan Sonjéstraat** 4a, b, in bod op ƒ7300,
tezamen verk*ocht* voor ƒ27.150.

Id*em*, id*em* 17a b, id*em* ƒ7100, verk*ocht* voor ƒ7200.
