---
toegevoegd: 2024-08-13
gepubliceerd: 1919-03-20
decennium: 1910-1919
bron: Nieuwe Rotterdamsche Courant
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010036622:mpeg21:a0114"
onderwerpen: [bedrijfsinformatie]
huisnummers: [38]
kop: "Rotterdamsche IJsinrichting der Vereenigde Banketbakkers."
---
# Rotterdamsche IJsinrichting der Vereenigde Banketbakkers.

**Jan Sonjéstraat** 38. Dir*ectie* A.C. Haalebos.

Vanaf heden telefonisch aangesloten onder N*ummer* 1017.

Levering van alle soorten ijspuddingen etc*era*.

19215.14
