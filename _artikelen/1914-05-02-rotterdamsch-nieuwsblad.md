---
toegevoegd: 2024-08-13
gepubliceerd: 1914-05-02
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295500:mpeg21:a0217"
onderwerpen: [familiebericht]
huisnummers: [11]
achternamen: [elfring]
kop: "Jacob Elfring,"
---
Heden overleed zacht en
kalm, na eene ongesteldheid
van slechts enkele dagen, mijn
geliefde Echtgenoot, onze Vader,
Behuwd- en Grootvader,
de Heer

# Jacob Elfring,

in den ouderdom van bijna
73 jaren.

Wed*uwe* J. Elfring-C. Venema.

A. Elfring.

A. de Jong-Elfring.

H.A. de Jong.

H. Elfring.

A. Elfring-Werner.

T. v*an* d*er* Graaf-Elfring.

P.C. v*an* d*er* Graaf

en Kleinkinderen.

Rotterdam, 30 April 1914.

**Jan Sonjéstraat** 11b.

28
