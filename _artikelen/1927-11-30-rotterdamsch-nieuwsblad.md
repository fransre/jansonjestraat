---
toegevoegd: 2024-08-07
gepubliceerd: 1927-11-30
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514157:mpeg21:a0198"
onderwerpen: [werknemer]
huisnummers: [29]
kop: "Biedt zich aan:"
---
# Biedt zich aan:

halfwas Timmerman,
18 jaar, flink van Persoon,
prima ref*erenties*. Adres
**Jan Sonjéstraat** 29b.
