---
toegevoegd: 2021-04-29
gepubliceerd: 1907-02-06
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196594:mpeg21:a0001"
onderwerpen: [diefstal]
huisnummers: [22]
kop: "Diefstalkroniek"
---
# Diefstalkroniek

Uit een in aanbouw zijnd pand aan de
**Jan Sonjéstraat** n*ummer* 22 is ten nadeele van
de bouwondernemers V. en Zoon, wonende
Molenwaterweg, een looden privaatbuis
gestolen.
