---
toegevoegd: 2024-08-05
gepubliceerd: 1931-05-11
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164653012:mpeg21:a00209"
onderwerpen: [familiebericht]
huisnummers: [13]
achternamen: [barends]
kop: "Maria Barents, geboren Van der Vegt,"
---
Eenige en Algemeene Kennisgeving.

Heden ontsliep geheel plotseling
mijn geliefde Echtgenoote, onze
zorgzame Moeder, Behuwd-, Groot- en
Overgrootmoeder, Mevr*ouw*

# Maria Barents, geb*oren* Van der Vegt,

in den ouderdom van 72 jaar.

Rotterdam, 10 Mei 1931.

**Jan Sonjéstraat** 13a.

A. Barents.

A. Barents J*unio*r.

M. Barents-Erdtsieck.

J. Barents.

J. Barents-Boogerd.

Hillegersberg, M. Barents.

C. Barents-Leeuwenstein.

Voorburg, G. Barents.

W.H.C. Barents-Mulder.

Scheveningen, A. C. Barents-Kapel.

G.G. Kapel.

K.W. Barents.

C. Barents-Noorlander.

Kinderen en Kleinkind.

Geen rouwbezoek. Geen Bloemen.

De teraardebestelling zal plaats
hebben a*an*s*taande* Woensdag *13 mei 1931* op de Algemeene Begraafplaats Crooswijk.
Vertrek van het sterfhuis 1 uur.

45
