---
toegevoegd: 2024-08-03
gepubliceerd: 1936-10-03
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164685029:mpeg21:a00068"
onderwerpen: [bedrijfsinformatie, auto, motor, kleding, tekoop]
huisnummers: [4]
kop: "3 in zeer goeden staat verkeerende luxe gesloten Auto's."
---
Openbare verkooping

onder directie van Kruijne & Jonker.

Dinsdag 6 October 1936 des namiddags 2 uur, worden in de garage
van pand N*ummer* 4 aan de **Jan Sonjéstraat** te Rotterdam, publiek,
voetstoots en om contant geld verkocht:

# 3 in zeer goeden staat verkeerende luxe gesloten Auto's.

(resp*ectievelijk*: Chrysler, Hupmobile en Plymouth), diverse Garagebenoodigdheden
en Gereedschappen, eenige nieuwe Motorkleeding, Onderdelen
en aanverwanten.

Te zien ter plaatse der veiling **Jan Sonjéstraat** N*ummer* 4 te Rotterdam Dinsdag 6 October *1936* a*an*s*taande* van 9 tot 2 uur.

Nadere informatiën Kruijne & Jonker, Telef*oon* Interc*om* 53279.

31819 40
