---
toegevoegd: 2023-10-07
gepubliceerd: 1954-12-31
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000328104:mpeg21:a00015"
onderwerpen: [reclame]
huisnummers: [32]
achternamen: [van-der-velden]
kop: "Exclusieve reclame"
---
# Exclusieve reclame

A. van der Velden

**Jan Sonjéstraat** 32 — Rotterdam — Telefoon 51637
