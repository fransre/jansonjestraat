---
toegevoegd: 2024-08-10
gepubliceerd: 1924-12-12
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495225:mpeg21:a0148"
onderwerpen: [misdrijf]
kop: "Een nieuwe truc."
---
# Een nieuwe truc.

De politie waarschuwt tegen de praktijken
van iemand die voorgeeft J. de G. te
heeten en in de **Jan Sonjéstraat** te wonen,
daar hij verdacht wordt in de bladen advertenties
te plaatsen, in welke, tegen de
verplichting van het schoonhouden van
kantoren, vrije woning wordt aangeboden.

Bewoners van de Taborstraat en Wandeloordstraat,
die op zoo'n advertentie hadden
geschreven, ontvingen daarna bezoek
van den bewusten persoon, die hen ƒ2.10
voor informatiekosten liet betalen en daarna
niets meer van zich liet hooren. Op de
door hem opgegeven adressen aan de
Boompjes en in de **Jan Sonjéstraat** bleek
hij niet bekend te zijn. Men zij gewaarschuwd.
