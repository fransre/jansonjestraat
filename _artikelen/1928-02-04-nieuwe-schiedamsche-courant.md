---
toegevoegd: 2024-05-20
gepubliceerd: 1928-02-04
decennium: 1920-1929
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1928-02-04/edition/0/page/12"
onderwerpen: [huispersoneel]
huisnummers: [10]
kop: "Net Rooms Katholiek meisje"
---
# Net R*ooms* K*atholiek* meisje

gevr*aagd* in kl*ein* gezin, 14 à 16 jaar, voor
hulp in de huish*ouding* v*an* 8-2 uur. Loon
pl*us*m*inus* ƒ4.50 p*er* w*eek*. Zondags vrij. **Jan Sonjéstraat** 10a
v*an* 7-8 uur.

7250
