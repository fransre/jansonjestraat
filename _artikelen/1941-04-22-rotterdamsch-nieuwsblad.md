---
toegevoegd: 2024-07-29
gepubliceerd: 1941-04-22
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002590:mpeg21:a0047"
onderwerpen: [verloren]
huisnummers: [37]
kop: "Verloren"
---
# Verloren

Zondag *20 april 1941* j*ongst*l*eden*, lederen
H*eren*portefeuille, inh*oud* v*oor*
vinder g. w. Tegen
bel*oning* terugbez*orgen*: **Jan Sonjéstraat** 37B.
