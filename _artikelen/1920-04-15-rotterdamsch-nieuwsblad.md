---
toegevoegd: 2023-01-21
gepubliceerd: 1920-04-15
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493620:mpeg21:a0163"
onderwerpen: [werknemer]
huisnummers: [15, 17]
kop: "12 wagenrijders"
---
Rotterdamsche IJsinrichting,

**Jan Sonjéstraat** 15-17,

kunnen nog

# 12 wagenrijders

geplaatst worden.

34325 30
