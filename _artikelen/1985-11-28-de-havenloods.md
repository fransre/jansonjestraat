---
toegevoegd: 2021-04-30
gepubliceerd: 1985-11-28
decennium: 1980-1989
bron: De Havenloods
externelink: "https://schiedam.courant.nu/issue/HAV/1985-11-28/edition/0/page/24"
onderwerpen: [woonruimte, tekoop]
huisnummers: [23]
kop: "Geheel pand"
---
# Geheel pand

a*an* d*e* **Jan Sonjéstraat** 23
te R*otter*dam-centrum.
In perfekte staat met
tuin en veel extra's. Ind*eling*: woon-eetkamer,
massief eiken keuk*en*
met app*aratuur*, zijkamer, keuken
onder het gehele huis; 1e verd*ieping*:
3 slaapkamers, luxe badkamer
met bad; 2e verd*ieping*: grote open
zolder, slaapkamer. Koopsom
ƒ79.000,— k*osten* k*oper* Mak*elaars*kant*oor* Kolpa
b.v., lid N.V.M., Straatweg 94,
3051 BL R*otter*dam, tel 010-226144
