---
toegevoegd: 2024-08-07
gepubliceerd: 1928-04-07
decennium: 1920-1929
bron: Nieuwe Utrechtsche courant
externelink: "https://resolver.kb.nl/resolve?urn=MMUTRA04:253381015:mpeg21:a00041"
onderwerpen: [familiebericht]
huisnummers: [6]
achternamen: [bosma]
kop: "C.J. van der Eijk en G.N. Bosma."
---
Ondertrouwd:

# C.J. v*an* d*er* Eijk en G.N. Bosma.

Rotterdam, 5 April 1928.

Kralingscheweg 375.

**Jan Sonjéstraat** 6.

Kerkelijke inzegening in de
Geref*ormeerde* N*ieuwe* Zuiderkerk aan de
Westzeedijk, op Donderdag 19
April *1928* a*an*s*taande* des namiddags ten 2
ure, door den Weleerw*aarde* Heer Ds.
S. Bosma, Predikant te Lochem.
