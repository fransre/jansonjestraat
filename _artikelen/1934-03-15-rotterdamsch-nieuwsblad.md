---
toegevoegd: 2024-08-04
gepubliceerd: 1934-03-15
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164670017:mpeg21:a00088"
onderwerpen: [woonruimte, tehuur]
huisnummers: [21]
kop: "Aangeboden:"
---
# Aangeboden:

Slaapkamer of Zit-Slaapkamer.
**Jan Sonjéstraat** 21a,
Vrij Bovenhuis.
