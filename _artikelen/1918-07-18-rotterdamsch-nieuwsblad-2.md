---
toegevoegd: 2024-08-13
gepubliceerd: 1918-07-18
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297064:mpeg21:a0101"
onderwerpen: [boek, tekoop]
huisnummers: [13]
achternamen: [weide]
kop: "Hogereburgerschool"
---
# H*ogere*B*urger*S*chool*

Te koop aangeboden
Boeken 1ste, 2de en 3e
klasse B, H*ogere*B*urger*S*chool* Kortenaerstraat.
Te bevragen Weide, **Jan Sonjéstraat** 13b.
