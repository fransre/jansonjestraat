---
toegevoegd: 2023-01-09
gepubliceerd: 2011-09-18
decennium: 2010-2019
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2011-09-18-de-middellander-origineel.jpg"
onderwerpen: [dagjevanplezier]
achternamen: [lutgerink, jager]
kop: "Zesde Dagje van Plezier"
---
# Zesde Dagje van Plezier

Op 18 september *2011* j*ongst*l*eden* hebben
de kinderen van Middelland
voor de zesde keer kunnen
genieten van het Dagje van Plezier.
De sport- en speldag,
bedacht en opgezet
door Aad Lutgerink uit de
**Jan Sonjéstraat**, heeft ondanks
de regen nog een
flink aantal kinderen weten
te vermaken.
Ook dit jaar heeft het Dagje
van Plezier, ondanks de belabberde
weersomstandigheden,
weer een beroep
kunnen doen op een grote
groep vrijwilligers en de volgende
sponsoren: Firma Koolmees,
Dolf van Eyk automaterialen,
F*irm*a Vaisnovi Isik Groente en Fruit,
City Silks
en Golden Fruit.
Maar ook wijkpartners zoals
TOS, Duimdrop (BSW) en
Stichting Delphi Opbouwwerk
hebben wederom hun
steentje bijgedragen.
Namens de organisatie van
het Dagje van Plezier willen
wij dus alle vrijwilligers,
wijkpartners en sponsoren
bedanken voor hun inzet.
Fantastisch!
Tot volgend jaar!

Aad Lutgerink,
Robert Jager en
Hutch Lourens
