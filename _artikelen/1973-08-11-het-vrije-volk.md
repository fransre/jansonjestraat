---
toegevoegd: 2024-07-21
gepubliceerd: 1973-08-11
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958194:mpeg21:a0049"
onderwerpen: [woonruimte, gevraagd]
huisnummers: [26]
achternamen: [poot]
kop: "huisjes,"
---
Te koop gevraagd één of twee
leegstaande

# huisjes,

apart
of twee onder één dak. Hoeft
geen nieuwbouw te zijn. In
Rotterdam of randgemeente.
J. Poot, **Jan Sonjéstraat** 26a
Rotterdam-3.

R 15
