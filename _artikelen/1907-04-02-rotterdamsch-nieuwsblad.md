---
toegevoegd: 2024-08-14
gepubliceerd: 1907-04-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196639:mpeg21:a0190"
onderwerpen: [woonruimte, tehuur]
huisnummers: [14]
kop: "Boven- en Benedenhuis"
---
Te huur in 't Westen, **Jan Sonjéstraat** N*ummer* 14,
een ruim

# Boven- en Benedenhuis

met Tuin.

Sleutels N*ummer* 16, beneden.

17479 6
