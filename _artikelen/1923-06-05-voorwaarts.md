---
toegevoegd: 2024-08-11
gepubliceerd: 1923-06-05
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010210551:mpeg21:a0097"
onderwerpen: [diefstal, fiets]
kop: "Diefstallen."
---
# Diefstallen.

Van een portier aan de Maashaven is ten
nadeele van B. R. uit de **Jan Sonjéstraat**, een
rijwiel ontvreemd.
