---
toegevoegd: 2024-07-29
gepubliceerd: 1940-08-17
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002559:mpeg21:a0046"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [14]
achternamen: [verschoor]
kop: "Aangeboden"
---
# Aangeboden

op vrij bovenhuis gem*eubileerde*
Zit-Slaapkamer m*et* of z*onder*
pension, vaste waschtafel,
alleen inwonend.
Verschoor. **Jan Sonjéstraat** 14B.
