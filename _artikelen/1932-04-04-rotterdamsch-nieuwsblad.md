---
toegevoegd: 2024-08-05
gepubliceerd: 1932-04-04
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164658038:mpeg21:a00207"
onderwerpen: [familiebericht]
huisnummers: [14]
achternamen: [van-poppel]
kop: "Adolph Maria van Poppel,"
---
Eenige en algemeene kennisgeving

Heden overleed plotseling, tot
mijn diepe droefheid, voorzien van
de H*eilige* Sacramenten, mijn geliefde
Vader, de Heer

# Adolph Maria van Poppel,

In den ouderdom van ruim 67 jaar.

H. van Poppel.

Rotterdam, 3 April *19*32.

S*in*t Andriesstraat 35a.

Toekomstig adres:

**Jan Sonjéstraat** 14b.

De teraardebestelling zal plaats
hebben Donderdag *7 april 1932* a*an*s*taande* op de R*ooms* K*atholieke* Begraafplaats Crooswijk.
Vertrek
van het S*in*t Franciscus Gasthuis
half 9 v*oor*m*iddag*.

26
