---
toegevoegd: 2021-07-24
gepubliceerd: 1958-09-23
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000358073:mpeg21:a00115"
onderwerpen: [reclame, auto, motor]
huisnummers: [4]
achternamen: [take]
kop: "Auto- en motorrijschool H.P. Take"
---
# Auto- en motorrijschool H.P. Take

rijdt met nieuwe
Opels Rekords 1958 Gedipl*omeerd* KNAC-instructeur. **Jan Sonjéstraat** 4,
telef*oon* 36777, Rotterdam
