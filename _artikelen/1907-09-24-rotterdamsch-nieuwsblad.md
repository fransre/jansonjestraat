---
toegevoegd: 2024-03-07
gepubliceerd: 1907-09-24
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196931:mpeg21:a0123"
onderwerpen: [dieren]
huisnummers: [42]
kop: "Gratis"
---
# Gratis

aangeboden een mooi geteekend,
zindelijk Hondje,
6 m*aand* oud, wegens
vertrek naar buiten. Te
bevragen **Jan Sonjéstraat** 42
