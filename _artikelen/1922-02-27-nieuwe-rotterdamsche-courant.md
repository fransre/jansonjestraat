---
toegevoegd: 2024-08-11
gepubliceerd: 1922-02-27
decennium: 1920-1929
bron: Nieuwe Rotterdamsche Courant
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010025943:mpeg21:a0010"
onderwerpen: [werknemer]
huisnummers: [38]
achternamen: [gogarn]
kop: "De matrassenmaker W.G. Vermeulen"
---
# De matrassenmaker W.G. Vermeulen

is vandaag *27 februari 1922*
50 jaar, en de magazijnbediende W.L. Romken 40
jaar in dienst van de firma J.H.A. Gogarn, fabriek
ter bereiding van bedveeren en kapok in de **Jan Sonjéstraat**.
De jubilarissen zijn vanmorgen *27 februari 1922* door den patroon,
don heer J.F.A. Gogarn, toegesproken en gelukgewenscht,
waarbij hun een geschenk in couvert is
overhandigd. Ook het personeel deed van zijn belangstelling
blijken, het bood bij monde van den
boekhouder, den heer K. Edenburg, stoffelijke blijken
van belangstelling aan. Vanavond *27 februari 1922* worden de
jubilé's in intiemen kring gevierd.
