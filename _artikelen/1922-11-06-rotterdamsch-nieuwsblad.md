---
toegevoegd: 2024-08-11
gepubliceerd: 1922-11-06
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494491:mpeg21:a0069"
onderwerpen: [tekoop]
huisnummers: [38]
achternamen: [weier]
kop: "Bioscoop,"
---
Een Huis-

# Bioscoop,

geheel compleet te koop
Voor elk aannemelijk
bod. E.J. Weier. **Jan Sonjéstraat** 38,
Telefoon 10084.
