---
toegevoegd: 2024-08-04
gepubliceerd: 1934-10-04
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164673038:mpeg21:a00178"
onderwerpen: [woonruimte, tehuur]
huisnummers: [4]
kop: "mooie voorkamer"
---
Ongemeub.

# mooie voork*amer*

met Slaapk*mer*, bij M*an* z*onder*
K*inderen*, liefst v*oor* J*ongen* alleen.
**Jan Sonjéstraat** 4a.
