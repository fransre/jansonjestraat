---
toegevoegd: 2024-07-21
gepubliceerd: 1978-06-17
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002935015:mpeg21:a00337"
onderwerpen: [woonruimte, tehuur]
huisnummers: [46]
kop: "3 gemeubileerde kamers"
---
T*e* h*uur* 3 gemeub*ileerde* kamers met telef*oon*
TV, douche+keuken, voor
dames. Tel*efoon* 010-258873
R*otter*dam-W*est* **Jan Sonjéstraat** 46.

R12
