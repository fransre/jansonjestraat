---
toegevoegd: 2023-01-21
gepubliceerd: 1971-10-23
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010957676:mpeg21:a0023"
onderwerpen: [angelique]
huisnummers: [33]
kop: "Filmclub Angelique"
---
# Filmclub Angelique

Vanaf heden iedere dag: geopend
van 11.00 tot 01.00 uur.
Geluidsfilms, striptease en liveshows.
Verkoop en verhuur van
films. **Jan Sonjéstraat** 33a
Rotterdam-Centrum, tel*efoon* 010-256188.
