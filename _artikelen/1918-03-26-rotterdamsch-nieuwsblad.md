---
toegevoegd: 2024-08-13
gepubliceerd: 1918-03-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296972:mpeg21:a0025"
onderwerpen: [werknemer]
huisnummers: [8]
achternamen: [van-essen]
kop: "Werkvrouw"
---
# Werkvrouw

gevraagd. Aanmelden
's avonds tusschen 7-8 uur:
Van Essen,
**Jan Sonjéstraat** 8.
