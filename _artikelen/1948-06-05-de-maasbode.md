---
toegevoegd: 2024-07-26
gepubliceerd: 1948-06-05
decennium: 1940-1949
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB15:000554131:mpeg21:a00060"
onderwerpen: [woonruimte, tekoop]
huisnummers: [28]
kop: "Openbare vrijwillige verkoping"
---
# Openbare vrijwillige verkoping

door Not*aris*s*en* H.A.W. Zeijlmans van Emmichoven te
Rotterdam op Woensdag 16
Juni (inzet) en zo nodig op
Woensdag 23 Juni 1948 (afslag)
telkens te 14 ure in veilingzaal 5
van het Beursgebouw
aldaar (Meent) van het pand
best*aande* uit beneden en bovenhuis
en erf te R*otter*dam aan de

**Jan Sonjéstraat** 28

kad*astraal* bek*end* Gem*eente* Delfshaven,
Sectie C. N*ummer* 3743 groot 1 Are
totaal verhuurd voor ƒ810.— p*er* j*aar*.
Grondbel. ƒ63.63, straatbel*asting*
ƒ19.69 p*er* j*aar*; waterleiding
ƒ12.42 per kwartaal. Aanv*aarde* en
bet*aalde* kooppenningen in vrij
geld 22 Juli 1948. Bez*ichtigen* 14, 15, 16
en 22 Juni 1948 van 10-16 uur.
Inl*ichtingen* en toeg. bew. ten kantore
van gen*oemde* notaris, Rochussenstraat 55,
telefoon 28095.
