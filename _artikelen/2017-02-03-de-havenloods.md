---
toegevoegd: 2023-04-10
gepubliceerd: 2017-02-03
decennium: 2010-2019
bron: De Havenloods
externelink: "https://www.dehavenloods.nl/nieuws/algemeen/38380/bloembak-gestolen-in-middelland"
onderwerpen: [verkeer]
kop: "Bloembak gestolen in Middelland"
---
# Bloembak gestolen in Middelland

Rotterdam — Dit bericht uit Rotterdam-West kregen we gisteravond *2 februari 2017* binnen via de facebookpagina van De Havenloods. In Middelland is een bloembak gestolen, vermoedelijk met behulp van een rode vrachtwagen. Wie weet daar meer van?

‘Beste buren wie kan ons helpen.

Vandaag *2 februari 2017* is een bloembak gestolen op de hoek van de Schermlaan en de **Jan Sonjéstraat**.
Deze bloembak zoals te zien op de foto was een onderdeel van het project Veilig, Veiliger Middelland.
De informatie die we nu hebben is dat er een rode vrachtauto hem heeft weggehaald.
Wie heeft er meer informatie voor ons.
Alvast dank allemaal.

Vriendelijke groet,

Team Veilig, Veiliger Middelland.’

*(Zie <https://veilig-veiliger-middelland.nl> voor het eindrapport van het project Veilig, Veiliger Middelland)*
