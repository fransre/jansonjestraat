---
toegevoegd: 2024-08-13
gepubliceerd: 1917-10-22
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296744:mpeg21:a0099"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [8]
achternamen: [van-essen]
kop: "Vaten."
---
# Vaten.

pl*us*m*inus* 120 Teerolievaten
(zeer vuil) aangeboden à
ƒ7.50 p*er* st*uk*. H. van Essen,
**Jan Sonjéstraat** 8. Telef*oon* 12026,
R*otter*dam.

53005 7
