---
toegevoegd: 2024-08-04
gepubliceerd: 1933-09-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164667008:mpeg21:a00042"
onderwerpen: [reclame, dans]
huisnummers: [38]
achternamen: [beyleveldt]
kop: "Dansinstituut A. Beyleveldt"
---
# Dansinstituut A. Beyleveldt

Zondag *10 september 1933* a*an*s*taande* aanvang der Danslessen van 4-5½, 6-7½ en van 8-11 uur. Inschrijving
tijdens de lessen aan de zaal, Vlietstraat 27, privé adres **Jan Sonjéstraat** 38a.

31989 10
