---
toegevoegd: 2024-08-14
gepubliceerd: 1909-02-03
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197346:mpeg21:a0173"
onderwerpen: [woonruimte, tehuur]
huisnummers: [19]
kop: "Te huur"
---
# Te huur

een mooi Benedenhuis,
**Jan Sonjéstraat** 19 bij
Middellandstraat. Te bevragen
aldaar en bij
Wessels, N*ieuwe* Binnenweg 345a.
