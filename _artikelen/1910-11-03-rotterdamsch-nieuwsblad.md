---
toegevoegd: 2024-08-14
gepubliceerd: 1910-11-03
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197118:mpeg21:a0035"
onderwerpen: [woonruimte, tehuur]
huisnummers: [38]
kop: "Te Huur:"
---
# Te Huur:

ongem*eubileerde* Voorkamer, met
of zonder Alkoof. **Jan Sonjéstraat** 38c,
1-m*aal* b*ellen*.
