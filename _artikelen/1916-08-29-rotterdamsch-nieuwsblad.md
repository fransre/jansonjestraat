---
toegevoegd: 2024-08-13
gepubliceerd: 1916-08-29
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296475:mpeg21:a0097"
onderwerpen: [gevraagd]
huisnummers: [29]
achternamen: [schmitz]
kop: "Onderwijzer"
---
Engelsch

# Onderwijzer

wordt gevraagd voor een
Club van 17 Personen,
kunnende beschikken
over Maandag, Woensdag
en Vrijdag 8-10 des
avonds. Opgave Condities
enz*ovoort*. Brieven Schmitz,
**Jan Sonjéstraat** 29, alhier.
