---
toegevoegd: 2024-07-24
gepubliceerd: 1960-08-18
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951129:mpeg21:a0245"
onderwerpen: [reclame, schilder]
huisnummers: [29]
achternamen: [pennings]
kop: "schilders"
---
Vakbekwame

# schilders

gevraagd, voor nieuw- en burgerwerk.

P. Levering, Spechtstraat 7b, telefoon 74798.

Na 6 uur: P. Pennings, **Jan Sonjéstraat** 29b.
