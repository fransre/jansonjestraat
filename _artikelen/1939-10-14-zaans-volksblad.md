---
toegevoegd: 2024-07-30
gepubliceerd: 1939-10-14
decennium: 1930-1939
bron: Zaans volksblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010944461:mpeg21:a0091"
onderwerpen: [familiebericht]
huisnummers: [29]
achternamen: [marcus]
kop: "Halfwas Loodgieter"
---
Verloofd

15 October 1939:

# Tonie v*an* 't Hof en Adrianus Marcus.

Rotterdam.

**Jan Sonjéstraat** 29.
