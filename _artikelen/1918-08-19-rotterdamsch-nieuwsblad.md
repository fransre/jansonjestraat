---
toegevoegd: 2024-08-13
gepubliceerd: 1918-08-19
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297091:mpeg21:a0054"
onderwerpen: [werknemer]
huisnummers: [13]
achternamen: [weide]
kop: "Heren tuinders!"
---
# H*eren* tuinders!

Plaatsing gevraagd
voor een gezonden,
sterken eenigszins achterlijken
Jongen, oud
17 jaar, 1½ jaar in
een Tuin werkzaam geweest,
niet bij groot
Personeel, geen loon.
Adres Weide. **Jan Sonjéstraat** 13b.
