---
toegevoegd: 2024-07-23
gepubliceerd: 1968-10-18
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956738:mpeg21:a0082"
onderwerpen: [woonruimte, tehuur]
huisnummers: [19]
achternamen: [roodenburg]
kop: "Benedenhuis"
---
# Benedenhuis

met grote kelder
te huur. Met urgent*ie*verkl*aring* en
overname stoffering. J. C. Roodenburg,
**Jan Sonjéstraat** 19b,
Rotterdam-3.
