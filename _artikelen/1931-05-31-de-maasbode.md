---
toegevoegd: 2024-08-05
gepubliceerd: 1931-05-31
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000198666:mpeg21:a0095"
onderwerpen: [familiebericht]
huisnummers: [39]
achternamen: [van-wulven]
kop: "Elisabeth van Wulven-geboren Van de Broek"
---
Allen, die iets verschuldigd zijn
aan, of onder hun berusting dan
wel te vorderen hebben van nu
wijlen Mevrouw

# Elisabeth van Wulven-geboren Van de Broek

in leven particuliere, laatst gewoond
hebbend te Rotterdam aan
de **Jan Sonjéstraat** n*ummer* 39a, en
overleden den 4 Mei 1931, wordt
verzocht daarvan ten spoedigste
opgaaf, afgifte of betaling te doen
ten kantore van de Notarissen

Van Kayesteyn & Hussem

te Rotterdam, Nieuwe Haven 137.

6644 18
