---
toegevoegd: 2024-05-16
gepubliceerd: 1935-10-18
decennium: 1930-1939
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005428042:00001"
onderwerpen: [familiebericht]
huisnummers: [28]
achternamen: [meijer]
kop: "Mevrouw de Weduwe Betje Meijer geboren Stern"
---
Heden overleed na een smartelijk
lijden onze innig geliefde Moeder,
Behuwd- en Grootmoeder

# Mevr*ouw* de Wed*uwe* Betje Meijer geb*oren* Stern

in den ouderdom van 70 jaar, diep betreurd
door haar liefhebbende kinderen

Borger (Dr*enthe*)

Ph. Dalsheim

F. Dalsheim-Meijer

J. Velleman

S. Velleman-Meijer

S. Velleman

J. Velleman-Meijer

en Kleinkinderen

Rotterdam,

15 October 1935

18 Tischrie 5696

**Jan Sonjéstraat** 28B
