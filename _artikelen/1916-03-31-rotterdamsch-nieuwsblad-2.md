---
toegevoegd: 2024-08-13
gepubliceerd: 1916-03-31
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296277:mpeg21:a0080"
onderwerpen: [tekoop]
huisnummers: [22]
achternamen: [spitters]
kop: "Geneeswijze."
---
# Geneeswijze.

Te koop aangeboden
de nieuwe Geneeswijze
door M. Platen, 2 Deelen
met 432 tusschen den
tekst gedrukte Afbeeldingen,
21 Platen in
kleurendruk en 10 ontleedbare
modellen voor
den spotprijs van 7.50.
J.W. Spitters, **Jan Sonjéstraat** 22b.
