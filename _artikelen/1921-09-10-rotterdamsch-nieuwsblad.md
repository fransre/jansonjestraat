---
toegevoegd: 2024-08-12
gepubliceerd: 1921-09-10
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493841:mpeg21:a0054"
onderwerpen: [bedrijfsinformatie]
huisnummers: [17]
kop: "Openbare Vrijwillige Verkooping."
---
# Openbare Vrijwillige Verkooping.

De Notarissen
Klootwijk & Verlinden.

te Rotterdam, zullen op Donderdagen
22 en 29 September 1921, telkens
nam*iddags* 2 uur, in het Notarishuis
in het openbaar verkoopen:

IV. Het pand en erf te Rotterdam,

**Jan Sonjéstraat** 17,

bestaande uit Pakhuis, Kantoor,
Open Plaats en vrije Bovenwoning,
groot 81 Centiaren.

De Bovenwoning is verhuurd
voor ƒ26.— per maand zonder
contract en overigens ledig te aanvaarden.
