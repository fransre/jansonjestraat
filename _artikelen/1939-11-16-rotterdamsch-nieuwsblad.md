---
toegevoegd: 2024-07-30
gepubliceerd: 1939-11-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164704014:mpeg21:a00136"
onderwerpen: [woonruimte, tehuur]
huisnummers: [29]
kop: "Benedenhuis"
---
Groot

# Benedenhuis

met Tuin te huur, 3
Kamers, hoge Kelder,
ƒ30 per maand of ƒ7
per week. **Jan Sonjéstraat** 29b.
