---
toegevoegd: 2024-07-26
gepubliceerd: 1948-10-30
decennium: 1940-1949
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010949879:mpeg21:a0069"
onderwerpen: [reclame, muziek]
huisnummers: [25]
achternamen: [gantevoort]
kop: "Muzieklessen"
---
# Muzieklessen

in Hawaiian,
banjo, mandoline, guitaar, ukelele
en accordeon vanaf ƒ3.50
per maand. Moderne methode.
Gantevoort, Muziekschool, **Jan Sonjéstraat** 25a.
