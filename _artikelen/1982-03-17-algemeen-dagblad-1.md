---
toegevoegd: 2021-05-04
gepubliceerd: 1982-03-17
decennium: 1980-1989
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002989014:mpeg21:a00137"
onderwerpen: [cafe, bedrijfsinventaris, radio, tekoop]
huisnummers: [15]
achternamen: [kok]
kop: "Executieverkoop"
---
# Executieverkoop

Donderdag 18 maart 1982
te 11.00 uur te Rotterdam,
**Jan Sonjéstraat** 15
ten laste van N. Kok van
een caféinventaris o*nder* a*ndere*
een fruitautomaat, radiocassette
deck, en wat
verder te koop zal worden
aangeboden.

Bezichtiging een half uur
voor de verkoop.

Contante betaling. Geen
opgeld.

De deurwaarder,
