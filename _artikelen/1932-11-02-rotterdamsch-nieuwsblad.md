---
toegevoegd: 2024-08-05
gepubliceerd: 1932-11-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164662002:mpeg21:a00126"
onderwerpen: [familiebericht]
huisnummers: [21]
achternamen: [den-besten]
kop: "Corrie den Besten en Allard Westergaard"
---
Ondertrouwd:

# Corrie den Besten en Allard Westergaard

**Jan Sonjéstraat** 21, Rotterdam.

Hoogstraat 2, Den Haag.

Receptie 18 November *1932* a*an*s*taande* 2-4 uur,
Rest*aurant* Het Gouden Hoofd, Den Haag

13
