---
toegevoegd: 2024-08-14
gepubliceerd: 1910-06-14
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000186082:mpeg21:a0045"
onderwerpen: [bedrijfsinformatie]
huisnummers: [15]
achternamen: [wiegmans]
kop: "Kennisgevingen."
---
# Kennisgevingen.

Drankwet.

Burgemeester en Wethouders van Rotterdam,
Gelet op art*ikel* 8 der bovenvermelde wet;

Doen te weten:

dat de navolgende vergunningen zijn verleend:

2\. aan L. Wiegmans tot het oprichten van eene
wasch- en strijkinrichting, met een electromotor van
3 p*aarden*k*rachten* voor het drijven van werktuigen in het
pand aan de **Jan Sonjéstraat** n*ummer* 15 (kad*astraal* Delfshaven
sectie D, n*ummer* 506, 507);
