---
toegevoegd: 2024-08-14
gepubliceerd: 1910-01-17
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197788:mpeg21:a0072"
onderwerpen: [bedrijfsruimte]
huisnummers: [3]
kop: "Te Huur"
---
# Te Huur

terstond een flink Winkelhuis
met ruime Woning,
Kelder en Plaats,
huur 5 per week, vier
jaar door dezelfde menschen
een Groentenzaak
in gedreven, ook zeer geschikt
voor Bierbottelarij
of Melkzaak of voor
vele andere doeleinden
geschikt, **Jan Sonjéstraat** N*ummer* 3,
eerste huis vanaf
1e Middellandstraat. Te
bevragen Voorhaven N*ummer* 123.
