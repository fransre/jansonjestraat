---
toegevoegd: 2024-08-06
gepubliceerd: 1929-07-05
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164644006:mpeg21:a00172"
onderwerpen: [werknemer]
huisnummers: [17]
kop: "Jongen"
---
# Jongen

gevraagd voor loop- en
Pakhuiswerk c*irc*a 15 à
16 jaar. **Jan Sonjéstraat** N*ummer* 17.
