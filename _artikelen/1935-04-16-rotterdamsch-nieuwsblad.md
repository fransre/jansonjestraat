---
toegevoegd: 2024-08-03
gepubliceerd: 1935-04-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164676053:mpeg21:a00037"
onderwerpen: [kleding, tekoop]
huisnummers: [29]
kop: "Te koop:"
---
# Te koop:

witte Trouwjapon,
pracht kwaliteit en
model, maat 42-44, billijke
prijs. **Jan Sonjéstraat** 29b.
