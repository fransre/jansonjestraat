---
toegevoegd: 2023-12-28
gepubliceerd: 2011-11-26
decennium: 2010-2019
bron: Sky Sports
externelink: "https://www.skysports.com/more-sports/news/12993/7323721/MLB-Halman-killed"
onderwerpen: [gregory]
huisnummers: [17]
achternamen: [halman]
kop: "MLB: Halman killed"
---
# MLB: Halman killed

Seattle Mariners' Dutch outfielder Gregory Halman was stabbed to death in Rotterdam on Monday morning *November 21, 2011*.

*Onderschrift bij de foto*
Halman: Died aged 24

Seattle Mariners outfielder Gregory Halman was stabbed to death in Rotterdam on Monday morning *November 21, 2011*, local police have confirmed.

Haarlem-born Halman, who appeared in 44 games over the last two seasons for Seattle, was 24.

His brother has been arrested and taken in for questioning.

A statement from Rotterdam police read: „Just after half past five, police received a report of a stabbing at a house in **Jan Sonjéstraat**.”

„Once there police found a badly injured victim. They worked with paramedics to try to resuscitate the victim but they could not manage it.”

„A suspect was arrested immediately at the house.”

„The police have started an investigation to clarify exactly what took place. They will talk with witnesses and a forensic crime scene investigation will be conducted in the home.”

„The identity of the victim and suspect in the fatal stabbing are known.”

„The victim was a 24-year-old man from Rotterdam. His brother, a 22-year-old from Rotterdam, was arrested and detained for questioning.”

Having spent much of his career in the minor leagues, Halman was called up by the Mariners from Tacoma Rainiers in September 2010.

He was sent down again but returned to the big leagues in June *2011* and hit his first home run on June 15 in a 3-1 victory over the Los Angeles Angels.

He hit .207 with two homers and six RBIs overall.
