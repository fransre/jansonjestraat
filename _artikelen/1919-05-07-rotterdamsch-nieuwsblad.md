---
toegevoegd: 2024-03-07
gepubliceerd: 1919-05-07
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010333537:mpeg21:a0086"
onderwerpen: [huispersoneel]
huisnummers: [28]
kop: "waschvrouw"
---
Flinke

# waschvrouw

gevraagd, Dinsd*ag* en
Woensdag, ƒ1.75 per
dag. Tevens een Strijkster
voor Donderdag
en Vrijdag. **Jan Sonjéstraat** 28a.
