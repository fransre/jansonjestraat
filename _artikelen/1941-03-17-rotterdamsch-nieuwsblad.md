---
toegevoegd: 2024-07-29
gepubliceerd: 1941-03-17
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002679:mpeg21:a0041"
onderwerpen: [werknemer]
huisnummers: [34]
achternamen: [leen]
kop: "Flinke nette Jongen"
---
# Binnen komen loopen

op 3 Maart *1941* een ruwharige
Terriër. D. Leen,
**Jan Sonjéstraat** 34B.
