---
toegevoegd: 2024-08-14
gepubliceerd: 1910-09-21
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197083:mpeg21:a0085"
onderwerpen: [woonruimte, tehuur]
huisnummers: [15]
kop: "Een Bovenhuis"
---
# Een Bovenhuis

te huur,

**Jan Sonjéstraat** 15a.

Te bevragen Electr*ische* Wasscherij.

38500 6
