---
toegevoegd: 2024-08-05
gepubliceerd: 1932-01-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164657014:mpeg21:a00192"
onderwerpen: [werknemer]
huisnummers: [30]
achternamen: [dekkers]
kop: "Gevraagd:"
---
# Gevraagd:

Voor het Vestenvak
een Meisje, 14 jaar, en
een Jongen, 14 jaar,
voor Boodschappen. S.A. Dekkers,
**Jan Sonjéstraat** 39a.
