---
toegevoegd: 2024-07-24
gepubliceerd: 1959-06-10
decennium: 1950-1959
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000350059:mpeg21:a00078"
onderwerpen: [huispersoneel]
huisnummers: [33]
achternamen: [spitters]
kop: "net dagmeisje"
---
Gevraagd

# net dagmeisje

met huiselijk verkeer, boven
20 jaar, weekend vrij. Mevrouw Spitters,
**Jan Sonjéstraat** 33,
tel*efoon* 34142, Rotterdam.
