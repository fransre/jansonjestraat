---
toegevoegd: 2024-07-22
gepubliceerd: 1970-05-01
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002881001:mpeg21:a00276"
onderwerpen: [woonruimte, tekoop]
huisnummers: [35]
kop: "Leeg vrij bovenhuis"
---
# Leeg vrij bovenhuis

aan
de **Jan Sonjéstraat** 35 te
R*otter*dam-C*entrum* met 5 kamers, keuken,
berging. Beneden verhuurd
ƒ1251,60 p*er* j*aar*. Koopsom
ƒ25.000,— k*osten* k*oper*. Makelaarskantoor
Arkenbout, Hillevliet 118a,
Rotterdam-Z*uid*. Telefoon 270358.

AD14
