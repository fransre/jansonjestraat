---
toegevoegd: 2024-08-02
gepubliceerd: 1938-02-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164693044:mpeg21:a00206"
onderwerpen: [fiets, tekoop]
huisnummers: [22]
achternamen: [vermeule]
kop: "Heren- en damesrijwiel"
---
# H*eren*- en d*ames*rijwiel

te koop beide geheel
comp*leet* met bel*asting* merken.
Samen: ƒ25, niet afz*onderlijk*.
A. Vermeule, **Jan Sonjéstraat** 22b.
