---
toegevoegd: 2024-07-30
gepubliceerd: 1939-12-04
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164704028:mpeg21:a00106"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Gemeubileerde"
---
# Gemeubileerde

Achterkamer met Warande
aangeboden ƒ2.50
desverkiezend Broodkost
25 cent per keer,
goede behandeling. **Jan Sonjéstraat** 20b,
vrij
Bovenhuis.
