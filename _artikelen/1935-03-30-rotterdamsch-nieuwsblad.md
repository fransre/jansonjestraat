---
toegevoegd: 2024-08-03
gepubliceerd: 1935-03-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164676035:mpeg21:a00156"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [34]
kop: "Vijftig gulden"
---
# Vijftig gulden

Café te koop, contant,
Centrum, vrij van Brouwerij.
**Jan Sonjéstraat** N*ummer* 34b.
