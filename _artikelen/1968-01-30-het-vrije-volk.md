---
toegevoegd: 2024-07-23
gepubliceerd: 1968-01-30
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956530:mpeg21:a0028"
onderwerpen: [huisraad, tekoop]
huisnummers: [13]
achternamen: [van-hooijdonk]
kop: "ledikant"
---
Modern 2-persoons

# ledikant

met achterwand, 2 nachtkastjes.
Gezondheids matras en
bed, lichtblauw. Modern 1-persoons
ledikant met bed ƒ35,—.
Te bevr*agen* J.H.L. van Hooijdonk,
**Jan Sonjéstraat** 13b, beneden.
Tel*efoon* 253880.
