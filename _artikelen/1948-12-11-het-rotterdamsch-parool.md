---
toegevoegd: 2024-07-26
gepubliceerd: 1948-12-11
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164871139:mpeg21:a00112"
onderwerpen: [huisraad, tekoop]
huisnummers: [5]
achternamen: [oostenveld]
kop: "6- of 12-persoons servies,"
---
Gevr*aagd*

# 6- of 12-persoons servies,

compleet. Oostenveld, **Jan Sonjéstraat** 5,
Tel*efoon* 39005.
