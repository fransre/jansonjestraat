---
toegevoegd: 2024-08-07
gepubliceerd: 1928-02-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514223:mpeg21:a0200"
onderwerpen: [muziek, tekoop]
huisnummers: [20]
kop: "Te koop:"
---
# Te koop:

een zoo goed als nieuw
Orgel, merk Mannborg.
**Jan Sonjéstraat** 20a,
R*otter*dam.
