---
toegevoegd: 2024-08-05
gepubliceerd: 1932-05-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164659015:mpeg21:a00087"
onderwerpen: [werknemer]
huisnummers: [2]
achternamen: [smit]
kop: "leerling"
---
Autogarage

# leerling

gevraagd. Aanmelden
hedenmiddag *14 mei 1932* voor 8
uur. J. Smit, **Jan Sonjéstraat** 2.
