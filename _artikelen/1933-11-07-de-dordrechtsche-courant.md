---
toegevoegd: 2023-08-01
gepubliceerd: 1933-11-07
decennium: 1930-1939
bron: De Dordrechtsche Courant
externelink: "https://proxy.archieven.nl/0/3240BF09022543E5A8F47814A28FBF31"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [41]
kop: "Meubelmakerswerkbanken"
---
# Meubelmakerswerkbanken

te koop aangeboden. Te bevragen: **Jan Sonjéstraat** 41,
Rotterdam.

7115
