---
toegevoegd: 2024-08-13
gepubliceerd: 1917-07-03
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296649:mpeg21:a0137"
onderwerpen: [familiebericht]
huisnummers: [19]
achternamen: [weeteling]
kop: "Mejuffrouw Grietje Weeteling,"
---
Heden overleed, zacht en
kalm, onze geliefde Moeder,
Behuwd- en Grootmoeder, Zuster,
Behuwdzuster en Tante,

# Mejuffrouw Grietje Weeteling,

Weduwe van den Heer

C. de Lezenne Coulander,

in den ouderdom van ruim 83
jaar.

Uit aller naam:

J.H. Willebrant,
Exec*uteur* test*estamentair*

Rotterdam, 30 Juni 1917.

De begrafenis zal plaats hebben
a*an*s*taande* Dinsdag *3 juli 1917* op de Algem*ene* Begraafplaats Crooswijk.
Vertrek **Jan Sonjéstraat** 19b te
12 uur.

23
