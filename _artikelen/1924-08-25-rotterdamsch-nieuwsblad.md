---
toegevoegd: 2024-03-07
gepubliceerd: 1924-08-25
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495132:mpeg21:a0028"
onderwerpen: [verloren]
huisnummers: [17]
kop: "Verloren!"
---
# Verloren!

Vrijdagavond *22 augustus 1924* 9 uur
zwarte Zak met Portemonnaie,
inhoudend
geld, van **Jan Sonjéstraat**
naar Claes de Vrieselaan.
**Jan Sonjéstraat** 17.
