---
toegevoegd: 2024-07-29
gepubliceerd: 1940-06-25
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002422:mpeg21:a0046"
onderwerpen: [reclame]
huisnummers: [25]
achternamen: [ossendrijver]
kop: "Kleermakerij S. Ossendrijver"
---
# Kleermakerij S. Ossendrijver

v*oor*h*een* Stationsweg 29

is thans gevestigd:

**Jan Sonjéstraat** 25

Telefoon 33577.

3610
