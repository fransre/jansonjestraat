---
toegevoegd: 2024-08-06
gepubliceerd: 1930-09-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002188:mpeg21:a0301"
onderwerpen: [ongeval, fiets, auto]
huisnummers: [14]
kop: "Door auto aangereden."
---
# Door auto aangereden.

Hedenmiddag *2 september 1930*, ongeveer kwart over
twee, is op den Beukelsdijk, de 17-jarige
A. V., wonende **Jan Sonjéstraat** 14, door
een luxe-auto aangereden en tegen de
straat geworpen. Zij wilde op haar fiets
den weg oversteken, waarbij de auto nog
juist haar achterwiel raakte en haar
omver wierp. Zij werd de apotheek, van
den heer A.W. van Doorne, Beukelsdijk 132,
binnengebracht, waar de hoofdwond,
welke zij had bekomen, voorloopig werd
verbonden. Per auto van den Geneeskundige
Dienst is zij vervolgens naar het Ziekenhuis
Coolsingel gebracht.
