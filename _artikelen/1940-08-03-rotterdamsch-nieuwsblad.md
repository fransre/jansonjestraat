---
toegevoegd: 2024-07-29
gepubliceerd: 1940-08-03
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002487:mpeg21:a0054"
onderwerpen: [bedrijfsinventaris, tekoop]
huisnummers: [14]
achternamen: [verschoor]
kop: "Rekenmachine"
---
# Rekenmachine

Odhner, in prima staat
aangeboden. Slechts ƒ40.
Vaste prijs. C. Verschoor,
**Jan Sonjéstraat** n*ummer* 14B.
