---
toegevoegd: 2024-07-23
gepubliceerd: 1968-11-06
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956823:mpeg21:a0245"
onderwerpen: [familiebericht]
huisnummers: [30]
achternamen: [smeets]
kop: "Yvonne en Johnny"
---
Vrijdag 8 novembere *1968* hopen onze
lieve tweeling

# Yvonne en Johnny

hun twaalfde verjaardag te
vieren.

Fam*ilie* N.J. Smeets

Rotterdam, **Jan Sonjéstraat** 30a.
