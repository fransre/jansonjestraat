---
toegevoegd: 2024-08-04
gepubliceerd: 1934-06-02
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164671037:mpeg21:a00017"
onderwerpen: [bedrijfsruimte, dans]
huisnummers: [38]
kop: "Jan Sonjéstraat Numero 38abc,"
---
De Notarissen

S.S. Wijsenbeek,

te Rotterdam en

G.P. Valette te Maasland,

op Woensdagen 6 en 13 Juni 1934
telkens des namiddags 2 uur
in het Notarishuis te Rotterdam, in
het openbaar verkoopen:

~~I. Het cafépand met twee verdiepingen
te Rotterdam, aan de~~

~~Hillelaan 26,
hoek Korte Hillestraat 21,~~

~~kad*astraal* Sectie P N*ummer* 671, groot 1 A. 11
c.A.~~

~~Café in eigen gebruik, 1ste verdieping
Hillelaan verhuurd voor
ƒ4.— per week, 2de verdieping voor
ƒ3.80 per week, 1ste en 2de verdieping
Hillestraat ieder verhuurd voor
ƒ5.— per week.~~

~~Grond- en straatbel, resp*ectievelijk* ƒ128.19
en ƒ90.—.~~

II. Het pand, bestaande uit danszaal
met 4 Etagewoningen te Rotterdam,
aan de

# **Jan Sonjéstraat** N*ummer* 38abc,

kad*astraal* Delfshaven Sectie C N*ummer* 3748,
groot 1 A. 93 c.A.

Grond- en straatbel*asting* resp*ectievelijk* ƒ117.47
en ƒ82.50.

De danszaal is onverhuurd, zoomede
de 1ste, de Etage N*ummer* 38a, 1ste
en 2de Etage N*ummer* 38c zijn verhuurd
voor ƒ30.— en ƒ27.50 per maand.

Aanvaarding en betaling der kooppenningen
5 Juli 1934.

De Panden zijn te bezichtigen 4,
5 en 6 Juni 1934 van 10-3 uur, op
vertoon van een permissiebiljet, af
te geven door genoemden Notaris
Wijsenbeek, te wiens kantore
aan den Mauritsweg 11 alle nadere
inlichtingen te bekomen zijn.

Het sub II genoemde Pand is inmiddels
uit de hand te koop.

22621 56
