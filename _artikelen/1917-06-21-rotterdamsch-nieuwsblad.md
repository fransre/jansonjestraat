---
toegevoegd: 2024-08-13
gepubliceerd: 1917-06-21
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296565:mpeg21:a0125"
onderwerpen: [werknemer]
huisnummers: [28]
achternamen: [de-mink]
kop: "Gevraagd:"
---
# Gevraagd:

eenige Blousenaaisters
Winkelwerk. Adr*es* J. de Mink,
**Jan Sonjéstraat** 28b.
