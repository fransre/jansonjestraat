---
toegevoegd: 2024-08-03
gepubliceerd: 1936-05-25
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164683027:mpeg21:a00086"
onderwerpen: [kampeerhuisje, tehuur]
huisnummers: [31]
achternamen: [valster]
kop: "Hoek van Holland"
---
# Hoek v*an* Holland

groot Kampeerhuis, 8-10
Personen, comfort*abel*
ingericht. A. Valster,
**Jan Sonjéstraat** N*ummer* 31b.
