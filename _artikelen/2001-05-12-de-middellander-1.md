---
toegevoegd: 2023-01-08
gepubliceerd: 2001-05-12
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2001-05-12-de-middellander-1-origineel.jpg"
onderwerpen: [geraniumdag]
kop: "Geraniums in de Jan Sonjéstraat"
---
# Geraniums in de **Jan Sonjéstraat**

Wie onlangs de **Jan Sonjéstraat**
bezocht moet
het wel zijn opgevallen: de
straat is een ware bloemenzee. U raadt het
natuurlijk al, de jaarlijkse
geraniumdag heet weer
plaats gevonden.

Na een lange periode van minder goed
weer waren de weergoden de bewoners
van de **Jan Sonjéstraat** dan eindelijk goed
gezind.

Op zaterdag 12 mei *2001* scheen de zon al om
negen uur 's ochtends buitengewoon uitbundig
en dat beloofde dus veel goeds.
De kweker kwam om negen uur de planten
brengen en toen was de straat al feestelijk
versierd door een groep bewoners. Er was
een marktkraam met bovenzeil opgesteld
om lekker beschut tegen de zon te kunnen
werken, er was een terras opgesteld met
parasols waar bewoners even konden zitten;
er was een hele ploeg straatbewoners
om te helpen bij de zogenaamde vultafels
en om de gevulde bakken naar de verschillende
adressen te sjouwen. Er werd even
lekker uitgerust met broodjes, krentenbollen
en koffie.

's Middags om een uur of twee hingen er
negentig (!) bakken aan de gevels. Toen
moest er nog worden opgeruimd en
geveegd. De middag werd verder aangenaam
loom doorgebracht op het terras
waar de bewoners nog wat dronken en
gezellig met elkaar bijkletsten. Iedereen
was het er over eens: het was weer een
geslaagde plantjesaktie geweest. Er was
alleen één probleem: men kon even geen
geranium meer zien.

En op 14 juli *2001* kunnen de bewoners van de
**Jan Sonjéstraat** weer aan de bak. De straat
doet mee aan de opzoomeruitdaging ‘Eet Smakelijk’
en organiseert die dag ongetwijfeld
een weergaloos straatdiner.

*Onderschrift bij de foto*
even uitblazen…
