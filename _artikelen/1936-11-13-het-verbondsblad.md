---
toegevoegd: 2024-08-03
gepubliceerd: 1936-11-13
decennium: 1930-1939
bron: Het verbondsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB19:000214011:mpeg21:a00036"
onderwerpen: [reclame]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "L. van den Bos"
---
# L. van den Bos

Electr*ische* wasscherij en
strijkinrichting.

Wasschen gemangeld, opgemaakt
en droog toegeslagen.
Wascht ook per K*ilo*G*ram* à ƒ0,25.
Billijke tarieven.

**Jan Sonjéstraat** 36, Tel*efoon* 36565
Rotterdam.
