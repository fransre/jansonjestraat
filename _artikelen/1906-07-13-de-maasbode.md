---
toegevoegd: 2021-04-29
gepubliceerd: 1906-07-13
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000189268:mpeg21:p002"
onderwerpen: [ongeval]
kop: "Stadsnieuws."
---
# Stadsnieuws.

In de **Jan Sonjéstraat** werd gisterenavond *12 juli 1906*
de 6-jarige J. R., die voor de
ouderlijke woning speelde, door een
rijtuig aangereden. Hij bekwam een
verwonding aan het rechterbeen en werd
in den politiepost Duivenvoordestraat
verbonden.
