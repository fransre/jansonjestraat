---
toegevoegd: 2024-08-13
gepubliceerd: 1918-10-07
decennium: 1910-1919
bron: Nieuwsblad van het Zuiden
externelink: "https://resolver.kb.nl/resolve?urn=MMKB15:000796068:mpeg21:a00012"
onderwerpen: [bedrijfsinventaris, gevraagd]
huisnummers: [8]
achternamen: [van-essen]
kop: "IJzeren vaten."
---
# IJzeren vaten.

Te koop gevraagd ijzeren
vaten en gegolfde platen.
Brieven met prijs H. van Essen,
**Jan Sonjéstraat** 8, R*otter*dam.

7 4811
