---
toegevoegd: 2021-04-29
gepubliceerd: 2018-03-14
decennium: 2010-2019
bron: Algemeen Dagblad
externelink: "https://www.ad.nl/rotterdam/wijk-middelland-veiliger-door-auto-onvriendelijke-aanpak~a508ec39/"
onderwerpen: [verkeer, fiets]
achternamen: [jager, reijnhoudt]
kop: "Wijk Middelland veiliger door ‘auto-onvriendelijke’ aanpak"
---
# Wijk Middelland veiliger door ‘auto-onvriendelijke’ aanpak

De Rotterdamse wijk Middelland is veiliger geworden door een ‘auto-onvriendelijke’ aanpak. In de Brancobuurt (met 10 straten en 800 bewoners) wierpen de bewoners hobbels op zodat autobestuurders nu de wijk mijden.

Bijna drie jaar werkten Robert Jager en Dolf Klijn, bewoners van Middelland, aan een plan dat een deel van hun wijk (verkeers-)veiliger zou moeten maken. Het ging dan om het gedeelte tussen de Middellandstraat en Mathenesserlaan en de 's-Gravendijkwal en Claes de Vrieselaan. Woensdag *14 maart 2018* presenteerde het duo, bijgestaan door Frans Reijnhoudt, de uitkomsten van een serie experimenten.

Niet eens zo lang geleden was Middelland nog onderdeel van het beruchte ‘wilde westen’ van Rotterdam. Junks en hoeren bepaalden het straatbeeld, veel huizen waren dichtgetimmerd met luiken. De situatie van toen is inmiddels veranderd, de buurt is flink opgeknapt.

## Heilige koe

Als de ‘bezorgde vaders’ werd er gewerkt aan verkeersplannen die de straten vol met blik weer terug moesten geven aan de bewoners. „Kinderen moeten kunnen spelen. We hebben wel gemerkt dat de auto een heilige koe is. Nog steeds. Automobilisten dachten dat de straten hier snelwegen waren”, zegt Jager. „We hebben wel eens de grap gemaakt over een buurman. Die ging zo: je kan beter aan de buurvrouw zitten dan aan zijn auto”, vult mede-onderzoeker Frans Reijnhoudt aan.

In het rapport ‘Veilig, Veiliger Middelland’ stellen de bewoners vast dat het creëren van blokkades helpt om de auto's de wijk uit te krijgen. „We hadden eerst circa 300 auto's per dag per straat, dat zijn er 200 geworden. Dat is toch pure winst.”

Het duidelijker vastleggen van de 30 kilometer-zones was daarbij een van de hulpmiddelen. Verder hebben het ophogen van de verkeersdrempels en het plaatsen van Nijntje-borden bijgedragen in de strijd tegen de auto's. „We wilden de voetganger weer op 1 hebben in deze buurt en de fietser op 2. Dat is gelukt. Pas dan komt de auto.”

## Kroeg

Jager en Klijn gingen tegelijkertijd de strijd aan met een kroeg in de **Jan Sonjéstraat**, die voor overlast zorgde. „Er moesten wel camerabeelden aan te pas komen, maar de kroeg en de overlast zijn sinds vorig jaar november *2017* verdwenen. Alleen moet de horeca-vergunning er nog van af.”

De bewoners van Middelland worden eind maart *2018* op de hoogte gesteld. De plannen worden aangeboden aan de gebiedscommissie Delfshaven en de de gemeente Rotterdam.

De onderzoekers werken ook nog aan een voorstel voor de Kruidenbuurt in Ommoord (Prins Alexander), een wijk waar grote parkeerproblemen zijn.

*Onderschrift bij de foto*
Bewoners Robert Jager (l.) en Dolf Klijn werken aan project om de wijk Middelland veiliger te maken. © Jan de Groen

*(Zie <https://veilig-veiliger-middelland.nl> voor het eindrapport van het project Veilig, Veiliger Middelland)*
