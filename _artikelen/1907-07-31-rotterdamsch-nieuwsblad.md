---
toegevoegd: 2024-08-14
gepubliceerd: 1907-07-31
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196883:mpeg21:a0119"
onderwerpen: [woonruimte, tehuur, tram]
huisnummers: [27]
kop: "Te Huur:"
---
# Te Huur:

Benedenhuis met Tuintje
en Kelderruimte onder
het geheele Huis, à
ƒ19 p*er* m*aand*, en vrij Bovenhuis,
waarop Zolder m.
Kamertje, à ƒ21 p*er* m*aand*,
terstond te aanvaarden
en dagelijks te bezichtigen,
gelegen **Jan Sonjéstraat** N*ummer* 27,
bij de El*ectrische* Tram.
Adres: Claes de Vrieselaan 35, Benedenhuis.
