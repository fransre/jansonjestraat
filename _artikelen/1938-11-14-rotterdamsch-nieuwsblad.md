---
toegevoegd: 2024-08-01
gepubliceerd: 1938-11-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164698012:mpeg21:a00133"
onderwerpen: [werknemer]
huisnummers: [23]
kop: "reiziger"
---
Jong provisie

# reiziger

gevraagd door papier
groothandel. Goede
vooruitzichten. Br*ieven*
letter M. d*e* R. **Jan Sonjéstraat** 23,
R*otter*dam.
