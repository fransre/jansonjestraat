---
toegevoegd: 2024-07-29
gepubliceerd: 1940-07-19
decennium: 1940-1949
bron: Nieuwsblad van het Noorden
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011024294:mpeg21:a0036"
onderwerpen: [familiebericht]
huisnummers: [16]
achternamen: [van-der-werf]
kop: "Luktje van Hoogen,"
---
Na een smartelijk lijden
overleed in den ouderdom
van 74 jaar, onze lieve Moeder,
Behuwd- en Grootmoeder

# Luktje van Hoogen,

sedert 27 Juli 1935 wed*uwe* van
Tj. van der Werf.

Rotterdam, 17 Juli 1940.

**Jan Sonjéstraat** 16.

J. van der werf.

G.S. van der Werf-Guillot.

A.L. Guillot-van der Werf.

J. Guillot

en Kleinkinderen.
