---
toegevoegd: 2024-08-03
gepubliceerd: 1935-12-23
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164680060:mpeg21:a00135"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Biedt zich aan:"
---
# B*iedt* z*ich* a*an*:

bekwaam Chauffeur,
met Referentiën, Borgstorting.
Event*ueel* als
Vertegenwoordiger.
Goed met publiek kunnende
omgaan. Br*ieven*
**Jan Sonjéstraat** 36a.
