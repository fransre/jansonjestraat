---
toegevoegd: 2024-08-13
gepubliceerd: 1915-11-08
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296157:mpeg21:a0192"
onderwerpen: [reclame]
huisnummers: [12]
kop: "Gediplomeerd Kapster"
---
# Gedipl*omeerd* Kapster

biedt zich aan, bekend
met de nieuwste Coiffures.
Abonnement per
week en per maand. **Jan Sonjéstraat** 12b.
