---
toegevoegd: 2024-08-14
gepubliceerd: 1910-03-21
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197842:mpeg21:a0301"
onderwerpen: [woonruimte, tehuur]
huisnummers: [22, 24]
kop: "Te Huur"
---
# Te Huur

~~Boezemsingel 148, groote 2de
Etage ƒ21 per maand.~~

~~Goudschestraat 8, een Benedenhuis
ƒ15 per maand.~~

**Jan Sonjéstraat** 22, Benedenhuis
ƒ19 per maand.

**Jan Sonjéstraat** 24, Bovenhuis
ƒ21 per maand.
