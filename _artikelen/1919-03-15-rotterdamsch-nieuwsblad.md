---
toegevoegd: 2024-08-13
gepubliceerd: 1919-03-15
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010995672:mpeg21:a0136"
onderwerpen: [werknemer]
huisnummers: [38]
kop: "Een chef-glacier, Een assistent-glacier, 20 Pakmeisjes en 20 Wagenrijders"
---
Rotterdamsche IJsinrichting

**Jan Sonjéstraat** 38. Dir*ectie*: A.C. Haalebos,

kunnen geplaatst worden:

# Een chef-glacier, Een assistent-glacier, 20 Pakmeisjes en 20 Wagenrijders

Aanmelding Zaterdag *16 maart 1919* tusschen 1-3 uur.

21088 38
