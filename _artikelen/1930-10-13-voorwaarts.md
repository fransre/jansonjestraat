---
toegevoegd: 2024-08-06
gepubliceerd: 1930-10-13
decennium: 1930-1939
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010212907:mpeg21:a0179"
onderwerpen: [diefstal]
kop: "Inbrekers hebben druk werk."
---
# Inbrekers hebben druk werk.

Verschillende inbraakjes in
alle deelen der stad.

De buit is steeds gering.

Met een valsche sleutel heeft men zich ook
toegang verschaft tot de woning van den gepensionneerden
loods C.T. W. in de **Jan Sonjéstraat**
Een koralen tasch, waarin een portemonnaie met
ƒ10.— zat, alsmede een bos sleutels, werd meegenomen.
Ook een portemonnaie met klein geld
verdween.
