---
toegevoegd: 2024-07-26
gepubliceerd: 1948-08-17
decennium: 1940-1949
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO03:259005041:mpeg21:a00103"
onderwerpen: [kleding, tekoop]
huisnummers: [28]
achternamen: [stam]
kop: "Te koop:"
---
# Te koop:

chique witte
georgette trouwjapon en
rode kanten, met klein
*sleepje[?)*, iets aparts en billijke
prijs. Slank figuur, 1
dag gedragen. Stam, **Jan Sonjéstraat** 28B,
R*otter*dam.
