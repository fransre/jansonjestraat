---
toegevoegd: 2024-08-06
gepubliceerd: 1928-07-05
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010211988:mpeg21:a0140"
onderwerpen: [ongeval, fiets]
kop: "Van de fiets gevallen."
---
# Van de fiets gevallen.

In de **Jan Sonjéstraat**
wilde de 21-jarige mej*uffrouw* B. B. uit de Boezemstraat
gisteravond *4 juli 1928* op haar fiets stappen, zij viel er evenwel
weer af en kwam met het achterhoofd op
straat terecht. De juffrouw bekwam een ernstige
hoofdwonde, waarom de G*emeentelijke* G*eneeskundige* D*ienst* haar naar het ziekenhuis
aan den Coolsingel bracht. Na daar behandeld
te zijn, kon zij huiswaarts keeren.
