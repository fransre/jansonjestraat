---
toegevoegd: 2024-08-07
gepubliceerd: 1928-04-07
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514264:mpeg21:a0124"
onderwerpen: [woonruimte, tehuur]
huisnummers: [11]
kop: "Te koop:"
---
# Benedenhuis

te huur per 1 Mei *1928* a*an*s*taande*
à ƒ35 per maand. Bez*ichtiging*
Dinsd*ag* en Donderd*ag* 2-4.
**Jan Sonjéstraat** 11.
