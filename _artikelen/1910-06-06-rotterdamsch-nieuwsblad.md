---
toegevoegd: 2024-08-14
gepubliceerd: 1910-06-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197904:mpeg21:a0275"
onderwerpen: [werknemer]
huisnummers: [30]
achternamen: [theunissen]
kop: "Naaisters"
---
# Naaisters

en aankomende Naaisters
gevraagd voor dadelijk.
Adres: B. Theunissen, **Jan Sonjéstraat** 30B.

26929 6
