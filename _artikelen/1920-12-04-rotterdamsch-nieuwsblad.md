---
toegevoegd: 2024-08-12
gepubliceerd: 1920-12-04
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494191:mpeg21:a0150"
onderwerpen: [familiebericht]
huisnummers: [16]
achternamen: [sneepels]
kop: "Jo Lecomte en Piet Sneepels."
---
Verloofd:

# Jo Lecomte en Piet Sneepels.

Jensiusstraat 26.

**Jan Sonjéstraat** 16.

Geen ontvangdag.

Rotterdam, 2 December 1920.

11
