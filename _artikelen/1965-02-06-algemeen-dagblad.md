---
toegevoegd: 2024-07-23
gepubliceerd: 1965-02-06
decennium: 1960-1969
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002826031:mpeg21:a00336"
onderwerpen: [reclame, tekoop]
huisnummers: [41]
kop: "Vloeistof-duplicators"
---
# Vloeistof-duplicators

Zolang de voorraad strekt !!!

„Ditto” Amerikaanse duplicator, gebruikt maar goed

ƒ75,—

„Centograph” Duitse duplicator dubbelbreed — nieuw

ƒ150,—

Iedere machine in speciale kist. Een goede machine voor een
fractie van de normale prijs! Het afdrukken in kleuren is
mogelijk.

Het namaken van geld helaas niet.

„BICO”

**Jan Sonjéstraat** 41b, Rotterdam — tel*efoon*: 010-250226.

AD 6
