---
toegevoegd: 2024-08-08
gepubliceerd: 1926-08-11
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010495420:mpeg21:a0221"
onderwerpen: [werknemer]
huisnummers: [3]
kop: "Leerling"
---
# Leerling

of een aankomende Jongen
gevraagd, in het
Rijwielvak **Jan Sonjéstraat** 3a,
Rijwielhandel.
