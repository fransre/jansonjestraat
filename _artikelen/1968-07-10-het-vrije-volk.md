---
toegevoegd: 2024-07-23
gepubliceerd: 1968-07-10
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956665:mpeg21:a0120"
onderwerpen: [actie]
huisnummers: [30]
achternamen: [smeets]
kop: "ƒ36,— nog te veel"
---
# ƒ36,— nog te veel

Ik ben het volkomen eens met
andere briefschrijvers over de verhoging
van het kijkgeld. We zijn ja-knikkers
en slikken maar alles. Het
is vast pandoer dat de prijsverhogingen
van suiker, boter, brood e*n* d*ergelijke* altijd
op zaterdagavond worden gepubliceerd.
Mijn tv kunnen ze komen
verzegelen. De hele enkele keer dat
er een leuk programma is, is met
ƒ36,— nog te duur betaald.

N.J. Smeets

**Jan Sonjéstraat** 30a, Rotterdam 3.
