---
toegevoegd: 2024-08-11
gepubliceerd: 1922-02-17
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494076:mpeg21:a0035"
onderwerpen: [werknemer]
huisnummers: [38]
achternamen: [gogarn]
kop: "Reiziger"
---
# Reiziger

door Engros en Exportzaak in Bedtijken, Damasten, Opvullings
Materiaal en alle artikelen ten behoeve van H*eren* Behangers,
Stoffeerders en Beddenmakers.

Zij, die eenige bekendheid in deze branche bezitten genieten de voorkeur.

Adres: Firma J.H.A. Gogarn, **Jan Sonjéstraat** N*ummer* 38, te Rotterdam.

16284 22
