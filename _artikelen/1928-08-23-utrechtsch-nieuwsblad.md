---
toegevoegd: 2023-08-01
gepubliceerd: 1928-08-23
decennium: 1920-1929
bron: Utrechtsch Nieuwsblad
externelink: "https://proxy.archieven.nl/0/50BD029928CE4A66A3294FD5E4E6AC9F"
onderwerpen: [ongeval, motor, auto]
kop: "Om een weddenschap!"
---
# Om een weddenschap!

Te Rotterdam
is de 21-jarige A.L. H. uit de
**Jan Sonjéstraat** met zijn motorfiets tegen
een vluchtheuvel en een daarop-staanden
paal aan den Coolsingel gereden,
tengevolge waarvan bij van de motorfiets
werd geslingerd, evenals een zekere
J.W. J. uit de Jacob Catsstraat. H.
had een weddenschap aangegaan om
binnen een bepaalden tijd den Coolsingel
rond te rijden. Bij de Kruiskade gekomen
— H. reed in de richting Hofplein —
moest hij plotseling uitwijken
voor een auto en daardoor is hij op den
vluchtheuvel gereden.

W. kreeg het voorwiel van de auto
over het lichaam. Hij klaagde over inwendige
pijnen. Per auto van den geneeskundigen
dienst is hij naar het ziekenhuis
aan den Coolsingel vervoerd,
waar hij ter verpleging is opgenomen.
Hij bleek een nier te hebben gescheurd.

H. die eveneens onder den auto terecht
was gekomen, heeft slechts lichte
schaafwonden opgeloopen.

Het voorwiel van de motorfiets is ernstig
beschadigd.
