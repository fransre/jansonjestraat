---
toegevoegd: 2024-08-13
gepubliceerd: 1918-08-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010297083:mpeg21:a0041"
onderwerpen: [huisraad, tekoop]
huisnummers: [42]
kop: "kachel,"
---
Te koop een zoo goed
als nieuwe vierkante

# kachel,

waarop gekookt kan
worden. Te zien: **Jan Sonjéstraat** 42b,
2-m*aal* b*ellen*.
