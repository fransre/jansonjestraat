---
toegevoegd: 2024-05-19
gepubliceerd: 1922-06-01
decennium: 1920-1929
bron: Vlissingse Courant
externelink: "https://krantenbankzeeland.nl/issue/vco/1922-06-01/edition/0/page/3"
onderwerpen: [familiebericht]
huisnummers: [44]
achternamen: [de-geus]
kop: "Nelly van der Struijs en P. de Geus."
---
Ondertrouwd:

# Nelly van der Struijs en P. de Geus.

Oppert 137,
**Jan Sonjéstraat** 44b, Rotterdam
