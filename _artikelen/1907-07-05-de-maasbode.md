---
toegevoegd: 2021-05-04
gepubliceerd: 1907-07-05
decennium: 1900-1909
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000189491:mpeg21:a0007"
onderwerpen: [ongeval]
huisnummers: [15, 17]
achternamen: [van-uitert]
kop: "Stadsnieuws"
---
# Stadsnieuws

Woensdagavond *3 juli 1907* had de 19-jarige
wasch- en loopknecht J. M., wonende
Mauritsstraat 12/2, het ongeluk
in de stoomwasscherij van den heer
M. van Uitert, **Jan Sonjéstraat** 15-17,
met zijn rechterhand te dicht bij
de centrifuge te komen, waardoor
hij gegrepen werd en meegesleurd;
gelukkig geraakte hij spoedig los,
doch had desniettemin zijn arm op
twee plaatsen gebroken. Naar het
ziekenhuis gebracht, werd hij aldaar
ter verpleging opgenomen.
