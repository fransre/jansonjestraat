---
toegevoegd: 2024-08-01
gepubliceerd: 1938-12-06
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164698031:mpeg21:a00102"
onderwerpen: [woonruimte, tehuur]
huisnummers: [24]
kop: "Te huur:"
---
# Te huur:

ruim Benedenhuis **Jan Sonjéstraat** 24.
