---
toegevoegd: 2024-07-26
gepubliceerd: 1944-02-15
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003635:mpeg21:a0018"
onderwerpen: [huisraad, tekoop]
huisnummers: [22]
achternamen: [mulder]
kop: "zeil"
---
1 Kamer

# zeil

ƒ52.50. Mulder,
**Jan Sonjéstraat** 22.

A607
