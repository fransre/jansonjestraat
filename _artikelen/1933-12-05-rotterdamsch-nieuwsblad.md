---
toegevoegd: 2024-08-04
gepubliceerd: 1933-12-05
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164668040:mpeg21:a00021"
onderwerpen: [woonruimte, tehuur]
huisnummers: [38]
kop: "2e etage,"
---
Te huur:

# 2e etage,

**Jan Sonjéstraat** 38, ƒ30
per maand. Bevr*agen*: Mauritsweg 11,
Telefoon N*ummer* 56204.
