---
toegevoegd: 2024-07-26
gepubliceerd: 1943-03-25
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003450:mpeg21:a0017"
onderwerpen: [kleding, tekoop]
huisnummers: [6]
achternamen: [de-vroed]
kop: "Trouwjapon,"
---
# Trouwjapon,

bleu m*et* onderj*urk*
en tasje, m*aat* 42 ƒ45 De Vroed,
**Jan Sonjéstraat** 6

A560
