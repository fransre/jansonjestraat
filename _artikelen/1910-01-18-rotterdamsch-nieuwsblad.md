---
toegevoegd: 2024-08-14
gepubliceerd: 1910-01-18
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197789:mpeg21:a0083"
onderwerpen: [werknemer, schilder]
huisnummers: [38]
achternamen: [weier]
kop: "Een Schilders-"
---
# Een Schilders-

Leerjongen gevraagd. E.J. Weier, Werkpl*aats* Josephstr*aat* 63a.
Woonplaats
**Jan Sonjéstraat** 38a.
