---
toegevoegd: 2024-07-20
gepubliceerd: 1990-07-07
decennium: 1990-1999
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:003088006:mpeg21:a00091"
onderwerpen: [familiebericht]
huisnummers: [32]
achternamen: [van-brenkelen]
kop: "Johannes Antonius van Brenkelen"
---
Enige en algemene kennisgeving

Hierbij geven wij kennis dat na een kortstondig
ziekbed van ons is heengegaan

# Johannes Antonius van Brenkelen

\* 26 augustus 1913 ✝ 2 juli 1990

L.M. van Brenkelen-Noordijk

Kinderen

Kleinkinderen

Achterkleinkind

**Jan Sonjéstraat** 32-b

3021 TX Rotterdam

Geen bezoek aan huis

Overeenkomstig zijn wens heeft de crematie
in stilte plaatsgevonden.
