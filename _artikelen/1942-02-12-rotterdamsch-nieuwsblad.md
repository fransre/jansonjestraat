---
toegevoegd: 2024-07-28
gepubliceerd: 1942-02-12
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003075:mpeg21:a0010"
onderwerpen: [huisraad, kleding, tekoop]
huisnummers: [29]
achternamen: [augustijn]
kop: "Te koop"
---
# Te koop

1 p*aar* br*uine* pumps
m*aat* 38 en 18 zw*art* suède
pumps m*aat* 40, z*o* g*oed* a*ls* n*ieuw*,
tevens kachelscherm.
Na 6.30 Augustijn, **Jan Sonjéstraat** 29B.
