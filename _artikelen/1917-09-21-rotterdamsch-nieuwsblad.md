---
toegevoegd: 2024-08-13
gepubliceerd: 1917-09-21
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296718:mpeg21:a0027"
onderwerpen: [bedrijfsinventaris, gevraagd]
huisnummers: [8]
achternamen: [van-essen]
kop: "Vaten."
---
# Vaten.

Gevraagd: alle soorten Vet- en Olievaten.

Aanbiedingen aan H. van Essen, **Jan Sonjéstraat** 8,
Telef*oon* 12016, R*otter*dam.

47991 12
