---
toegevoegd: 2024-07-23
gepubliceerd: 1964-04-10
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010954421:mpeg21:a0136"
onderwerpen: [auto, tekoop]
huisnummers: [42]
achternamen: [heyster]
kop: "Heinkel scooter"
---
Te koop v*an* part*iculier* op zat*erdag* v*an* 12
tot 14 uur

# Heinkel scooter

met zijspan en windschermen
ƒ850. Heyster, **Jan Sonjéstraat** 42b,
R*otter*dam-C*entrum*.
