---
toegevoegd: 2024-08-06
gepubliceerd: 1930-01-03
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164645002:mpeg21:a00042"
onderwerpen: [bedrijfsruimte]
huisnummers: [17]
kop: "Te koop:"
---
# Te koop:

een Pakhuispand met
vrije bovenwoning, alles
leeg. **Jan Sonjéstraat** 17.
Bevragen
Beukelsdijk 98. Telefoon 34436.
