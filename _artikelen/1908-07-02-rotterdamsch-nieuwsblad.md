---
toegevoegd: 2024-03-07
gepubliceerd: 1908-07-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197170:mpeg21:a0097"
onderwerpen: [huispersoneel]
huisnummers: [24]
kop: "net Meisje,"
---
Terstond gevraagd een

# net Meisje,

voor halve dagen in
klein gezin, P*rotestantse* G*ezindte*, beneden
16 jaar. Aanmelding
na 7 uur. **Jan Sonjéstraat** 24.
