---
toegevoegd: 2024-07-26
gepubliceerd: 1943-12-08
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011003374:mpeg21:a0016"
onderwerpen: [huisraad, gevraagd]
huisnummers: [5]
achternamen: [oostenveld]
kop: "Vloerkleed"
---
# Vloerkleed

of klein kleedje.
Liefst Perzisch. Oostenveld, **Jan Sonjéstraat** 5.
Tel*efoon* 37897.

S934
