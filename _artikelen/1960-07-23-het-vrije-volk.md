---
toegevoegd: 2024-07-24
gepubliceerd: 1960-07-23
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951218:mpeg21:a0020"
onderwerpen: [fiets, motor, tekoop]
huisnummers: [26]
achternamen: [van-den-akker]
kop: "kinderdriewieler,"
---
Te koop

# kinderdriewieler,

i*n* g*oede* st*aat*, bill*ijke* prijs, DKW-motor, 98
c.c met duosit, t*egen* e*lk* a*annemelijk* b*od*, na 6 uur.
**Jan Sonjéstraat** 26b, J. v*an* d*en* Akker.
Rotterdam 3.
