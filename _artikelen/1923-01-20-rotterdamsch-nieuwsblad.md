---
toegevoegd: 2024-08-11
gepubliceerd: 1923-01-20
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494627:mpeg21:a0131"
onderwerpen: [diefstal]
kop: "Diefstalkroniek."
---
# Diefstalkroniek.

Van A. B., uit de **Jan Sonjéstraat** is in
do Boompjes een rijwiel gestolen.
