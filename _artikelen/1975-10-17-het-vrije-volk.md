---
toegevoegd: 2021-07-20
gepubliceerd: 1975-10-17
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010958899:mpeg21:a0203"
onderwerpen: [verkeer]
kop: "Schermlaan wacht op autovrije tijd"
---
# Schermlaan wacht op autovrije tijd

Redactie Jan Meijer / Annemiek van Oosten / Telefoon 147400 / Toestel 135

Middelland — Dat iedereen
zijn auto per se voor
eigen deur wil parkeren, is
niet waar. Tenminste niet in
de Schermlaan, waar bewoners
begin vorig jaar een plan
maakten om hun straat gedeeltelijk
voor verkeer af te
sluiten. Van het lofwaardige
initiatief van de volwassen
Middellanders is echter nog
steeds niets te zien. Het plan
ligt sinds februari *19*74 op het
stadhuis en sindsdien hebben
zowel de bewoners als de actiegroep
Middelland niets
meer gehoord.

Reden van dat lange wachten
is volgens een woordvoerder
van de Verkeersdienst het feit
dat het om een ingrijpende
verandering gaat. De bewoners
willen het middenstuk
van de Schermlaan tussen de
Bellevoysstraat en de **Jan Sonjéstraat**
helemaal afsluiten.
Nu raast het verkeer nog aan
twee kanten van de speelplaats
daar.

Wanneer er geld voor uitvoering
van het plan ter beschikking
komt, zal nog moeten
blijken. Herbestrating en het
aanleggen van verkeersdrempels
in de wijk hebben voorrang gekregen.
Kortom, de bewoners
zullen nog wat langer
moeten wachten, maar zoekgeraakt
is het plan in ieder
geval niet.

*Onderschrift bij de foto*
Het stukje Schermlaan, dat kinderdomein moet worden.
