---
toegevoegd: 2024-08-14
gepubliceerd: 1905-04-25
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213626:mpeg21:a0213"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [4]
achternamen: [groenewegen]
kop: "Heilige Communie."
---
# H*eilige* Communie.

Ter overname aangeboden
een keurig fijne neteldoeksche
Communiejurk,
bewerkt met entredeux
en oprijgsels en satijnen
Onderjurk, Sluier,
Schoentjes, Kousen, Zakdoek
enz*ovoort*, tegen billijken
prijs. Adres J. Groenewegen,
**Jan Sonjéstraat** 4, Bovenh*uis*,
in de Middellandstraat, einde West-Kruiskade.
