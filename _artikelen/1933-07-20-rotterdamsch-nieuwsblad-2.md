---
toegevoegd: 2024-08-04
gepubliceerd: 1933-07-20
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164666022:mpeg21:a00130"
onderwerpen: [woonruimte, tehuur]
huisnummers: [13]
kop: "Vrij bovenhuis"
---
# Vrij bovenhuis

te huur voor klein, net
gezin, opnieuw geschilderd,
behangen, ƒ37.50.
**Jan Sonjéstraat** 13.
Adres A. Barents,
Lorentzplein 13, Schiedam.
