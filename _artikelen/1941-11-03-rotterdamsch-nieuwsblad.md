---
toegevoegd: 2024-07-28
gepubliceerd: 1941-11-03
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002810:mpeg21:a0050"
onderwerpen: [sport, reclame]
huisnummers: [32]
achternamen: [hulsker]
kop: "Zelfverdediging"
---
# Zelfverdediging

3× gewapend is hij, die Jiu Jitsu heeft
geleerd bij:

Reinier, Hulsker, **Jan Sonjéstraat** 32, R*otter*dam.

Deze week beginnen de cursussen. Speciale
Damesclubs op Zaterdagmiddag.

Recl. 32132 7
