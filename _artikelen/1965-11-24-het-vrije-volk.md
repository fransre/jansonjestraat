---
toegevoegd: 2024-07-23
gepubliceerd: 1965-11-24
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010955855:mpeg21:a0039"
onderwerpen: [auto, tekoop]
huisnummers: [31]
achternamen: [both]
kop: "Ford Taunus 17M,"
---
Te koop van part.

# Ford Taunus 17M,

i*n* z*eer* g*oede* st*aat*, eind 1958.
Both, **Jan Sonjéstraat** 31b, Rotterdam.
