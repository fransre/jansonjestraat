---
toegevoegd: 2024-08-14
gepubliceerd: 1904-05-21
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=KBDDD02:000202717:mpeg21:a0065"
onderwerpen: [huispersoneel]
huisnummers: [4]
kop: "Meisje,"
---
Door nette burgerouders
wordt plaatsing gevraagd
voor een net R*ooms* K*atholieke*

# Meisje,

van 14 jaar, voor Kindermeisje,
onder de nette stand.
Adres: **Jan Sonjéstraat** 4,
bovenhuis, in de Middellandstraat
einde West Kruiskade.
