---
toegevoegd: 2024-08-05
gepubliceerd: 1932-01-21
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164657022:mpeg21:a00086"
onderwerpen: [woonruimte, tehuur]
huisnummers: [13]
kop: "benedenhuis"
---
Mooi

# benedenhuis

te huur, 3 Kamers
Keuken, geheel droge
Kelder, Warande en
Tuin ƒ40 **Jan Sonjéstraat** 13b.
