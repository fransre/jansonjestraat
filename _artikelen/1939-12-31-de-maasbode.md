---
toegevoegd: 2024-07-30
gepubliceerd: 1939-12-31
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000194599:mpeg21:a0196"
onderwerpen: [reclame]
huisnummers: [34]
achternamen: [hollink]
kop: "Frans W.J. Hollink"
---
# Frans W.J. Hollink

Loodgieter

Coolschestr*aat* 56b, Tel*efoon* 32039

Woonhuis: **Jan Sonjéstraat** 34a,

Z.N. Rotterdam

1126M
