---
toegevoegd: 2024-07-20
gepubliceerd: 1931-12-03
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000198772:mpeg21:a0082"
onderwerpen: [auto, tekoop]
huisnummers: [2]
achternamen: [smit]
kop: "Ford Sedan en Ford Tudor"
---
# Ford Sedan

de Luxe, model 1930, met open dak.
Nog als nieuw. Zeer billijk te koop.
J. Smit, **Jan Sonjéstraat** 2. Telef*oon*
31600, Rotterdam.

19784

Te koop

# Ford Tudor

1931\. Heeft slechts 2000 K*ilo*M*eter* geloopen.
Is nog nieuw. J. Smit, **Jan Sonjéstraat** 2.
Tel*efoon* 31600, R*otter*dam.

19783
