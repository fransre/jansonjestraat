---
toegevoegd: 2024-07-23
gepubliceerd: 1967-05-18
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956312:mpeg21:a0101"
onderwerpen: [auto, tekoop]
huisnummers: [19]
achternamen: [rodenburg]
kop: "Vespa scooter"
---
# Vespa scooter

grand luxe
weinig bereden. Zien is kopen.
Rodenburg, **Jan Sonjéstraat** 19B
R*otter*dam-3.
