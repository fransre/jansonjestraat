---
toegevoegd: 2021-07-20
gepubliceerd: 1988-01-26
decennium: 1980-1989
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010962762:mpeg21:a0181"
onderwerpen: [bellebom]
kop: "Zure bommen in Bellevoysstraat"
---
# Zure bommen in Bellevoysstraat

Rotterdam — Goed, twee
vliegtuigbommen uit de grond
halen is geen kleinigheidje,
maar je hoeft toch niet te
schudden op je stoel, twee weken
die stookolielucht in je huis
te ruiken en met hoofdpijn rond
te lopen? De bewoners van de
Bellevoysstraat en **Jan Sonjéstraat**
in Rotterdam-West praten
inmiddels niet over de twee
vliegtuigbommen maar over de
‘zure bommen’.

De wijsvinger van mevrouw
J.M.M. de Jong (71) priemt in
de richting van de hei-installatie
vlak achter haar woning.
Haar boze woorden zijn voor
dovemansoren bestemd als de
zoveelste damwand de grond in
gaat. Dan komen er tranen op
haar wangen en vlucht zij naar
binnen waar de herrie verflauwt,
maar waar ook dampen
van stookolie hangen en de
schokgolven net zo voelbaar
zijn. Gisteren *25 januari 1988* viel er nog een
kaars van haar televisie.

De Explosieven Opruimingsdienst
(EOD) heeft de 21 nog
bewoonde panden in de buurt
van de twee vliegtuigbommen
aan de achterkant laten blinderen
met houten platen. Er komt
nog een beetje daglicht binnen
door houten deuren in het
plaatwerk die alleen morgen *27 januari 1988*
dicht gaan als de laatste damwanden
vlak naast de bommen
in de grond gaan. Het heien is
overmorgen *28 januari 1988* afgelopen.

Mevrouw De Jong zal nog
een paar dagen daarna last
hebben van gesuis en gebonk in
haar hoofd. „In het weekeinde,
als de heiers naar huis gaan,
loop ik nog rond te tollen in
mijn huis. Ik word er echt gek
van,” zegt ze.

## Vergoeding

Ze moet morgen *27 januari 1988* haar huis
uit. Er bestaat een kleine kans
dat een bom explodeert als er
een damwand dichtbij in de
grond wordt geheid. Ze gaat
naar een zoon in Hellevoetsluis.
Op 27 maart *1988*, als de bommen
worden gedemonteerd, gaat ze
een dagje naar een andere zoon
in Hellevoetsluis. Twee weken
geleden moest zij ook al haar
huis uit toen de damwand rond
de eerste bom werd geslagen.
Toen ging zij met haar huisgenoot
J. van 't Hoff naar de vakantiebeurs
in Utrecht. Al met
al maakt ze behoorlijk wat
reiskosten en daar zou mevrouw
De Jong graag een vergoeding
voor terug willen zien.
De gemeente heeft op dit verzoek
nog niet gereageerd.

De heer Van 't Hoff heeft zo
zijn bedenkingen over de evacuaties.
„Twee weken geleden,
toen iedereen uit de huizen
moest, kwamen er wel twintig
mensen van de gemeente Rotterdam
een kijkje nemen,” vertelt
hij. „Terwijl wij uit de
buurt moesten blijven, stonden
er ambtenaren en raadsleden
met hun neus bovenop.”

*Onderschrift bij de foto*
Mevrouw De Jong: „Als de heiers naar huis gaan, loop ik nog rond te tollen in mijn huis. Ik word er echt
gek van.” (Foto Tieleman van Rijnberk)
