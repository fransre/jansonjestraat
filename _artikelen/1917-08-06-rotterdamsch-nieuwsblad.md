---
toegevoegd: 2024-08-13
gepubliceerd: 1917-08-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296678:mpeg21:a0063"
onderwerpen: [verloren]
achternamen: [roodenburg]
kop: "Medegenomen."
---
# Medegenomen.

De Juffrouw, die
Woensdagmiddag *1 augustus 1917* twee
uur, in den Winkel
Roodenburg, **Jan Sonjéstraat**,
hoek 1ste
Middellandstraat, een
verkeerde Parapluie
heeft medegenomen,
verzoeke beleefd tegen
belooning terug te bezorgen
v*an* d*e* Ven, 1ste
Middellandstraat 127a.
