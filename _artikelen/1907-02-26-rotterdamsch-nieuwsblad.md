---
toegevoegd: 2022-07-23
gepubliceerd: 1907-02-26
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010196611:mpeg21:a0037"
onderwerpen: [familiebericht]
huisnummers: [8]
achternamen: [bronder]
kop: "Johannes Bronder,"
---
Tot mijne en mijner kinderen
diepe droefheid overleed
op 21 Febr*uari* *1907* j*ongst*l*eden*, bij het vergaan
van het s*toom*s*chip* „Berlin” te Hoek
van Holland, mijn innig geliefde
Echtgenoot en der kinderen
zorgvolle Vader, de Heer

# Johannes Bronder,

in den leeftijd van 53 jaar.

Hoe zwaar ons dit verlies
treft, kunnen alleen zij beseffen,
die den overledene van
nabij hebben gekend.

Uit aller naam,

De Wed*uwe* J. Bronder-Brobbel.

21

Rotterdam, **Jan Sonjéstraat** 8.
