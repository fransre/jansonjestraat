---
toegevoegd: 2024-08-13
gepubliceerd: 1919-09-05
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010494289:mpeg21:a0052"
onderwerpen: [werknemer]
huisnummers: [36]
kop: "Jongen gevraagd,"
---
# Jongen gevr*aagd*,

15 of 16 jaar, loon ƒ5
voor Cartonnagewerk
en enkele Boodschappen,
tevens een Inpakster
gevraagd. **Jan Sonjéstraat** 36.
