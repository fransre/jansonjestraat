---
toegevoegd: 2024-08-09
gepubliceerd: 1926-02-01
decennium: 1920-1929
bron: Voorwaarts
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010211654:mpeg21:a0124"
onderwerpen: [diefstal]
kop: "Diefstallen."
---
# Diefstallen.

In de hal van het postkantoor aan den Coolsingel
is van H.M.J. T., wonende in de **Jan Sonjéstraat**,
een actetasch gestolen, waarvan
de inhoud uit voor den dief waardelooze papieren
bestond.
