---
toegevoegd: 2024-08-13
gepubliceerd: 1914-02-17
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295441:mpeg21:a0098"
onderwerpen: [werknemer]
huisnummers: [21]
kop: "Stukadoors."
---
# Stukadoors.

Gevraagd bekwame
Stukadoors tegen verhoogd
tarief, tevens Witters
en een Opperman
gevraagd. Adres **Jan Sonjéstraat** 21b.
