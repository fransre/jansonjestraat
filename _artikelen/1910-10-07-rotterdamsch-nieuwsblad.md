---
toegevoegd: 2024-08-14
gepubliceerd: 1910-10-07
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197097:mpeg21:a0138"
onderwerpen: [huispersoneel]
huisnummers: [18]
achternamen: [van-de-berg]
kop: "een net Meisje,"
---
Wordt gevraagd

# een net Meisje,

voor halve dagen. Aanbieding
's avonds na 7 u*ur*
bij G.Th. v*an* d*e* Berg,
**Jan Sonjéstraat** 18a.
