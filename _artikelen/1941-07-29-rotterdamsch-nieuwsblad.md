---
toegevoegd: 2024-07-28
gepubliceerd: 1941-07-29
decennium: 1940-1949
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:011002710:mpeg21:a0015"
onderwerpen: [reclame, fiets]
huisnummers: [36]
achternamen: [wessels]
kop: "Tob niet Fietsers."
---
# Tob niet Fietsers.

voor alle onderdeelen en reparaties:
naar Wessels — **Jan Sonjéstraat** 36.
