---
toegevoegd: 2024-05-16
gepubliceerd: 1921-03-25
decennium: 1920-1929
bron: Weekblad voor Israëlietische huisgezinnen
externelink: "https://resolver.kb.nl/resolve?urn=MMUBA15:005422012:00001"
onderwerpen: [familiebericht]
huisnummers: [24]
achternamen: [kets-de-vries]
kop: "Johanna E. Kets de Vries en Philip A. Kets de Vries"
---
Verloofd:

# Johanna E. Kets de Vries en Philip A. Kets de Vries

24 Maart 1921.

Amsterdam, Nieuwe Prinsengracht 77.

Rotterdam, **Jan Sonjéstraat** 24b.

Ontvangdag: Zondag 27 Maart *1921* 2-4½
uur, N*ieuwe* Prinsengracht 77, Amsterdam.
