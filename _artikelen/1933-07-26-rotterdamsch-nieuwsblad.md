---
toegevoegd: 2024-08-04
gepubliceerd: 1933-07-26
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164666028:mpeg21:a00069"
onderwerpen: [woonruimte, tehuur]
huisnummers: [26]
kop: "Westen."
---
# Westen.

Mooi Benedenhuis te
huur, **Jan Sonjéstraat** 26,
ƒ38 per maand.
Bevragen Bovenhuis.
