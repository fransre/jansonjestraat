---
toegevoegd: 2024-05-19
gepubliceerd: 1934-12-31
decennium: 1930-1939
bron: Maas- en Scheldebode
externelink: "https://krantenbankzeeland.nl/issue/mas/1934-12-31/edition/null/page/2"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [knulst]
kop: "Pieter Marinus Knulst en Cornelia Muilwijk"
---
Op D*eo* V*olente* 12 Januari 1935
hopen wij met onze geliefde
Ouders

# Pieter Marinus Knulst en Cornelia Muilwijk

dankbaar en blijde te herdenken,
dat zij voor 25 jaar
in den echt verbonden zijn.
Moge God hen nog vele jaren
voor ons sparen.

Hun dankbare kinderen:

Jeanne Corie

Alie Bennie

Leen Klazien

Bер Nellie

Piet

Rotterdam, 30 Dec*ember* 1934

**Jan Sonjéstraat** 33b.

Receptie: Donderdagavond
3 Januari *1935* van 8 tot 10 uur.
