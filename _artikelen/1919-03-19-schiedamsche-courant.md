---
toegevoegd: 2023-01-21
gepubliceerd: 1919-03-19
decennium: 1910-1919
bron: Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/SC/1919-03-19/edition/0/page/3"
onderwerpen: [diefstal]
kop: "Donker Rotterdam"
---
# Donker Rotterdam

Op het achterbalcon van lijn 10 is van
J. v*an* d*er* M., uit de **Jan Sonjéstraat**, een
portefeuille met ƒ185 gerold.
