---
toegevoegd: 2024-08-08
gepubliceerd: 1927-04-05
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000196615:mpeg21:a0046"
onderwerpen: [ongeval]
kop: "Gevaarlijke stoeipartij."
---
# Gevaarlijke stoeipartij.

Gisterenavond *4 april 1927* waren eenige jongens in de
portiek van de Haagsche Automobielm*aatschapp*ij aan
den Coolsingel aan het stoeien. Daarbij gaf
de 21-jarige huisknecht M. G. wonende in de
**Jan Sonjéstraat** den 15-jarigen C.M. B. op den
Goudsche Singel woonachtig, een trap in zijn
rug. B. ging naar huis doch werd korten tijd
later onwel. In het ziekenhuis aan den Bergweg
onderzocht, constateerde men dat zijn nieren
gekwetst waren. G. wordt politioneel vervolgd.
