---
toegevoegd: 2024-07-25
gepubliceerd: 1954-03-17
decennium: 1950-1959
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010951863:mpeg21:a0120"
onderwerpen: [bewoner]
huisnummers: [11]
achternamen: [reinders]
kop: "Nieuw pand N.V. Van Torren"
---
# Nieuw pand N.V. Van Torren

Het bouwen van een bedrijfspand
met kantoren aan de Jensiusstraat
voor rekening van de N.V. N. van
Torrens slagerij- en vleeswarenfabrieken
is na onderhandse inschrijving
gegund aan T.J. Hoebe te Rotterdam
voor ƒ169.000. Architect is
de heer B. Reinders, **Jan Sonjéstraat** 11,
Rotterdam.
