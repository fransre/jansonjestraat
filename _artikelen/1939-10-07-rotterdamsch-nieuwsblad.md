---
toegevoegd: 2024-07-30
gepubliceerd: 1939-10-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164703031:mpeg21:a00106"
onderwerpen: [brand, fiets, motor]
huisnummers: [2]
achternamen: [dekker, jansen, otterspoor]
kop: "Winkel en werkplaats uitgebrand"
---
# Winkel en werkplaats uitgebrand

Aan de 1e Middellandstraat.

Hedenmorgen *7 oktober 1939* omstreeks kwart voor 9
was de rijwiel- en motorhandelaar P. Dekker in
zijn werkplaats aan de **Jan Sonjéstraat** 2, welke
gelegen is achter zijn zaak aan de 1e Middellandstraat 100b,
bezig met een met een gasvlam verhitten
bout een ledige benzinetank te lasschen.
Plotseling schoot een groote vlam uit de tank
en zette de omgeving in vlam. Dekker poogde de
vlammen nog te dooven, doch moest, daar de
brand zeer snel in omvang toenam, de vlucht
nemen. Men trachtte nog enkele motorrijwielen
en fietsen uit den winkel te redden, doch ook
deze was door de licht brandbare voorraden weldra
één vuurzee, zoodat men de pogingen tot berging
van de aanwezige goederen moest staken.
Steeds verder drong het vuur in den overvollen
winkel door en spoedig sloegen de vlammen aan
alle kanten van het hoekhuis fel naar buiten.

Ondertusschen was slangenwagen 31 met den
onderbrandmeester, den heer M. Verhoeven, en
den brandmeester, den heer J. van Onlangs, ter
plaatse gekomen en dadelijk werd met twee
stralen op de waterleiding met de blussching begonnen.
Van slangenwagen 28, welke vlak daarop
werd gemeld, werd nog één straal in werking gesteld
en met deze drie stralen slaagde men er in
den brand, nu onder leiding van den hoofdman,
den heer A.J. Kruis, tot het winkelhuis en de
daarachter gelegen werkplaats te beperken. Men
kon voorkomen, dat de bovengelegen verdiepingen
bewoond door de gezinnen Jansen en Otterspoor,
ook door het vuur werden aangetast. De winkel
en de werkplaats brandden echter schoon leeg.
Van den daarin aanwezig geweest zijnden voorraad
was alleen een verkoolde puinhoop over.

De bovenwoningen hadden alleen rookschade
bekomen. Door het snelle optreden der brandweer
was doorbranden van de winkel- en werkplaatszoldering
voorkomen.

Daar gevaar bestond dat het vuur nog tusschen
de plafonds doorsmeulde, werden deze op last
van hoofdman Kruis afgestoken.

Omstreeks half elf was de brand gebluscht en
ging men tot de ruiming en nablussching over.

Van de hulp van het personeel der slangenwagens 32
(in reserve), 56 en 49, welke eveneens
waren uitgerukt, behoefde geen gebruik te
worden gemaakt.

Personeel der Afd*eling* Brandbluschmiddelen, dat
onder bevel van den opzichter, den heer J.W. Mallan,
met den gereedschapswagen n*ummer* 1 aanwezig
was, verleende goede diensten bij de
blussching.

De schade wordt door verzekering gedekt.

De talrijke nieuwsgierigen werden op voldoenden
afstand gehouden door een uitgebreide
politiemacht onder bevel van den hoofdinspecteur,
den heer G.W. Valken, zoodat de brandweer
ongestoord haar werk kon doen en het
verkeer door de 1e Middellandstraat ook niet
werd gestoord. Gedeeltelijk werd dit tijdens de
blussching omgelegd.
