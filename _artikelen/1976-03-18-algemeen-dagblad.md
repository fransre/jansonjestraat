---
toegevoegd: 2024-07-21
gepubliceerd: 1976-03-18
decennium: 1970-1979
bron: Algemeen Dagblad
externelink: "https://resolver.kb.nl/resolve?urn=KBPERS01:002926016:mpeg21:a00231"
onderwerpen: [woonruimte, tekoop]
huisnummers: [23]
kop: "beleggingspand"
---
T*e* k*oop*

# beleggingspand

**Jan Sonjéstraat** 23a-b, huur
ƒ3.316,80 p*er* j*aar*. Inl*ichtingen* F*irm*a A.F.
Diepstraten & Z*oo*n, tel*efoon* 010-244438
of 203876 's avonds.
