---
toegevoegd: 2021-05-03
gepubliceerd: 1940-09-16
decennium: 1940-1949
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1940-09-16/edition/0/page/4"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Aangeboden"
---
# Aangeb*oden*

ongem*eubileerd* 1 gr*ote* voork*amer*, 3 ram*en* straatz*ijde*,
ƒ17.50, of achterk*amer* m*et* gr*ote* tussenk*amer*,
m*et* v*aste* wast*afel*, vrije keuken, ƒ28 p*er* m*aand*,
elec*trisch* licht inbegr*epen*; ook zeer gesch*ikt*
v*oor* kant*oor*, op 1ste ét*age* van vrij bovenh*uis*.
**Jan Sonjéstraat** 20b, Rotterdam.
