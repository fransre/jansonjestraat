---
toegevoegd: 2024-08-10
gepubliceerd: 1925-08-17
decennium: 1920-1929
bron: Nederlandsche staatscourant
externelink: "https://resolver.kb.nl/resolve?urn=MMKB08:000181112:mpeg21:p007"
onderwerpen: [faillissement]
huisnummers: [35]
achternamen: [dekker]
kop: "Faillissementen"
---
# Faillissementen

Uitgesproken.

H.F. Dekker, decorateur, vroeger wonende Zwaanshalskade 4a,
thans **Jan Sonjéstraat** 35, Rotterdam; arr*ondissements*-rechtb*ank*
Rotterdam, 12 Augustus 1925; rechter-comm*issaris* mr. J.J. ter Maten;
cur*ator* mr. A. van der Wilde, Aert van Nesstraat 98,
Rotterdam.
