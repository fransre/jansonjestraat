---
toegevoegd: 2024-08-05
gepubliceerd: 1931-09-30
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164655034:mpeg21:a00152"
onderwerpen: [reclame, dans]
huisnummers: [38]
achternamen: [kuijt]
kop: "Dans-instituut J.C. Kuijt"
---
# Dans-instituut J.C. Kuijt

**Jan Sonjéstraat** 38 — Telef*oon* 31777.

Nabij 1ste Middellandstraat.

Aanvang der nieuwe Zondaglessen begin October *1931* volgens den nieuwsten
Engelschen stijl. Inschrijving dag*elijks* aan de zaal van 7-9½ uur, beh*oudens* Zaterdags.

Woensdag- en zondagavond groot bal.

's Zondagmiddags gezellige thé-dansant.

43449 24
