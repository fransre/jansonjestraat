---
toegevoegd: 2024-07-26
gepubliceerd: 1949-02-19
decennium: 1940-1949
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164872042:mpeg21:a00104"
onderwerpen: [huisraad, tekoop]
huisnummers: [22]
achternamen: [mulder]
kop: "Mooi nieuw vloerkleed"
---
Te koop:

# Mooi nieuw vloerkleed

2×3 meter. Na 7 uur.
Mulder **Jan Sonjéstraat** 22
(boven).
