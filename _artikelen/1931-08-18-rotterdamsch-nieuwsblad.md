---
toegevoegd: 2024-08-05
gepubliceerd: 1931-08-18
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164654056:mpeg21:a00077"
onderwerpen: [verloren]
huisnummers: [2]
achternamen: [smit]
kop: "Verloren:"
---
# Verloren:

1 Swan Vulpen, gaande
van Hillegersberg,
Bergweg naar Middellandstraat.
Tegen Belooning
terug te bezorgen
J. Smit, **Jan Sonjéstraat** 2,
R*otter*dam.
