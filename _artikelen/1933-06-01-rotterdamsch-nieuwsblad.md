---
toegevoegd: 2024-08-04
gepubliceerd: 1933-06-01
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164665036:mpeg21:a00169"
onderwerpen: [woonruimte, tehuur]
huisnummers: [21]
achternamen: [dekker]
kop: "zit- en slaapkamer"
---
Gem*eubileerde* of ongem*eubileerde*

# zit- en sl*aap*kamer

aangeboden voor 2
Vrienden of Echtpaar
of 2 Zusters. Dekker
**Jan Sonjéstraat** 21a.
