---
toegevoegd: 2024-05-20
gepubliceerd: 1947-05-17
decennium: 1940-1949
bron: De Stem
externelink: "https://krantenbankzeeland.nl/issue/stm/1947-05-17/edition/null/page/4"
onderwerpen: [werknemer]
huisnummers: [12]
achternamen: [dirven]
kop: "bekwame Loodbranders"
---
N.V. Constructie Werkplaats
v*oor*h*een* J. Dirven

**Jan Sonjéstraat** 12

Rotterdam Tel*efoon* 30538

vragen terstond

# bekwame Loodbranders

Prima vaklieden,

vast werk.

Na oproep reis- en verblijfkosten
worden vergoed.

371-o
