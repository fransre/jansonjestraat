---
toegevoegd: 2024-03-07
gepubliceerd: 1909-11-27
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010198053:mpeg21:a0076"
onderwerpen: [huispersoneel]
huisnummers: [28]
kop: "Strijkster,"
---
Terstond gevraagd bekwame

# Strijkster,

goed op de hoogte zijnde
met fijn goed, voor
3 à 4 dagen in de week,
alsmede aankomende
Plankstrijkster, bekwame
Waschvrouw en net
Loopmeisje dat het
strijken kan leeren.
Loon ƒ1.75. **Jan Sonjéstraat** 28a.
