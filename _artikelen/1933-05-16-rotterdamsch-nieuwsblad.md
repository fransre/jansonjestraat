---
toegevoegd: 2024-08-04
gepubliceerd: 1933-05-16
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164665018:mpeg21:a00103"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [21]
kop: "voorkamer"
---
Groote ongemeubileerde

# voorkamer

voor Heer of Dame te
huur, met Pension. **Jan Sonjéstraat** 21a,
vrij
Bovenhuis.
