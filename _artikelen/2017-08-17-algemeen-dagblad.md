---
toegevoegd: 2022-07-27
gepubliceerd: 2017-08-17
decennium: 2010-2019
bron: Algemeen Dagblad
externelink: "https://www.ad.nl/rotterdam/bewoners-ruimen-obikes-op-we-moeten-wel~ac9faee9/"
onderwerpen: [obike, fiets]
achternamen: [jager]
kop: "Bewoners ruimen oBikes op: We moeten wel"
---
# Bewoners ruimen oBikes op: We moeten wel

Lang niet iedereen in Rotterdam is blij met de komst van de deelfietsen
van oBike. In de Rotterdamse wijk Middelland zijn bewoners er zo klaar
mee dat ze zelf de fietsen verplaatsen. „We moeten wel”, legt Robert Jager
uit. „Er is gewoon geen ruimte voor rolstoelen of kinderwagens.”

Jager is wijkcoördinator van Mooi Mooier Middelland voor de gemeente
Rotterdam en zet zich vol enthousiasme in voor zijn buurt. „In zijstraten van
drukke straten als de Middellandstraat zorgen de fietsen echt voor overlast,
met de mensen in de buurt die de fietsen gebruiken hebben we al afspraken
gemaakt, maar er duiken er elke keer zoveel op. Soms staan er 18 van die
oBikes in onze kleine zijstraat, dat neemt gewoon te veel ruimte in.” Samen
met mensen uit de buurt haalde hij afgelopen weekend voor de derde keer
met een bakfiets de oBikes van de stoep in de **Jan Sonjéstraat**.

Hij is niet tegen de oBikes benadrukt
Jager. „Ik ben er in het buitenland
echt fan van, maar ik snap niet dat
ze hier overal staan. Ik vind het echt
jammer dat er geen duidelijke
afspraken zijn gemaakt tussen de
gemeente en het bedrijf over een
rekken-systeem. Maar het is nooit te
laat om dingen aan te passen… Op
openbare plekken zoals bij de
speeltuin, of in een brede straat kan
je die fietsen prima kwijt. Ik hoop
echt dat er iets aan gedaan wordt.”

Tot die tijd zal Jager zelf de fietsen verzamelen op plekken waar ze niet voor
overlast zorgen. „Van mensen uit nauwe straten in de buurt heb ik vernomen
dat ook zij de fietsen willen verplaatsen, zolang het bedrijf het niet doet, doen
we het maar even zo.”

*Onderschrift bij de foto*
De mensen van Mooi Mooier Middelland verplaatst de fietsen van zijstraatjes waar ze in de weg staan, naar een parkeerplek op de Middellandstraat. © MooiMooierMiddelland
