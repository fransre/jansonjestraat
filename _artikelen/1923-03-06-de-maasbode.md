---
toegevoegd: 2024-08-11
gepubliceerd: 1923-03-06
decennium: 1920-1929
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000191548:mpeg21:p003"
onderwerpen: [werknemer]
huisnummers: [38]
achternamen: [gogarn]
kop: "Jubilé A.J. van Vliet."
---
# Jubilé A.J. van Vliet.

Bij de firma J.H.A. Gogarn aan de **Jan Sonjéstraat**
herdacht de heer A.J. van Vliet
gisteren *5 maart 1923* een veertig-jarige werkzaamheid. De
jubilaris is vanmorgen *6 maart 1923* in het bijzijn van het
personeel toegesproken door den heer J.F.A. Gogarn,
die namens de firma een geschenk
onder couvert overhandigde.

Van het personeel voerde daarna de heer
P.J.H. Kaptein het woord, onder aanbieding
van een rooktafel met toebehooren.
