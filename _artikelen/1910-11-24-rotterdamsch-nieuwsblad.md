---
toegevoegd: 2024-08-14
gepubliceerd: 1910-11-24
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197138:mpeg21:a0101"
onderwerpen: [woonruimte, tehuur]
huisnummers: [15]
kop: "Te Huur terstond"
---
# Te Huur terstond

Een vrij Bovenhuis,
**Jan Sonjéstraat**, met beschilderde
Plafonds en Muren, opnieuw
behangen, geverfd.

Te bevragen Benedenhuis N*ummer* 15b.

46106 10
