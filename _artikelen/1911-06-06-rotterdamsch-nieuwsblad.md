---
toegevoegd: 2024-08-14
gepubliceerd: 1911-06-06
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000186236:mpeg21:a0078"
onderwerpen: [diefstal]
kop: "Diefstal"
---
In den winkel van den banketbakker V. aan
de Kruiskade, liet mej*uffrouw* P. K., wonende **Jan Sonjéstraat**,
een portemannaie met ƒ7.50 liggen. Zij
bemerkte het bijna onmiddellijk, maar toen zij
terugkeerde was de portemonnaie verdwenen.
