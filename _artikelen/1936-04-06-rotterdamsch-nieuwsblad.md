---
toegevoegd: 2024-08-03
gepubliceerd: 1936-04-06
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164682041:mpeg21:a00083"
onderwerpen: [brand]
huisnummers: [29]
achternamen: [hoogmolen]
kop: "Schoorsteenbranden."
---
# Schoorsteenbranden.

Schoorsteenbranden werden gebluscht: ~~om
kwart vóór 3 Vrijdagmiddag *3 april 1936* door gasten van
slangenwagen 59 bij 11. E. Breyt aan de Groepstraat 31;
om kwart vóór zéven gisteravond *5 april 1936*
door die van slangenwagen 43 bij C. P. Groenenboom
aan de Gouwstraat 28;~~ om 7 uur door die
van slangenwagen 56 bij bij H. Hoogmolen aan
de **Jan Sonjéstraat** 29 ~~en om kwart vóór acht
door die van slangenwagen 47 bij J.J.C. Langeveld
aan de Zwaanshalskade 56b.~~
