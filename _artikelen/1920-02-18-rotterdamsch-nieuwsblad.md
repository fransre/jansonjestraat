---
toegevoegd: 2024-08-13
gepubliceerd: 1920-02-18
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010493573:mpeg21:a0067"
onderwerpen: [tekoop, dieren]
huisnummers: [42]
kop: "Jachthond"
---
# Jachthond

te koop, mits goed tehuis,
korth*arig*, Duitsche
st*aande* hond. 11 m*aanden* oud,
ziekte gehad, uitersten
prijs ƒ40. Te bevr*agen* **Jan Sonjéstraat** 42,
ben*eden*.
