---
toegevoegd: 2021-07-24
gepubliceerd: 1906-08-06
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010213861:mpeg21:a0155"
onderwerpen: [woonruimte, tehuur]
huisnummers: [8, 10, 12, 14, 16]
kop: "Te huur"
---
# Te huur

in 't Westen, **Jan Sonjéstraat** 8-16
(in de Middellandstraat):
vrije Boven- en Benedenhuizen,
Kelders en Zolders,
voorzien van Gas, Waterleiding
en Stookplaats.

Te bezichtigen aldaar, ook Zondag,
van 1-4 uur.

29854 10
