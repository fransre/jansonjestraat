---
toegevoegd: 2024-08-13
gepubliceerd: 1915-04-27
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010295992:mpeg21:a0154"
onderwerpen: [werknemer]
huisnummers: [31]
achternamen: [moll]
kop: "Strijkster!"
---
# Strijkster!

Gevraagd een aankomende
Plankstrijkster
voor de 5 laatste dagen
der week. A.J. Moll,
**Jan Sonjéstraat** 31b.
