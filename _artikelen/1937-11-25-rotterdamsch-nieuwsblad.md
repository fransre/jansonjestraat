---
toegevoegd: 2024-08-02
gepubliceerd: 1937-11-25
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164692022:mpeg21:a00184"
onderwerpen: [werknemer]
huisnummers: [36]
achternamen: [van-den-bos]
kop: "Gevraagd:"
---
# Gevraagd:

flinke Jongen voor verschillende
werkzaamheden
met vak bekend,
genieten voorkeur. Wasscherij
v*an* d*en* Bos, **Jan Sonjéstraat** 36.
