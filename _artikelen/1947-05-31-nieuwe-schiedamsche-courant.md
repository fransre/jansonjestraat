---
toegevoegd: 2023-10-07
gepubliceerd: 1947-05-31
decennium: 1940-1949
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/index.php/issue/NSC/1947-05-31/edition/null/page/4"
onderwerpen: [werknemer]
huisnummers: [26]
kop: "Personeel gevraagd"
---
# Personeel gevraagd

Gevraagd meubelmakers, halfwas
en leerlingen. Meubelfabriek
„Jehovu”, **Jan Sonjéstraat** 36.
Na 6 uur: P. Verbrugge,
Adr*es* Niemantstraat 19,
Rotterdam
