---
toegevoegd: 2024-08-06
gepubliceerd: 1930-11-03
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164650002:mpeg21:a00029"
onderwerpen: [reclame]
huisnummers: [7]
kop: "Maison Mastenbroek"
---
# Maison Mastenbroek

Vierambachtsstraat 31a

Jonkerfransstraat 8 en

**Jan Sonjéstraat** 7.

Het oudste en soliedste adres voor

Dames- heeren- en
kinderkleeding.

Chique modellen! Lage prijzen! Gemakkelijke
betalingsvoorwaarden.

Telefoon 33519.

Vossen

Bontmantels

52952 42
