---
toegevoegd: 2024-08-14
gepubliceerd: 1911-08-09
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010200185:mpeg21:a0055"
onderwerpen: [reclame, muziek]
huisnummers: [36]
kop: "Orgels zonder brand"
---
# Orgels zonder brand

of Waterschade en toch concurreerend in prijs, is
beslist een punt om in overweging te nemen. Aangezien
brand en water de grootste vijanden zijn van een
instrument.

10 j*aar* Garantie. Vergelijk onze prijzen. 10 j*aar* Garantie.

Orgels vanaf ƒ40.—. Piano's vanaf ƒ275, inbouwen
van Kunstspeelapparaten in iedere Piano.

Overgauw & Westerhof,

Electrische Fabriek,

**Jan Sonjéstraat** 36a, Rotterdam.

34643 44
