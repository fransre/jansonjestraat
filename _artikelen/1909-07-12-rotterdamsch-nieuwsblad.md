---
toegevoegd: 2021-05-18
gepubliceerd: 1907-07-12
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197934:mpeg21:a0064"
onderwerpen: [woonruimte, tehuur]
huisnummers: [20]
kop: "Te Huur:"
---
# Te Huur:

ruim Benedenhuis **Jan Sonjéstraat** 20,
bevat 3
Kamers en Keuken, 2 Alkoven,
Warande, grooten
Kelder met vooringang,
Tuin enz*ovoort*. Huurprijs
ƒ18 per maand en
te bevragen Heemraadssingel
128, hoek Middellandstraat.
