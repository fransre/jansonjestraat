---
toegevoegd: 2024-08-04
gepubliceerd: 1934-03-22
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164670025:mpeg21:a00152"
onderwerpen: [auto, tehuur]
huisnummers: [34]
kop: "Te huur:"
---
# Te huur:

luxe Wagen met Chauffeur
voor geheelen dag.
**Jan Sonjéstraat** 34b,
Telefoon 31707.
