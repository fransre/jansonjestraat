---
toegevoegd: 2021-04-29
gepubliceerd: 1912-08-17
decennium: 1910-1919
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000185747:mpeg21:a0047"
onderwerpen: [brand]
huisnummers: [19]
kop: "Rotterdam."
---
# Rotterdam.

Hedenmorgen *17 augustus 1912* kwart over vijf ontstond brand
in de wasch- en strijkinrichting van L. d*e* G.,
**Jan Sonjéstraat** 19a. Een ijzeren plaat was uit
den schoorsteen naar beneden gevallen. Eenig
wasch- en strijkgoed en een paar drooglatten
waren daardoor op het heete fornuis gevallen
en hadden vlam gevat.

Het vuur werd door de bewoners met emmers
water en door gasten van spuit 49 met een gummislang
op de huiswaterleiding gebluscht. Aanwezig
waren de spuiten 49, 32, de brigade en
spuit 31.
