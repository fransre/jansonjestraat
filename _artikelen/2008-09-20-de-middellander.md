---
toegevoegd: 2023-01-09
gepubliceerd: 2008-09-20
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2008-09-20-de-middellander-origineel.jpg"
onderwerpen: [dagjevanplezier]
achternamen: [lutgerink]
kop: "Dagje van Plezier 2008"
---
# Dagje van Plezier 2008

Op zaterdag 20 september *2008* vond het Dagje van Plezier 2008 plaats
op het Branco van Dantzigpark. De dag is een initiatief van buurtbewoners
uit o*nder* a*ndere* de **Jan Sonjéstraat** en wordt georganiseerd in samenwerking
met Delphibuurtwerk.

Zoals elk jaar richtte het evenement zich voornamelijk op de kinderen in de
wijk Middelland, dus ook dit jaar was er veel te doen voor onze kleine
buurtbewoners. De kids konden lekker wild op een luchtkussen springen,
circusacts uitvoeren, genieten van het Sprookjestheater en nog veel meer!
De dag heeft ook een sociale functie voor de volwassen bewoners: het viel
op dat veel bewoners even lekker met elkaar stonden te babbelen; iets
waar zij, mede door onze gejaagde levens, misschien normaal iets minder
tijd voor hebben. Op het Dagje van Plezier werd dit even ruimschoots ingehaald!
Het succes was tevens te danken aan de goede samenwerking tussen
partijen. Zo zorgde TOS (Thuis Op Straat) in samenwerking met Duimdrop
voor het sportelement en werd het park de volgende dag schoongemaakt
door de heren van het Boumanhuis.

Dit jaar was het thema ‘Gezonde Voeding’, een onderwerp dat in heel
Rotterdam onder de kinderen wordt gepromoot. Zoals u misschien al weet
van uw eigen kinderen wordt er in het basisonderwijs veel aandacht besteed
aan het belang van voldoende lichaamsbeweging en gezond eten.
Deze ontwikkeling wilden wij dus doortrekken naar deze dag. De kinderen
kregen, in plaats van snoep en andere zoetigheid, veel fruit aangeboden
en dat vond gretig aftrek.

De buurtbewoners en Delphi Opbouwwerk kijken terug op een geslaagde
dag. Gedurende de middag zijn er tussen de 200 en 250 kinderen geweest,
die na afloop allemaal een vaantje en de brochure ‘Gezond eten’
kregen. Natuurlijk speelde het mooie ‘lenteweer’ ook een grote rol.
Het spreekt voor zich dat er volgend jaar een Dagje van Plezier 2009 zal
worden georganiseerd met wederom allerlei spelletjes, een circus en het
gerucht gaat dat er volgend jaar een brandweerwagen komt voor de kinderen.
Spannend!

Tenslotte willen wij in het bijzonder Aad Lutgerink en Ome Gerrit bedanken
voor alle extra inzet. Zonder die twee was de dag een stuk minder leuk geweest!
Maar natuurlijk bedanken wij ook de vrijwilligers, de sponsoren, het
Boumanhuis, TOS, Duimdrop en alle anderen die meegewerkt hebben aan
het Dagje van Plezier 2008.

Voor vragen en/of opmerkingen kun u contact opnemen met Hutch Lourens,
tel*efoon*: 4257304 of mail: hutch.lourens@delphiopbouwwerk.nl

Onze dank gaat uit naar de volgende sponsoren:
Firma Koolmees, Dolf van Eyk automaterialen, F*irm*a Vaisnovi, City Silks,
Atma Shippina, d*e* h*ee*r Suleyman en d*e* h*ee*r Polat

Tot volgend jaar!
