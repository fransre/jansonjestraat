---
toegevoegd: 2024-08-14
gepubliceerd: 1910-08-26
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197061:mpeg21:a0084"
onderwerpen: [werknemer]
huisnummers: [8]
kop: "Loopjongen,"
---
Gevraagd een

# Loopjongen,

15-16 jaar, en een Meisje
voor het plakken van
Etiketten enz*ovoort*. Adr*es* **Jan Sonjéstraat** 8a.
