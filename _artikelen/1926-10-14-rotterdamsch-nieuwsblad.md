---
toegevoegd: 2024-08-08
gepubliceerd: 1926-10-14
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010513813:mpeg21:a0404"
onderwerpen: [woonruimte, tekoop]
huisnummers: [36]
kop: "Publieke Verkoopingen"
---
# Publieke Verkoopingen

In het Notarishuis aan de Gelderschekade
te Rotterdam.

Woensdag 13 October *1926*.

Eindafslag:

Door de Notarissen J.M. Slingert en
P.J.M. Dolk, Leuvehaven 135.

Pand en Erf, **Jan Sonjéstraat** 36a-b. Trekgeld
ƒ11.500. Voor ƒ12.500 verkocht.
