---
toegevoegd: 2024-08-07
gepubliceerd: 1928-03-06
decennium: 1920-1929
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010514238:mpeg21:a0235"
onderwerpen: [familiebericht]
huisnummers: [44]
achternamen: [hoogwinkel]
kop: "meisje,"
---
Eenige en Algemeene Kennisgeving.

Heden overleed onze Nicht

# Louisa Arnoldina Remy,

in den ouderdom van 77 jaar.

Uit aller naam,

W. Hoogwinkel.

Rotterdam, 4 Maart 1928.

**Jan Sonjéstraat** 44a.

De teraardebesteiling zal plaats
hebben a*an*s*taande* Woensdag *7 maart 1928* op de Alg*emene* Begraafplaats Crooswijk.
Vertrek
van „Bethesda”, ten 11 uur.

19
