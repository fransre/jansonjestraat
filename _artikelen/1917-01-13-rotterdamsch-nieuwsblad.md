---
toegevoegd: 2024-08-13
gepubliceerd: 1917-01-13
decennium: 1910-1919
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010296360:mpeg21:a0021"
onderwerpen: [diefstal]
kop: "De diefstal aan de Jan Sonjéstraat."
---
# De diefstal aan de **Jan Sonjéstraat**.

Het onderzoek, verricht door de politie
van het bureau Witte de Withstraat, in
verband met den brutalen diefstal van
ruim 50 vaten machine-olie uit een pakhuis
aan de **Jan Sonjéstraat** ten nadeele
van een der veemen, heeft er toe geleid,
dat de inspecteur, de heer C.W. van Vleuten,
te Zaandam, op de geheele partij heeft
beslag gelegd. De olie was daar door den
koopman H. gekocht van iemand te Amsterdam.
Diensvolgens heeft de heer Van Vleuten
zich van Zaandam naar Amsterdam
begeven, om zijn onderzoek voort te
zetten.

De olie vertegenwoordigt een waarde van
tusschen ƒ5000 en ƒ6000.
