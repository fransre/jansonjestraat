---
toegevoegd: 2024-08-03
gepubliceerd: 1936-10-27
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164685049:mpeg21:a00140"
onderwerpen: [kinderartikel, tekoop]
huisnummers: [34]
achternamen: [katoen]
kop: "Te koop:"
---
# Te koop:

wegens plaatsgebrek
Kinderwagen, diepe
Lakbak, Rem, Spatborden,
Sierkussen, Regenzeil
enz*ovoort*. Wed*uwe* Katoen,
**Jan Sonjéstraat** N*ummer* 34a.
