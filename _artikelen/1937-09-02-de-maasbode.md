---
toegevoegd: 2024-08-02
gepubliceerd: 1937-09-14
decennium: 1930-1939
bron: De Maasbode
externelink: "https://resolver.kb.nl/resolve?urn=MMKB04:000193269:mpeg21:a0159"
onderwerpen: [familiebericht]
huisnummers: [33]
achternamen: [spitters]
kop: "Jacquelina,"
---
Jong gestorven, vroeg bij God.

Heden overleed tot onze
diepe droefheid, na langdurig
lijden, voorzien van de H*eilige*
Sacramenten der Stervenden,
ons innig geliefd jongste
Dochtertje, Zusje, Kleindochtertje
en Nichtje

# Jacquelina,

op den jeugdigen leeftijd van
9 jaar.

Familie Spitters.

Rotterdam, 1 September 1937.

**Jan Sonjéstraat** 33a.

De plechtige H. Uitvaartdienst
zal worden opgedragen
in de parochiekerk van de
H*eilige* Elisabeth (Mathenesserl*aan*)
Zaterdag 4 September *1937* om 10
uur, waarna de begrafenis zal
plaats hebben van uit de
kerk in het familiegraf op het
R*ooms* K*atholieke* Kerkhof te Crooswijk.
