---
toegevoegd: 2021-04-29
gepubliceerd: 1976-04-29
decennium: 1970-1979
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010959217:mpeg21:a0083"
onderwerpen: [woonruimte, tekoop]
huisnummers: [18]
kop: "Rotterdam-centrum,"
---
Te koop:

# R*otter*dam-centr*um*,

pand **Jan Sonjéstraat** 18, dubbele
benedenwoning leeg op
te leveren, bovenwoning verhuurd
voor ƒ2.000,— per jaar.
Prijs voor het gehele pand
ƒ49.500,— k*osten* k*oper* Zeer hoge hypotheek
mogelijk. Inlichtingen:
070-830520.

R14
