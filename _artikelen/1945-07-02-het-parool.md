---
toegevoegd: 2024-07-26
gepubliceerd: 1945-07-02
decennium: 1940-1949
bron: Het parool
externelink: "https://resolver.kb.nl/resolve?urn=MMNIOD05:000107149:mpeg21:a0016"
onderwerpen: [familiebericht]
huisnummers: [16]
achternamen: [van-der-werf]
kop: "Geertruida Sophia van der Werf, geboren Guillot,"
---
Overleden

# Geertruida Sophia van der Werf, geb*oren* Guillot,

46 jaren. U*it* a*ller* n*aam*
J. v*an* d*er* Werf, Rotterdam,
26 Juni 1945,
**Jan Sonjéstraat** 16a.
