---
toegevoegd: 2023-01-08
gepubliceerd: 2001-04-25
decennium: 2000-2009
bron: De Middellander
externelink: "https://www.jansonjestraat.nl/originelen/2001-04-25-de-middellander-origineel.jpg"
onderwerpen: [bewoner]
achternamen: [poot]
kop: "Een vroeg feestje in de Jan Sonjéstraat"
---
# Een vroeg feestje in de **Jan Sonjéstraat**

Verbaasd keek mijnheer Poot uit zijn raam. Aan zijn gevel hingen slingers en verschillende bewoners hadden de
vlag gehesen. Recht tegenover zijn woning was over de gehele gevel een geel spandoek bevestigd met een liedtekst.
En dat lied werd uit volle borst gezongen.

Het was woensdag 25 april *2001* en de bewoners
uit de **Jan Sonjéstraat** wilden mijnheer
Poot en zijn vrouw in het zonnetje zetten,
want vandaag *25 april 2001* waren ze immers vijftig(!)
jaar getrouwd.

Mijnheer Poot is een van de meest actieve
bewoners uit de **Jan Sonjéstraat**. Hij is al
heel lang lid van de bewonersgroep en
organiseert plantdagen, straatdîners, feesten,
enzovoort.

Natuurlijk kwam het feestvarken snel naar
buiten, waar hij hartelijk gefeliciteerd werd
door medebewoners. Rond de voor deze
gelegenheid opgezette koffietafel werd nog
even vrolijk feest gevierd.
„Ik kom net uit de nachtdienst”, lacht een
bewoner, „het wordt dus een latertje voor
mij”.

Verschillende bewoners, op weg naar hun
werk of om de kinderen naar school te
brengen, feliciteerden het echtpaar en bleven
nog even voor het nuttigen van een
kop koffie met wat lekkers erbij.

*Onderschrift bij de foto*
Het jubilerende echtpaar
