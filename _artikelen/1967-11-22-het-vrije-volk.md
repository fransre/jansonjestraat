---
toegevoegd: 2024-07-23
gepubliceerd: 1967-11-22
decennium: 1960-1969
bron: Het Vrije Volk
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010956467:mpeg21:a0107"
onderwerpen: [woonruimte, tehuur, pension]
huisnummers: [3]
achternamen: [uilenbroek]
kop: "kostganger"
---
Nette

# kostganger

gevr*aagd*, degelijk
pensioen, huiselijk verkeer.
Geen beroeps, vrije kamer
met stromend water. Uilenbroek,
**Jan Sonjéstraat** 3b,
R*otter*dam-West.
