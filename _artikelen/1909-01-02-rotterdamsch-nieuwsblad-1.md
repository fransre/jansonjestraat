---
toegevoegd: 2024-08-14
gepubliceerd: 1909-01-02
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197319:mpeg21:a0225"
onderwerpen: [bedrijfsinformatie]
huisnummers: [39]
achternamen: [assies]
kop: "K. Assies, Mr. Timmerman en Aannemer,"
---
Aan al mijn Vrienden en Begunstigers
heil en zegen in het
nieuwe jaar.

# K. Assies, M*eeste*r Timmerman en Aannemer,

**Jan Sonjéstraat** N*ummer* 39a.
