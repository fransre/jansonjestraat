---
toegevoegd: 2024-07-25
gepubliceerd: 1953-01-28
decennium: 1950-1959
bron: Het Rotterdamsch parool
externelink: "https://resolver.kb.nl/resolve?urn=MMSARO02:164883023:mpeg21:a00142"
onderwerpen: [muziek, tekoop]
huisnummers: [18]
achternamen: [van-trigt]
kop: "Jazz-Guitaar"
---
Zeer mooie

# Jazz-Guitaar

z*o* g*oed* a*ls* n*ieuw*
m*et* hoes. Vaste prijs ƒ145.—.
Na 19 uur v*an* Trigt, **Jan Sonjéstraat** 18b.
