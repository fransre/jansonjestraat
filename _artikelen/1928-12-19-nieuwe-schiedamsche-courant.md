---
toegevoegd: 2021-04-30
gepubliceerd: 1928-12-19
decennium: 1920-1929
bron: Nieuwe Schiedamsche Courant
externelink: "https://schiedam.courant.nu/issue/NSC/1928-12-19/edition/0/page/2"
onderwerpen: [brand]
huisnummers: [38]
kop: "Brandjes."
---
# Brandjes.

Gisterenmiddag *18 december 1928* is, doordat men met een
brandende kaars op zolder aan het zoeken was,
brand ontstaan in een zolderkamertje van
pand 38a in de **Jan Sonjéstraat**, bewoond door
A.B. Gasten van spuit 31 bluschten het vuur
met een slang op de waterleiding en een
gummislang onder leiding van hoofdman
Mirrer. Spuit 56 was in reserve afgepakt. De
spuiten 32 en 28 kwamen ter plaatse.
