---
toegevoegd: 2024-08-04
gepubliceerd: 1934-06-14
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164671050:mpeg21:a00143"
onderwerpen: [bedrijfsruimte]
huisnummers: [38]
kop: "Publieke verkoopingen."
---
# Publieke verkoopingen.

In het Notarishuis aan de Gelderschekade
te Rotterdam.

woensdag 13 juni *1934*

Eindafslag:

Door de Notarissen S.S. Wijsenbeek en
G.P. Velette,

Cafépand, Hillelaan 26, h*oe*k K*orte* Hillestraat 21,
in bod op ƒ17.000, daarop verkocht,

Pand. **Jan Sonjéstraat** 38a, b, c, in bod op
ƒ19.500, daarop verkocht.
