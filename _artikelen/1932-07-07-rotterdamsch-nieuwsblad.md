---
toegevoegd: 2024-08-05
gepubliceerd: 1932-07-07
decennium: 1930-1939
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=MMKB32:164660008:mpeg21:a00179"
onderwerpen: [fiets, reclame]
huisnummers: [2]
achternamen: [smit]
kop: "Examen geslaagd 'n gazelle gevraagd!"
---
# Examen geslaagd 'n gazelle gevraagd!

Het betrouwbare, snelle, lichtloopende en toch niet dure rijwiel.

Agenten voor Rotterdam

~~C.G. van Arnhem, Plantageweg 55a.~~

~~Hofman & Co*mpagnon*, Nieuwe Binnenweg 131.~~

J. Smit, **Jan Sonjéstraat** 2.

~~J.C.J. Teygeler, Bergweg 82a.~~

29608 42
