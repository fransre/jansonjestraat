---
toegevoegd: 2024-08-14
gepubliceerd: 1909-04-29
decennium: 1900-1909
bron: Rotterdamsch nieuwsblad
externelink: "https://resolver.kb.nl/resolve?urn=ddd:010197416:mpeg21:a0137"
onderwerpen: [woonruimte, tehuur]
huisnummers: [12]
achternamen: [wentink]
kop: "Kamer"
---
Een Juffrouw vraagt
een groote ongemeubileerde

# Kamer

of Zolderkamer voor 1
à 1.50 p*er* w*eek*. Aan hetzelfde
Adres een paar Naaihuizen
gevraagd met alles
volkomen op de hoogte.
Br*ieven* fr*anco* Wentink,
**Jan Sonjéstraat** 12.
